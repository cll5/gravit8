<?php
    //determine if this is from the development servers
    $developmentServers = array('www.development.gravit8.co', 'development.gravit8.co', 'www.test.gravit8.co', 'test.gravit8.co', 'localhost');
    $isDevelopmentMachine = in_array($_SERVER['SERVER_NAME'], $developmentServers);

    if (!defined('__GRAVIT8__')) {
        $gravit8Directory = dirname(__File__);
        define('__GRAVIT8__', $gravit8Directory);
    }
    //NOTE: this gets called everytime a page is requested by the clients
    //      more details at http://blogs.shephertz.com/2014/05/21/how-to-implement-url-routing-in-php/
    try {
        //include modules
        require_once __GRAVIT8__ . '/app/commons/utilities.php';
        require_once __GRAVIT8__ . '/app/commons/logger/logger.php';
        require_once __GRAVIT8__ . '/app/routes/helpers.php';

        //database handlers
        require_once __GRAVIT8__ . '/app/model/DatabaseHandler.php';
        $databaseHandler = DatabaseHandler::getInstance();

        //model handlers
        require_once __GRAVIT8__ . '/app/model/OrganizationHandler.php';
        require_once __GRAVIT8__ . '/app/model/UserHandler.php';
        require_once __GRAVIT8__ . '/app/model/PortfolioHandler.php';
        require_once __GRAVIT8__ . '/app/model/EntityHandler.php';
        require_once __GRAVIT8__ . '/app/model/entities/IdeaHandler.php';
        require_once __GRAVIT8__ . '/app/model/entities/PitchHandler.php';
        require_once __GRAVIT8__ . '/app/model/Attachment.php';
        require_once __GRAVIT8__ . '/app/model/AttachmentHandler.php';

        $organizationHandler = OrganizationHandler::getInstance();
        $userHandler = UserHandler::getInstance();
        $portfolioHandler = PortfolioHandler::getInstance();
        $entityHandler = EntityHandler::getInstance();
        $ideaHandler = IdeaHandler::getInstance();
        $pitchHandler = PitchHandler::getInstance();
        $attachmentHandler = AttachmentHandler::getInstance();

        //account handler
        require_once __GRAVIT8__ . '/app/model/AccountHandler.php';
        $accountHandler = new AccountHandler();

        //comment handler
        require_once __GRAVIT8__ . '/app/model/CommentHandler.php';
        $commentHandler = new CommentHandler();

        //template rendering engine handlers
        //more details: https://www.smashingmagazine.com/2011/10/getting-started-with-php-templating/#twig-library-with-tutorial
        //to activate twig debugging: http://twig.sensiolabs.org/doc/2.x/functions/dump.html
        require_once __GRAVIT8__ . '/renderingEngine/twig/lib/Twig/Autoloader.php';
        Twig_Autoloader::register();

        $loader = new Twig_Loader_Filesystem('./templates');
        $twig = new Twig_Environment($loader, array(
            'auto_reload' => true,
            'debug' => true
        ));

        require_once __GRAVIT8__ . '/renderingEngine/twig/lib/Twig/Extension/Debug.php';
        $twig->addExtension(new Twig_Extension_Debug());

        //set global date timezone and format for twig
        $twig->getExtension('core')->setTimezone($UTC_TIMEZONE);
        $timeFormat = $isDevelopmentMachine ? 'F j, Y g:i:s A' : 'F j, Y';
        $twig->getExtension('core')->setDateFormat($timeFormat, '%d days');

        //add twig extensions
        $filters = glob(__GRAVIT8__ . '/app/commons/extensions/twig/filters/*.php');
        foreach ($filters as $filter) {
            require_once $filter;
        }
        $twig->addFilter($pluckFilter);
        $twig->addFilter($anyFilter);

        //register a cleanup function to run whenever app.php ends (ex. trigger by exit())
        function cleanup() {
            //shutdown the database session
            $databaseHandler = DatabaseHandler::getInstance();
            $databaseHandler->closeDatabase();
        }
        register_shutdown_function('cleanup');

        session_start();

        //routes handler
        require_once __GRAVIT8__ . '/app/routesController.php';

    } catch (Exception $error) {
        debugError($error);
    } finally {
        exit();
    }
?>
