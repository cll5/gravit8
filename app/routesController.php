<?php
    //discover the default php file upload limits
    // debug('upload_max_filesize is ' . ini_get('upload_max_filesize'));
    // debug('max_file_uploads per request is ' . ini_get('max_file_uploads'));
    // debug('post_max_size is ' . ini_get('post_max_size'));

    //template parameters setup
    $parameters = array(
        'isDevelopmentMachine' => $isDevelopmentMachine,
        'timeFormat' => $timeFormat    //NOTE: unfortunately, this variable is defined in app.php when configuring twig's default time format and is necessary? for displaying timestamps in specific timezones
    );

    //check cookie if custom fonts are cached previously
    $parameters['fontsAreLoaded'] = isset($_COOKIE['fontsLoaded']);

    //identify which route handlers to use and what organization is requested
    $hostname = $_SERVER['SERVER_NAME'];

    //temporary subdomain for testing Hatching Health on the development server
    if (preg_match('/^test.([a-zA-Z0-9\_\-]+)\.gravit8.co[\/]?$/', $hostname, $subdomains)) {
        // $organization = 'hatchinghealth';
        $organization = $subdomains[1];
        require_once __GRAVIT8__ . '/app/routes/appRoutes.php';
        return;
    }

    //temporarily redirecting hatching health to 404 page until we can export the data out before shutting down the organization
    if (preg_match('/^(?:www\.)?hatchinghealth.gravit8.co[\/]?$/', $hostname)) {
        gotoErrorPage('404', $parameters);
        return;
    }

    if (preg_match('/^(?:www\.)?gravit8\.co[\/]?$/', $hostname)) {
        //goto Gravit8 homepage
        require_once __GRAVIT8__ . '/app/routes/landingPageRoutes.php';
    } else if (preg_match('/^(?:www\.)?([a-zA-Z0-9\_\-]+)\.gravit8\.co[\/]?$/', $hostname, $subdomains)) {
        //goto Gravit8 app
        $organization = $subdomains[1];
        require_once __GRAVIT8__ . '/app/routes/appRoutes.php';
    } else if (preg_match('/^localhost[\/]?$/', $hostname)) {
        //for development use: goto localhost default
        $organization = 'iweekend'; //NOTE: change organization for localhost as long the organization exists in your local database
        require_once __GRAVIT8__ . '/app/routes/appRoutes.php';
    } else {
        gotoErrorPage('404', $parameters);
    }

    return;
?>