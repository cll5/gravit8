<?php
    $route = $_SERVER['REQUEST_URI'];

    if ($_SERVER['REQUEST_METHOD'] === 'GET') {

        //route: /
        if (preg_match('/^\/$/', $route)) {
            renderTemplate('landing/home.phtml', NULL, $parameters);
            return;
        }
    }

    return;
?>