<?php
    if (!defined('__GRAVIT8__')) {
        $gravit8Directory = dirname(dirname(dirname(__File__)));
        define('__GRAVIT8__', $gravit8Directory);
    }

    function renderTemplate($templatePath, $organization = NULL, $parameters = NULL) {
        try {
            global $twig;

            //set up defaults
            if (is_null($parameters)) {
                $parameters = array();
            }

            //display the organization custom templates if they exist, otherwise fallback to the default templates
            $organizationCustomTemplate = __GRAVIT8__ . '/templates/' . $organization . '/' . $templatePath;
            $organizationCustomTemplate = str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, $organizationCustomTemplate);

            if (!is_null($organization) && file_exists($organizationCustomTemplate)) {
                $customTemplatePath = $organization . '/' . $templatePath;
                $twig->display($customTemplatePath, $parameters);
                return;
            }

            //fallback to the default templates
            $defaultTemplatePath = 'default/' . $templatePath;
            $twig->display($defaultTemplatePath, $parameters);
            return;
        } catch (Exception $error) {
            throw $error;
        }
    }

    function gotoErrorPage($errorCode = '404', $parameters = NULL) {
        global $twig;

        //set up defaults
        if (is_null($parameters)) {
            $parameters = array();
        }

        switch ($errorCode) {
            case '404':
                http_response_code(404);
                renderTemplate('error/pageNotFound.phtml', NULL, $parameters);
                break;
        }
        exit();
    }

    function logUserOut() {
        //see: http://stackoverflow.com/questions/15474548/how-do-i-unset-the-session-id-so-as-to-logout-the-user
        session_unset();
        if (isset($_COOKIE[session_name()])) {
            setcookie(session_name(), '', -1);
        }
        session_destroy();
        header('Location: /login');
        exit();
    }

    //NOTE: should accountExists be TRUE by default?
    function createDefaultWarningModel() {
        return array(
            'accountExists' => FALSE,
            'invalidCredentials' => FALSE,
            'invalidSignupCode' => FALSE,
            'invalidResetKey' => FALSE,
            'passwordsMismatch' => FALSE,
            'unableToResetPassword' => FALSE
        );
    }
?>