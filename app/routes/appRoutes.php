<?php
    require_once __GRAVIT8__ . '/app/model/DatabaseHandler.php';
    $databaseHandler = DatabaseHandler::getInstance();

    require_once __GRAVIT8__ . '/app/model/EntityHandler.php';
    require_once __GRAVIT8__ . '/app/model/entities/IdeaHandler.php';
    require_once __GRAVIT8__ . '/app/model/entities/PitchHandler.php';
    require_once __GRAVIT8__ . '/app/model/entities/ProjectHandler.php';
    $entityHandler = EntityHandler::getInstance();
    $ideaHandler = IdeaHandler::getInstance();
    $pitchHandler = PitchHandler::getInstance();
    $projectHandler = ProjectHandler::getInstance();

    //get the current time
    $currentTimeStamp = getTimeStamp();

    //condition to create a new organization
    if (!$organizationHandler->doesOrganizationExists($organization)) {
        //create organization if it doesn't exists
        // $organizationModel = $organizationHandler->createOrganization($organization, $organization);
        gotoErrorPage('404', $parameters);
    } else {
        $organizationModel = $organizationHandler->createOrganizationModel($organization);
        $parameters['organization'] = $organizationModel;
    }

    //check if user is already logged in
    $userIsLoggedIn = isset($_SESSION['userId']);

    //log user out if he/she is inactive for XX mins
    //See http://stackoverflow.com/questions/520237/how-do-i-expire-a-php-session-after-30-minutes
    if ($userIsLoggedIn) {
        //logUserOut();
    }

    //routes handling
    $route = $_SERVER['REQUEST_URI'];

    if ($isDevelopmentMachine) {
        $userAgent = $_SERVER['HTTP_USER_AGENT'];
        debug($_SERVER['REQUEST_METHOD'] . ': ' . $route);
        debug('User Agent: ' . $userAgent);
    }

    if ($_SERVER['REQUEST_METHOD'] === 'GET') {

        //route: hostname - redirect to login page
        if (preg_match('/^\/$/', $route)) {
            if ($userIsLoggedIn) {
                 //organization specific redirect
                switch ($organizationModel['organizationDomain']) {
                    case 'hatchinghealth':
                    case 'codehack-2019':
                        header('Location: /browse/pitches');
                        break;

                    default:
                        header('Location: /browse/ideas');
                }
            } else {
                header('Location: /login');
            }
            exit();
        }

        //route: /login
        if (preg_match('/^\/login[\/]?$/', $route)) {
            if ($userIsLoggedIn) {
                //organization specific redirect
                switch ($organizationModel['organizationDomain']) {
                    case 'hatchinghealth':
                    case 'codehack-2019':
                        header('Location: /browse/pitches');
                        break;

                    default:
                        header('Location: /browse/ideas');
                }
            } else {
                $parameters['showWarnings'] = createDefaultWarningModel();
                renderTemplate('landing/login.phtml', $organizationModel['organizationDomain'], $parameters);
            }
            exit();
        }

        //route: /logout
        if (preg_match('/^\/logout[\/]?$/', $route)) {
            logUserOut();
            exit();
        }

        //route: /reset
        if (preg_match('/^\/reset[\/]?$/', $route)) {
            $parameters['showWarnings'] = createDefaultWarningModel();
            $parameters['showWarnings']['accountExists'] = TRUE;
            renderTemplate('landing/resetPassword.phtml', $organizationModel['organizationDomain'], $parameters);
            exit();
        }

        //route: /signup
        if (preg_match('/^\/signup[\/]?$/', $route)) {
            if ($userIsLoggedIn) {
                //organization specific redirect
                switch ($organizationModel['organizationDomain']) {
                    case 'hatchinghealth':
                    case 'codehack-2019':
                        header('Location: /browse/pitches');
                        break;

                    default:
                        header('Location: /browse/ideas');
                }
            } else {
                $parameters['showWarnings'] = createDefaultWarningModel();
                renderTemplate('landing/signup.phtml', $organizationModel['organizationDomain'], $parameters);
            }
            exit();
        }

        //go back to login page if the user is not logged in
        if (!$userIsLoggedIn) {
            header('Location: /login');
            exit();
        }

        //generate the user model
        $userId = (int) $_SESSION['userId'];
        if (isset($parameters['organization'])) {
            $parameters['user'] = $userHandler->createBasicUserProfileModel($userId, $organizationModel);
        } else {
            $parameters['user'] = $userHandler->createBasicUserProfileModel($userId);
        }

        //route: /ping
        if (preg_match('/^\/ping[\/]?$/', $route)) {
            debug('>>> Ping received from client. NOTE: This is a temporary hack to keep the session alive from timing out.');
            debug('>>> User id: ' . $userId . "\n\n");
            exit();
        }

        //route: /help
        if (preg_match('/^\/help[\/]?$/', $route)) {
            renderTemplate('commons/faq.phtml', $organizationModel['organizationDomain'], $parameters);
            exit();
        }

        //route: /browse
        if (preg_match('/^\/browse[\/]?$/', $route)) {
            //organization specific redirect
            switch ($organizationModel['organizationDomain']) {
                case 'hatchinghealth':
                case 'codehack-2019':
                    header('Location: /browse/pitches');
                    break;

                default:
                    header('Location: /browse/ideas');
            }
            exit();
        }

        //route: /browse/(ideas OR pitches OR projects OR people)
        if (preg_match('/^\/browse\/(ideas|pitches|projects|people)[\/]?$/', $route, $url)) {
            $parameters['browseTab'] = $url[1];
            $parameters['user']['voteCapacity'] = $entityHandler->votesByUser($userId, $organizationModel['organizationId'], $organizationModel['organizationGroupId']);
            $parameters['categories'] = $entityHandler->listAllCategories($organizationModel);
            renderTemplate('gallery/explore.phtml', $organizationModel['organizationDomain'], $parameters);
            exit();
        }

        //route: /get/ideas
        if (preg_match('/^\/get\/ideas\?/', $route)) {
            $request = parseJSON($_GET);

            $entities = array();
            if (isset($request['ids'])) {
                //get by multiple ids
                $entities = $ideaHandler->createBatchBasicIdeaModels($request['ids'], $userId);
            } else if (isset($request['id']) && isset($request['lookupDirection'])) {
                //get by pagination
                $entities = $ideaHandler->getBatchIdeas($request['id'], $organizationModel, $request['lookupDirection'], $userId);
            }debug('get ideas');debug($request);debug($entities);

            $response = formatJSON(TRUE, array(
                'entities' => $entities
            ));debug($response);
            echo $response;
            exit();
        }

        //route: /get/pitches
        if (preg_match('/^\/get\/pitches\?/', $route)) {
            $request = parseJSON($_GET);

            $entities = array();
            if (isset($request['ids'])) {
                //get by multiple ids
                $entities = $pitchHandler->createBatchBasicPitchModels($request['ids'], $userId);
            } else if (isset($request['id']) && isset($request['lookupDirection'])) {
                //get by pagination
                $entities = $pitchHandler->getBatchPitches($request['id'], $organizationModel, $request['lookupDirection'], $userId);
            }

            $response = formatJSON(TRUE, array(
                'entities' => $entities
            ));
            echo $response;
            exit();
        }

        //route: /get/projects
        if (preg_match('/^\/get\/projects\?/', $route)) {
            $request = parseJSON($_GET);

            $entities = array();
            if (isset($request['ids'])) {
                //get by multiple ids
                $entities = $projectHandler->createBatchBasicProjectModels($request['ids'], $userId);
            } else if (isset($request['id']) && isset($request['lookupDirection'])) {
                //get by pagination
                $entities = $projectHandler->getBatchProjects($request['id'], $organizationModel, $request['lookupDirection'], $userId);
            }

            $response = formatJSON(TRUE, array(
                'entities' => $entities
            ));
            echo $response;
            exit();
        }

        //route: /create
        if (preg_match('/^\/create[\/]?$/', $route)) {
            renderTemplate('entity/create.phtml', $organizationModel['organizationDomain'], $parameters);
            exit();
        }

        //route: /create/idea
        if (preg_match('/^\/create\/idea/', $route)) {
            $parameters['categories'] = $entityHandler->listAllCategories($organizationModel);
            renderTemplate('entity/createIdea.phtml', $organizationModel['organizationDomain'], $parameters);
            exit();
        }

        //route: /create/pitch
        if (preg_match('/^\/create\/pitch/', $route)) {
            $parameters['categories'] = $entityHandler->listAllCategories($organizationModel);
            renderTemplate('entity/createPitch.phtml', $organizationModel['organizationDomain'], $parameters);
            exit();
        }

        //route: /create/project
        if (preg_match('/^\/create\/project/', $route)) {
            $parameters['categories'] = $entityHandler->listAllCategories($organizationModel);
            renderTemplate('entity/createProject.phtml', $organizationModel['organizationDomain'], $parameters);
            exit();
        }

        //route: /idea/(id)
        if (preg_match('/^\/idea\/(\d+)[\/]?$/', $route, $url)) {
            $entityId = (int) $url[1];
            if ($entityHandler->isEntityInOrganization($entityId, $organizationModel)) {
                $parameters['idea'] = $ideaHandler->createIdeaModel($entityId, $userId);
                $parameters['categories'] = $entityHandler->listAllCategories($organizationModel);
                $parameters['user']['voteCapacity'] = $entityHandler->votesByUser($userId, $organizationModel['organizationId'], $organizationModel['organizationGroupId']);
                renderTemplate('entity/idea.phtml', $organizationModel['organizationDomain'], $parameters);
            } else {
                gotoErrorPage('404', $parameters);
            }
            exit();
        }

        //route: /get/idea/(id)
        if (preg_match('/^\/get\/idea\/(\d+)[\/]?$/', $route, $url)) {
            $entityId = (int) $url[1];

            $entityModel = array();
            if ($entityHandler->isEntityInOrganization($entityId, $organizationModel)) {
                $entityModel = $ideaHandler->createIdeaModel($entityId, $userId);
            }

            $response = formatJSON(TRUE, $entityModel);
            echo $response;
            exit();
        }

        //route: /pitch/(id)
        if (preg_match('/^\/pitch\/(\d+)[\/]?$/', $route, $url)) {
            $entityId = (int) $url[1];
            if ($entityHandler->isEntityInOrganization($entityId, $organizationModel)) {
                $parameters['pitch'] = $pitchHandler->createPitchModel($entityId, $userId);
                $parameters['categories'] = $entityHandler->listAllCategories($organizationModel);
                $parameters['user']['voteCapacity'] = $entityHandler->votesByUser($userId, $organizationModel['organizationId'], $organizationModel['organizationGroupId']);
                renderTemplate('entity/pitch.phtml', $organizationModel['organizationDomain'], $parameters);
            } else {
                gotoErrorPage('404', $parameters);
            }
            exit();
        }

        //route: /get/pitch/(id)
        if (preg_match('/^\/get\/pitch\/(\d+)[\/]?$/', $route, $url)) {
            $entityId = (int) $url[1];

            $entityModel = array();
            if ($entityHandler->isEntityInOrganization($entityId, $organizationModel)) {
                $entityModel = $pitchHandler->createPitchModel($entityId, $userId);
            }

            $response = formatJSON(TRUE, $entityModel);
            echo $response;
            exit();
        }

        //route: /project/(id)
        if (preg_match('/^\/project\/(\d+)[\/]?$/', $route, $url)) {
            $entityId = (int) $url[1];
            if ($entityHandler->isEntityInOrganization($entityId, $organizationModel)) {
                $parameters['project'] = $projectHandler->createProjectModel($entityId, $userId);
                $parameters['categories'] = $entityHandler->listAllCategories($organizationModel);
                $parameters['user']['voteCapacity'] = $entityHandler->votesByUser($userId, $organizationModel['organizationId'], $organizationModel['organizationGroupId']);
                renderTemplate('entity/project.phtml', $organizationModel['organizationDomain'], $parameters);
            } else {
                gotoErrorPage('404', $parameters);
            }
            exit();
        }

        //route: /get/project/(id)
        if (preg_match('/^\/get\/project\/(\d+)[\/]?$/', $route, $url)) {
            $entityId = (int) $url[1];

            $entityModel = array();
            if ($entityHandler->isEntityInOrganization($entityId, $organizationModel)) {
                $entityModel = $projectHandler->createProjectModel($entityId, $userId);
            }

            $response = formatJSON(TRUE, $entityModel);
            echo $response;
            exit();
        }

        //route: /people/(id)
        if (preg_match('/^\/people\/(\d+)[\/]?$/', $route, $url)) {
            $personId = (int) $url[1];
            if ($userHandler->isUserInOrganization($personId, $organizationModel)) {
                $parameters['person'] = $userHandler->createAdvanceUserProfileModel($personId);
                $parameters['contactTypes'] = $userHandler->listAllContactTypes();
                renderTemplate('profile/profile.phtml', $organizationModel['organizationDomain'], $parameters);
            } else {
                gotoErrorPage('404', $parameters);
            }
            exit();
        }

        //route: /get/person/(id)
        if (preg_match('/^\/get\/person\/(\d+)[\/]?$/', $route, $url)) {
            $personId = (int) $url[1];
            $response = formatJSON(FALSE);

            if ($userHandler->isUserInOrganization($personId, $organizationModel)) {
                $profile = $userHandler->createAdvanceUserProfileModel($personId);
                $response = formatJSON(TRUE, $profile);
            }
            echo $response;
            exit();
        }

        //route: /get/people
        if (preg_match('/^\/get\/people\?/', $route)) {
            $request = parseJSON($_GET);

            $people = array();
            if (isset($request['ids'])) {
                //get by multiple ids
                foreach ($request['ids'] as $personId) {
                    $people[] = $userHandler->createBasicUserProfileModel($personId);
                }
            } else if (isset($request['id']) && isset($request['lookupDirection'])) {
                //get by pagination
                $people = $userHandler->getBatchPeople($request['id'], $organizationModel, $request['lookupDirection']);
            }debug('get people');debug($request);debug($people);

            $response = formatJSON(TRUE, array(
                'people' => $people
            ));
            echo $response;
            exit();
        }

        //route: /get/portfolios
        if (preg_match('/^\/get\/portfolios\?/', $route)) {
            $person = parseJSON($_GET);
            $portfolios = $portfolioHandler->createPortfolioModel($person['id'], $userId, $organizationModel);
            $response = formatJSON(TRUE, $portfolios);
            echo $response;
            exit();
        }

        //route: /get/list/contact-types
        if (preg_match('/^\/get\/list\/contact-types[\/]?$/', $route)) {
            $contactTypes = array(
                'contactTypes' => $userHandler->listAllContactTypes()
            );
            $response = formatJSON(TRUE, $contactTypes);
            echo $response;
            exit();
        }

        //route: /get/list/categories
        if (preg_match('/^\/get\/list\/categories[\/]?$/', $route)) {
            $categories = array(
                'categories' => $entityHandler->listAllCategories($organizationModel)
            );
            $response = formatJSON(TRUE, $categories);
            echo $response;
            exit();
        }

        //route: /get/comments
        if (preg_match('/^\/get\/comments\?/', $route)) {
            $request = parseJSON($_GET);

            if (isset($request['entityId'])) {
                $comments = $commentHandler->loadCommentsByEntity($request['entityId'], $request['lookupDirection'], $request['referenceId'], $userId);
                $numberOfComments = $entityHandler->numberOfCommentInEntity($request['entityId']);
            } else if (isset($request['personId'])) {
                $comments = $commentHandler->loadCommentsByUser($request['personId'], $request['commentId'], $userId);
                $numberOfComments = $entityHandler->numberOfCommentByCommenter($request['personId']);
            }

            $response = formatJSON(TRUE, array(
                'numberOfComments' => $numberOfComments,
                'comments' => $comments
            ));
            echo $response;
            exit();
        }

        //route: /get/attachment
        if (preg_match('/^\/get\/attachment\?/', $route)) {
            $request = parseJSON($_GET);
            $attachment = $attachmentHandler->getAttachmentByChecksum($request['checksum']);
            $response = formatJSON(TRUE, $attachment);
            echo $response;
            exit();
        }

        //route: /get/summary
        if (preg_match('/^\/get\/summary[\/]?$/', $route)) {
            $response = formatJSON(TRUE, array(
                'summary' => $organizationModel['summary']
            ));
            echo $response;
            exit();
        }

        //route: anything else
        else {
            gotoErrorPage('404');
            exit();
        }
    }

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        //route: /login
        if (preg_match('/^\/login[\/]?$/', $route)) {
            $parameters['showWarnings'] = createDefaultWarningModel();

            $email = $_POST['email'];
            $password = $_POST['password'];

            $verificationResult = $accountHandler->verifyAccount($email, $password);
            if ($verificationResult['isValidAccount'] && $userHandler->isUserInOrganization($verificationResult['userId'], $organizationModel)) {
                $_SESSION['userId'] = $verificationResult['userId'];

                //organization specific redirect
                switch ($organizationModel['organizationDomain']) {
                    case 'hatchinghealth':
                    case 'codehack-2019':
                        header('Location: /browse/pitches');
                        break;

                    default:
                        header('Location: /browse/ideas');
                }
            } else {
                $parameters['showWarnings']['invalidCredentials'] = TRUE;
                renderTemplate('landing/login.phtml', $organizationModel['organizationDomain'], $parameters);
            }
            exit();
        }

        //route: /signup
        if (preg_match('/^\/signup[\/]?$/', $route)) {
            $parameters['showWarnings'] = createDefaultWarningModel();

            //TODO: validate that the email is a valid email and not just random gibberish, use confirm email technique?
            $firstName = $_POST['first-name'];
            $lastName = $_POST['last-name'];
            $email = $_POST['email'];
            $password = $_POST['password'];
            $confirmPassword = $_POST['confirm-password'];
            $signupKey = $_POST['signup-key'];

            //password confirmation don't match, have them retype it again
            $passwordMatch = $accountHandler->confirmPassword($password, $confirmPassword);
            if ($passwordMatch) {
                $result = $accountHandler->createAccount($firstName, $lastName, $email, $password, $signupKey, $organizationModel);
                switch ($result['status']) {
                    case AccountHandler::SUCCESS: {
                        $_SESSION['userId'] = $result['userId'];

                        //organization specific redirect
                        switch ($organizationModel['organizationDomain']) {
                            case 'codehack-2019':
                                header('Location: /help');
                                break;
                            default: {
                                header('Location: /people/' . $result['userId']);
                            }
                        }
                        break;

                    } case AccountHandler::ACCOUNT_ALREADY_EXISTS: {
                        $parameters['showWarnings']['accountExists'] = TRUE;
                        renderTemplate('landing/signup.phtml', $organizationModel['organizationDomain'], $parameters);
                        break;

                    } case AccountHandler::INVALID_SIGNUP_CODE: {
                        $parameters['showWarnings']['invalidSignupCode'] = TRUE;
                        renderTemplate('landing/signup.phtml', $organizationModel['organizationDomain'], $parameters);
                        break;

                    } default: {
                        //something went terribly wrong
                        throw new Exception('Something went terribly wrong with signing up an account!');
                    }
                }
            } else {
                $parameters['showWarnings']['passwordsMismatch'] = TRUE;
                renderTemplate('landing/signup.phtml', $organizationModel['organizationDomain'], $parameters);
            }
            exit();
        }

        //route: /reset
        if (preg_match('/^\/reset[\/]?$/', $route)) {
            $parameters['showWarnings'] = createDefaultWarningModel();
            $parameters['showWarnings']['accountExists'] = TRUE;

            $email = $_POST['email'];
            $password = $_POST['password'];
            $confirmPassword = $_POST['confirm-password'];
            $resetKey = $_POST['reset-key'];

            $passwordMatch = $accountHandler->confirmPassword($password, $confirmPassword);
            if ($passwordMatch) {
                //check if user belongs to this organization
                $query = "SELECT id FROM User WHERE account_name = :accountName";
                $queryVariables = array(
                    ':accountName' => $email
                );
                $result = $databaseHandler->queryByColumn($query, $queryVariables);
                $userId = reset($result['data']);
                if (!$userHandler->isUserInOrganization($userId, $organizationModel)) {
                    $parameters['showWarnings']['invalidCredentials'] = TRUE;
                    renderTemplate('landing/resetPassword.phtml', $organizationModel['organizationDomain'], $parameters);
                    exit();
                }

                //try resetting the password now
                $result = $accountHandler->resetPassword($email, $password, $resetKey);
                switch ($result['status']) {
                    case AccountHandler::SUCCESS:
                        $_SESSION['userId'] = $result['userId'];
                        header('Location: /people/' . $result['userId']);
                        break;

                    case AccountHandler::ACCOUNT_DOES_NOT_EXISTS:
                        $parameters['showWarnings']['accountExists'] = FALSE;
                        renderTemplate('landing/resetPassword.phtml', $organizationModel['organizationDomain'], $parameters);
                        break;

                    case AccountHandler::INVALID_RESET_KEY:
                        $parameters['showWarnings']['invalidResetKey'] = TRUE;
                        renderTemplate('landing/resetPassword.phtml', $organizationModel['organizationDomain'], $parameters);
                        break;

                    case AccountHandler::UNSUCCESSFUL:
                        $parameters['showWarnings']['unableToResetPassword'] = TRUE;
                        renderTemplate('landing/resetPassword.phtml', $organizationModel['organizationDomain'], $parameters);
                        break;
                }
            } else {
                $parameters['showWarnings']['passwordsMismatch'] = TRUE;
                renderTemplate('landing/resetPassword.phtml', $organizationModel['organizationDomain'], $parameters);
            }

            exit();
        }

        //go back to login page if the user is not logged in
        if (!isset($_SESSION['userId'])) {
            header('Location: /login');
            exit();
        }

        $userId = (int) $_SESSION['userId'];

        //route: /submit/new/idea
        if (preg_match('/^\/submit\/new\/idea[\/]?$/', $route)) {
            try {
                $contents = parseJSON($_POST);
                $entityId = $ideaHandler->submitIdea($userId, $organizationModel, $contents);
                $response = formatJSON(TRUE, array(
                    'entityId' => $entityId
                ));
                echo $response;
            } catch (Exception $error) {
                throw $error;
            }
            exit();
        }

        //route: /submit/new/pitch
        if (preg_match('/^\/submit\/new\/pitch[\/]?$/', $route)) {
            try {
                $contents = parseJSON($_POST);
                $entityId = $pitchHandler->submitPitch($userId, $organizationModel, $contents);
                $response = formatJSON(TRUE, array(
                    'entityId' => $entityId
                ));
                echo $response;
            } catch (Exception $error) {
                throw $error;
            }
            exit();
        }

        //route: /submit/new/project
        if (preg_match('/^\/submit\/new\/project[\/]?$/', $route)) {
            try {
                $contents = parseJSON($_POST);
                $entityId = $projectHandler->submitProject($userId, $organizationModel, $contents);
                $response = formatJSON(TRUE, array(
                    'entityId' => $entityId
                ));
                echo $response;
            } catch (Exception $error) {
                throw $error;
            }
            exit();
        }

        //route: /update/idea
        if (preg_match('/^\/update\/idea[\/]?$/', $route)) {
            $entity = parseJSON($_POST);

            if (!isset($entity['id']) || !isset($entity['contents'])) {
                http_response_code(412);
                throw new Exception('payload to /update/idea is missing either id and/or contents');
            } else {
                $updateSuccess = $ideaHandler->updateIdea($entity['id'], $entity['contents'], $userId);
                if (!$updateSuccess) {
                    http_response_code(500);
                    throw new Exception('update entity failed for entity ' . $entity['id'] . ' in /update/idea');
                }

                $response = formatJSON($updateSuccess);
                echo $response;
            }
            exit();
        }

        //route: /update/pitch
        if (preg_match('/^\/update\/pitch[\/]?$/', $route)) {
            $entity = parseJSON($_POST);
            if (!isset($entity['id']) || !isset($entity['contents'])) {
                http_response_code(412);
                throw new Exception('payload to /update/pitch is missing either id and/or contents');
            } else {
                $updateSuccess = $pitchHandler->updatePitch($entity['id'], $entity['contents'], $userId);
                if (!$updateSuccess) {
                    http_response_code(500);
                    throw new Exception('update entity failed for entity ' . $entity['id'] . ' in /update/pitch');
                }

                $response = formatJSON($updateSuccess);
                echo $response;
            }
            exit();
        }

        //route: /update/project
        if (preg_match('/^\/update\/project[\/]?$/', $route)) {
            $entity = parseJSON($_POST);
            if (!isset($entity['id']) || !isset($entity['contents'])) {
                http_response_code(412);
                throw new Exception('payload to /update/project is missing either id and/or contents');
            } else {
                $updateSuccess = $projectHandler->updateProject($entity['id'], $entity['contents'], $userId);
                if (!$updateSuccess) {
                    http_response_code(500);
                    throw new Exception('update entity failed for entity ' . $entity['id'] . ' in /update/project');
                }

                $response = formatJSON($updateSuccess);
                echo $response;
            }
            exit();
        }

        //route: /update/profile
        if (preg_match('/^\/update\/profile[\/]?$/', $route)) {
            $profile = parseJSON($_POST);
            $isSuccessful = $userHandler->updateProfile($profile['id'], $profile['attributes']);
            $response = formatJSON($isSuccessful);
            echo $response;
            exit();
        }

        //route: /submit/comment
        if (preg_match('/^\/submit\/comment[\/]?$/', $route)) {
            $comment = parseJSON($_POST);
            $entityId = $comment['entityId'];

            $response = formatJSON(FALSE);
            if ($entityId > 0) {
                $comments = $commentHandler->postComment($entityId, $userId, $comment['message'], $comment['latestCommentId']);
                $numberOfComments = $entityHandler->numberOfCommentInEntity($entityId);

                $response = formatJSON(TRUE, array(
                    'numberOfComments' => $numberOfComments,
                    'comments' => $comments
                ));
            }
            echo $response;
            exit();
        }

        //route: /like/entity
        if (preg_match('/^\/like\/entity[\/]?$/', $route)) {
            $entity = parseJSON($_POST);
            $isSuccessful = FALSE;

            //like or dislike the entity
            if ($entityHandler->isEntityLikedByUser($entity['id'], $userId)) {
                $isSuccessful = $entityHandler->dislikeEntity($entity['id'], $userId);
            } else {
                $isSuccessful = $entityHandler->likeEntity($entity['id'], $userId);
            }

            //number of likes for this entity
            $numberOfLikes = $entityHandler->numberOfLikeForEntity($entity['id']);
            $likeStatistics = array(
                'numberOfLikesForEntity' => $numberOfLikes
            );

            $response = formatJSON($isSuccessful, $likeStatistics);
            echo $response;
            exit();
        }

        //route: /vote/entity
        if (preg_match('/^\/vote\/entity[\/]?$/', $route)) {
            $entity = parseJSON($_POST);
            $entityId = $entity['id'];
            $organizationId = $organizationModel['organizationId'];
            $organizationGroupId = $organizationModel['organizationGroupId'];
            $entityModel = $entityHandler->createBasicEntityModel($entityId);

            //vote or unvote the entity
            $isSuccessful = FALSE;
            if ($entityHandler->isEntityVotedByUser($entityId, $userId)) {
                $isSuccessful = $entityHandler->unvoteEntity($entityId, $userId);
            } else if ($entityHandler->canUserStillVote($userId, $entityId, $organizationId, $organizationGroupId)) {
                $isSuccessful = $entityHandler->voteEntity($entityId, $userId);
            }

            //number of voted entities
            $voteCapacity = $entityHandler->votesByUser($userId, $organizationId, $organizationGroupId);
            $numberOfVotesForEntity = $entityHandler->numberOfVoteForEntity($entityId);

            $voteStatistics = array(
                'entityType' => $entityModel['entityType'],
                'numberOfVotesForEntity' => $numberOfVotesForEntity,
                'voteCapacity' => $voteCapacity
            );

            $response = formatJSON($isSuccessful, $voteStatistics);
            echo $response;
            exit();
        }

        //route: /submit/attachment
        if (preg_match('/^\/submit\/attachment[\/]?$/', $route)) {
            $partialAttachmentAsBinary = file_get_contents('php://input');
            $attachmentName = $_SERVER['HTTP_CONTENT_ATTACHMENT_NAME'];
            $attachmentExtension = $_SERVER['HTTP_CONTENT_ATTACHMENT_EXTENSION'];
            $attachmentType = $_SERVER['HTTP_CONTENT_ATTACHMENT_TYPE'];
            $attachmentSize = (int) $_SERVER['HTTP_CONTENT_ATTACHMENT_SIZE'];
            $attachmentChecksum = $_SERVER['HTTP_CONTENT_ATTACHMENT_CHECKSUM'];
            $partialAttachmentChecksum = $_SERVER['HTTP_CONTENT_ATTACHMENT_PARTIAL_CHECKSUM'];
            $part = (int) $_SERVER['HTTP_CONTENT_ATTACHMENT_PART'];
            $totalParts = (int) $_SERVER['HTTP_CONTENT_ATTACHMENT_TOTAL_PARTS'];

            //create new attachment object
            $attachment = new Attachment($attachmentName, $attachmentExtension, $attachmentType, $attachmentSize, $totalParts, $attachmentChecksum);

            $attachmentId = (int) $_SERVER['HTTP_CONTENT_ATTACHMENT_ID'];
            if ($attachmentId > 0) {
                //valid attachment id is greater than 0
                $attachment->setAttachmentId($attachmentId);
            }

            //save the attachment or partial attachment
            $isSaveSuccessful = $attachment->saveAttachmentToDirectory($partialAttachmentAsBinary, $part);
            $attachmentId = $attachment->getAttachmentId();

            //add a record of the attachment to this organization
            $organizationHandler->addAttachmentsToOrganization($attachmentId, $organizationModel['organizationId']);

            //respond with the generated attachment id, and if it's successful
            $response = formatJSON($isSaveSuccessful, array(
                'attachmentId' => $attachmentId
            ));
            echo $response;
            exit();
        }

        //route: /remove/attachment
        if (preg_match('/^\/remove\/attachment[\/]?$/', $route)) {
            $request = parseJSON($_POST);
            $attachmentId = $request['attachmentId'];

            //remove attachment from file system
            $isSuccessful = $attachmentHandler->removeAttachmentsFromFileSystem($attachmentId);

            //remove attachment information from the database
            $isRemoveSuccessful = $attachmentHandler->removeAttachmentsFromDatabase($attachmentId);
            $isSuccessful = $isSuccessful && $isRemoveSuccessful;

            //remove attachment from organization
            $isRemoveSuccessful = $organizationHandler->removeAttachmentsFromOrganization($attachmentId, $organizationModel['organizationId']);
            $isSuccessful = $isSuccessful && $isRemoveSuccessful;

            //remove attachment from entity
            if (isset($request['entityId'])) {
                $entityId = $request['entityId'];
                $isRemoveSuccessful = $entityHandler->removeAttachmentsFromDatabase($attachmentId, $entityId);
                $isSuccessful = $isSuccessful && $isRemoveSuccessful;
            }

            $response = formatJSON($isSuccessful);
            echo $response;
            exit();
        }

        //TODO: to be implemented
        //route: /submit/search/ideas
        if (preg_match('/^\/submit\/search\/ideas/', $route)) {
            exit();
        }

        //TODO: to be implemented
        //route: /submit/search/pitches
        if (preg_match('/^\/submit\/search\/pitches/', $route)) {
            exit();
        }

        //TODO: to be implemented
        //route: /submit/search/projects
        if (preg_match('/^\/submit\/search\/projects/', $route)) {
            exit();
        }

        //route: /submit/search/people
        if (preg_match('/^\/submit\/search\/people/', $route)) {
            $request = parseJSON($_POST);
            $searchResults = $userHandler->searchUsers($request['keywords'], $organizationModel, $request['excludePeople']);
            $response = formatJSON(TRUE, $searchResults);
            echo $response;
            exit();
        }

        //route: anything else
        else {
            gotoErrorPage('404');
            exit();
        }
    }

    return;
?>
