<?php
    $route = $_SERVER['REQUEST_URI'];

    if ($_SERVER['REQUEST_METHOD'] === 'GET') {

        //route: /
        if (preg_match('/^\/$/', $route)) {
            $parameters['organizations'] = $organizationHandler->listAllOrganization();
            renderTemplate('export/controller.phtml', NULL, $parameters);
            return;
        }

        if (preg_match('/\/download\/pitches\/organization\/(.+)$/', $route, $organization)) {
            $organizationModel = $organizationHandler->createOrganizationModel($organization[1]);
            $pitchHandler->exportPitchesInOrganization($organizationModel);
            return;
        }
    }

    return;
?>