<?php
    if (!defined('__GRAVIT8__')) {
        $gravit8Directory = dirname(dirname(dirname(dirname(__File__))));
        define('__GRAVIT8__', $gravit8Directory);
    }

    require_once __GRAVIT8__ . '/app/commons/utilities.php';
    require_once __GRAVIT8__ . '/app/commons/interfaces/Singleton.php';
    require_once __GRAVIT8__ . '/app/model/EntityHandler.php';

    Class IdeaHandler extends EntityHandler implements Singleton {
        private static $instance;

        public function __construct() {
            parent::__construct();
        }

        public static function getInstance() {
            if (self::$instance === NULL) {
                self::$instance = new IdeaHandler();
            }

            return self::$instance;
        }

        public function createIdeaModel($entityId, $userId = NULL) {
            try {
                $basicIdeaModel = $this->createBasicIdeaModel($entityId, $userId);
                $advancedIdeaModel = $this->createAdvancedIdeaModel($entityId);
                $ideaModel = array_merge($basicIdeaModel, $advancedIdeaModel);

                return $ideaModel;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function createBatchIdeaModels($entityIds, $userId = NULL) {
            try {
                $ideaModels = array();
                foreach ($entityIds as $index => $entityId) {
                    $ideaModels[$index] = $this->createIdeaModel($entityId, $userId);
                }
                return $ideaModels;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function createBasicIdeaModel($entityId, $userId = NULL) {
            try {
                $ideaModel = $this->defaultBasicIdeaModel();
                $ideaModel['id'] = $entityId;

                //get idea, idea's title, and it's entity fields
                $query = "SELECT Entity.*, EntityType.entity_type FROM Entity INNER JOIN EntityType ON (Entity.entity_type_id = EntityType.id) WHERE (Entity.id = :entityId)";
                $queryVariables = array(
                    ':entityId' => $entityId
                );
                $result = $this->databaseHandler->query($query, $queryVariables);
                if (!$result['isEmpty']) {
                    $entity = reset($result['data']);
                    $ideaModel['entityType'] = $entity['entityType'];
                    $ideaModel['creator'] = $this->userHandler->createBasicUserProfileModel($entity['creatorId']);
                    $ideaModel['isPrivate'] = $entity['isPrivate'];
                    $ideaModel['createdOn'] = $entity['createdOn'];
                }

                //get idea specific properties
                $query = "SELECT * FROM Idea WHERE entity_id = :entityId";
                $result = $this->databaseHandler->query($query, $queryVariables);
                if (!$result['isEmpty']) {
                    $idea = reset($result['data']);
                    $ideaModel['title'] = $idea['title'];
                    $ideaModel['idea'] = $idea['idea'];
                }

                //get the categories
                $categories = $this->getEntityCategories($entityId);
                if (!empty($categories)) {
                    $ideaModel['categories'] = $categories;
                }

                //get the number of likes
                $ideaModel['numberOfLikes'] = $this->numberOfLikeForEntity($entityId);
                $ideaModel['liked'] = is_null($userId) ? FALSE : $this->isEntityLikedByUser($entityId, $userId);

                //get the number of comments
                $ideaModel['numberOfComments'] = $this->numberOfCommentInEntity($entityId);

                return $ideaModel;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function createBatchBasicIdeaModels($entityIds, $userId = NULL) {
            try {
                $ideaModels = array();
                foreach ($entityIds as $entityId) {
                    $ideaModels[] = $this->createBasicIdeaModel($entityId, $userId);
                }
                return $ideaModels;
            } catch (Exception $error) {
                throw $error;
            }
        }

        //TODO: complete this method
        public function createAdvancedIdeaModel($entityId) {
            try {
                $ideaModel = $this->defaultAdvancedIdeaModel();

                //get description (assume there is only one per idea)
                $description = $this->getDescription($entityId);
                if (!is_null($description)) {
                    $ideaModel['description'] = $description;
                }

                //get background
                $background = $this->getBackground($entityId);
                if (!is_null($background)) {
                    $ideaModel['background'] = $background;
                }

                //get stakeholder
                $stakeholder = $this->getStakeholder($entityId);
                if (!is_null($stakeholder)) {
                    $ideaModel['stakeholder'] = $stakeholder;
                }

                //get existing solution
                $existingSolution = $this->getExistingSolution($entityId);
                if (!is_null($existingSolution)) {
                    $ideaModel['existingSolution'] = $existingSolution;
                }

                //get market analysis
                $marketAnalysis = $this->getMarketAnalysis($entityId);
                if (!is_null($marketAnalysis)) {
                    $ideaModel['marketAnalysis'] = $marketAnalysis;
                }

                //get other information
                $otherInformation = $this->getOtherInformation($entityId);
                if (!is_null($otherInformation)) {
                    $ideaModel['otherInformation'] = $otherInformation;
                }

                //get journeys

                return $ideaModel;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public static function defaultBasicIdeaModel() {
            return array(
                'id' => NULL,
                'entityType' => NULL,
                'creator' => UserHandler::defaultBasicUserProfile(),
                'isPrivate' => NULL,
                'createdOn' => NULL,
                'liked' => FALSE,
                'title' => NULL,
                'idea' => NULL,
                'categories' => array(),
                'numberOfLikes' => 0,
                'numberOfComments' => 0
            );
        }

        public static function defaultAdvancedIdeaModel() {
            return array(
                'description' => NULL,
                'background' => NULL,
                'stakeholder' => NULL,
                'existingSolution' => NULL,
                'marketAnalysis' => NULL,
                'otherInformation' => NULL,
                'journeys' => array()
            );
        }

        public function getBatchIdeas($ideaId, $organizationModel, $lookupOrder = 'newest', $userId = NULL) {
            try {
                switch ($lookupOrder) {
                    case 'newest':
                        $query = "SELECT Entity.id, Entity.creator_id, Entity.is_private, Entity.created_on, Idea.title, Idea.idea FROM Idea INNER JOIN Entity ON (Idea.entity_id = Entity.id) INNER JOIN EntityOrganization ON (Idea.entity_id = EntityOrganization.entity_id) WHERE ((EntityOrganization.organization_id, EntityOrganization.organization_group_id) = (:organizationId, :organizationGroupId)) AND (Entity.id > :ideaId) ORDER BY id DESC LIMIT :batchSize";
                        break;

                    case 'oldest':
                        $query = "SELECT Entity.id, Entity.creator_id, Entity.is_private, Entity.created_on, Idea.title, Idea.idea FROM Idea INNER JOIN Entity ON (Idea.entity_id = Entity.id) INNER JOIN EntityOrganization ON (Idea.entity_id = EntityOrganization.entity_id) WHERE ((EntityOrganization.organization_id, EntityOrganization.organization_group_id) = (:organizationId, :organizationGroupId)) AND (Entity.id < :ideaId) ORDER BY id DESC LIMIT :batchSize";
                        break;
                }
                $queryVariables = array(
                    ':ideaId' => $ideaId,
                    ':organizationId' => $organizationModel['organizationId'],
                    ':organizationGroupId' => $organizationModel['organizationGroupId'],
                    ':batchSize' => 20
                );
                $result = $this->databaseHandler->query($query, $queryVariables);

                $ideas = array();
                if (!$result['isEmpty']) {
                    foreach ($result['data'] as $index => $idea) {
                        $ideas[$index] = $this->defaultBasicIdeaModel();

                        $ideas[$index]['id'] = $idea['id'];
                        $ideas[$index]['creator'] = $this->userHandler->createBasicUserProfileModel($idea['creatorId']);
                        $ideas[$index]['isPrivate'] = $idea['isPrivate'];
                        $ideas[$index]['createdOn'] = $idea['createdOn'];
                        $ideas[$index]['title'] = $idea['title'];
                        $ideas[$index]['idea'] = $idea['idea'];

                        //get the categories
                        $ideas[$index]['categories'] = $this->getEntityCategories($idea['id']);

                        //get the number of likes
                        $ideas[$index]['numberOfLikes'] = $this->numberOfLikeForEntity($idea['id']);
                        $ideas[$index]['liked'] = is_null($userId) ? FALSE : $this->isEntityLikedByUser($idea['id'], $userId);

                        //get the number of comments
                        $ideas[$index]['numberOfComments'] = $this->numberOfCommentInEntity($idea['id']);
                    }
                }

                return $ideas;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function searchIdeas($keywords, $organizationModel) {
            try {
                $query = "SELECT * FROM Idea INNER JOIN Entity ON (Idea.entity_id = Entity.id) INNER JOIN EntityOrganization ON (Idea.entity_id = EntityOrganization.entity_id) WHERE ((EntityOrganization.organization_id, EntityOrganization.organization_group_id) = (:organizationId, :organizationGroupId)) AND MATCH (title, idea) AGAINST (:keywords IN NATURAL LANGUAGE MODE WITH QUERY EXPANSION)";
                $queryVariables = array(
                    ':organizationId' => $organizationModel['organizationId'],
                    ':organizationGroupId' => $organizationModel['organizationGroupId'],
                    ':keywords' => $keywords
                );
                $searchResult = $this->databaseHandler->query($query, $queryVariables);
                return $searchResult;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function submitIdea($creatorId, $organizationModel, $contents) {
            try {
                if (count(array_keys($contents)) < 4) {
                    throw New Exception('>>> ERROR: Idea submission has less entity contents than expected');
                }

                $now = getTimeStamp();

                $this->databaseHandler->beginTransaction();

                //adding new entry to entity
                $isPrivate = isset($contents['setPrivate']) ? $contents['setPrivate'] : FALSE;
                $ideaId = $this->addNewEntity('idea', $organizationModel, $creatorId, $now, $isPrivate);

                //add the idea categories
                if (isset($contents['categories'])) {
                    $this->addCategoriesByIds($ideaId, $contents['categories']);
                }

                //add new entry to idea
                if (isset($contents['oneLiner']) && isset($contents['title'])) {
                    $query = "INSERT INTO Idea (entity_id, title, idea) VALUES (:entityId, :title, :idea)";
                    $queryVariables = array(
                        ':entityId' => $ideaId,
                        ':title' => $contents['title'],
                        ':idea' => $contents['oneLiner']
                    );
                    $this->databaseHandler->query($query, $queryVariables);
                }

                //add the description (opportunity or problem statement)
                if (isset($contents['description'])) {
                    $this->addNewDescription($ideaId, $contents['description'], $creatorId, $now);
                }

                //add the background
                if (isset($contents['background'])) {
                    $this->addNewBackground($ideaId, $contents['background'], $creatorId, $now);
                }

                //add the stakeholder
                if (isset($contents['stakeholder'])) {
                    $this->addNewStakeholder($ideaId, $contents['stakeholder'], $creatorId, $now);
                }

                //add the existing solution
                if (isset($contents['existing-solution'])) {
                    $this->addNewExistingSolution($ideaId, $contents['existing-solution'], $creatorId, $now);
                }

                //add the market analysis
                if (isset($contents['market-analysis'])) {
                    $this->addNewMarketAnalysis($ideaId, $contents['market-analysis'], $creatorId, $now);
                }

                //add the other information
                if (isset($contents['other-information'])) {
                    $this->addNewOtherInformation($ideaId, $contents['other-information'], $creatorId, $now);
                }

                //TODO: add the scenario(s?)
                if (isset($contents['scenarios'])) {}

                //TODO: add reference links
                if (isset($contents['references'])) {}

                $this->databaseHandler->commitTransaction();

                return $ideaId;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function updateIdea($entityId, $entityContents, $userId) {
            try {
                $this->databaseHandler->beginTransaction();

                $this->updateEntity($entityId, $entityContents, $userId);

                if (isset($entityContents['edited'])) {
                    $editedContents = $entityContents['edited'];
                    if (!empty($editedContents)) {
                        if (isset($editedContents['oneLiner'])) {
                            $query = "UPDATE Idea SET idea = :idea WHERE (entity_id = :entityId)";
                            $queryVariables = array(
                                ':entityId' => $entityId,
                                ':idea' => $editedContents['oneLiner']['value']
                            );
                            $this->databaseHandler->query($query, $queryVariables);
                        }

                        if (isset($editedContents['title'])) {
                            $query = "UPDATE Idea SET title = :title WHERE (entity_id = :entityId)";
                            $queryVariables = array(
                                ':entityId' => $entityId,
                                ':title' => $editedContents['title']['value']
                            );
                            $this->databaseHandler->query($query, $queryVariables);
                        }
                    }
                }

                if (isset($entityContents['removed'])) {
                    $removedContents = $entityContents['removed'];
                    if (!empty($removedContents)) {}
                }

                if (isset($entityContents['added'])) {
                    $addedContents = $entityContents['added'];
                    if (!empty($addedContents)) {}
                }

                return $this->databaseHandler->commitTransaction();
            } catch (Exception $error) {
                throw $error;
            }
        }
    }
?>