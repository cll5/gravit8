<?php
    if (!defined('__GRAVIT8__')) {
        $gravit8Directory = dirname(dirname(dirname(dirname(__File__))));
        define('__GRAVIT8__', $gravit8Directory);
    }

    require_once __GRAVIT8__ . '/app/commons/utilities.php';
    require_once __GRAVIT8__ . '/app/commons/interfaces/Singleton.php';
    require_once __GRAVIT8__ . '/app/model/EntityHandler.php';

    Class PitchHandler extends EntityHandler implements Singleton {
        private static $instance;

        public function __construct() {
            parent::__construct();
        }

        public static function getInstance() {
            if (self::$instance === NULL) {
                self::$instance = new PitchHandler();
            }

            return self::$instance;
        }

        public function createPitchModel($entityId, $userId = NULL) {
            try {
                $basicPitchModel = $this->createBasicPitchModel($entityId, $userId);
                $advancedPitchModel = $this->createAdvancedPitchModel($entityId);
                $pitchModel = array_merge($basicPitchModel, $advancedPitchModel);
                return $pitchModel;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function createBatchPitchModels($entityIds, $userId = NULL) {
            try {
                $pitchModels = array();
                foreach ($entityIds as $index => $entityId) {
                    $pitchModels[$index] = $this->createPitchModel($entityId, $userId);
                }
                return $pitchModels;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function createBasicPitchModel($entityId, $userId = NULL) {
            try {
                $pitchModel = $this->defaultBasicPitchModel();
                $pitchModel['id'] = $entityId;

                //get pitch, pitch's title, and it's entity fields
                $query = "SELECT Entity.id, Entity.creator_id, Entity.is_private, Entity.created_on, EntityType.entity_type, Pitch.title, Pitch.pitch FROM Pitch INNER JOIN Entity ON (Pitch.entity_id = Entity.id) INNER JOIN EntityType ON (Entity.entity_type_id = EntityType.id) WHERE Entity.id = :entityId";
                $queryVariables = array(
                    ':entityId' => $entityId
                );
                $result = $this->databaseHandler->query($query, $queryVariables);
                $pitch = reset($result['data']);

                $pitchModel['entityType'] = $pitch['entityType'];
                $pitchModel['creator'] = $this->userHandler->createBasicUserProfileModel($pitch['creatorId']);
                $pitchModel['isPrivate'] = $pitch['isPrivate'];
                $pitchModel['createdOn'] = $pitch['createdOn'];
                $pitchModel['title'] = $pitch['title'];
                $pitchModel['pitch'] = $pitch['pitch'];

                //get the categories
                $pitchModel['categories'] = $this->getEntityCategories($entityId);

                //get the number of likes
                $pitchModel['numberOfLikes'] = $this->numberOfLikeForEntity($entityId);
                $pitchModel['liked'] = is_null($userId) ? FALSE : $this->isEntityLikedByUser($entityId, $userId);

                //get the number of votes
                $pitchModel['numberOfVotes'] = $this->numberOfVoteForEntity($entityId);
                $pitchModel['voted'] = is_null($userId) ? FALSE : $this->isEntityVotedByUser($entityId, $userId);

                //get the number of comments
                $pitchModel['numberOfComments'] = $this->numberOfCommentInEntity($entityId);

                return $pitchModel;
            } catch (Exception $error) {
                throw $error;
            }
        }

        //TODO: optimize the loop by doing a batch query instead of querying one entity at a time
        public function createBatchBasicPitchModels($entityIds, $userId = NULL) {
            try {
                $pitchModels = array();
                foreach ($entityIds as $entityId) {
                    $pitchModels[] = $this->createBasicPitchModel($entityId, $userId);
                }
                return $pitchModels;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function createAdvancedPitchModel($entityId) {
            try {
                $pitchModel = $this->defaultAdvancedPitchModel();

                //(assume the following only has max of one per pitch)
                //get description
                $description = $this->getDescription($entityId);
                if (!is_null($description)) {
                    $pitchModel['description'] = $description;
                }

                //get background
                $background = $this->getBackground($entityId);
                if (!is_null($background)) {
                    $pitchModel['background'] = $background;
                }

                //get stakeholder
                $stakeholder = $this->getStakeholder($entityId);
                if (!is_null($stakeholder)) {
                    $pitchModel['stakeholder'] = $stakeholder;
                }

                //get existing solution
                $existingSolution = $this->getExistingSolution($entityId);
                if (!is_null($existingSolution)) {
                    $pitchModel['existingSolution'] = $existingSolution;
                }

                //get market analysis
                $marketAnalysis = $this->getMarketAnalysis($entityId);
                if (!is_null($marketAnalysis)) {
                    $pitchModel['marketAnalysis'] = $marketAnalysis;
                }

                //get other information
                $otherInformation = $this->getOtherInformation($entityId);
                if (!is_null($otherInformation)) {
                    $pitchModel['otherInformation'] = $otherInformation;
                }

                //get journeys

                //get the roles
                $query = "SELECT EntityContent.id, Role.role FROM EntityContent INNER JOIN ContentType ON (EntityContent.content_type_id = ContentType.id) RIGHT JOIN Role ON (EntityContent.id = Role.entity_content_id) WHERE (EntityContent.entity_id = :entityId) AND (ContentType.content_type = :contentType)";
                $queryVariables = array(
                    ':entityId' => $entityId,
                    ':contentType' => 'role'
                );
                $result = $this->databaseHandler->query($query, $queryVariables);
                if (!$result['isEmpty']) {
                    $pitchModel['roles'] = array();
                    foreach ($result['data'] as $index => $entityContent) {
                        $pitchModel['roles'][$index] = $this->createEntityContentModel($entityContent['id']);
                        $pitchModel['roles'][$index]['value'] = $entityContent['role'];
                    }
                }

                return $pitchModel;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public static function defaultBasicPitchModel() {
            return array(
                'id' => NULL,
                'entityType' => NULL,
                'creator' => UserHandler::defaultBasicUserProfile(),
                'isPrivate' => NULL,
                'createdOn' => NULL,
                'liked' => FALSE,
                'voted' => FALSE,
                'title' => NULL,
                'pitch' => NULL,
                'categories' => array(),
                'numberOfLikes' => 0,
                'numberOfVotes' => 0,
                'numberOfComments' => 0
            );
        }

        public static function defaultAdvancedPitchModel() {
            return array(
                'description' => NULL,
                'background' => NULL,
                'stakeholder' => NULL,
                'existingSolution' => NULL,
                'marketAnalysis' => NULL,
                'otherInformation' => NULL,
                'journeys' => array(),
                'roles' => array()
            );
        }

        public function getBatchPitches($pitchId, $organizationModel, $lookupOrder = 'newest', $userId = NULL) {
            try {
                switch ($lookupOrder) {
                    case 'newest':
                        $query = "SELECT Entity.id, Entity.creator_id, Entity.is_private, Entity.created_on, Pitch.title, Pitch.pitch FROM Pitch INNER JOIN Entity ON (Pitch.entity_id = Entity.id) INNER JOIN EntityOrganization ON (Pitch.entity_id = EntityOrganization.entity_id) WHERE ((EntityOrganization.organization_id, EntityOrganization.organization_group_id) = (:organizationId, :organizationGroupId)) AND (Entity.id > :pitchId) ORDER BY id DESC LIMIT :batchSize";
                        break;

                    case 'oldest':
                        $query = "SELECT Entity.id, Entity.creator_id, Entity.is_private, Entity.created_on, Pitch.title, Pitch.pitch FROM Pitch INNER JOIN Entity ON (Pitch.entity_id = Entity.id) INNER JOIN EntityOrganization ON (Pitch.entity_id = EntityOrganization.entity_id) WHERE ((EntityOrganization.organization_id, EntityOrganization.organization_group_id) = (:organizationId, :organizationGroupId)) AND (Entity.id < :pitchId) ORDER BY id DESC LIMIT :batchSize";
                        break;
                }
                $queryVariables = array(
                    ':pitchId' => $pitchId,
                    ':organizationId' => $organizationModel['organizationId'],
                    ':organizationGroupId' => $organizationModel['organizationGroupId'],
                    ':batchSize' => 20
                );
                $result = $this->databaseHandler->query($query, $queryVariables);

                $pitches = array();
                if (!$result['isEmpty']) {
                    foreach ($result['data'] as $index => $pitch) {
                        $pitches[$index] = $this->defaultBasicPitchModel();
                        $entityId = $pitch['id'];

                        $pitches[$index]['id'] = $entityId;
                        $pitches[$index]['creator'] = $this->userHandler->createBasicUserProfileModel($pitch['creatorId']);
                        $pitches[$index]['isPrivate'] = $pitch['isPrivate'];
                        $pitches[$index]['createdOn'] = $pitch['createdOn'];
                        $pitches[$index]['title'] = $pitch['title'];
                        $pitches[$index]['pitch'] = $pitch['pitch'];

                        //get the categories
                        $pitches[$index]['categories'] = $this->getEntityCategories($entityId);

                        //get the number of likes
                        $pitches[$index]['numberOfLikes'] = $this->numberOfLikeForEntity($entityId);
                        $pitches[$index]['liked'] = is_null($userId) ? FALSE : $this->isEntityLikedByUser($entityId, $userId);

                        //get the number of votes
                        $pitches[$index]['numberOfVotes'] = $this->numberOfVoteForEntity($entityId);
                        $pitches[$index]['voted'] = is_null($userId) ? FALSE : $this->isEntityVotedByUser($entityId, $userId);

                        //get the number of comments
                        $pitches[$index]['numberOfComments'] = $this->numberOfCommentInEntity($entityId);
                    }
                }

                return $pitches;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function searchPitches($keywords, $organizationModel) {
            try {

            } catch (Exception $error) {
                throw $error;
            }
        }

        public function submitPitch($creatorId, $organizationModel, $contents) {
            try {
                if (count(array_keys($contents)) < 4) {
                    throw New Exception('>>> ERROR: Pitch submission has less entity contents than expected');
                }

                $now = getTimeStamp();
                $this->databaseHandler->beginTransaction();

                //adding new entry to entity
                $isPrivate = isset($contents['setPrivate']) ? $contents['setPrivate'] : FALSE;
                $pitchId = $this->addNewEntity('pitch', $organizationModel, $creatorId, $now, $isPrivate);

                //add the pitch categories
                if (isset($contents['categories'])) {
                    $this->addCategoriesByIds($pitchId, $contents['categories']);
                }

                //add new entry to pitch
                if (isset($contents['oneLiner']) && isset($contents['title'])) {
                    $query = "INSERT INTO Pitch (entity_id, title, pitch) VALUES (:entityId, :title, :pitch)";
                    $queryVariables = array(
                        ':entityId' => $pitchId,
                        ':title' => $contents['title'],
                        ':pitch' => $contents['oneLiner']
                    );
                    $this->databaseHandler->query($query, $queryVariables);
                }

                //add the roles
                if (isset($contents['roles'])) {
                    foreach ($contents['roles'] as $assignableRole) {
                        $this->addNewRole($pitchId, $assignableRole['role'], $assignableRole['assignees'], $creatorId, $now);
                    }
                }

                //add the description
                if (isset($contents['description'])) {
                    $this->addNewDescription($pitchId, $contents['description'], $creatorId, $now);
                }

                //add the background
                if (isset($contents['background'])) {
                    $this->addNewBackground($pitchId, $contents['background'], $creatorId, $now);
                }

                //add the stakeholder
                if (isset($contents['stakeholder'])) {
                    $this->addNewStakeholder($pitchId, $contents['stakeholder'], $creatorId, $now);
                }

                //add the existing solution
                if (isset($contents['existing-solution'])) {
                    $this->addNewExistingSolution($pitchId, $contents['existing-solution'], $creatorId, $now);
                }

                //add the market analysis
                if (isset($contents['market-analysis'])) {
                    $this->addNewMarketAnalysis($pitchId, $contents['market-analysis'], $creatorId, $now);
                }

                //add the other information
                if (isset($contents['other-information'])) {
                    $this->addNewOtherInformation($pitchId, $contents['other-information'], $creatorId, $now);
                }

                //FUTURE TODO: add more entity contents

                $this->databaseHandler->commitTransaction();
                return $pitchId;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function updatePitch($entityId, $entityContents, $userId) {
            try {
                $this->databaseHandler->beginTransaction();

                $this->updateEntity($entityId, $entityContents, $userId);

                if (isset($entityContents['edited'])) {
                    $editedContents = $entityContents['edited'];
                    if (!empty($editedContents)) {
                        if (isset($editedContents['oneLiner'])) {
                            $query = "UPDATE Pitch SET pitch = :pitch WHERE (entity_id = :entityId)";
                            $queryVariables = array(
                                ':entityId' => $entityId,
                                ':pitch' => $editedContents['oneLiner']['value']
                            );
                            $this->databaseHandler->query($query, $queryVariables);
                        }

                        if (isset($editedContents['title'])) {
                            $query = "UPDATE Pitch SET title = :title WHERE (entity_id = :entityId)";
                            $queryVariables = array(
                                ':entityId' => $entityId,
                                ':title' => $editedContents['title']['value']
                            );
                            $this->databaseHandler->query($query, $queryVariables);
                        }
                    }
                }

                if (isset($entityContents['removed'])) {
                    $removedContents = $entityContents['removed'];
                    if (!empty($removedContents)) {}
                }

                if (isset($entityContents['added'])) {
                    $addedContents = $entityContents['added'];
                    if (!empty($addedContents)) {}
                }

                return $this->databaseHandler->commitTransaction();
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function exportPitchesInOrganization($organizationModel, $filename = NULL) {
            try {
                $folderPath = __GRAVIT8__ . '/database/data/backup/' . $organizationModel['organizationDomain'];
                if (!file_exists($folderPath)) {
                    $folder = str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, $folderPath);
                    $isFolderCreated = mkdir($folder, 0750, TRUE);
                    if ($isFolderCreated == FALSE) {
                        throw New Exception('>>> ERROR: Could not create data export folder');
                    }
                }

                $query = "SELECT Entity.id FROM EntityOrganization INNER JOIN Entity ON (EntityOrganization.entity_id = Entity.id) LEFT JOIN EntityType ON (Entity.entity_type_id = EntityType.id) LEFT JOIN Pitch ON (Entity.id = Pitch.entity_id) WHERE ((EntityOrganization.organization_id, EntityOrganization.organization_group_id) = (:organizationId, :organizationGroupId)) AND (EntityType.entity_type = :entityType) ORDER BY Pitch.title ASC";
                $queryVariables = array(
                    ':organizationId' => $organizationModel['organizationId'],
                    ':organizationGroupId' => $organizationModel['organizationGroupId'],
                    ':entityType' => 'pitch'
                );
                $result = $this->databaseHandler->queryByColumn($query, $queryVariables);

                $csvData = array(
                    array('id', 'Creator', 'Creator Title', 'Category', 'Number of Likes', 'Number of Comments', 'Title', 'Short Problem Description', 'Detailed Problem Description', 'Relevant Clinical Background', 'Stakeholders', 'Current Options', 'Market Analysis', 'Additional Information')
                );
                foreach ($result['data'] as $pitchId) {
                    $pitch = $this->createPitchModel($pitchId);
                    $creator = $pitch['creator']['firstName'] . ' ' . $pitch['creator']['lastName'];
                    $csvData[] = array($pitchId, $creator, $pitch['creator']['title'], $pitch['categories'][0]['name'], $pitch['numberOfLikes'], $pitch['numberOfComments'], $pitch['title'], $pitch['pitch'], $pitch['description']['value'], $pitch['background']['value'], $pitch['stakeholder']['value'], $pitch['existingSolution']['value'], $pitch['marketAnalysis']['value'], $pitch['otherInformation']['value']);

                    //export the comments in this pitch
                    if ($pitch['numberOfComments'] > 0) {
                        $this->exportCommentsInEntity($pitchId, $folderPath);
                    }
                }

                if (is_null($filename)) {
                    $filename = $organizationModel['organizationDomain'] . 'Pitches.csv';
                } else if (preg_match('/.*[^(\.csv)]$/', $filename)) {
                    $filename = $filename . '.csv';
                }

                $filepath = $folderPath . '/' . $filename;
                $csv = fopen($filepath, 'w');
                foreach ($csvData as $fields) {
                    fputcsv($csv, $fields, ',', '"', '\\');
                }

                $fcloseStatus = fclose($csv);
                if (!$fcloseStatus) {
                    throw new Exception('>>> Cannot close pitch export data csv.');
                }

                return $fcloseStatus;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function exportCommentsInEntity($entityId, $folderPath) {
            try {
                require_once __GRAVIT8__ . '/app/model/CommentHandler.php';
                $commentHandler = CommentHandler::getInstance();
                $numberOfComments = $this->numberOfCommentInEntity($entityId);

                $comments = $commentHandler->loadCommentsByEntity($entityId, 'newest');
                while (count($comments) < $numberOfComments) {
                    $oldestComment = end($comments);
                    $newComments = $commentHandler->loadCommentsByEntity($entityId, 'oldest', $oldestComment['id']);
                    $comments = array_merge($comments, $newComments);
                }

                $csvData = array(
                    array('id', 'Commenter', 'Commenter Title', 'Posted On (UTC)', 'Message')
                );

                foreach ($comments as $comment) {
                    $commenter = $comment['commenter']['firstName'] . ' ' . $comment['commenter']['lastName'];
                    $csvData[] = array($comment['id'], $commenter, $comment['commenter']['title'], $comment['postedOn'], $comment['message']);
                }

                $filepath = $folderPath . '/' . 'entity' . $entityId . 'Comments.csv';
                $csv = fopen($filepath, 'w');
                foreach ($csvData as $fields) {
                    fputcsv($csv, $fields, ',', '"', '\\');
                }

                $fcloseStatus = fclose($csv);
                if (!$fcloseStatus) {
                    throw new Exception('>>> Cannot close comment export data csv.');
                }

                return $fcloseStatus;

                // $query = "SELECT * FROM EntityComment WHERE entity_id = :entityId";
                // $queryVariables = array(
                //     ':entityId' => $entityId
                // );
                // $result = $this->databaseHandler->query($query, $queryVariables);
            } catch (Exception $error) {
                throw $error;
            }
        }
    }
?>