<?php
    if (!defined('__GRAVIT8__')) {
        $gravit8Directory = dirname(dirname(dirname(dirname(__File__))));
        define('__GRAVIT8__', $gravit8Directory);
    }

    require_once __GRAVIT8__ . '/app/commons/utilities.php';
    require_once __GRAVIT8__ . '/app/commons/interfaces/Singleton.php';
    require_once __GRAVIT8__ . '/app/model/EntityHandler.php';

    Class ProjectHandler extends EntityHandler implements Singleton {
        private static $instance;

        public function __construct() {
            parent::__construct();
        }

        public static function getInstance() {
            if (self::$instance === NULL) {
                self::$instance = new ProjectHandler();
            }

            return self::$instance;
        }

        public function createProjectModel($entityId, $userId = NULL) {
            try {
                $basicProjectModel = $this->createBasicProjectModel($entityId, $userId);
                $advancedProjectModel = $this->createAdvancedProjectModel($entityId);
                $projectModel = array_merge($basicProjectModel, $advancedProjectModel);
                return $projectModel;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function createBasicProjectModel($entityId, $userId = NULL) {
            try {
                $projectModel = $this->defaultBasicProjectModel();
                $projectModel['id'] = $entityId;

                // $query = "SELECT Entity.id, Entity.creator_id, Entity.is_private, Entity.created_on, EntityType.entity_type, Project.title, Project.summary FROM Project INNER JOIN Entity ON (Project.entity_id = Entity.id) INNER JOIN EntityType ON (Entity.entity_type_id = EntityType.id) WHERE Entity.id = :entityId";
                // $queryVariables = array(
                //     ':entityId' => $entityId
                // );
                // $result = $this->databaseHandler->query($query, $queryVariables);

                //get common entity properties
                $query = "SELECT Entity.*, EntityType.entity_type FROM Entity INNER JOIN EntityType ON (Entity.entity_type_id = EntityType.id) WHERE (Entity.id = :entityId)";
                $queryVariables = array(
                    ':entityId' => $entityId
                );
                $result = $this->databaseHandler->query($query, $queryVariables);
                if (!$result['isEmpty']) {
                    $entity = reset($result['data']);
                    $projectModel['entityType'] = $entity['entityType'];
                    $projectModel['creator'] = $this->userHandler->createBasicUserProfileModel($entity['creatorId']);
                    $projectModel['isPrivate'] = $entity['isPrivate'];
                    $projectModel['createdOn'] = $entity['createdOn'];
                }

                //get project specific properties
                $query = "SELECT * FROM Project WHERE entity_id = :entityId";
                $result = $this->databaseHandler->query($query, $queryVariables);
                if (!$result['isEmpty']) {
                    $project = reset($result['data']);
                    $projectModel['title'] = $project['title'];
                    $projectModel['summary'] = $project['summary'];
                }

                //get the categories
                $categories = $this->getEntityCategories($entityId);
                if (!empty($categories)) {
                    $projectModel['categories'] = $categories;
                }

                //get the number of likes
                $projectModel['numberOfLikes'] = $this->numberOfLikeForEntity($entityId);
                $projectModel['liked'] = is_null($userId) ? FALSE : $this->isEntityLikedByUser($entityId, $userId);

                //get the number of votes
                $projectModel['numberOfVotes'] = $this->numberOfVoteForEntity($entityId);
                $projectModel['voted'] = is_null($userId) ? FALSE : $this->isEntityVotedByUser($entityId, $userId);

                //get the number of comments
                $projectModel['numberOfComments'] = $this->numberOfCommentInEntity($entityId);

                return $projectModel;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function createAdvancedProjectModel($entityId) {
            try {
                $projectModel = $this->defaultAdvancedProjectModel();

                //TODO: abstract these optional contents to EntityHandler
                //(assume the following only has max of one per entity)
                //get description
                $description = $this->getDescription($entityId);
                if (!is_null($description)) {
                    $projectModel['description'] = $description;
                }

                //get background
                $background = $this->getBackground($entityId);
                if (!is_null($background)) {
                    $projectModel['background'] = $background;
                }

                //get stakeholder
                $stakeholder = $this->getStakeholder($entityId);
                if (!is_null($stakeholder)) {
                    $projectModel['stakeholder'] = $stakeholder;
                }

                //get existing solution
                $existingSolution = $this->getExistingSolution($entityId);
                if (!is_null($existingSolution)) {
                    $projectModel['existingSolution'] = $existingSolution;
                }

                //get market analysis
                $marketAnalysis = $this->getMarketAnalysis($entityId);
                if (!is_null($marketAnalysis)) {
                    $projectModel['marketAnalysis'] = $marketAnalysis;
                }

                //get other information
                $otherInformation = $this->getOtherInformation($entityId);
                if (!is_null($otherInformation)) {
                    $projectModel['otherInformation'] = $otherInformation;
                }

                //get team members
                $projectModel['teamMembers'] = $this->getTeamMembers($entityId);

                //get roles
                $projectModel['roles'] = $this->getRoles($entityId);

                //get attachments
                $projectModel['attachments'] = $this->getAttachments($entityId);

                //journeys

                return $projectModel;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public static function defaultBasicProjectModel() {
            return array(
                'id' => NULL,
                'entityType' => NULL,
                'creator' => UserHandler::defaultBasicUserProfile(),
                'isPrivate' => NULL,
                'createdOn' => NULL,
                'liked' => FALSE,
                'voted' => FALSE,
                'title' => NULL,
                'summary' => NULL,
                'categories' => array(),
                'numberOfLikes' => 0,
                'numberOfVotes' => 0,
                'numberOfComments' => 0
            );
        }

        public static function defaultAdvancedProjectModel() {
            return array(
                'description' => NULL,
                'background' => NULL,
                'stakeholder' => NULL,
                'existingSolution' => NULL,
                'marketAnalysis' => NULL,
                'otherInformation' => NULL,
                'teamMembers' => array(),
                'roles' => array(),
                'attachments' => EntityHandler::defaultAttachmentsModel(),
                'journeys' => array()
            );
        }

        public function createBatchBasicProjectModels($entityIds, $userId = NULL) {
            try {
                $projectModels = array();
                foreach ($entityIds as $entityId) {
                    $projectModels[] = $this->createBasicProjectModel($entityId, $userId);
                }
                return $projectModels;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function getBatchProjects($entityId, $organizationModel, $lookupOrder = 'newest', $userId = NULL) {
            try {
                switch ($lookupOrder) {
                    case 'newest':
                        $query = "SELECT Entity.id From Entity INNER JOIN EntityOrganization ON (Entity.id = EntityOrganization.entity_id) WHERE ((EntityOrganization.organization_id, EntityOrganization.organization_group_id) = (:organizationId, :organizationGroupId)) AND (Entity.entity_type_id = (SELECT id FROM EntityType WHERE (entity_type = :entityType))) AND (Entity.id > :entityId) ORDER BY id DESC LIMIT :batchSize";
                        break;

                    case 'oldest':
                        $query = "SELECT Entity.id From Entity INNER JOIN EntityOrganization ON (Entity.id = EntityOrganization.entity_id) WHERE ((EntityOrganization.organization_id, EntityOrganization.organization_group_id) = (:organizationId, :organizationGroupId)) AND (Entity.entity_type_id = (SELECT id FROM EntityType WHERE (entity_type = :entityType))) AND (Entity.id < :entityId) ORDER BY id DESC LIMIT :batchSize";
                        break;
                }

                $queryVariables = array(
                    ':entityId' => $entityId,
                    ':entityType' => 'project',
                    ':organizationId' => $organizationModel['organizationId'],
                    ':organizationGroupId' => $organizationModel['organizationGroupId'],
                    ':batchSize' => 20
                );
                $result = $this->databaseHandler->queryByColumn($query, $queryVariables);

                $projectModels = array();
                if (!$result['isEmpty']) {
                    $entityIds = $result['data'];
                    foreach ($entityIds as $entityId) {
                        $projectModels[] = $this->createBasicProjectModel($entityId, $userId);
                    }
                }
                return $projectModels;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function searchProjects() {
            try {
                
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function submitProject($creatorId, $organizationModel, $contents) {
            try {
                if (count(array_keys($contents)) < 2) {
                    throw New Exception('>>> ERROR: Project submission has less entity contents than expected');
                }

                $now = getTimeStamp();
                $this->databaseHandler->beginTransaction();

                //adding new entry to entity
                $isPrivate = isset($contents['setPrivate']) ? $contents['setPrivate'] : FALSE;
                $projectId = $this->addNewEntity('project', $organizationModel, $creatorId, $now, $isPrivate);

                //add the project categories
                if (isset($contents['categories'])) {
                    $this->addCategoriesByIds($projectId, $contents['categories']);
                }

                //add new entry to project
                if (isset($contents['oneLiner']) && isset($contents['title'])) {
                    $query = "INSERT INTO Project (entity_id, title, summary) VALUES (:entityId, :title, :summary)";
                    $queryVariables = array(
                        ':entityId' => $projectId,
                        ':title' => $contents['title'],
                        ':summary' => $contents['oneLiner']
                    );
                    $this->databaseHandler->query($query, $queryVariables);
                }

                //TODO: add team members to project
                if (isset($contents['teamMembers'])) {
                    foreach ($contents['teamMembers'] as $teamMemberId) {
                        $query = "INSERT INTO EntityTeamMember (entity_id, user_id) VALUES (:entityId, :userId)";
                        $queryVariables = array(
                            ':entityId' => $projectId,
                            ':userId' => $teamMemberId
                        );
                        $this->databaseHandler->query($query, $queryVariables);
                    }
                }

                if (isset($contents['roles'])) {
                    foreach ($contents['roles'] as $assignableRole) {
                        $this->addNewRole($projectId, $assignableRole['role'], $assignableRole['assignees'], $creatorId, $now);
                    }
                }

                if (isset($contents['attachments'])) {
                    foreach ($contents['attachments'] as $attachment) {
                        $this->addNewAttachment($attachment['id'], $projectId);

                        //TODO: update the attachment name if it is different than the existing name
                        //NOTE: actually, this should only need to be done if it's for editted contents because added contents will only use the changed name
                        if (!is_null($attachment['value'])) {
                            //1. rename the attachment/partial attachments in the file system
                            //2. update the attachment record in the database
                            debug($attachment['value']);
                        }
                    }
                }


                //TODO: abstract these optional contents to EntityHandler, something like addOptionalContents($entityId, $contents, $creatorId, $createdOn);
                //add the description
                if (isset($contents['description'])) {
                    $this->addNewDescription($projectId, $contents['description'], $creatorId, $now);
                }

                //add the background
                if (isset($contents['background'])) {
                    $this->addNewBackground($projectId, $contents['background'], $creatorId, $now);
                }

                //add the stakeholder
                if (isset($contents['stakeholder'])) {
                    $this->addNewStakeholder($projectId, $contents['stakeholder'], $creatorId, $now);
                }

                //add the existing solution
                if (isset($contents['existing-solution'])) {
                    $this->addNewExistingSolution($projectId, $contents['existing-solution'], $creatorId, $now);
                }

                //add the market analysis
                if (isset($contents['market-analysis'])) {
                    $this->addNewMarketAnalysis($projectId, $contents['market-analysis'], $creatorId, $now);
                }

                //add the other information
                if (isset($contents['other-information'])) {
                    $this->addNewOtherInformation($projectId, $contents['other-information'], $creatorId, $now);
                }

                //FUTURE TODO: add more entity contents

                $this->databaseHandler->commitTransaction();
                return $projectId;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function updateProject($entityId, $entityContents, $userId) {
            try {
                $this->databaseHandler->beginTransaction();

                $this->updateEntity($entityId, $entityContents, $userId);

                if (isset($entityContents['edited'])) {
                    $editedContents = $entityContents['edited'];
                    if (!empty($editedContents)) {
                        if (isset($editedContents['oneLiner'])) {
                            $query = "UPDATE Project SET summary = :summary WHERE (entity_id = :entityId)";
                            $queryVariables = array(
                                ':entityId' => $entityId,
                                ':summary' => $editedContents['oneLiner']['value']
                            );
                            $this->databaseHandler->query($query, $queryVariables);
                        }

                        if (isset($editedContents['title'])) {
                            $query = "UPDATE Project SET title = :title WHERE (entity_id = :entityId)";
                            $queryVariables = array(
                                ':entityId' => $entityId,
                                ':title' => $editedContents['title']['value']
                            );
                            $this->databaseHandler->query($query, $queryVariables);
                        }
                    }
                }

                if (isset($entityContents['removed'])) {
                    $removedContents = $entityContents['removed'];
                    if (!empty($removedContents)) {}
                }

                if (isset($entityContents['added'])) {
                    $addedContents = $entityContents['added'];
                    if (!empty($addedContents)) {}
                }

                return $this->databaseHandler->commitTransaction();
            } catch (Exception $error) {
                throw $error;
            }
        }
    }
?>