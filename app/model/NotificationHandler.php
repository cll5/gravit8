<?php
    if (!defined('__GRAVIT8__')) {
        $gravit8Directory = dirname(__File__, 3);
        define('__GRAVIT8__', $gravit8Directory);
    }
    require_once __GRAVIT8__ . '/app/model/DatabaseHandler.php';

    Class NotificationHandler {
        const REQUESTING_ROLES = 'requesting_roles';
        const ACCEPTED_REQUEST = 'accepted_request';
        const DECLINED_REQUEST = 'declined_request';
        const CONFIRMING_ROLES = 'confirming_roles';
        const CONFIRMED_ROLES = 'confirmed_roles';
        const WITHDRAWN_ROLES = 'withdrawn_roles';
        const ALL_ROLES_FILLED = 'all_roles_filled';

        private $databaseHandler;

        public function __construct() {
            $this->databaseHandler = DatabaseHandler::getInstance();
        }

        public function getNotifications($userId, $queryBatchSize = 1, $queryOffset = 0) {
            try {
                $projectIds = $this->getProjectCollectionIds($userId, $queryBatchSize, $queryOffset);
                $numberOfProjects = count($projectIds);

                if ($numberOfProjects > 0) {
                    $this->databaseHandler->beginTransaction();
                    $projectIdMarkers = $this->createMultipleMarkers('?, ', $numberOfProjects);

                    $query = "SELECT * FROM ( "
                        . "(SELECT "
                            . "Notifications.id AS notification_id, "
                            . "NULL AS project_update_id, "
                            . "Notifications.project_id AS project_id, "
                            . "NULL AS contributor_id, "
                            . "Notifications.sender_id AS sender_id, "
                            . "Notifications.receiver_id AS receiver_id, "
                            . "Notifications.event_type AS event_type, "
                            . "Notifications.message AS message, "
                            . "Notifications.updated_on AS updated_on "
                        . "FROM Notifications "
                        . "WHERE Notifications.receiver_id = ?) "
                        . "UNION DISTINCT "
                        . "(SELECT "
                            . "NULL AS notification_id, "
                            . "ProjectUpdates.id AS project_update_id, "
                            . "ProjectUpdates.project_id AS project_id, "
                            . "ProjectUpdates.contributor_id AS contributor_id, "
                            . "NULL AS sender_id, "
                            . "NULL AS receiver_id, "
                            . "ProjectUpdates.event_type AS event_type, "
                            . "ProjectUpdates.message AS message, "
                            . "ProjectUpdates.updated_on AS updated_on "
                        . "FROM ProjectUpdates "
                        . "WHERE ProjectUpdates.project_id IN (" . $projectIdMarkers . ")) "
                        . "UNION DISTINCT "
                        . "(SELECT "
                            . "NULL AS notification_id, "
                            . "NULL AS project_update_id, "
                            . "Projects.id AS project_id, "
                            . "NULL AS contributor_id, "
                            . "NULL AS sender_id, "
                            . "NULL AS receiver_id, "
                            . "NULL AS event_type, "
                            . "NULL AS message, "
                            . "NULL AS updated_on "
                        . "FROM Projects "
                        . "WHERE Projects.id IN (" . $projectIdMarkers . "))) "
                        . "AS UserNotifications "
                        . "WHERE updated_on IS NOT NULL "
                        . "ORDER BY updated_on DESC";

                    $queryVariables = array_merge(array($userId), $projectIds, $projectIds);
                    $results = $this->databaseHandler->query($query, $queryVariables);

                    $notificationModel = $results['data'];
                    $this->databaseHandler->commitTransaction();

                    foreach ($notificationModel as $index => $notification) {
                        $project = $this->createSimpleProjectModel($notification['projectId']);
                        $contributor = $this->createUserModel($notification['contributorId']);
                        $sender = $this->createUserModel($notification['senderId']);
                        $receiver = $this->createUserModel($notification['receiverId']);

                        $isProjectUpdate = !empty($notification['projectUpdateId']);
                        $isNotification = !empty($notification['notificationId']);

                        $notificationModel[$index] = array(
                            'isNotification' => $isNotification,
                            'isProjectUpdate' => $isProjectUpdate,
                            'id' => $isProjectUpdate ? $notification['projectUpdateId'] : $notification['notificationId'],
                            'eventType' => $notification['eventType'],
                            'message' => $notification['message'],
                            'updatedOn' => $notification['updatedOn'],
                            'project' => empty($project) ? $project : $project[0],
                            'contributor' => empty($contributor) ? $contributor : $contributor[0],
                            'sender' => empty($sender) ? $sender : $sender[0],
                            'receiver' => empty($receiver) ? $receiver : $receiver[0]
                        );
                    }

                    return $notificationModel;
                }

                return array();
            } catch (Exception $error) {
                throw $error;
            }
        }
    }
?>