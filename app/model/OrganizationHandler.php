<?php
    if (!defined('__GRAVIT8__')) {
        $gravit8Directory = dirname(dirname(dirname(__File__)));
        define('__GRAVIT8__', $gravit8Directory);
    }

    require_once __GRAVIT8__ . '/app/commons/interfaces/Singleton.php';
    require_once __GRAVIT8__ . '/app/model/DatabaseHandler.php';

    Class OrganizationHandler implements Singleton {
        private static $instance;
        private $databaseHandler;

        public function __construct() {
            if (self::$instance !== NULL) {
                return self::$instance;
            }

            $this->databaseHandler = DatabaseHandler::getInstance();
        }

        public static function getInstance() {
            if (self::$instance === NULL) {
                self::$instance = new OrganizationHandler();
            }

            return self::$instance;
        }

        public function createOrganizationModel($organization, $organizationGroup = NULL) {
            try {
                //default model
                $organizationModel = $this->defaultOrganizationModel();

                //fetch the organization and organization group information
                if (is_null($organizationGroup)) {
                    //use the default organization group of the organization if it is not provided
                    $query = "SELECT "
                                . "Organization.id AS organization_id, "
                                . "Organization.organization_domain, "
                                . "Organization.organization, "
                                . "OrganizationGroup.id AS organization_group_id, "
                                . "OrganizationGroup.organization_group "
                           . "FROM "
                                . "OrganizationDefaultGroup "
                                . "INNER JOIN Organization "
                                . "ON (Organization.id = OrganizationDefaultGroup.organization_id) "
                                . "INNER JOIN OrganizationGroup "
                                . "ON (OrganizationGroup.id = OrganizationDefaultGroup.organization_group_id) "
                           . "WHERE "
                                . "(Organization.organization_domain = :organizationDomain)";

                    $queryVariables = array(
                        ':organizationDomain' => $organization
                    );
                } else {
                    //otherwise, fetch the specific organization group of the organization
                    $query = "SELECT "
                                . "Organization.id AS organization_id, "
                                . "Organization.organization_domain, "
                                . "Organization.organization, "
                                . "OrganizationGroup.id AS organization_group_id, "
                                . "OrganizationGroup.organization_group "
                           . "FROM "
                                . "Organization INNER JOIN OrganizationGroup "
                                . "ON (Organization.id = OrganizationGroup.organization_id) "
                           . "WHERE "
                                . "(Organization.organization_domain = :organizationDomain) AND "
                                . "(OrganizationGroup.organization_group = :organizationGroup)";

                    $queryVariables = array(
                        ':organizationDomain' => $organization,
                        ':organizationGroup' => $organizationGroup
                    );
                }

                $result = $this->databaseHandler->query($query, $queryVariables);
                if ($result['isSuccess']) {
                    $organizationModel = reset($result['data']);
                }

                // fetch the number of ideas, pitches, projects in the organization group
                $query = "SELECT entity_type, SUM(size) AS size " .
                        "FROM (" .
                            "(SELECT entity_type, 0 AS size FROM EntityType) " .

                            "UNION DISTINCT " .

                            "(SELECT entity_type, COUNT(*) AS size " .
                            "FROM Entity " .
                            "INNER JOIN EntityType ON (Entity.entity_type_id = EntityType.id) " .
                            "INNER JOIN EntityOrganization ON (Entity.id = EntityOrganization.entity_id) " .
                            "WHERE (EntityOrganization.organization_id, EntityOrganization.organization_group_id) = (:organizationId, :organizationGroupId) " .
                            "GROUP BY entity_type) " .
                        ") AS EntitySummary " .
                        "GROUP BY entity_type";

                $queryVariables = array(
                    ':organizationId' => $organizationModel['organizationId'],
                    ':organizationGroupId' => $organizationModel['organizationGroupId']
                );

                $result = $this->databaseHandler->query($query, $queryVariables);
                if ($result['isSuccess']) {
                    foreach ($result['data'] as $row) {
                        $organizationModel['summary']['count'][$row['entityType']] = $row['size'];
                    }
                }

                // fetch the number of users in the organization group
                $query = "SELECT COUNT(*) FROM UserOrganization WHERE (organization_id, organization_group_id) = (:organizationId, :organizationGroupId)";
                $queryVariables = array(
                    ':organizationId' => $organizationModel['organizationId'],
                    ':organizationGroupId' => $organizationModel['organizationGroupId']
                );
                $result = $this->databaseHandler->queryByColumn($query, $queryVariables);
                if ($result['isSuccess']) {
                    $organizationModel['summary']['count']['user'] = reset($result['data']);
                }

                return $organizationModel;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public static function defaultOrganizationModel() {
            return array(
                'organizationId' => NULL,
                'organizationGroupId' => NULL,
                'organizationDomain' => NULL,
                'organization' => NULL,
                'organizationGroup' => NULL,
                'summary' => array(
                    'count' => array(
                        'idea' => 0,
                        'pitch' => 0,
                        'project' => 0,
                        'user' => 0
                    )
                )
            );
        }

        public function doesOrganizationExists($organizationDomain) {
            try {
                //check if this organization exists
                $query = "SELECT * FROM Organization WHERE organization_domain = :organizationDomain";
                $queryVariables = array(
                    ':organizationDomain' => $organizationDomain
                );
                $result = $this->databaseHandler->query($query, $queryVariables);

                return !$result['isEmpty'];
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function createOrganization($organizationDomain, $organization) {
            try {
                //add new the organization in the database
                $query = "INSERT INTO Organization (organization_domain, organization) VALUES (:organizationDomain, :organization)";
                $queryVariables = array(
                    ':organizationDomain' => $organizationDomain,
                    ':organization' => $organization
                );
                $this->databaseHandler->query($query, $queryVariables);
                $organizationId = $this->databaseHandler->getLastInsertId();

                //create a new public group for this organization
                $query = "INSERT INTO OrganizationGroup (organization_id, organization_group) VALUES (:organizationId, :organizationGroup)";
                $queryVariables = array(
                    ':organizationId' => $organizationId,
                    ':organizationGroup' => 'public'
                );
                $this->databaseHandler->query($query, $queryVariables);
                $organizationGroupId = $this->databaseHandler->getLastInsertId();

                $organizationModel = $this->defaultOrganizationModel();
                $organizationModel['organizationId'] = $organizationId;
                $organizationModel['organizationGroupId'] = $organizationGroupId;
                $organizationModel['organizationDomain'] = $organizationDomain;
                $organizationModel['organization'] = $organization;
                $organizationModel['organizationGroup'] = 'public';

                //return the organization ids
                return $organizationModel;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function createOrganizationGroup($organizationDomain, $organizationGroup) {
            try {
                $organizationModel = $this->defaultOrganizationModel();

                //get the organization's data
                $query = "SELECT * FROM Organization WHERE organization_domain = :organizationDomain";
                $queryVariables = array(
                    ':organizationDomain' => $organizationDomain
                );
                $result = $this->databaseHandler->query($query, $queryVariables);

                if (!$result['isEmpty']) {
                    $organization = reset($result['data']);

                    //create the new organization group
                    $query = "INSERT INTO OrganizationGroup (organization_id, organization_group) VALUES (:organizationId, :organizationGroup)";
                    $queryVariables = array(
                        ':organizationId' => $organization['id'],
                        ':organizationGroup' => $organizationGroup
                    );
                    $result = $this->databaseHandler->query($query, $queryVariables);

                    if ($result['isSuccess']) {
                        $organizationGroupId = $this->databaseHandler->getLastInsertId();

                        $organizationModel['organizationId'] = $organization['id'];
                        $organizationModel['organizationGroupId'] = $organizationGroupId;
                        $organizationModel['organizationDomain'] = $organizationDomain;
                        $organizationModel['organization'] = $organization['organization'];
                        $organizationModel['organizationGroup'] = $organizationGroup;
                    }
                }

                //return the organization ids
                return $organizationModel;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function listAllOrganization() {
            try {
                $query = "SELECT * FROM Organization";
                $result = $this->databaseHandler->query($query);

                if (!$result['isSuccess']) {
                    throw new Exception('>>>> Cannot list all organizations.');
                }

                return $result['data'];
            } catch (Exception $error) {
                throw $error;
            }
        }

        //TODO: complete this method
        public function listAllOrganizationGroup($organization) {
            try {
                $query = "SELECT "
                            . "Organization.id AS organization_id, "
                            . "OrganizationGroup.id AS organization_group_id, "
                            . "OrganizationGroup.organization_group AS organization_group "
                       . "FROM "
                            . "Organization INNER JOIN OrganizationGroup "
                            . "ON (Organization.id = OrganizationGroup.organization_id) "
                       . "WHERE "
                            . "Organization.organization_domain = :organization_domain";
                $queryVariables = array(
                    ':organization_domain' => $organization
                );
                $result = $this->databaseHandler->query($query, $queryVariables);
            } catch (Exception $error) {
                throw $error;
            }
        }

        //attachment related methods
        public function addAttachmentsToOrganization($attachmentIds, $organizationId) {
            try {
                //attachment and organization identifiers must be of valid forms
                if (empty($attachmentIds) || !is_int($organizationId)) {
                    return FALSE;
                }

                //format the query parameters appropriately depending if the attachment ids are given as an integer or an array of integers
                $parameters = '';
                $queryVariables = array();
                if (is_int($attachmentIds)) {
                    //attachment id is given as an integer
                    $parameters = '(:organizationId, :attachmentId)';
                    $queryVariables[':organizationId'] = $organizationId;
                    $queryVariables[':attachmentId'] = $attachmentIds;
                } else if (is_array($attachmentIds)) {
                    //attachment ids are given as an array of integers
                    foreach ($attachmentIds as $index => $attachmentId) {
                        $organizationParameterName = ':organizationId' . $index;
                        $queryVariables[$organizationParameterName] = $organizationId;

                        $attachmentParameterName = ':attachmentId' . $index;
                        $queryVariables[$attachmentParameterName] = $attachmentId;

                        $parameters .= '(' . $organizationParameterName . ',' . $attachmentParameterName . '),';
                    }
                    $parameters = substr($parameters, 0, -1);
                }

                //perform the query - associate the attachments to the organization
                $query = "INSERT IGNORE INTO OrganizationAttachment (organization_id, attachment_id) VALUES " . $parameters;
                $result = $this->databaseHandler->query($query, $queryVariables);
                return $result['isSuccess'];
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function removeAttachmentsFromOrganization($attachmentIds, $organizationId) {
            try {
                //attachment and organization identifiers must be of valid forms
                if (empty($attachmentIds) || !is_int($organizationId)) {
                    return FALSE;
                }

                //format the query parameters appropriately depending if the attachment ids are given as an integer or an array of integers
                $parameters = '';
                $queryVariables = array();
                if (is_int($attachmentIds)) {
                    //attachment id is given as an integer
                    $parameters = '(:organizationId, :attachmentId)';
                    $queryVariables[':organizationId'] = $organizationId;
                    $queryVariables[':attachmentId'] = $attachmentIds;
                } else if (is_array($attachmentIds)) {
                    //attachment ids are given as an array of integers
                    foreach ($attachmentIds as $index => $attachmentId) {
                        $organizationParameterName = ':organizationId' . $index;
                        $queryVariables[$organizationParameterName] = $organizationId;

                        $attachmentParameterName = ':attachmentId' . $index;
                        $queryVariables[$attachmentParameterName] = $attachmentId;

                        $parameters .= '(' . $organizationParameterName . ',' . $attachmentParameterName . '),';
                    }
                    $parameters = substr($parameters, 0, -1);
                }

                //perform the query - remove the attachments association from the organization
                $query = "DELETE IGNORE FROM OrganizationAttachment WHERE (organization_id, attachment_id) IN (" . $parameters . ")";
                $result = $this->databaseHandler->query($query, $queryVariables);
                return $result['isSuccess'];
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function doesAttachmentExistsInOrganization($attachmentId, $organizationId) {
            try {
                $query = "SELECT * FROM OrganizationAttachment WHERE (organization_id, attachment_id) = (:organizationId, :attachmentId)";
                $queryVariables = array(
                    ':organizationId' => $organizationId,
                    ':attachmentId' => $attachmentId
                );
                $result = $this->databaseHandler->query($query, $queryVariables);
                return ($result['isSuccess'] && !$result['isEmpty']);
            } catch (Exception $error) {
                throw $error;
            }
        }
    }
?>