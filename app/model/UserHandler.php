<?php
    if (!defined('__GRAVIT8__')) {
        $gravit8Directory = dirname(dirname(dirname(__File__)));
        define('__GRAVIT8__', $gravit8Directory);
    }

    require_once __GRAVIT8__ . '/app/commons/utilities.php';
    require_once __GRAVIT8__ . '/app/commons/interfaces/Singleton.php';
    require_once __GRAVIT8__ . '/app/model/DatabaseHandler.php';

    Class UserHandler implements Singleton {
        private static $instance;
        private $databaseHandler;

        public function __construct() {
            $this->databaseHandler = DatabaseHandler::getInstance();
        }

        public static function getInstance() {
            if (self::$instance === NULL) {
                self::$instance = new UserHandler();
            }

            return self::$instance;
        }

        public function isUserInOrganization($userId, $organizationModel) {
            try {
                $query = "SELECT * FROM UserOrganization WHERE (user_id, organization_id, organization_group_id) = (:userId, :organizationId, :organizationGroupId)";
                $queryVariables = array(
                    ':userId' => $userId,
                    ':organizationId' => $organizationModel['organizationId'],
                    ':organizationGroupId' => $organizationModel['organizationGroupId']
                );
                $result = $this->databaseHandler->query($query, $queryVariables);

                return !$result['isEmpty'];
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function createBasicUserProfileModel($userId, $organizationModel = NULL) {
            try {
                //check if this user exists
                $query = "SELECT user_id AS id, first_name, last_name, title, image AS image_path FROM UserProfile WHERE user_id = :userId";
                $queryVariables = array(
                    ':userId' => $userId
                );
                $result = $this->databaseHandler->query($query, $queryVariables);

                //create default basic user profile
                $userModel = $this->defaultBasicUserProfile();

                if (!$result['isEmpty']) {
                    $userModel = reset($result['data']);
                    $userModel['initials'] = getInitials($userModel['firstName']) . getInitials($userModel['lastName']);

                    //expand the profile image path
                    if (!is_null($userModel['imagePath'])) {
                        $userModel['imagePath'] = '/data/files/profiles/' . $userId . '/' . $userModel['imagePath'];
                    }
                }

                //organization specific data
                if (!is_null($organizationModel)) {
                    $query = "SELECT UserAccountType.account_type FROM UserAccountType INNER JOIN UserOrganization ON (UserAccountType.id = UserOrganization.user_account_type_id) WHERE (UserOrganization.user_id, UserOrganization.organization_id, UserOrganization.organization_group_id) = (:userId, :organizationId, :organizationGroupId)";
                    $queryVariables = array(
                        ':userId' => $userId,
                        ':organizationId' => $organizationModel['organizationId'],
                        ':organizationGroupId' => $organizationModel['organizationGroupId']
                    );
                    $result = $this->databaseHandler->queryByColumn($query, $queryVariables);

                    $userModel['accountType'] = reset($result['data']);
                }

                return $userModel;
            } catch (Exception $error) {
                throw $error;
            }
        }

        //TODO: simplify the fetch data codes
        public function createAdvanceUserProfileModel($userId, $organizationModel = NULL) {
            try {
                //create basic user profile
                $basicUserProfile = $this->createBasicUserProfileModel($userId, $organizationModel);

                //create default advance user profile
                $advanceUserProfile = $this->defaultAdvanceUserProfile();

                //fetch the advance user profile
                $queryVariables = array(
                    ':userId' => $userId
                );

                //fetch the user biography
                $profileAttributeType = 'biography';

                $query = "SELECT id FROM ProfileAttributeType WHERE profile_attribute_type = :profileAttributeType";
                $queryVariables = array(
                    ':profileAttributeType' => $profileAttributeType
                );
                $profileAttributeTypeId = $this->databaseHandler->queryByColumn($query, $queryVariables);

                $query = "SELECT id FROM UserProfileAttribute WHERE (user_id, profile_attribute_type_id) = (:userId, :profileAttributeTypeId)";
                $queryVariables = array(
                    ':userId' => $userId,
                    ':profileAttributeTypeId' => reset($profileAttributeTypeId['data'])
                );
                $profileAttributeId = $this->databaseHandler->queryByColumn($query, $queryVariables);

                if (!$profileAttributeId['isEmpty']) {
                    $query = "SELECT * FROM Biography WHERE profile_attribute_id = :profileAttributeId";
                    $queryVariables = array(
                        ':profileAttributeId' => reset($profileAttributeId['data'])
                    );
                    $result = $this->databaseHandler->query($query, $queryVariables);
                    $biography = reset($result['data']);

                    $advanceUserProfile['biography'] = array(
                        'id' => $biography['profileAttributeId'],
                        'value' => $biography['biography']
                    );
                }


                //fetch the user contacts
                $profileAttributeType = 'contact';

                $query = "SELECT id FROM ProfileAttributeType WHERE profile_attribute_type = :profileAttributeType";
                $queryVariables = array(
                    ':profileAttributeType' => $profileAttributeType
                );
                $profileAttributeTypeId = $this->databaseHandler->queryByColumn($query, $queryVariables);

                $query = "SELECT id FROM UserProfileAttribute WHERE (user_id, profile_attribute_type_id) = (:userId, :profileAttributeTypeId)";
                $queryVariables = array(
                    ':userId' => $userId,
                    ':profileAttributeTypeId' => reset($profileAttributeTypeId['data'])
                );
                $profileAttributeIds = $this->databaseHandler->queryByColumn($query, $queryVariables);

                if (!$profileAttributeIds['isEmpty']) {
                    $markers = $this->databaseHandler->createMultipleMarkers($profileAttributeIds['numberOfRows']);
                    $query = "SELECT UserContact.profile_attribute_id AS id, UserContact.contact AS value, UserContact.contact_type_id, ContactType.contact_type AS type FROM UserContact INNER JOIN ContactType ON (UserContact.contact_type_id = ContactType.id) WHERE profile_attribute_id IN " . $markers . "ORDER BY contact_type_id";
                    $queryVariables = $profileAttributeIds['data'];
                    $result = $this->databaseHandler->query($query, $queryVariables);
                    $advanceUserProfile['contacts'] = $result['data'];
                }

                //organization specific data
                if (!is_null($organizationModel)) {
                    //populate organization specific data
                }

                //extend the basic user profile with the advance user profile
                $userProfileModel = array_merge($basicUserProfile, $advanceUserProfile);
                return $userProfileModel;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public static function defaultBasicUserProfile() {
            return array(
                'id' => NULL,
                'accountType' => NULL,
                'firstName' => NULL,
                'lastName' => NULL,
                'initials' => NULL,
                'title' => NULL,
                'imagePath' => NULL
            );
        }

        public static function defaultAdvanceUserProfile() {
            return array(
                'biography' => NULL,
                'contacts' => array()
            );
        }

        public function updateProfile($userId, $profileAttributes) {
            try {
                $this->databaseHandler->beginTransaction();

                //update edited profile attributes
                $editedAttributes = $profileAttributes['edited'];
                if (!empty($editedAttributes)) {
                    if (isset($editedAttributes['first-name'])) {
                        $query = "UPDATE UserProfile SET first_name = :firstName WHERE user_id = :userId";
                        $queryVariables = array(
                            ':firstName' => $editedAttributes['first-name'],
                            ':userId' => $userId
                        );
                        $this->databaseHandler->query($query, $queryVariables);
                    }

                    if (isset($editedAttributes['last-name'])) {
                        $query = "UPDATE UserProfile SET last_name = :lastName WHERE user_id = :userId";
                        $queryVariables = array(
                            ':lastName' => $editedAttributes['last-name'],
                            ':userId' => $userId
                        );
                        $this->databaseHandler->query($query, $queryVariables);
                    }

                    if (isset($editedAttributes['title'])) {
                        $query = "UPDATE UserProfile SET title = :title WHERE user_id = :userId";
                        $queryVariables = array(
                            ':title' => $editedAttributes['title'],
                            ':userId' => $userId
                        );
                        $this->databaseHandler->query($query, $queryVariables);
                    }

                    if (isset($editedAttributes['image'])) {
                        $imagePage = $this->saveProfileImage($userId, $editedAttributes['image']);
                        $query = "UPDATE UserProfile SET image = :imagePath WHERE user_id = :userId";
                        $queryVariables = array(
                            ':imagePath' => $imagePage,
                            ':userId' => $userId
                        );
                        $this->databaseHandler->query($query, $queryVariables);
                    }

                    if (isset($editedAttributes['biography'])) {
                        $query = "UPDATE Biography SET biography = :biography WHERE profile_attribute_id = :profileAttributeId";
                        $queryVariables = array(
                            ':biography' => $editedAttributes['biography']['value'],
                            ':profileAttributeId' => $editedAttributes['biography']['id']
                        );
                        $this->databaseHandler->query($query, $queryVariables);
                    }

                    if (isset($editedAttributes['contacts'])) {
                        foreach ($editedAttributes['contacts'] as $contact) {
                            $query = "UPDATE UserContact SET contact_type_id = :contactTypeId, contact = :contact WHERE profile_attribute_id = :profileAttributeId";
                            $queryVariables = array(
                                ':contactTypeId' => $contact['contactTypeId'],
                                ':contact' => $contact['value'],
                                ':profileAttributeId' => $contact['id']
                            );
                            $this->databaseHandler->query($query, $queryVariables);
                        }
                    }
                }

                //remove existing profile attributes
                $removedAttributes = $profileAttributes['removed'];
                if (!empty($removedAttributes)) {
                    //remove biography
                    if (isset($removedAttributes['biography'])) {
                        $this->removeProfileAttribute($userId, $removedAttributes['biography']['id']);
                    }

                    //remove contacts
                    if (isset($removedAttributes['contacts'])) {
                        foreach ($removedAttributes['contacts'] as $contact) {
                            $this->removeProfileAttribute($userId, $contact['id']);
                        }
                    }
                }

                //add new profile attributes
                $addedAttributes = $profileAttributes['added'];
                if (!empty($addedAttributes)) {
                    //add biography
                    if (isset($addedAttributes['biography'])) {
                        $profileAttributeId = $this->addNewProfileAttribute($userId, 'biography');

                        $query = "INSERT INTO Biography (profile_attribute_id, biography) VALUES (:profileAttributeId, :biography)";
                        $queryVariables = array(
                            ':profileAttributeId' => $profileAttributeId,
                            ':biography' => $addedAttributes['biography']
                        );
                        $this->databaseHandler->query($query, $queryVariables);
                    }

                    //add contacts
                    if (isset($addedAttributes['contacts'])) {
                        foreach ($addedAttributes['contacts'] as $contact) {
                            $profileAttributeId = $this->addNewProfileAttribute($userId, 'contact');

                            $query = "INSERT INTO UserContact (profile_attribute_id, contact_type_id, contact) VALUES (:profileAttributeId, :contactTypeId, :contact)";
                            $queryVariables = array(
                                ':profileAttributeId' => $profileAttributeId,
                                ':contactTypeId' => $contact['contactTypeId'],
                                ':contact' => $contact['value']
                            );
                            $this->databaseHandler->query($query, $queryVariables);
                        }
                    }
                }

                return $this->databaseHandler->commitTransaction();
            } catch (Exception $error) {
                throw $error;
            }
        }

        //add a new user profile attribute and returns the profile attribute id
        public function addNewProfileAttribute($userId, $profileAttributeType) {
            try {
                //get profile attribute type id
                $query = "SELECT id FROM ProfileAttributeType WHERE profile_attribute_type = :profileAttributeType";
                $queryVariables = array(
                    ':profileAttributeType' => $profileAttributeType
                );
                $profileAttributeTypeId = $this->databaseHandler->queryByColumn($query, $queryVariables);

                //add the new profile attribute
                $query = "INSERT INTO UserProfileAttribute (user_id, profile_attribute_type_id) VALUES (:userId, :profileAttributeTypeId)";
                $queryVariables = array(
                    ':userId' => $userId,
                    ':profileAttributeTypeId' => reset($profileAttributeTypeId['data'])
                );
                $this->databaseHandler->query($query, $queryVariables);

                //return the profile attribute id
                return $this->databaseHandler->getLastInsertId();
            } catch (Exception $error) {
                throw $error;
            }
        }

        //remove a profile attribute
        public function removeProfileAttribute($userId, $profileAttributeId) {
            try {
                $queryVariables = array(
                    ':profileAttributeId' => $profileAttributeId
                );

                $query = "SELECT ProfileAttributeType.profile_attribute_type FROM UserProfileAttribute INNER JOIN ProfileAttributeType ON (UserProfileAttribute.profile_attribute_type_id = ProfileAttributeType.id) WHERE UserProfileAttribute.id = :profileAttributeId";
                $result = $this->databaseHandler->queryByColumn($query, $queryVariables);
                $profileAttributeType = reset($result['data']);

                $query = "DELETE FROM UserProfileAttribute WHERE id = :profileAttributeId";
                $this->databaseHandler->query($query, $queryVariables);

                switch ($profileAttributeType) {
                    case 'biography':
                        $query = "DELETE FROM Biography WHERE profile_attribute_id = :profileAttributeId";
                        break;

                    case 'contact':
                        $query = "DELETE FROM UserContact WHERE profile_attribute_id = :profileAttributeId";
                        break;
                }
                $this->databaseHandler->query($query, $queryVariables);
            } catch (Exception $error) {
                throw $error;
            }
        }

        //data URL has this schema: https://en.wikipedia.org/wiki/Data_URI_scheme
        private function saveProfileImage($userId, $imageDataURL) {
            try {
                //make sure the profile image folder exists
                try {
                    //create the profile image folder path
                    $profileImageFolder = __GRAVIT8__ . '/data/files/profiles/' . $userId . '/';
                    $profileImageFolder = str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, $profileImageFolder);

                    //remove previous profile image if the profile image folder exists
                    if (file_exists($profileImageFolder)) {
                        $filesInFolder = scandir($profileImageFolder);
                        if ($filesInFolder == FALSE) {
                            throw New Exception('>>> ERROR: Could not scan the profile image folder');
                        }

                        //single level folder removal
                        foreach ($filesInFolder as $filename) {
                            if (!in_array($filename, array('.', '..'))) {
                                $pathname = $profileImageFolder . $filename;

                                $isFileRemoved = unlink($pathname);
                                if ($isFileRemoved == FALSE) {
                                    throw New Exception('>>> ERROR: Could not delete the files in the profile image folder');
                                }
                            }
                        }

                        $isFolderRemoved = rmdir($profileImageFolder);
                        if ($isFolderRemoved == FALSE) {
                            throw New Exception('>>> ERROR: Could not delete the profile image folder');
                        }
                    }

                    $isFolderCreated = mkdir($profileImageFolder, 0740, TRUE);
                    if ($isFolderCreated == FALSE) {
                        throw New Exception('>>> ERROR: Could not create profile image folder');
                    }
                } catch (Exception $error) {
                    throw $error;
                }

                //extract the profile image data from the data URL
                list($imageMetadata, $base64EncodedImage) = explode(',', $imageDataURL);
                $base64EncodedImage = str_replace(' ', '+', $base64EncodedImage);
                $decodedImage = base64_decode($base64EncodedImage);

                preg_match('/data:image\/(.*);/', $imageMetadata, $matches);
                $imageType = $matches[1];

                //save profile image to server
                $imageFilename = generateFilename($userId) . '.' . $imageType;
                $imagePath = $profileImageFolder . $imageFilename;

                try {
                    $filePointer = fopen($imagePath, 'cb');
                    $fileSize = fwrite($filePointer, $decodedImage);
                    $isFileClosed = fclose($filePointer);

                    if (($filePointer == FALSE) || ($fileSize == FALSE) || ($isFileClosed == FALSE)) {
                        throw New Exception('>>> ERROR: Failed to write profile image');
                    }
                } catch (Exception $error) {
                    throw $error;
                }

                //return the profile image file name
                return $imageFilename;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function getBatchPeople($personId, $organizationModel, $lookupOrder) {
            try {
                switch ($lookupOrder) {
                    case 'newest':
                        $query = "SELECT UserProfile.user_id AS id, UserProfile.first_name, UserProfile.last_name, UserProfile.title, UserProfile.image AS image_path FROM UserProfile LEFT JOIN UserOrganization ON (UserProfile.user_id = UserOrganization.user_id) WHERE ((UserOrganization.organization_id, UserOrganization.organization_group_id) = (:organizationId, :organizationGroupId)) AND (UserOrganization.user_id > :personId) ORDER BY id DESC LIMIT :batchSize";
                        break;

                    case 'oldest':
                        $query = "SELECT UserProfile.user_id AS id, UserProfile.first_name, UserProfile.last_name, UserProfile.title, UserProfile.image AS image_path FROM UserProfile LEFT JOIN UserOrganization ON (UserProfile.user_id = UserOrganization.user_id) WHERE ((UserOrganization.organization_id, UserOrganization.organization_group_id) = (:organizationId, :organizationGroupId)) AND (UserOrganization.user_id < :personId) ORDER BY id DESC LIMIT :batchSize";
                        break;
                }
                $queryVariables = array(
                    ':personId' => $personId,
                    ':organizationId' => $organizationModel['organizationId'],
                    ':organizationGroupId' => $organizationModel['organizationGroupId'],
                    ':batchSize' => 20
                );
                $result = $this->databaseHandler->query($query, $queryVariables);

                $people = $result['isEmpty'] ? array() : $result['data'];
                foreach ($people as $index => $person) {
                    $people[$index]['initials'] = getInitials($person['firstName']) . getInitials($person['lastName']);

                    if (!is_null($person['imagePath'])) {
                        $people[$index]['imagePath'] = '/data/files/profiles/' . $person['id'] . '/' . $person['imagePath'];
                    }
                }

                return $people;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function searchUsers($keywords, $organizationModel, $excludeUsers = [], $resultSize = 10) {
            try {
                $searchResults = array();

                //trim the keywords and expand each keyword with the wildcard character prior to searching
                $keywords = trim($keywords);
                if (empty($keywords)) {
                    return $searchResults;
                }

                $expandedKeywords = expandKeywordsWithWildcard($keywords);

                //search the database for the keywords
                //FUTURE TODO: create an abstract search layer of the form "SELECT MATCH(:fields) AGAINST(:keywords IN BOOLEAN MODE) relevance, :lookupFields FROM :table HAVING (relevance > 0) DESC :includeLimit"
                $queryVariables = array(
                    ':keywords' => $expandedKeywords,
                    ':organizationId' => $organizationModel['organizationId'],
                    ':organizationGroupId' => $organizationModel['organizationGroupId'],
                    ':resultSize' => $resultSize
                );

                if (empty($excludeUsers)) {
                    $query = "SELECT MATCH(first_name, last_name, title) AGAINST(:keywords IN BOOLEAN MODE) relevance, UserProfile.user_id FROM UserProfile INNER JOIN UserOrganization ON (UserProfile.user_id = UserOrganization.user_id) WHERE ((UserOrganization.organization_id, UserOrganization.organization_group_id) = (:organizationId, :organizationGroupId)) HAVING (relevance > 0) ORDER BY relevance DESC LIMIT :resultSize";
                } else {
                    //filter out the existing users if a list of users are given to exclude
                    $parameters = '(';
                    foreach ($excludeUsers as $index => $userId) {
                        $parameter = ':userId' . $index;
                        $parameters .= $parameter . ',';
                        $queryVariables[$parameter] = $userId;
                    }

                    if (endsWith($parameters, ',')) {
                        $parameters = substr($parameters, 0, -1);
                    }
                    $parameters .= ')';
                    $query = "SELECT MATCH(first_name, last_name, title) AGAINST(:keywords IN BOOLEAN MODE) relevance, UserProfile.user_id FROM UserProfile INNER JOIN UserOrganization ON (UserProfile.user_id = UserOrganization.user_id) WHERE ((UserOrganization.organization_id, UserOrganization.organization_group_id) = (:organizationId, :organizationGroupId)) AND (UserProfile.user_id NOT IN " . $parameters . ") HAVING (relevance > 0) ORDER BY relevance DESC LIMIT :resultSize";
                }
                
                $result = $this->databaseHandler->query($query, $queryVariables);

                //package the search results
                foreach ($result['data'] as $user) {
                    $searchResults[] = $this->createBasicUserProfileModel($user['userId']);
                }

                return $searchResults;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function listAllContactTypes() {
            try {
                $query = "SELECT id, contact_type AS value FROM ContactType ORDER BY id ASC";
                $result = $this->databaseHandler->query($query);
                $contacts = $result['isEmpty'] ? array() : $result['data'];
                return $contacts;
            } catch (Exception $error) {
                throw $error;
            }
        }
    }
?>