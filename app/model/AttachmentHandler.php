<?php
    if (!defined('__GRAVIT8__')) {
        $gravit8Directory = dirname(dirname(dirname(__File__)));
        define('__GRAVIT8__', $gravit8Directory);
    }

    require_once __GRAVIT8__ . '/app/commons/utilities.php';
    require_once __GRAVIT8__ . '/app/commons/interfaces/Singleton.php';
    require_once __GRAVIT8__ . '/app/model/DatabaseHandler.php';

    Class AttachmentHandler implements Singleton {
        private static $instance;
        private $databaseHandler;
        private $attachmentDirectory;

        public function __construct() {
            if (self::$instance !== NULL) {
                return self::$instance;
            }

            $this->databaseHandler = DatabaseHandler::getInstance();

            $this->attachmentDirectory = '/data/attachments/';
        }

        public static function getInstance() {
            if (self::$instance === NULL) {
                self::$instance = new AttachmentHandler();
            }

            return self::$instance;
        }

        //TODO: is there an efficient way of doing this?
        public function removeAttachmentsFromFileSystem($attachmentIds) {
            try {
                //attachment ids should be given as an array
                if (is_int($attachmentIds)) {
                    $attachmentIds = array($attachmentIds);
                }

                if (empty($attachmentIds)) {
                    return FALSE;
                }

                //format the named parameters
                $parameters = '';
                $queryVariables = array();
                foreach ($attachmentIds as $index => $attachmentId) {
                    $parameterName = ':attachmentId' . $index;
                    $queryVariables[$parameterName] = $attachmentId;
                    $parameters .= $parameterName . ',';
                }
                $parameters = substr($parameters, 0, -1);

                //perform the queries
                //NOTE: here, we are assuming that Attachment and Organization has a 1:N relation (i.e an attachment can be shared among multiple entities and organizations)
                $query = "SELECT * FROM Attachment WHERE id IN (" . $parameters . ")";
                $results = $this->databaseHandler->query($query, $queryVariables);
                $attachments = $results['data'];

                //remove the attachments and partial attachments from the file system
                $unableToRemovePaths = array();
                foreach ($attachments as $attachment) {
                    //make attachment name
                    $extension = empty($attachment['extension']) ? '' : '.' . $attachment['extension'];
                    $attachmentName = $attachment['id'] . $extension;

                    //make attachment path
                    $attachmentPath = __GRAVIT8__ . $this->attachmentDirectory . $attachmentName;
                    $attachmentPath = str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, $attachmentPath);

                    $hasPartialAttachments = ($attachment['totalParts'] > 1) && ($attachment['uploaded'] === 0);
                    if ($hasPartialAttachments) {
                        $partialAttachmentPattern = $attachmentPath . '.part*';
                        $partialAttachmentPattern = str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, $partialAttachmentPattern);
                        $partialAttachmentPaths = glob($partialAttachmentPattern, GLOB_NOSORT);

                        foreach ($partialAttachmentPaths as $partialAttachmentPath) {
                            if (file_exists($partialAttachmentPath)) {
                                $isFileRemoved = unlink($partialAttachmentPath);
                                if ($isFileRemoved === FALSE) {
                                    $unableToRemovePaths[] = $partialAttachmentPath;
                                }
                            }
                        }
                    } else {
                        if (file_exists($attachmentPath)) {
                            $isFileRemoved = unlink($attachmentPath);
                            if ($isFileRemoved === FALSE) {
                                $unableToRemovePaths[] = $attachmentPath;
                            }
                        }
                    }
                }

                if (!empty($unableToRemovePaths)) {
                    throw new Exception('>>> Cannot remove the attachment at ' . reset($unableToRemovePaths));
                }

                return TRUE;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function removeAttachmentsFromDatabase($attachmentIds) {
            try {
                //attachment ids should be given as an array
                if (is_int($attachmentIds)) {
                    $attachmentIds = array($attachmentIds);
                }

                //format the named parameters
                $parameters = '';
                $queryVariables = array();
                foreach ($attachmentIds as $index => $attachmentId) {
                    $parameterName = ':attachmentId' . $index;
                    $queryVariables[$parameterName] = $attachmentId;
                    $parameters .= $parameterName . ',';
                }
                $parameters = substr($parameters, 0, -1);

                //perform the query
                $query = "DELETE IGNORE FROM Attachment WHERE id IN (" . $parameters . ")";
                $result = $this->databaseHandler->query($query, $queryVariables);

                return $result['isSuccess'];
            } catch (Exception $error) {
                throw $error;
            }
        }

        //TODO: implement and check
        //get all attachments by attachment ids (an array or integer)
        //NOTE: here, we are assuming that Attachment and Organization has a 1:1 relation (i.e an attachment cannot be shared among multiple entities and organizations)
        public function getAttachments($attachmentIds) {
            try {
                $attachments =  array(
                    'images' => array(),
                    'others' => array()
                );

                if (is_int($attachmentIds)) {
                    $attachmentIds = array($attachmentIds);
                }

                if (empty($attachmentIds)) {
                    return $attachments;
                }

                $queryVariables = array(
                    ':attachmentType' => 'image'
                );
                $parameters = '';
                foreach ($attachmentIds as $index => $attachmentId) {
                    $parameterName = ':attachmentId' . $index;
                    $parameters .= $parameterName . ',';
                    $queryVariables[$parameterName] = $attachmentId;
                }
                $parameters = substr($parameters, 0, -1);

                //images only
                //NOTE: here, we are assuming that Attachment and Organization has a 1:N relation (i.e an attachment can be shared among multiple entities and organizations)
                //FUTURE TODO: figure out how to find the original organization where the attachment is uploaded
                $query = "SELECT id, name, extension, size FROM Attachment WHERE (id IN (" . $parameters . ")) AND (type_id = (SELECT id FROM AttachmentType WHERE attachment_type = :attachmentType))";
                $results = $this->databaseHandler->query($query, $queryVariables);

                foreach ($results['data'] as $attachment) {
                    $extension = empty($attachment['extension']) ? '' : '.' . $attachment['extension'];
                    $attachmentName = $attachment['id'] . $extension;
                    $attachment['path'] = $this->attachmentDirectory . $attachmentName;
                    $attachment['formattedSize'] = formatBytes($attachment['size']);
                    unset($attachment['extension']);
                    $attachments['images'][] = $attachment;
                }

                //non images
                //NOTE: here, we are assuming that Attachment and Organization has a 1:N relation (i.e an attachment can be shared among multiple entities and organizations)
                //FUTURE TODO: figure out how to find the original organization where the attachment is uploaded
                $query = "SELECT id, name, extension, size FROM Attachment WHERE (id IN (" . $parameters . ")) AND (type_id IN (SELECT id FROM AttachmentType WHERE attachment_type != :attachmentType))";
                $results = $this->databaseHandler->query($query, $queryVariables);

                foreach ($results['data'] as $attachment) {
                    $extension = empty($attachment['extension']) ? '' : '.' . $attachment['extension'];
                    $attachmentName = $attachment['id'] . $extension;
                    $attachment['path'] = $this->attachmentDirectory . $attachmentName;
                    $attachment['formattedSize'] = formatBytes($attachment['size']);
                    unset($attachment['extension']);
                    $attachments['others'][] = $attachment;
                }

                return $attachments;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function getAttachmentByChecksum($checksum) {
            try {
                $attachmentModel = array(
                    'exists' => FALSE,
                    'attachment' => array()
                );

                $query = "SELECT id, name, extension, size FROM Attachment WHERE (checksum = :checksum)";
                $queryVariables = array(
                    ':checksum' => $checksum
                );
                $result = $this->databaseHandler->query($query, $queryVariables);

                if ($result['isSuccess'] && !$result['isEmpty']) {
                    $attachment = reset($result['data']);

                    $extension = empty($attachment['extension']) ? '' : '.' . $attachment['extension'];
                    $attachmentName = $attachment['id'] . $extension;
                    $attachment['path'] = $this->attachmentDirectory . $attachmentName;
                    $attachment['formattedSize'] = formatBytes($attachment['size']);
                    unset($attachment['extension']);

                    $attachmentModel['exists'] = TRUE;
                    $attachmentModel['attachment'] = $attachment;
                }

                return $attachmentModel;
            } catch (Exception $error) {
                throw $error;
            }
        }

        //TODO: implement all these
        private function isRenamingNeeded($attachmentId, $newName) {
            try {
                return FALSE;
            } catch (Exception $error) {
                throw $error;
            }
        }

        private function renameAttachmentInFileSystem($attachmentName, $newName) {
            try {
            } catch (Exception $error) {
                throw $error;
            }
        }

        private function renameAttachmentInDatabase($attachmentId, $newName) {
            try {
                $query = "UPDATE Attachment SET name = :attachmentName WHERE id = :attachmentId";
                $queryVariables = array(
                    ':attachmentId' => $attachmentId,
                    ':attachmentName' => $newName
                );
                $result = $this->databaseHandler->query($query, $queryVariables);
                return $result['isSuccess'];
            } catch (Exception $error) {
                throw $error;
            }
        }

        //TODO: complete this implementation
        public function renameAttachment($attachmentId, $newName) {
            try {
                //check if the new name is different
                if ($this->isRenamingNeeded($attachmentId, $newName)) {
                    //1. rename attachment/partial attachments in file system

                    //2. rename attachment/partial attachments in database
                    $this->renameAttachmentInDatabase($attachmentId, $newName);
                }
            } catch (Exception $error) {
                throw $error;
            }
        }
    }
?>