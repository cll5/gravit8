<?php
    if (!defined('__GRAVIT8__')) {
        $gravit8Directory = dirname(dirname(dirname(__File__)));
        define('__GRAVIT8__', $gravit8Directory);
    }

    require_once __GRAVIT8__ . '/app/commons/utilities.php';
    require_once __GRAVIT8__ . '/app/commons/interfaces/Singleton.php';
    require_once __GRAVIT8__ . '/app/model/DatabaseHandler.php';
    require_once __GRAVIT8__ . '/app/model/UserHandler.php';
    require_once __GRAVIT8__ . '/app/model/entities/IdeaHandler.php';
    require_once __GRAVIT8__ . '/app/model/entities/PitchHandler.php';
    require_once __GRAVIT8__ . '/app/model/entities/ProjectHandler.php';

    Class PortfolioHandler implements Singleton {
        private static $instance;
        private $databaseHandler;
        private $userHandler;
        private $ideaHandler;
        private $pitchHandler;
        private $projectHandler;

        public function __construct() {
            $this->databaseHandler = DatabaseHandler::getInstance();
            $this->userHandler = UserHandler::getInstance();
            $this->ideaHandler = IdeaHandler::getInstance();
            $this->pitchHandler = PitchHandler::getInstance();
            $this->projectHandler = ProjectHandler::getInstance();
        }

        public static function getInstance() {
            if (self::$instance === NULL) {
                self::$instance = new PortfolioHandler();
            }

            return self::$instance;
        }

        //TODO: make it load in batches
        public function createPortfolioModel($personId, $userId = NULL, $organizationModel = NULL, $lookupOrder = 'newest') {
            try {
                if (is_null($organizationModel)) {
                    //fetch all your work?
                }

                $portfolioModel = $this->defaultUserPortfolioModel();

                //get the entities started by user
                $query = "SELECT Entity.id, EntityType.entity_type FROM Entity INNER JOIN EntityType ON (Entity.entity_type_id = EntityType.id) INNER JOIN EntityOrganization ON (EntityOrganization.entity_id = Entity.id) WHERE ((EntityOrganization.organization_id, EntityOrganization.organization_group_id) = (:organizationId, :organizationGroupId)) AND (Entity.creator_id = :userId)";
                $queryVariables = array(
                    ':userId' => $personId,
                    ':organizationId' => $organizationModel['organizationId'],
                    ':organizationGroupId' => $organizationModel['organizationGroupId']
                );
                $result = $this->databaseHandler->query($query, $queryVariables);

                if (!$result['isEmpty']) {
                    foreach ($result['data'] as $entity) {
                        switch ($entity['entityType']) {
                            case 'idea':
                                $portfolioModel['started'][] = $this->ideaHandler->createBasicIdeaModel($entity['id']);
                                break;

                            case 'pitch':
                                $portfolioModel['started'][] = $this->pitchHandler->createBasicPitchModel($entity['id'], $userId);
                                break;

                            case 'project':
                                $portfolioModel['started'][] = $this->projectHandler->createBasicProjectModel($entity['id'], $userId);
                                break;
                        }
                    }
                }

                return $portfolioModel;
            } catch (Exception $error) {
                throw $error;
            }
        }

        private function defaultUserPortfolioModel() {
            return array(
                'started' => array(),
                'commented' => array(),
                'liked' => array()
            );
        }
    }
?>