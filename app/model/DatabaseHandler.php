<?php
    if (!defined('__GRAVIT8__')) {
        $gravit8Directory = dirname(dirname(dirname(__File__)));
        define('__GRAVIT8__', $gravit8Directory);
    }

    //handles database queries and transactions
    require_once __GRAVIT8__ . '/app/commons/interfaces/Singleton.php';
    require_once __GRAVIT8__ . '/app/commons/utilities.php';

    Class DatabaseHandler implements Singleton {
        private static $instance;
        private $database;

        private function __construct() {
            //buffer size, see comments: http://php.net/manual/en/pdo.setattribute.php
            //TODO: the password here should be typed in through console interactively, so as to prevent hackers taking over the database
            require_once __GRAVIT8__ . '/database/credentials/login.php';

            $dsn = 'mysql:dbname=' . $db_database . ';host=' . $db_hostname;
            $databaseOptions = array();
            $this->database = new PDO($dsn, $db_username, $db_password, $databaseOptions);

            //settings
            $this->database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->database->setAttribute(PDO::ATTR_ORACLE_NULLS, PDO::NULL_NATURAL);
            $this->database->setAttribute(PDO::ATTR_STRINGIFY_FETCHES, FALSE);
            $this->database->setAttribute(PDO::ATTR_CASE, PDO::CASE_LOWER);
            $this->database->setAttribute(PDO::ATTR_AUTOCOMMIT, TRUE);
            $this->database->setAttribute(PDO::ATTR_EMULATE_PREPARES, FALSE);
            $this->database->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_NAMED);
        }

        public static function getInstance() {
            if (self::$instance === NULL) {
                self::$instance = new DatabaseHandler();
            }

            return self::$instance;
        }

        //NOTE: this is probably no long relevant, but just in case, see the first comment in http://php.net/manual/en/function.mysqli-report.php
        public function closeDatabase() {
            //to close PDO connection, see: http://stackoverflow.com/questions/18277233/pdo-closing-connection
            $this->database = NULL;
        }

        //NOTE: About 1st and 2nd order SQL injection, see: http://stackoverflow.com/questions/134099/are-pdo-prepared-statements-sufficient-to-prevent-sql-injection
        public function query($prepareStatement, $queryVariables = NULL) {
            try {
                $statement = $this->database->prepare($prepareStatement);

                if (!empty($queryVariables)) {
                    $this->bindValuesToStatement($statement, $queryVariables);
                }
                $success = $statement->execute();

                $result = array(
                    'isSuccess' => $success,
                    'isEmpty' => ($statement->rowCount() === 0),
                    'numberOfRows' => $statement->rowCount(),
                    'data' => mapFieldsToKeys($statement->fetchAll(PDO::FETCH_NAMED)),
                    'errorReport' => $statement->errorInfo()
                );
                return $result;
            } catch (Exception $error) {
                $this->debugQuery($prepareStatement, $queryVariables);
                throw $error;
            }
        }

        //method to return a single column as an array instead of nested arrays
        public function queryByColumn($prepareStatement, $queryVariables = NULL) {
            try {
                $statement = $this->database->prepare($prepareStatement);
                if (!empty($queryVariables)) {
                    $this->bindValuesToStatement($statement, $queryVariables);
                }
                $success = $statement->execute();

                $result = array(
                    'isSuccess' => $success,
                    'isEmpty' => ($statement->rowCount() === 0),
                    'numberOfRows' => $statement->rowCount(),
                    'data' => $statement->fetchAll(PDO::FETCH_COLUMN),
                    'errorReport' => $statement->errorInfo()
                );
                return $result;
            } catch (Exception $error) {
                $this->debugQuery($prepareStatement, $queryVariables);
                throw $error;
            }
        }

        public function getLastInsertId($name = NULL) {
            return (int) $this->database->lastInsertId($name);
        }

        public function beginTransaction() {
            return $this->database->beginTransaction();
        }

        public function commitTransaction() {
            return $this->database->commit();
        }

        public function createMultipleMarkers($multiplier) {
            try {
                return '(' . rtrim(str_repeat('?,', $multiplier), ',') . ')';
            } catch (Exception $error) {
                throw $error;
            }
        }

        //php types, see: http://php.net/manual/en/function.gettype.php
        //pdo types, see: http://php.net/manual/en/pdo.constants.php
        //pdo for floats, see: http://stackoverflow.com/questions/2718628/pdoparam-for-type-decimal
        //queryVariables is an array where the index or key and corresponding value represent the sequential ordering of the prepare statement's markers
        private function bindValuesToStatement($statement, $queryVariables) {
            foreach ($queryVariables as $identifier => $value) {
                if (is_numeric($identifier)) {
                    //bindValue is 1-based indices, not 0-based indices
                    $identifier += 1;
                }
                switch (gettype($value)) {
                    case 'boolean':
                        //TOOD: figure out how to convert php boolean to mysql boolean properly, instead of relying on integer values
                        $statement->bindValue($identifier, $value ? 1 : 0, PDO::PARAM_INT);
                        break;
                    case 'integer':
                        $statement->bindValue($identifier, $value, PDO::PARAM_INT);
                        break;
                    case 'double':
                        $statement->bindValue($identifier, strval($value), PDO::PARAM_STR);
                        break;
                    case 'string':
                        $statement->bindValue($identifier, $value, PDO::PARAM_STR);
                        break;
                    case 'NULL':
                        $statement->bindValue($identifier, $value, PDO::PARAM_NULL);
                        break;
                    default:
                        $statement->bindValue($identifier, $value, PDO::PARAM_LOB);
                }
            }
        }

        //methods for debugging the binded prepare statements
        private function debugQuery($prepareStatement, $queryVariables) {
            if ($this->containsBothParameters($prepareStatement)) {
                debug('SQL Statement contains both numeric and named parameters!');
                $sqlStatement = $prepareStatement;

            } else if ($this->containsNoParameters($prepareStatement)) {
                $sqlStatement = $prepareStatement;

            } else if ($this->containsNumericParameters($prepareStatement) && !isAssociativeArray($queryVariables)) {
                $sqlStatement = $this->bindNumericParameters($prepareStatement, $queryVariables);

            } else if ($this->containsNamedParameters($prepareStatement) && isAssociativeArray($queryVariables)) {
                $sqlStatement = $prepareStatement;
                $namedParameters = array_keys($queryVariables);
                foreach ($namedParameters as $namedParameter) {
                    $sqlStatement = str_replace($namedParameter, $queryVariables[$namedParameter], $sqlStatement);
                }

            } else {
                debug('SQL Statement contains invalid binding variable type');
                $sqlStatement = $prepareStatement;
            }

            debug('>>> Debug SQL Statement: "' . $sqlStatement . ';"');
        }

        private function containsNumericParameters($prepareStatement) {
            return (strpos($prepareStatement, '?') !== FALSE);
        }

        private function containsNamedParameters($prepareStatement) {
            return (strpos($prepareStatement, ':') !== FALSE);
        }

        private function containsNoParameters($prepareStatement) {
            return !($this->containsNumericParameters($prepareStatement) || $this->containsNamedParameters($prepareStatement));
        }

        private function containsBothParameters($prepareStatement) {
            return ($this->containsNumericParameters($prepareStatement) && $this->containsNamedParameters($prepareStatement));
        }

        private function bindNumericParameters($prepareStatement, $queryVariables) {
            $prepareStatements = explode('?', $prepareStatement);
            $length = count($prepareStatements);
            $index = 0;
            $sqlStatement = '';
            while ($index < ($length - 1)) {
                $sqlStatement .= ($prepareStatements[$index] . $queryVariables[$index++]);
            }
            $sqlStatement .= $prepareStatements[$index];
            return $sqlStatement;
        }
    }
?>