<?php
    if (!defined('__GRAVIT8__')) {
        $gravit8Directory = dirname(dirname(dirname(__File__)));
        define('__GRAVIT8__', $gravit8Directory);
    }

    require_once __GRAVIT8__ . '/app/commons/utilities.php';
    require_once __GRAVIT8__ . '/app/commons/interfaces/Singleton.php';
    require_once __GRAVIT8__ . '/app/model/DatabaseHandler.php';
    require_once __GRAVIT8__ . '/app/model/UserHandler.php';
    require_once __GRAVIT8__ . '/app/model/EntityHandler.php';

    class CommentHandler implements Singleton {
        private static $instance;
        private $databaseHandler;
        private $userHandler;
        private $entityHandler;

        public function __construct() {
            $this->databaseHandler = DatabaseHandler::getInstance();
            $this->userHandler = UserHandler::getInstance();
            $this->entityHandler = EntityHandler::getInstance();
        }

        public static function getInstance() {
            if (self::$instance === NULL) {
                self::$instance = new CommentHandler();
            }

            return self::$instance;
        }

        //FUTURE TODO: include entity information in comment model? Why?
        public function createCommentModel($commentId, $userId = NULL) {
            try {
                $commentModel = $this->defaultCommentModel();

                //get the comment
                $query = "SELECT * FROM EntityComment WHERE (id = :commentId)";
                $queryVariables = array(
                    ':commentId' => $commentId
                );
                $result = $this->databaseHandler->query($query, $queryVariables);

                if (!$result['isEmpty']) {
                    $entityComment = reset($result['data']);
                    $commentModel['id'] = $entityComment['id'];
                    $commentModel['commenter'] = $this->userHandler->createBasicUserProfileModel($entityComment['commenterId']);
                    $commentModel['message'] = $entityComment['message'];
                    $commentModel['postedOn'] = $entityComment['postedOn'];

                    if (!is_null($userId)) {
                        $commentModel['editable'] = ($userId === $commentModel['commenter']['id']);
                    }
                }

                return $commentModel;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public static function defaultCommentModel() {
            return array(
                'id' => NULL,
                'commenter' => UserHandler::defaultBasicUserProfile(),
                'message' => NULL,
                'postedOn' => NULL,
                'editable' => FALSE
            );
        }

        public function postComment($entityId, $commenterId, $message = '', $previousCommentId = 0) {
            try {
                
            } catch (Exception $error) {
                throw $error;
            }
            $postedOn = getTimeStamp();

            //add the new comment
            $query = "INSERT INTO EntityComment (entity_id, commenter_id, message, posted_on) VALUES (:entityId, :commenterId, :message, :postedOn)";
            $queryVariables = array(
                ':entityId' => $entityId,
                ':commenterId' => $commenterId,
                ':message' => $message,
                ':postedOn' => $postedOn
            );
            $this->databaseHandler->query($query, $queryVariables);
            $commentId = $this->databaseHandler->getLastInsertId();

            $comments = array();
            $comments[] = $this->createCommentModel($commentId);

            $query = "SELECT id FROM EntityComment WHERE (entity_id = :entityId) AND (id > :previousCommentId) AND (id < :newestCommentId) ORDER BY id DESC";
            $queryVariables = array(
                ':entityId' => $entityId,
                ':previousCommentId' => $previousCommentId,
                ':newestCommentId' => $commentId
            );
            $result = $this->databaseHandler->queryByColumn($query, $queryVariables);

            if (!$result['isEmpty']) {
                foreach ($result['data'] as $commentId) {
                    $comments[] = $this->createCommentModel($commentId, $commenterId);
                }
            }

            return $comments;
        }

        public function loadCommentsByEntity($entityId, $lookupOrder = 'newest', $commentId = 0, $requesterId = NULL) {
            try {
                switch ($lookupOrder) {
                    case 'newest':
                        $query = "SELECT id FROM EntityComment WHERE (entity_id = :entityId) AND (id > :commentId) ORDER BY posted_on DESC LIMIT :batchSize";
                        break;

                    case 'oldest':
                        $query = "SELECT id FROM EntityComment WHERE (entity_id = :entityId) AND (id < :commentId) ORDER BY posted_on DESC LIMIT :batchSize";
                        break;
                }
                $queryVariables = array(
                    ':entityId' => $entityId,
                    ':commentId' => $commentId,
                    ':batchSize' => 20
                );
                $result = $this->databaseHandler->queryByColumn($query, $queryVariables);

                $comments = array();
                if (!$result['isEmpty']) {
                    foreach ($result['data'] as $commentId) {
                        $comments[] = $this->createCommentModel($commentId, $requesterId);
                    }
                }

                return $comments;
            } catch (Exception $error) {
                throw $error;
            }
        }

        //TODO: to be tested
        public function loadCommentsByUser($commenterId, $commentId = 0, $requesterId = NULL) {
            try {
                $query = "SELECT * FROM EntityComment WHERE (commenter_id = :commenterId) AND (id > :commentId) ORDER BY posted_on DESC LIMIT :batchSize";
                $queryVariables = array(
                    ':commenterId' => $commenterId,
                    ':commentId' => $commentId,
                    ':batchSize' => 20
                );
                $result = $this->databaseHandler->queryByColumn($query, $queryVariables);

                $comments = array();
                if (!$result['isEmpty']) {
                    foreach ($result['data'] as $commentId) {
                        $comments[] = $this->createCommentModel($commentId, $commenterId);
                    }
                }

                return $comments;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public static function numberOfCommentByCommenter($commenterId) {
            try {
                
            } catch (Exception $error) {
                throw $error;
            }
            $query = "SELECT COUNT(*) FROM EntityComment WHERE (commenter_id = :commenterId)";
            $queryVariables = array(
                ':commenterId' => $commenterId
            );
            $result = $this->databaseHandler->queryByColumn($query, $queryVariables);
            return reset($result['data']);
        }
    }
?>