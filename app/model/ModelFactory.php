<?php
    if (!defined('__GRAVIT8__')) {
        $gravit8Directory = dirname(dirname(dirname(__File__)));
        define('__GRAVIT8__', $gravit8Directory);
    }

    require_once __GRAVIT8__ . '/app/commons/utilities.php';
    require_once __GRAVIT8__ . '/app/model/DatabaseHandler.php';

    class ModelFactory {
        private static $DEFAULT_BATCH_SIZE = 50;
        public static $TIME_FORMAT;
        public static $ISO_DATE_FORMAT;
        public static $UTC_TIMEZONE;

        //define asynchronous deadlines
        //see: http://php.net/manual/en/class.dateinterval.php
        //     http://www.php.net/manual/en/dateinterval.construct.php
        //     http://stackoverflow.com/questions/9188763/what-does-p-stand-for-in-the-dateinterval-format
        public static $IDEA_PERIOD;
        public static $SOLUTION_PERIOD;
        public static $TEAM_FORMATION_PERIOD;
        public static $PROJECT_PERIOD;

        private static $ARCHIVE_GRACE_PERIOD;

        private $databaseHandler;

        public function __construct() {
            try {
                //initializing static variables because before PHP 5.6, PHP can't handle trivial expression initializations for static variables
                if (!isset(self::$TIME_FORMAT)) {
                    self::$TIME_FORMAT = 'Y-m-d H:i:s';
                }

                if (!isset(self::$ISO_DATE_FORMAT)) {
                    self::$ISO_DATE_FORMAT = 'c';
                }

                if (!isset(self::$UTC_TIMEZONE)) {
                    self::$UTC_TIMEZONE = new DateTimeZone('UTC');
                }

                if (!isset(self::$IDEA_PERIOD)) {
                    self::$IDEA_PERIOD = new DateInterval('P5D');
                    // self::$IDEA_PERIOD = new DateInterval('PT1M');
                }

                if (!isset(self::$SOLUTION_PERIOD)) {
                    self::$SOLUTION_PERIOD = new DateInterval('P10D');
                    // self::$SOLUTION_PERIOD = new DateInterval('PT3M');
                }

                if (!isset(self::$TEAM_FORMATION_PERIOD)) {
                    self::$TEAM_FORMATION_PERIOD = new DateInterval('P11D');
                    // self::$TEAM_FORMATION_PERIOD = new DateInterval('PT5M');
                }

                if (!isset(self::$PROJECT_PERIOD)) {
                    self::$PROJECT_PERIOD = new DateInterval('P30D');
                    // self::$PROJECT_PERIOD = new DateInterval('P1D');
                }

                if (!isset(self::$ARCHIVE_GRACE_PERIOD)) {
                    self::$ARCHIVE_GRACE_PERIOD = new DateInterval('P5D');
                    // self::$ARCHIVE_GRACE_PERIOD = new DateInterval('P1D');
                }

                $this->databaseHandler = DatabaseHandler::getInstance();
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function fetchIdeas($queryBatchSize = NULL, $queryOffset = 0) {
            try {
                if (is_null($queryBatchSize)) {
                    $queryBatchSize = self::$DEFAULT_BATCH_SIZE;
                }

                $query = "SELECT id FROM Projects WHERE stage IN ('idea', 'brief') ORDER BY created_on DESC LIMIT :batchSize OFFSET :offset";
                $queryVariables = array(
                    ':batchSize' => $queryBatchSize,
                    ':offset' => $queryOffset
                );
                $results = $this->databaseHandler->queryByColumn($query, $queryVariables);
                $ideaIds = $results['data'];
                $ideas = $this->createSimpleProjectModel($ideaIds);

                return $ideas;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function fetchIdeasForUser($userId, $queryBatchSize = NULL, $queryOffset = 0) {
            try {
                $ideas = $this->fetchIdeas($queryBatchSize, $queryOffset);
                $user = $this->createUserModel($userId);

                if (!isEmptyObject($ideas) && !isEmptyObject($user)) {
                    $user = reset($user);
                    $ideas = $this->linkItemsToUser($ideas, $user);
                }
                return $ideas;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function fetchProjects($queryBatchSize = NULL, $queryOffset = 0) {
            try {
                if (is_null($queryBatchSize)) {
                    $queryBatchSize = self::$DEFAULT_BATCH_SIZE;
                }

                $query = "SELECT id FROM Projects WHERE stage IN ('recruitment', 'development') ORDER BY created_on DESC LIMIT :batchSize OFFSET :offset";
                $queryVariables = array(
                    ':batchSize' => $queryBatchSize,
                    ':offset' => $queryOffset
                );
                $results = $this->databaseHandler->queryByColumn($query, $queryVariables);
                $projectIds = $results['data'];
                $projects = $this->createSimpleProjectModel($projectIds);

                return $projects;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function fetchProjectsForUser($userId, $queryBatchSize = NULL, $queryOffset = 0) {
            try {
                $projects = $this->fetchProjects($queryBatchSize, $queryOffset);
                $user = $this->createUserModel($userId);

                if (!isEmptyObject($projects) && !isEmptyObject($user)) {
                    $user = reset($user);
                    $projects = $this->linkItemsToUser($projects, $user);
                }
                return $projects;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function linkItemsToUser($items, $user) {
            try {
                foreach ($user['liked'] as $itemId) {
                    if (array_key_exists($itemId, $items)) {
                        $items[$itemId]['liked'] = TRUE;
                    }
                }
                return $items;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function fetchPeople($queryBatchSize = NULL, $queryOffset = 0) {
            try {
                if (is_null($queryBatchSize)) {
                    $queryBatchSize = self::$DEFAULT_BATCH_SIZE;
                }

                $query = "SELECT id FROM Users ORDER BY name ASC LIMIT :batchSize OFFSET :offset";
                $queryVariables = array(
                    ':batchSize' => $queryBatchSize,
                    ':offset' => $queryOffset
                );
                $results = $this->databaseHandler->queryByColumn($query, $queryVariables);
                $peopleIds = $results['data'];

                if (!$results['isEmpty']) {
                    $people = $this->createUserModel($peopleIds);
                    return $people;
                }

                return createEmptyObject();
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function createBrowseModel($queryBatchSize = NULL, $queryOffset = 0) {
            try {
                if (is_null($queryBatchSize)) {
                    $queryBatchSize = self::$DEFAULT_BATCH_SIZE;
                }

                $today = getTimeStamp();

                $queryVariables = array(
                    ':today' => $today,
                    ':batchSize' => $queryBatchSize,
                    ':offset' => $queryOffset
                );

                //conditional WHERE clause for MySQL: http://stackoverflow.com/questions/18451207/conditional-in-mysql-where-clause
                $query = "SELECT id FROM Projects WHERE status = 'active' AND stage IN ('idea', 'brief') AND release_date <= :today ORDER BY created_on DESC LIMIT :batchSize OFFSET :offset";
                $results = $this->databaseHandler->queryByColumn($query, $queryVariables);
                $ideaIds = $results['data'];
                $ideas = $this->createSimpleProjectModel($ideaIds);

                $query = "SELECT id FROM Projects WHERE status = 'active' AND stage IN ('recruitment', 'development') AND release_date <= :today ORDER BY created_on DESC LIMIT :batchSize OFFSET :offset";
                $results = $this->databaseHandler->queryByColumn($query, $queryVariables);
                $projectIds = $results['data'];
                $projects = $this->createSimpleProjectModel($projectIds);

                $queryVariables = array(
                    ':batchSize' => $queryBatchSize,
                    ':offset' => $queryOffset
                );

                $query = "SELECT id FROM Users ORDER BY name ASC LIMIT :batchSize OFFSET :offset";
                $results = $this->databaseHandler->queryByColumn($query, $queryVariables);
                $peopleIds = $results['data'];
                $people = $this->createUserModel($peopleIds);

                $browseModel = array(
                    'ideas' => $ideas,
                    'projects' => $projects,
                    'people' => $people
                );
                return $browseModel;
            } catch (Exception $error) {
                throw $error;
            }
        }

        //TODO: decompose this to fetching ideas, or projects, and not together
        public function createArchiveModel($queryBatchSize = NULL, $queryOffset = 0) {
            try {
                if (is_null($queryBatchSize)) {
                    $queryBatchSize = self::$DEFAULT_BATCH_SIZE;
                }

                $today = getTimeStamp();

                $queryVariables = array(
                    ':batchSize' => $queryBatchSize,
                    ':offset' => $queryOffset
                );

                $query = "SELECT id FROM Projects WHERE status = 'archived' AND stage IN ('idea', 'brief') ORDER BY created_on DESC LIMIT :batchSize OFFSET :offset";
                $results = $this->databaseHandler->queryByColumn($query, $queryVariables);
                $ideaIds = $results['data'];
                $ideas = $this->createSimpleProjectModel($ideaIds);

                $query = "SELECT id FROM Projects WHERE status = 'archived' AND stage IN ('recruitment', 'development') ORDER BY created_on DESC LIMIT :batchSize OFFSET :offset";
                $results = $this->databaseHandler->queryByColumn($query, $queryVariables);
                $projectIds = $results['data'];
                $projects = $this->createSimpleProjectModel($projectIds);

                $browseModel = array(
                    'ideas' => $ideas,
                    'projects' => $projects
                );
                return $browseModel;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function createPublishModel($queryBatchSize = NULL, $queryOffset = 0) {
            try {
                if (is_null($queryBatchSize)) {
                    $queryBatchSize = self::$DEFAULT_BATCH_SIZE;
                }

                $today = getTimeStamp();

                $queryVariables = array(
                    ':batchSize' => $queryBatchSize,
                    ':offset' => $queryOffset
                );

                $query = "SELECT id FROM Projects WHERE status = 'published' ORDER BY created_on DESC LIMIT :batchSize OFFSET :offset";
                $results = $this->databaseHandler->queryByColumn($query, $queryVariables);
                $projectIds = $results['data'];
                $projects = $this->createSimpleProjectModel($projectIds);

                $browseModel = array(
                    'projects' => $projects
                );
                return $browseModel;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function createDashboardModel($userId, $queryBatchSize = NULL, $queryOffset = 0) {
            try {
                if (is_null($queryBatchSize)) {
                    $queryBatchSize = self::$DEFAULT_BATCH_SIZE;
                }

                $projectCollectionModel = $this->createProjectCollectionModel($userId, $queryBatchSize, $queryOffset);
                $dashboardModel = array(
                    'projectCollection' => $projectCollectionModel
                );
                return $dashboardModel;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function createProfileModel($userId, $queryBatchSize = NULL, $queryOffset = 0) {
            try {
                if (is_null($queryBatchSize)) {
                    $queryBatchSize = self::$DEFAULT_BATCH_SIZE;
                }

                $userModel = $this->createUserModel($userId);
                $experienceModel = $this->createExperienceModel($userId);
                $projectCollectionModel = $this->createProjectCollectionModel($userId, $queryBatchSize, $queryOffset);

                $profile = $userModel[0];
                $profile['experiences'] = $experienceModel;

                $profileModel = array(
                    'profile' => $profile,
                    'projectCollection' => $projectCollectionModel
                );

                return $profileModel;
            } catch (Exception $error) {
                throw $error;
            }
        }

        //TODO: maybe scenarios should be grouped by contributors => scenarios, instead of scenarios => contributors
        public function createScenarioModel($projectId) {
            try {
                $queryVariables = array(
                    ':projectId' => $projectId
                );
                $query = "SELECT * FROM Scenarios WHERE project_id = :projectId";
                $results = $this->databaseHandler->query($query, $queryVariables);
                $scenarios = $results['data'];

                return array_map(function($scenario) {
                    $contributor = $this->createUserModel($scenario['userId']);
                    return array(
                        'id' => $scenario['id'],
                        'before' => $scenario['scenarioBefore'],
                        'after' => $scenario['scenarioAfter'],
                        'contributor' => reset($contributor),
                        'createdOn' => $scenario['contributedOn'],
                        'iterationNumber' => $scenario['iteration'],
                        'isOfficial' => (bool) $scenario['isOfficial']
                    );
                }, $scenarios);
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function createScenarioCollectionModel($projectId) {
            try {
                $scenarioCollectionModel = array(
                    'contributors' => array(),
                    'unofficial' => array(),
                    'official' => array()
                );

                $scenarios = $this->createScenarioModel($projectId);
                foreach ($scenarios as $index => $scenario) {
                    $contributorId = $scenario['contributor']['id'];
                    if (!array_key_exists($contributorId, $scenarioCollectionModel['contributors'])) {
                        $scenarioCollectionModel['contributors'][$contributorId] = $scenario['contributor'];
                    }

                    if ($scenario['isOfficial']) {
                        $scenarioCollectionModel['official'] = $scenario;
                    } else {
                        $iteration = $scenario['iterationNumber'];
                        if (!array_key_exists($contributorId, $scenarioCollectionModel['unofficial'])) {
                            $scenarioCollectionModel['unofficial'][$contributorId] = array(
                                $iteration => $scenario
                            );
                        } else {
                            $scenarioCollectionModel['unofficial'][$contributorId][$iteration] = $scenario;
                        }
                    }
                }

                return $scenarioCollectionModel;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function createExperienceModel($userId) {
            try {
                $queryVariables = array(
                    ':userId' => $userId
                );
                $query = "SELECT id, title AS name, description, duration FROM Experiences WHERE user_id = :userId";
                $results = $this->databaseHandler->query($query, $queryVariables);
                $experiences = $results['data'];
                return $experiences;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function createUserModel($userIds) {
            try {
                $numberOfUsers = count($userIds);
                if ($numberOfUsers > 0) {
                    $queryVariables = is_array($userIds) ? $userIds : array($userIds);
                    $markers = $this->createMultipleMarkers('?, ', $numberOfUsers);
                    $query = "SELECT * FROM Users WHERE id IN (" . $markers . ")";
                    $results = $this->databaseHandler->query($query, $queryVariables);

                    if (!$results['isEmpty']) {
                        $users = $results['data'];
                        return array_map(function($user) {
                            return array(
                                'id' => $user['id'],
                                'name' => $user['name'],
                                'initials' => getInitials($user['name']),
                                'title' => $user['title'],
                                'image' => doesFileExists($user['image']) ? $user['image'] : '',
                                'description' => $user['bio'],
                                'email' => $user['email'],
                                'projects' => $this->getProjectContributions($user['id']),
                                'liked' => $this->getLikedProjects($user['id'])
                            );
                        }, $users);
                    }
                }
                return createEmptyObject();
            } catch (Exception $error) {
                throw $error;
            }
        }

        //This model is used for project previews (i.e. in browse, dashboard, and profile pages)
        public function createSimpleProjectModel($projectIds) {
            try {
                $numberOfProjects = count($projectIds);

                if ($numberOfProjects > 0) {
                    $markers = $this->createMultipleMarkers('?, ', $numberOfProjects);
                    $query = "SELECT * FROM Projects WHERE id IN (" . $markers . ")";
                    $queryVariables = is_array($projectIds) ? $projectIds : array($projectIds);
                    $results = $this->databaseHandler->query($query, $queryVariables);
                    $projects = $results['data'];
                    $simpleProjectModel = array();

                    if ($results['isEmpty']) {
                        $simpleProjectModel = createEmptyObject();
                    }

                    $ideaStages = array('idea', 'brief');
                    $stages = $this->getProjectStageKeys();
                    $projectStages = array_diff($stages, $ideaStages);
                    $now = new DateTime(NULL, self::$UTC_TIMEZONE);
                    foreach ($projects as $project) {
                        $projectIsInIdeaStage = in_array($project['stage'], $ideaStages);
                        $projectIsInSolutionStage = in_array($project['stage'], $projectStages);

                        //get the dates
                        $ideationTimer = new DateTime($project['releaseDate'], self::$UTC_TIMEZONE);
                        $ideationStartTime = $ideationTimer->add(self::$IDEA_PERIOD)->format(self::$ISO_DATE_FORMAT);
                        $ideationArchiveTime = $ideationTimer->add(self::$ARCHIVE_GRACE_PERIOD)->format(self::$ISO_DATE_FORMAT);

                        $solutionTimer = new DateTime($project['releaseDate'], self::$UTC_TIMEZONE);
                        $solutionStartTime = $solutionTimer->add(self::$SOLUTION_PERIOD)->format(self::$ISO_DATE_FORMAT);
                        $solutionArchiveTime = $solutionTimer->add(self::$ARCHIVE_GRACE_PERIOD)->format(self::$ISO_DATE_FORMAT);

                        $teamFormationTimer = new DateTime($project['releaseDate'], self::$UTC_TIMEZONE);
                        $teamFormationStartTime = $teamFormationTimer->add(self::$TEAM_FORMATION_PERIOD)->format(self::$ISO_DATE_FORMAT);
                        // $teamFormationArchiveTime = $teamFormationTimer->add(self::$ARCHIVE_GRACE_PERIOD)->format(self::$ISO_DATE_FORMAT);

                        $days = $project['iteration'] * (self::$PROJECT_PERIOD->d);
                        $projectPeriod = new DateInterval('P' . $days . 'D');
                        $projectTimer = new DateTime($project['releaseDate'], self::$UTC_TIMEZONE);
                        $projectDeadline = $projectTimer->add($projectPeriod)->format(self::$ISO_DATE_FORMAT);
                        $projectArchiveTime = $projectTimer->add(self::$ARCHIVE_GRACE_PERIOD)->format(self::$ISO_DATE_FORMAT);

                        //start building up the simple project model
                        $projectModel = array(
                            'id' => $project['id'],
                            'status' => $project['status'],
                            'stage' => $project['stage'],
                            'isAnIdea' => $projectIsInIdeaStage,
                            'isSolution' => $projectIsInSolutionStage,
                            'createdOn' => getTimeStamp($project['createdOn'], self::$ISO_DATE_FORMAT),
                            'ideationStartTime' => $ideationStartTime,
                            'solutionStartTime' => $solutionStartTime,
                            'teamFormationStartTime' => $teamFormationStartTime,
                            'deadline' => $projectDeadline,
                            'ideationArchiveTime' => $ideationArchiveTime,
                            'solutionArchiveTime' => $solutionArchiveTime,
                            'projectArchiveTime' => $projectArchiveTime,
                            'releaseDate' => getTimeStamp($project['releaseDate'], self::$ISO_DATE_FORMAT),
                            'likes' => $project['likes'],
                            'liked' => FALSE
                        );

                        if ($projectIsInIdeaStage) {
                            $projectModel['name'] = $project['ideaTitle'];
                            $projectModel['oneLiner'] = $project['ideaOneLiner'];
                            $projectModel['description'] = $project['ideaDescription'];
                        } else {
                            $projectModel['name'] = $project['solutionTitle'];
                            $projectModel['oneLiner'] = $project['solutionOneLiner'];
                            $projectModel['description'] = $project['solutionDescription'];
                        }

                        //get project champion information
                        $projectChampion = $this->createUserModel($project['championId']);
                        $projectModel['champion'] = $projectChampion[0];

                        //get project categories
                        $queryVariables = array(
                            ':projectId' => $project['id']
                        );
                        $query = "SELECT project_category FROM ProjectCategories WHERE project_id = :projectId";
                        $results = $this->databaseHandler->queryByColumn($query, $queryVariables);
                        $projectModel['categories'] = $results['data'];

                        //get project number of gravit8'ers
                        if ($projectIsInIdeaStage) {
                            $query = "SELECT DISTINCT user_id AS contributor_id FROM Scenarios WHERE project_id = :projectId AND user_id != :projectChampionId";
                        } else {
                            $query = "SELECT DISTINCT RoleContributors.user_id AS contributor_id FROM RoleContributors INNER JOIN Roles ON (RoleContributors.role_id = Roles.id) WHERE Roles.project_id = :projectId AND RoleContributors.user_id != :projectChampionId";
                        }
                        $queryVariables = array(
                            ':projectId' => $project['id'],
                            ':projectChampionId' => $project['championId']
                        );
                        $results = $this->databaseHandler->queryByColumn($query, $queryVariables);
                        $contributorIds = $results['data'];
                        $projectModel['numberOfContributors'] = $results['numberOfRows'];

                        //get contributors' information
                        $contributors = $this->createUserModel($contributorIds);
                        $projectModel['contributors'] = $contributors;

                        //get project updates
                        // $projectUpdates = $this->createProjectUpdateModel($project['id']);
                        // $projectModel['updates'] = $projectUpdates;

                        //push the additional information back into the collection of projects
                        $simpleProjectModel[$project['id']] = $projectModel;
                    }

                    return $simpleProjectModel;
                }
                return createEmptyObject();
            } catch (Exception $error) {
                throw $error;
            }
        }

        //This model is for when you view a single project in full details (i.e. route is /project/(id)/stage/(stages))
        public function createProjectModel($projectId) {

            try {
                $project = $this->createSimpleProjectModel($projectId);
                $project = reset($project);

                if ($project !== FALSE) {
                    $query = "SELECT * FROM Projects WHERE id = :projectId";
                    $queryVariables = array(
                        ':projectId' => $projectId
                    );
                    $results = $this->databaseHandler->query($query, $queryVariables);
                    $projectInformation = $results['data'][0];

                    $scenarioCollection = $this->createScenarioCollectionModel($projectId);

                    $project['idea'] = array(
                        'name' => $projectInformation['ideaTitle'],
                        'oneLiner' => $projectInformation['ideaOneLiner'],
                        'description' => $projectInformation['ideaDescription'],
                        'scenarios' => $scenarioCollection
                    );

                    $roles = $this->createRoleModel($projectId);
                    // $tasks = $this->createProjectTaskModel($projectId);
                    $project['solution'] = array(
                        'name' => $projectInformation['solutionTitle'],
                        'oneLiner' => $projectInformation['solutionOneLiner'],
                        'description' => $projectInformation['solutionDescription'],
                        'roles' => $roles,
                        // 'tasks' => $tasks
                    );

                    $project['isTeamFormed'] = $projectInformation['isTeamFormed'];

                    $projectModel = array();
                    $projectModel[$project['id']] = $project;
                    return $projectModel;
                }
                return createEmptyObject();
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function getUserContributions($userId) {
            try {
                $queryVariables = array(
                    ':userId' => $userId
                );

                debug($userId);

                $query = "SELECT DISTINCT project_id FROM ProjectContributions WHERE (user_id, contribution) = (:userId, 'liked')";
                $likeContribution = $this->databaseHandler->queryByColumn($query, $queryVariables);

                $query = "SELECT DISTINCT project_id FROM ProjectContributions WHERE (user_id, contribution) = (:userId, 'commented')";
                $commentContribution = $this->databaseHandler->queryByColumn($query, $queryVariables);

                $query = "SELECT DISTINCT project_id FROM ProjectContributions WHERE (user_id, contribution) = (:userId, 'added_scenario')";
                $scenarioContribution = $this->databaseHandler->queryByColumn($query, $queryVariables);

                $query = "SELECT DISTINCT project_id FROM ProjectContributions WHERE (user_id, contribution) = (:userId, 'joined_role')";
                $roleContribution = $this->databaseHandler->queryByColumn($query, $queryVariables);

                $query = "SELECT DISTINCT project_id FROM ProjectContributions WHERE (user_id, contribution) = (:userId, 'contributed_task')";
                $taskContribution = $this->databaseHandler->queryByColumn($query, $queryVariables);

                return array(
                    'contributions' => array(
                        'like' => $likeContribution['data'],
                        'comment' => $commentContribution['data'],
                        'scenario' => $scenarioContribution['data'],
                        'role' => $roleContribution['data'],
                        'task' => $taskContribution['data']
                    )
                );
            } catch (Exception $error) {
                throw $error;
            }
        }

        private function getLikedProjects($userId) {
            try {
                $query = "SELECT project_id FROM ProjectContributions WHERE (user_id, contribution) = (:userId, 'liked')";
                $queryVariables = array(
                    ':userId' => $userId
                );
                $results = $this->databaseHandler->queryByColumn($query, $queryVariables);
                return $results['data'];
            } catch (Exception $error) {
                throw $error;
            }
        }

        private function getProjectContributions($userId) {
            try {
                $query = "SELECT DISTINCT project_id FROM ProjectContributions WHERE (user_id = :userId)";
                $queryVariables = array(
                    ':userId' => $userId
                );
                $results = $this->databaseHandler->queryByColumn($query, $queryVariables);
                return $results['data'];
            } catch (Exception $error) {
                throw $error;
            }
        }

        private function getProjectCollectionIds($userId, $queryBatchSize = NULL, $queryOffset = 0) {
            try {
                if (is_null($queryBatchSize)) {
                    $queryBatchSize = self::$DEFAULT_BATCH_SIZE;
                }

                $this->databaseHandler->beginTransaction();
                $query = "SELECT id FROM ("
                    . "(SELECT DISTINCT "
                        . "Scenarios.project_id AS id, "
                        . "Projects.created_on as created_on "
                    . "FROM Scenarios "
                    . "INNER JOIN Projects ON (Projects.id = Scenarios.project_id) "
                    . "WHERE Scenarios.user_id = :scenarioContributorId) "
                    . "UNION DISTINCT "
                    . "(SELECT DISTINCT "
                        . "Roles.project_id AS id, "
                        . "Projects.created_on AS created_on "
                    . "FROM Roles "
                    . "INNER JOIN RoleContributors ON (Roles.id = RoleContributors.role_id) "
                    . "INNER JOIN Projects ON (Projects.id = Roles.project_id) "
                    . "WHERE RoleContributors.user_id = :roleContributorId) "
                    . "UNION DISTINCT "
                    . "(SELECT DISTINCT "
                        . "id, "
                        . "created_on "
                    . "FROM Projects "
                    . "WHERE champion_id = :championId)) "
                    . "AS ProjectIds "
                    . "ORDER BY created_on DESC "
                    . "LIMIT :batchSize OFFSET :offset";

                $queryVariables = array(
                    ':scenarioContributorId' => $userId,
                    ':roleContributorId' => $userId,
                    ':championId' => $userId,
                    ':batchSize' => $queryBatchSize,
                    ':offset' => $queryOffset
                );

                $results = $this->databaseHandler->queryByColumn($query, $queryVariables);

                $this->databaseHandler->commitTransaction();
                $projectIds = $results['data'];

                return $projectIds;
            } catch (Exception $error) {
                throw $error;
            }
        }

        //TODO: test out if the project collections are returned correctly
        public function createProjectCollectionModel($userId, $queryBatchSize = NULL, $queryOffset = 0) {
            try {
                if (is_null($queryBatchSize)) {
                    $queryBatchSize = self::$DEFAULT_BATCH_SIZE;
                }

                $projectIds = $this->getProjectCollectionIds($userId, $queryBatchSize, $queryOffset);
                $projects = $this->createSimpleProjectModel($projectIds);

                $userProjectCollectionModel = array(
                    'all' => $projects
                );
                return $userProjectCollectionModel;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function createRoleModel($projectId) {
            try {
                $query = "SELECT DISTINCT id, title AS name FROM Roles WHERE project_id = :projectId";
                $queryVariables = array(
                    ':projectId' => $projectId
                );
                $results = $this->databaseHandler->query($query, $queryVariables);
                $roleModel = $results['data'];

                //TODO: would it be more performance efficient if we do one query for all role contributors for all role ids, and then group them into roleModel with php?
                foreach ($roleModel as $index => $role) {
                    $query = "SELECT DISTINCT user_id AS contributor_id FROM RoleContributors WHERE role_id = :roleId AND status = :status";
                    $queryVariables = array(
                        ':roleId' => $role['id'],
                        ':status' => 'assigned'
                    );
                    $results = $this->databaseHandler->queryByColumn($query, $queryVariables);
                    $contributorIds = $results['data'];
                    $contributors = $this->createUserModel($contributorIds);
                    $roleModel[$index]['contributors'] = $contributors;
                }
                return $roleModel;
            } catch (Exception $error) {
                throw $error;
            }

        }

        //TODO: make this accept multiple task ids and return an array of task models
        public function createTaskModel($taskId) {
            try {
                $queryVariables = array(
                    ':taskId' => $taskId
                );

                $query = "SELECT text, start_date, duration FROM gantt_tasks WHERE id = :taskId";
                $result = $this->databaseHandler->query($query, $queryVariables);
                $ganttChartTask = $result['data'][0];

                $query = "SELECT Users.id, Users.name, Users.title, Users.image, Users.email FROM TaskAssignments JOIN Users ON (TaskAssignments.user_id = Users.id) WHERE task_id = :taskId";
                $results = $this->databaseHandler->query($query, $queryVariables);
                $taskContributors = $results['data'];

                $query = "SELECT * FROM TaskMeetings WHERE task_id = :taskId";
                $results = $this->databaseHandler->query($query, $queryVariables);
                $taskMeetings = $results['data'];
                
                $query = "SELECT * FROM Deliverables WHERE task_id = :taskId";
                $results = $this->databaseHandler->query($query, $queryVariables);

                if ($results['isEmpty']) {
                    $query = "INSERT INTO Deliverables (task_id, description) VALUES (:taskId, :description)";
                    $queryVariables = array(
                        ':taskId' => $taskId,
                        ':description' => 'Please enter your deliverable description by clicking the edit button.'
                    );
                    $result = $this->databaseHandler->query($query, $queryVariables);
                }

                
                $query = "SELECT * FROM Deliverables WHERE task_id = :taskId";
                $queryVariables = array(
                    ':taskId' => $taskId
                );
                $results = $this->databaseHandler->query($query, $queryVariables);
                $taskDeliverables = $results['data'];
                foreach ($taskDeliverables as $index => $taskDeliverable) {
                    $queryVariables = array(
                        ':deliverableId' => $taskDeliverable['id']
                    );

                    $query = "SELECT id, file, created_on FROM DeliverableFiles WHERE deliverable_id = :deliverableId";
                    $results = $this->databaseHandler->query($query, $queryVariables);
                    $taskDeliverables[$index]['files'] = $results['data'];
                }

                return array(
                    'id' => $taskId,
                    'title' => $ganttChartTask['text'],
                    'startDate' => $ganttChartTask['startDate'],
                    'duration' => $ganttChartTask['duration'],
                    'deliverables' => $taskDeliverables,
                    'contributors' => $taskContributors,
                    'meetings' => $taskMeetings
                );
                
            } catch (Exception $error) {
                throw $error;
            }
        }

        //TODO: complete this model to collect all tasks by the project id
        public function createProjectTaskModel($projectId) {
            try {
                $query = "SELECT id FROM ...";
                $queryVariables = array(
                    ':projectId' => $projectId
                );
                $results = $this->databaseHandler->queryByColumn($query, $queryVariables);
                $taskIds = $results['data'];
                $tasks = $this->createTaskModel($taskIds);  //TODO: this is likely to break because createTaskModel can only accept one id at a time currently

                return $tasks;
            } catch (Exception $error) {
                throw $error;
            }
        }

        //TODO: needs testing
        public function createProjectUpdateModel($projectId, $queryBatchSize = NULL, $queryOffset = 0) {
            try {
                if (is_null($queryBatchSize)) {
                    $queryBatchSize = self::$DEFAULT_BATCH_SIZE;
                }

                $query = "SELECT * FROM ProjectUpdates WHERE project_id = :projectId ORDER BY updated_on LIMIT :batchSize OFFSET :offset";
                $queryVariables = array(
                    ':projectId' => $projectId,
                    ':batchSize' => $queryBatchSize,
                    ':offset' => $queryOffset
                );
                $results = $this->databaseHandler->query($query, $queryVariables);
                $projectUpdates = $results['data'];

                foreach ($projectUpdates as $index => $projectUpdate) {
                    $project = $this->createSimpleProjectModel($projectUpdate['projectId']);
                    $contributor = $this->createUserModel($projectUpdate['contributorId']);

                    $projectUpdateModel = array(
                        'id' => $projectUpdate['id'],
                        'project' => $project[0],
                        'contributor' => $contributor[0],
                        'eventType' => $projectUpdate['eventType'],
                        'message' => $projectUpdate['message'],
                        'updatedOn' => $projectUpdate['updatedOn']
                    );

                    $projectUpdates[$index] = $projectUpdateModel;
                }
                return $projectUpdates;
            } catch (Exception $error) {
                throw $error;
            }
        }

        //TODO: needs testing
        public function createNotificationModel($userId, $queryBatchSize = NULL, $queryOffset = 0) {
            try {
                if (is_null($queryBatchSize)) {
                    $queryBatchSize = self::$DEFAULT_BATCH_SIZE;
                }

                $projectIds = $this->getProjectCollectionIds($userId, $queryBatchSize, $queryOffset);
                $numberOfProjects = count($projectIds);

                if ($numberOfProjects > 0) {
                    $this->databaseHandler->beginTransaction();
                    $projectIdMarkers = $this->createMultipleMarkers('?, ', $numberOfProjects);

                    $query = "SELECT * FROM ( "
                        . "(SELECT "
                            . "Notifications.id AS notification_id, "
                            . "NULL AS project_update_id, "
                            . "Notifications.project_id AS project_id, "
                            . "NULL AS contributor_id, "
                            . "Notifications.sender_id AS sender_id, "
                            . "Notifications.receiver_id AS receiver_id, "
                            . "Notifications.event_type AS event_type, "
                            . "Notifications.message AS message, "
                            . "Notifications.updated_on AS updated_on "
                        . "FROM Notifications "
                        . "WHERE Notifications.receiver_id = ?) "
                        . "UNION DISTINCT "
                        . "(SELECT "
                            . "NULL AS notification_id, "
                            . "ProjectUpdates.id AS project_update_id, "
                            . "ProjectUpdates.project_id AS project_id, "
                            . "ProjectUpdates.contributor_id AS contributor_id, "
                            . "NULL AS sender_id, "
                            . "NULL AS receiver_id, "
                            . "ProjectUpdates.event_type AS event_type, "
                            . "ProjectUpdates.message AS message, "
                            . "ProjectUpdates.updated_on AS updated_on "
                        . "FROM ProjectUpdates "
                        . "WHERE ProjectUpdates.project_id IN (" . $projectIdMarkers . ")) "
                        . "UNION DISTINCT "
                        . "(SELECT "
                            . "NULL AS notification_id, "
                            . "NULL AS project_update_id, "
                            . "Projects.id AS project_id, "
                            . "NULL AS contributor_id, "
                            . "NULL AS sender_id, "
                            . "NULL AS receiver_id, "
                            . "NULL AS event_type, "
                            . "NULL AS message, "
                            . "NULL AS updated_on "
                        . "FROM Projects "
                        . "WHERE Projects.id IN (" . $projectIdMarkers . "))) "
                        . "AS UserNotifications "
                        . "WHERE updated_on IS NOT NULL "
                        . "ORDER BY updated_on DESC";

                    $queryVariables = array_merge(array($userId), $projectIds, $projectIds);
                    $results = $this->databaseHandler->query($query, $queryVariables);

                    $notificationModel = $results['data'];
                    $this->databaseHandler->commitTransaction();

                    foreach ($notificationModel as $index => $notification) {
                        $project = $this->createSimpleProjectModel($notification['projectId']);
                        $contributor = $this->createUserModel($notification['contributorId']);
                        $sender = $this->createUserModel($notification['senderId']);
                        $receiver = $this->createUserModel($notification['receiverId']);

                        $isProjectUpdate = !empty($notification['projectUpdateId']);
                        $isNotification = !empty($notification['notificationId']);

                        $notificationModel[$index] = array(
                            'isNotification' => $isNotification,
                            'isProjectUpdate' => $isProjectUpdate,
                            'id' => $isProjectUpdate ? $notification['projectUpdateId'] : $notification['notificationId'],
                            'eventType' => $notification['eventType'],
                            'message' => $notification['message'],
                            'updatedOn' => $notification['updatedOn'],
                            'project' => isEmptyObject($project) ? $project : reset($project),
                            'contributor' => isEmptyObject($contributor) ? $contributor : reset($contributor),
                            'sender' => isEmptyObject($sender) ? $sender : reset($sender),
                            'receiver' => isEmptyObject($receiver) ? $receiver : reset($receiver)
                        );
                    }

                    return $notificationModel;
                }

                return array();
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function createLandingPageWarnings() {
            try {
                return array(
                    'invalidCredentials' => FALSE,
                    'accountExists' => FALSE,
                    'invalidSignupCode' => FALSE,
                    'passwordsMismatch' => FALSE
                );
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function getProjectStageKeys() {
            try {
                $query = "SELECT stage FROM ProjectStageKeys ORDER BY id";
                $results = $this->databaseHandler->queryByColumn($query);
                $projectStageKeys = $results['data'];
                return $projectStageKeys;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function getProjectCategoryKeys() {
            try {
                $query = "SELECT category FROM ProjectCategoryKeys ORDER BY id";
                $results = $this->databaseHandler->queryByColumn($query);
                $projectCategoryKeys = $results['data'];
                return $projectCategoryKeys;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function createMultipleMarkers($marker, $multiplier, $separator = ', ') {
            try {
                return rtrim(str_repeat($marker, $multiplier), $separator);
            } catch (Exception $error) {
                throw $error;
            }
        }
    }
?>
