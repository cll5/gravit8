<?php
    if (!defined('__GRAVIT8__')) {
        $gravit8Directory = dirname(dirname(dirname(__File__)));
        define('__GRAVIT8__', $gravit8Directory);
    }

    require_once __GRAVIT8__ . '/app/commons/utilities.php';
    require_once __GRAVIT8__ . '/app/model/DatabaseHandler.php';

    Class Attachment {
        private $databaseHandler;

        private $id;
        private $name;
        private $size;
        private $type;
        private $extension;
        private $checksum;
        private $totalParts;
        private $totalPartsLength;

        private $attachmentContainsParts = FALSE;
        private $partialAttachmentPaths;
        private $attachmentDirectory;

        public function __construct($name, $extension, $type = '', $size = 0, $totalParts = 1, $checksum = NULL) {
            $this->id = NULL;
            $this->name = $name;
            $this->extension = (($extension !== 'null') && ($extension !== 'undefined')) ? $extension : '';
            $this->type = $this->determineAttachmentType($type);
            $this->size = $size;
            $this->totalParts = $totalParts;
            $this->checksum = (($checksum !== 'null') && ($checksum !== 'undefined')) ? $checksum : $this->calculateChecksum('');

            //get the number of digits of the total parts
            $totalPartsAsString = (string) $totalParts;
            $this->totalPartsLength = strlen($totalPartsAsString);

            //check if this attachment is broken in parts
            $this->attachmentContainsParts = ($this->totalParts > 1);

            $this->attachmentDirectory = __GRAVIT8__ . '/data/attachments/';
            $this->attachmentDirectory = str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, $this->attachmentDirectory);

            //create an array of all the partial attachment names
            if ($this->attachmentContainsParts) {
                $this->partialAttachmentPaths = array_fill(0, $this->totalParts, '');
                for ($index = 0; $index < $this->totalParts; $index++) {
                    $partIndex = $index + 1;

                    //make attachment name
                    // $extension = empty($this->extension) ? '' : '.' . $this->extension;
                    // $attachmentName = $this->checksum . $extension;
                    $attachmentName = $this->name;

                    //pad zeroes to the part number to keep the partial attachment files in order
                    $partExtension = $this->getPartialAttachmentExtension($partIndex);
                    $partialAttachmentPath = $this->attachmentDirectory . $attachmentName . $partExtension;
                    $this->partialAttachmentPaths[$index] = $partialAttachmentPath;
                }
            }

            $this->databaseHandler = DatabaseHandler::getInstance();
        }

        /* Helper methods */
        //partial attachment extension - pad zeroes to the part number to keep the partial attachment files in order
        //creates the extension format '.part001'
        private function getPartialAttachmentExtension($part) {
            $partNumber = str_pad($part, $this->totalPartsLength, '0', STR_PAD_LEFT);
            $partExtension = '.part' . $partNumber;
            return $partExtension;
        }

        //only because it helps to quickly segregate between image and non-image attachments
        private function determineAttachmentType($mimeType) {
            try {
                if (empty($mimeType)) {
                    $type = $mimeType;
                } else {
                    preg_match('/(.+)\/(.+)/', $mimeType, $matches);
                    $type = $matches[1];
                }

                switch ($type) {
                    case 'image':
                    case 'audio':
                    case 'video':
                        return $type;
                        break;

                    default:
                        return 'other';
                }
            } catch (Exception $error) {
                throw $error;
            }
        }

        //the algorithm used to calculate the checksum needs to be the same in the client side
        private function calculateChecksum($data, $asBinaryFormat = FALSE) {
            try {
                //client side uses SHA-1 as the hashing algorithm
                return sha1($data, $asBinaryFormat);
            } catch (Exception $error) {
                throw $error;
            }
        }

        //check the data integrity by comparing checksums
        private function isAttachmentCorrupted($attachmentPath, $expectedChecksum) {
            //use sha1 to generate the checksum
            //$this->calculateChecksum($fileBinary);
            return FALSE;
        }

        /* Database storage methods */
        //TODO: does this need uploaded and checksum as optional parameters?
        private function addPartialAttachmentsInDatabase() {
            try {
                if ($this->attachmentContainsParts) {
                    //create multiple value markers of the form (attachment_id, part, uploaded)
                    $queryVariables = array();
                    $values = '';
                    for ($part = 1; $part <= $this->totalParts; $part++) {
                        $attachmentParameter = ':attachmentId' . $part;
                        $queryVariables[$attachmentParameter] = $this->id;

                        $partParameter = ':part' . $part;
                        $queryVariables[$partParameter] = $part;

                        $uploadedParameter = ':uploaded' . $part;
                        $queryVariables[$uploadedParameter] = FALSE;

                        $values .= '(' . $attachmentParameter . ',' . $partParameter . ',' . $uploadedParameter . '),';
                    }
                    $values = substr($values, 0, -1);

                    $query = "INSERT IGNORE INTO PartialAttachment (attachment_id, part, uploaded) VALUES " . $values;
                    $result = $this->databaseHandler->query($query, $queryVariables);
                    return $result['isSuccess'];
                }
            } catch (Exception $error) {
                throw $error;
            }
        }

        private function arePartialAttachmentsInDatabase() {
            try {
                $query = "SELECT COUNT(*) FROM PartialAttachment WHERE attachment_id = :attachmentId";
                $queryVariables = array(
                    ':attachmentId' => $this->id
                );
                $result = $this->databaseHandler->queryByColumn($query, $queryVariables);
                $numberOfPartialAttachmentsRecorded = reset($result['data']);
                return ($numberOfPartialAttachmentsRecorded === $this->totalParts);
            } catch (Exception $error) {
                throw $error;
            }
        }

        //$parts can be an array of part
        private function savePartialAttachmentsToDatabase($parts, $uploaded = TRUE) {
            try {
                if ($this->attachmentContainsParts && !empty($parts)) {
                    $queryVariables = array(
                        ':uploaded' => $uploaded
                    );

                    if (is_int($parts)) {
                        //format query differently for single partial attachment update
                        $parameters = '(:attachmentId, :part)';
                        $queryVariables[':attachmentId'] = $this->id;
                        $queryVariables[':part'] = $parts;
                    } else if (is_array($parts)) {
                        //create multiple value markers of the form (attachment_id, part)
                        $parameters = '';
                        foreach ($parts as $part) {
                            $attachmentParameter = ':attachmentId' . $part;
                            $queryVariables[$attachmentParameter] = $this->id;

                            $partParameter = ':part' . $part;
                            $queryVariables[$partParameter] = $part;

                            $parameters .= '(' . $attachmentParameter . ',' . $partParameter . '),';
                        }
                        $parameters = substr($parameters, 0, -1);
                    }

                    $query = "UPDATE PartialAttachment SET uploaded = :uploaded WHERE (attachment_id, part) IN (" . $parameters . ")";
                    $result = $this->databaseHandler->query($query, $queryVariables);
                    return $result['isSuccess'];
                }
            } catch (Exception $error) {
                throw $error;
            }
        }

        private function isAllPartialAttachmentsUploaded() {
            try {
                if ($this->attachmentContainsParts) {
                    $query = "SELECT COUNT(*) FROM PartialAttachment WHERE (attachment_id, uploaded) = (:attachmentId, :uploaded)";
                    $queryVariables = array(
                        ':attachmentId' => $this->id,
                        ':uploaded' => TRUE
                    );
                    $result = $this->databaseHandler->queryByColumn($query, $queryVariables);
                    $numberOfUploadedPartialAttachments = reset($result['data']);
                    return ($numberOfUploadedPartialAttachments === $this->totalParts);
                }
                return FALSE;
            } catch (Exception $error) {
                throw $error;
            }
        }

        private function removePartialAttachmentsFromDatabase() {
            try {
                $query = "DELETE IGNORE FROM PartialAttachment WHERE attachment_id = :attachmentId";
                $queryVariables = array(
                    ':attachmentId' => $this->id
                );
                $result = $this->databaseHandler->query($query, $queryVariables);
                return $result['isSuccess'];
            } catch (Exception $error) {
                throw $error;
            }
        }

        private function addAttachmentToDatabase($uploaded = FALSE) {
            try {
                $query = "INSERT INTO Attachment (type_id, name, extension, size, total_parts, uploaded, checksum) VALUES ((SELECT id FROM AttachmentType WHERE (attachment_type = :attachmentType)), :attachmentName, :attachmentExtension, :attachmentSize, :totalParts, :uploaded, :checksum)";
                $queryVariables = array(
                    ':attachmentType' => $this->type,
                    ':attachmentName' => $this->name,
                    ':attachmentExtension' => $this->extension,
                    ':attachmentSize' => $this->size,
                    ':totalParts' => $this->totalParts,
                    ':uploaded' => $uploaded,
                    ':checksum' => $this->checksum
                );
                $result = $this->databaseHandler->query($query, $queryVariables);
                $this->id = $this->databaseHandler->getLastInsertId();
                return $result['isSuccess'];
            } catch (Exception $error) {
                throw $error;
            }
        }

        private function doesAttachmentExistsInDatabase() {
            try {
                if (is_null($this->id)) {
                    return FALSE;
                }

                $query = "SELECT * FROM Attachment WHERE id = :attachmentId";
                $queryVariables = array(
                    ':attachmentId' => $this->id
                );
                $result = $this->databaseHandler->query($query, $queryVariables);
                return ($result['isSuccess'] && !$result['isEmpty']);
            } catch (Exception $error) {
                throw $error;
            }
        }

        private function saveAttachmentToDatabase($uploaded = FALSE) {
            try {
                $query = "UPDATE Attachment SET uploaded = :uploaded WHERE id = :attachmentId";
                $queryVariables = array(
                    ':attachmentId' => $this->id,
                    ':uploaded' => $uploaded
                );
                $result = $this->databaseHandler->query($query, $queryVariables);
                return $result['isSuccess'];
            } catch (Exception $error) {
                throw $error;
            }
        }

        private function updateAttachmentChecksumInDatabase($checksum) {
            try {
                $query = "UPDATE Attachment SET checksum = :checksum WHERE id = :attachmentId";
                $queryVariables = array(
                    ':attachmentId' => $this->id,
                    ':checksum' => $checksum
                );
                $result = $this->databaseHandler->query($query, $queryVariables);
                return $result['isSuccess'];
            } catch (Exception $error) {
                throw $error;
            }
        }

        /* File system methods */
        private function saveAttachmentToFileSystem(&$rawAttachment, $part) {
            try {
                //create the directory if it doesn't exists
                if (!file_exists($this->attachmentDirectory)) {
                    $isDirectoryCreated = mkdir($this->attachmentDirectory, 0740, TRUE);
                    if ($isDirectoryCreated === FALSE) {
                        throw new Exception('>>> ERROR: Could not create attachment folder');
                    }
                }

                //create the file path
                $extension = empty($this->extension) ? '' : '.' . $this->extension;
                $attachmentName = $this->id . $extension;
                if ($this->attachmentContainsParts) {
                    $partExtension = $this->getPartialAttachmentExtension($part);
                    // $attachmentName .= $partExtension;
                    $attachmentName = $this->name . $partExtension;
                }
                $attachmentPath = $this->attachmentDirectory . $attachmentName;

                //save the attachment to the file system
                $filePointer = fopen($attachmentPath, 'cb');
                $writtenBytes = fwrite($filePointer, $rawAttachment);
                $isFileClosed = fclose($filePointer);
                if (($filePointer === FALSE) || ($writtenBytes === FALSE) || ($isFileClosed === FALSE)) {
                    //remove the file if it fails to create
                    if ($filePointer !== FALSE) {
                        $isFileRemoved = unlink($attachmentPath);
                        if ($isFileRemoved === FALSE) {
                            throw new Exception('>>> ERROR: Could not delete the attachment ' . $attachmentName . ' in ' . $this->attachmentDirectory);
                        }
                    }
                    throw new Exception('>>> ERROR: Failed to save attachment ' . $attachmentName . ' to server');
                }

                //check if there were any data corruption in the saved attachment
                $isAttachmentCorrupted = $this->isAttachmentCorrupted($attachmentPath, $this->checksum);
                if ($isAttachmentCorrupted) {
                    //TODO: what about the attachment/partial attachments on the file system and database?
                    throw new Exception('>>> ERROR: Attachment is corrupted.');
                }

                return TRUE;
            } catch (Exception $error) {
                throw $error;
            }
        }

        private function mergePartialAttachmentsInFileSystem() {
            try {
                //create a new empty attachment for writing
                $extension = empty($this->extension) ? '' : '.' . $this->extension;
                $attachmentName = $this->id . $extension;
                $attachmentPath = $this->attachmentDirectory . $attachmentName;
                $attachmentPointer = fopen($attachmentPath, 'cb');
                if ($attachmentPointer === FALSE) {
                    throw new Exception('>>> ERROR: Failed to create main attachment file for ' . $attachmentName);
                }

                //merge the partial attachments together into the new empty attachment
                //use for loop instead of foreach to ensure the correct order of partial attachment merging
                $BYTES_TO_READ = 4096;
                for ($index = 0; $index < $this->totalParts; $index++) {
                    $partialAttachmentPath = $this->partialAttachmentPaths[$index];
                    $partialAttachmentPointer = fopen($partialAttachmentPath, 'rb');

                    //append the partial attachment data to the merged attachment file
                    $writtenBytes = TRUE;
                    while ($writtenBytes && ($dataBuffer = fread($partialAttachmentPointer, $BYTES_TO_READ))) {
                        $writtenBytes = fwrite($attachmentPointer, $dataBuffer);
                    }

                    $isFileClosed = fclose($partialAttachmentPointer);
                    if (($partialAttachmentPointer === FALSE) || ($dataBuffer === FALSE) || ($writtenBytes === FALSE) || ($isFileClosed === FALSE)) {
                        throw new Exception('>>> ERROR: Unable to open, read, or close the partial attachment at ' . $partialAttachmentPath);
                    }
                }

                //close the merged attachment file
                $isFileClosed = fclose($attachmentPointer);
                if ($isFileClosed === FALSE) {
                    throw new Exception('>>> ERROR: Failed to save attachment ' . $filename . ' to server');
                }

                //check the integrity of the merged attachment
                $isAttachmentCorrupted = $this->isAttachmentCorrupted($attachmentPath, $this->checksum);
                if ($isAttachmentCorrupted) {
                    //TODO: what about the attachment/partial attachments on the file system and database?
                    throw new Exception('>>> ERROR: Merged attachment is corrupted.');
                }

                //delete the partial attachments only if the merging was successful
                foreach ($this->partialAttachmentPaths as $partialAttachmentPath) {
                    $isFileRemoved = unlink($partialAttachmentPath);
                    if ($isFileRemoved === FALSE) {
                        throw new Exception('>>> ERROR: Could not delete the partial attachment ' . $partialAttachmentPath . ' after merging.');
                    }
                }

                return TRUE;
            } catch (Exception $error) {
                throw $error;
            }
        }

        /* Upload handlers */
        //attachment upload handler
        public function saveAttachmentToDirectory(&$rawAttachment, $part = 1) {
            try {
                $isSuccessful = TRUE;

                //1. handle attachment tracking in database and generate a unique attachment id
                //add a record of the attachment to the database if it hasn't been added yet (excluding partial attachments)
                if (is_null($this->id) || (!$this->doesAttachmentExistsInDatabase())) {
                    $isAddSuccessful = $this->addAttachmentToDatabase();
                    $isSuccessful = $isSuccessful && $isAddSuccessful;
                }

                //2. save the attachment to the file system
                $isSaveSuccessful = $this->saveAttachmentToFileSystem($rawAttachment, $part);
                $isSuccessful = $isSuccessful && $isSaveSuccessful;

                if (!$this->attachmentContainsParts) {
                    //update the attachment in the database as uploaded
                    $isUpdateSuccessful = $this->saveAttachmentToDatabase(TRUE);
                    $isSuccessful = $isSuccessful && $isUpdateSuccessful;
                }

                //3. handle partial attachments in the database
                if ($this->attachmentContainsParts) {
                    //add records of partial attachments into the database if they are not added yet
                    if (!$this->arePartialAttachmentsInDatabase()) {
                        $this->addPartialAttachmentsInDatabase();
                    }

                    //update this partial attachment on the database as uploaded
                    $this->savePartialAttachmentsToDatabase($part, TRUE);

                    //determine if all partial attachments are already uploaded
                    if ($this->isAllPartialAttachmentsUploaded()) {
                        //merge the partial attachments together, check for any corruption, and delete the partial files from the file system
                        $isMergeSuccessful = $this->mergePartialAttachmentsInFileSystem();
                        $isSuccessful = $isSuccessful && $isMergeSuccessful;

                        //update the database records of the attachment and partial attachments
                        //clean up the partial attachment records from the database
                        $isRemoveSuccessful = $this->removePartialAttachmentsFromDatabase();
                        $isSuccessful = $isSuccessful && $isRemoveSuccessful;

                        //update the attachment in the database as uploaded
                        $isUpdateSuccessful = $this->saveAttachmentToDatabase(TRUE);
                        $isSuccessful = $isSuccessful && $isUpdateSuccessful;

                        //update the attachment checksum
                        $isUpdateSuccessful = $this->updateAttachmentChecksumInDatabase($this->checksum);
                        $isSuccessful = $isSuccessful && $isUpdateSuccessful;
                    }
                }

                return $isSuccessful;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function setAttachmentId($attachmentId) {
            if (is_int($attachmentId) && ($attachmentId > 0)) {
                $this->id = $attachmentId;
            }
        }

        public function getAttachmentId() {
            return $this->id;
        }
    }
?>