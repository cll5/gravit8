<?php
    if (!defined('__GRAVIT8__')) {
        $gravit8Directory = dirname(dirname(dirname(__File__)));
        define('__GRAVIT8__', $gravit8Directory);
    }

    require_once __GRAVIT8__ . '/app/commons/utilities.php';
    require_once __GRAVIT8__ . '/app/commons/interfaces/Singleton.php';
    require_once __GRAVIT8__ . '/app/model/DatabaseHandler.php';
    require_once __GRAVIT8__ . '/app/model/OrganizationHandler.php';
    require_once __GRAVIT8__ . '/app/model/UserHandler.php';
    require_once __GRAVIT8__ . '/app/model/AttachmentHandler.php';

    Class EntityHandler implements Singleton {
        private static $instance;
        protected $databaseHandler;
        protected $organizationHandler;
        protected $userHandler;
        protected $attachmentHandler;

        public function __construct() {
            if (self::$instance !== NULL) {
                return self::$instance;
            }

            $this->databaseHandler = DatabaseHandler::getInstance();
            $this->organizationHandler = OrganizationHandler::getInstance();
            $this->userHandler = UserHandler::getInstance();
            $this->attachmentHandler = AttachmentHandler::getInstance();
        }

        public static function getInstance() {
            if (self::$instance === NULL) {
                self::$instance = new EntityHandler();
            }

            return self::$instance;
        }

        public function isEntityInOrganization($entityId, $organizationModel) {
            try {
                $query = "SELECT * FROM EntityOrganization WHERE (entity_id, organization_id, organization_group_id) = (:entityId, :organizationId, :organizationGroupId)";
                $queryVariables = array(
                    ':entityId' => $entityId,
                    ':organizationId' => $organizationModel['organizationId'],
                    ':organizationGroupId' => $organizationModel['organizationGroupId']
                );
                $result = $this->databaseHandler->query($query, $queryVariables);

                return !$result['isEmpty'];
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function createBasicEntityModel($entityId) {
            try {
                $entityModel = $this->defaultEntityModel();

                $query = "SELECT Entity.*, EntityType.entity_type FROM Entity JOIN EntityType ON (Entity.entity_type_id = EntityType.id) WHERE (Entity.id = :entityId)";
                $queryVariables = array(
                    ':entityId' => $entityId
                );
                $result = $this->databaseHandler->query($query, $queryVariables);
                if (!$result['isEmpty']) {
                    $entity = reset($result['data']);
                    $entityModel['id'] = $entity['id'];
                    $entityModel['entityTypeId'] = $entity['entityTypeId'];
                    $entityModel['entityType'] = $entity['entityType'];
                    $entityModel['isPrivate'] = $entity['isPrivate'];
                    $entityModel['creator'] = $this->userHandler->createBasicUserProfileModel($entity['creatorId']);
                    $entityModel['createdOn'] = $entity['createdOn'];
                }

                return $entityModel;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public static function defaultEntityModel() {
            return array(
                'id' => NULL,
                'entityTypeId' => NULL,
                'entityType' => NULL,
                'isPrivate' => FALSE,
                'creator' => UserHandler::defaultBasicUserProfile(),
                'createdOn' => NULL
            );
        }

        protected function getEntityCategories($entityId) {
            try {
                $categories = array();

                $query = "SELECT EntityCategory.id, EntityCategory.category_id, Category.name FROM EntityCategory LEFT JOIN Category ON (EntityCategory.category_id = Category.id) WHERE (EntityCategory.entity_id = :entityId)";
                $queryVariables = array(
                    ':entityId' => $entityId
                );
                $result = $this->databaseHandler->query($query, $queryVariables);

                if (!$result['isEmpty']) {
                    $categories = $result['data'];
                }

                return $categories;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function numberOfLikeForEntity($entityId) {
            try {
                $query = "SELECT COUNT(*) FROM EntityLiked WHERE (entity_id = :entityId)";
                $queryVariables = array(
                    ':entityId' => $entityId
                );
                $result = $this->databaseHandler->queryByColumn($query, $queryVariables);
                return reset($result['data']);
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function likeEntity($entityId, $userId) {
            try {
                $query = "INSERT IGNORE INTO EntityLiked (entity_id, user_id) VALUE (:entityId, :userId)";
                $queryVariables = array(
                    ':entityId' => $entityId,
                    ':userId' => $userId
                );
                $result = $this->databaseHandler->query($query, $queryVariables);
                return $result['isSuccess'];
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function dislikeEntity($entityId, $userId) {
            try {
                $query = "DELETE IGNORE FROM EntityLiked WHERE (entity_id, user_id) = (:entityId, :userId) LIMIT 1";
                $queryVariables = array(
                    ':entityId' => $entityId,
                    ':userId' => $userId
                );
                $result = $this->databaseHandler->query($query, $queryVariables);
                return $result['isSuccess'];
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function isEntityLikedByUser($entityId, $userId) {
            try {
                $query = "SELECT * FROM EntityLiked WHERE (entity_id, user_id) = (:entityId, :userId)";
                $queryVariables = array(
                    ':entityId' => $entityId,
                    ':userId' => $userId
                );
                $result = $this->databaseHandler->query($query, $queryVariables);
                return !$result['isEmpty'];
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function voteEntity($entityId, $userId) {
            try {
                $query = "INSERT IGNORE INTO EntityVoted (entity_id, user_id, entity_type_id) VALUES (:entityId, :userId, (SELECT entity_type_id FROM Entity WHERE (id = :id)))";
                $queryVariables = array(
                    ':entityId' => $entityId,
                    ':userId' => $userId,
                    ':id' => $entityId
                );
                $result = $this->databaseHandler->query($query, $queryVariables);
                return $result['isSuccess'];
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function unvoteEntity($entityId, $userId) {
            try {
                $query = "DELETE IGNORE FROM EntityVoted WHERE (entity_id, user_id) = (:entityId, :userId)";
                $queryVariables = array(
                    ':entityId' => $entityId,
                    ':userId' => $userId
                );
                $result = $this->databaseHandler->query($query, $queryVariables);
                return $result['isSuccess'];
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function isEntityVotedByUser($entityId, $userId) {
            try {
                $query = "SELECT * FROM EntityVoted WHERE (entity_id, user_id) = (:entityId, :userId)";
                $queryVariables = array(
                    ':entityId' => $entityId,
                    ':userId' => $userId
                );
                $result = $this->databaseHandler->query($query, $queryVariables);
                return !$result['isEmpty'];
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function canUserStillVote($userId, $entityId, $organizationId, $organizationGroupId) {
            try {
                $query = "SELECT maximum_vote FROM EntityVoteLimit WHERE (organization_id = :organizationId)";
                $queryVariables = array(
                    ':organizationId' => $organizationId
                );
                $result = $this->databaseHandler->queryByColumn($query, $queryVariables);

                if ($result['isEmpty']) {
                    //if the organization doesn't exist? then what?
                    throw new Exception('>>> EntityVoteLimit does not have a record for organization id: ' . $organizationId);
                }

                $voteLimit = reset($result['data']);

                //NOTE: NULL is defined to be unlimited votes
                if (is_null($voteLimit)) {
                    return TRUE;
                }

                //check if user can still vote for this entity type
                $query = "SELECT COUNT(EntityVoted.entity_id) AS votes_used " .
                    "FROM " .
                        "EntityOrganization " .
                        "INNER JOIN EntityVoted ON (EntityOrganization.entity_id = EntityVoted.entity_id) " .
                    "WHERE " .
                        "(organization_id, organization_group_id, user_id, entity_type_id) = (:organizationId, :organizationGroupId, :userId, (" .
                            "SELECT entity_type_id FROM Entity WHERE (id = :entityId)" .
                        ")) " .
                    "HAVING votes_used < :voteLimit";

                $queryVariables = array(
                    ':organizationId' => $organizationId,
                    ':organizationGroupId' => $organizationGroupId,
                    ':userId' => $userId,
                    ':entityId' => $entityId,
                    ':voteLimit' => $voteLimit
                );

                $result = $this->databaseHandler->queryByColumn($query, $queryVariables);
                return !$result['isEmpty'];
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function votesByUser($userId, $organizationId, $organizationGroupId) {
            try {
                //get the vote limit for this organization
                $query = "SELECT maximum_vote FROM EntityVoteLimit WHERE (organization_id = :organizationId)";
                $queryVariables = array(
                    ':organizationId' => $organizationId
                );
                $result = $this->databaseHandler->queryByColumn($query, $queryVariables);
                $voteLimit = reset($result['data']);

                if ($result['isEmpty']) {
                    //if the organization doesn't exist? then what?
                    throw new Exception('>>> EntityVoteLimit does not have a record for organization id: ' . $organizationId);
                }

                //get the number of votes used by this user grouped by entity types
                $query = "SELECT entity_type_id, entity_type, SUM(votes_used) AS votes_used " .
                        "FROM (" .
                            "(SELECT id AS entity_type_id, entity_type, 0 AS votes_used " .
                            "FROM EntityType) " .

                            "UNION DISTINCT " .

                            "(SELECT entity_type_id, entity_type, COUNT(entity_id) AS votes_used " .
                            "FROM Entity " .
                            "JOIN EntityType ON (Entity.entity_type_id = EntityType.id) " .
                            "JOIN EntityOrganization ON (Entity.id = EntityOrganization.entity_id) " .
                            "WHERE (organization_id, organization_group_id) = (:organizationId, :organizationGroupId) " .
                                "AND entity_id IN (" .
                                    "SELECT entity_id FROM EntityVoted WHERE (user_id = :userId)" .
                                ") GROUP BY entity_type_id, entity_type)" .
                        ") AS VotesByUser " .
                        "GROUP BY entity_type_id, entity_type " .
                        "ORDER BY entity_type_id ASC";

                $queryVariables = array(
                    ':organizationId' => $organizationId,
                    ':organizationGroupId' => $organizationGroupId,
                    ':userId' => $userId
                );
                $result = $this->databaseHandler->query($query, $queryVariables);
                $votesByEntityTypes = $result['data'];

                //construct the output array
                $votesByUser = array();
                foreach ($votesByEntityTypes as $votesByEntityType) {
                    $entityType = $votesByEntityType['entityType'];

                    //NOTE: NULL is defined to be unlimited votes, so $votesUsed and $votesRemaining are not bounded if $voteLimit is NULL
                    $votesUsed = (int) $votesByEntityType['votesUsed'];
                    $votesUsed = is_null($voteLimit) ? $votesUsed : min($votesUsed, $voteLimit);
                    $votesRemaining = is_null($voteLimit) ? $voteLimit : max(($voteLimit - $votesUsed), 0);

                    $votesByUser[$entityType] = array(
                        'votesUsed' => $votesUsed,
                        'votesRemaining' => $votesRemaining,
                        'voteLimit' => $voteLimit
                    );
                }
                return $votesByUser;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function numberOfVoteForEntity($entityId) {
            try {
                $query = "SELECT COUNT(*) FROM EntityVoted WHERE (entity_id = :entityId)";
                $queryVariables = array(
                    ':entityId' => $entityId
                );
                $result = $this->databaseHandler->queryByColumn($query, $queryVariables);
                return reset($result['data']);
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function numberOfCommentInEntity($entityId) {
            try {
                $query = "SELECT COUNT(*) FROM EntityComment WHERE (entity_id = :entityId)";
                $queryVariables = array(
                    ':entityId' => $entityId
                );
                $result = $this->databaseHandler->queryByColumn($query, $queryVariables);
                return reset($result['data']);
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function createEntityContentModel($entityContentId = NULL) {
            try {
                $entityContentModel = $this->defaultEntityContentModel();
                if (is_null($entityContentId)) {
                    return $entityContentModel;
                }

                $query = "SELECT EntityContent.*, ContentType.content_type FROM EntityContent INNER JOIN ContentType ON (EntityContent.content_type_id = ContentType.id) WHERE (EntityContent.id = :entityContentId)";
                $queryVariables = array(
                    ':entityContentId' => $entityContentId
                );
                $result = $this->databaseHandler->query($query, $queryVariables);

                if (!$result['isEmpty']) {
                    $entityContent = reset($result['data']);
                    $entityContentModel['id'] = $entityContent['id'];
                    $entityContentModel['contentTypeId'] = $entityContent['contentTypeId'];
                    $entityContentModel['contentType'] = $entityContent['contentType'];
                    $entityContentModel['creator'] = $this->userHandler->createBasicUserProfileModel($entityContent['creatorId']);
                    $entityContentModel['createdOn'] = $entityContent['createdOn'];
                }

                return $entityContentModel;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public static function defaultEntityContentModel() {
            return array(
                'id' => NULL,
                'contentTypeId' => NULL,
                'contentType' => NULL,
                'creator' => UserHandler::defaultBasicUserProfile(),
                'createdOn' => NULL
            );
        }

        public function listAllEntityContentsForEntity($entityId) {
            try {
                $query = "SELECT EntityContent.id, EntityContent.content_type_id, ContentType.content_type FROM EntityContent LEFT JOIN ContentType ON (EntityContent.content_type_id = ContentType.id) WHERE (EntityContent.entity_id = :entityId)";
                $queryVariables = array(
                    ':entityId' => $entityId
                );
                $result = $this->databaseHandler->query($query, $queryVariables);
                return $result['data'];
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function addNewEntity($entityType, $organizationModel, $creatorId, $createdOn = NULL, $isPrivate = FALSE) {
            try {
                $query = "SELECT id FROM EntityType WHERE entity_type = :entityType";
                $queryVariables = array(
                    ':entityType' => $entityType
                );
                $result = $this->databaseHandler->queryByColumn($query, $queryVariables);
                $entityTypeId = reset($result['data']);

                $query = "INSERT INTO Entity (entity_type_id, creator_id, is_private, created_on) VALUES (:entityTypeId, :creatorId, :isPrivate, :createdOn)";
                $queryVariables = array(
                    ':entityTypeId' => $entityTypeId,
                    ':creatorId' => $creatorId,
                    ':isPrivate' => $isPrivate,
                    ':createdOn' => $createdOn
                );
                $result = $this->databaseHandler->query($query, $queryVariables);

                $entityId = $this->databaseHandler->getLastInsertId();
                $query = "INSERT INTO EntityOrganization (entity_id, organization_id, organization_group_id) VALUES (:entityId, :organizationId, :organizationGroupId)";
                $queryVariables = array(
                    ':entityId' => $entityId,
                    ':organizationId' => $organizationModel['organizationId'],
                    ':organizationGroupId' => $organizationModel['organizationGroupId']
                );
                $result = $this->databaseHandler->query($query, $queryVariables);

                if (!$result['isSuccess']) {
                    throw New Exception('>>> Could not add new entity');
                }

                //return the entity id
                return $entityId;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function addCategoriesByIds($entityId, $categoryIds) {
            try {
                foreach ($categoryIds as $categoryId) {
                    $query = "INSERT INTO EntityCategory (entity_id, category_id) VALUES (:entityId, :categoryId)";
                    $queryVariables = array(
                        ':entityId' => $entityId,
                        ':categoryId' => $categoryId
                    );
                    $result = $this->databaseHandler->query($query, $queryVariables);

                    if (!$result['isSuccess']) {
                        throw New Exception('>>> Could not add categories to entity');
                    }
                }
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function addNewEntityContent($entityId, $entityContentType, $creatorId = NULL, $createdOn = NULL) {
            try {
                //get the entity content type id
                $query = "SELECT id FROM ContentType WHERE content_type = :contentType";
                $queryVariables = array(
                    ':contentType' => $entityContentType
                );
                $result = $this->databaseHandler->queryByColumn($query, $queryVariables);
                $entityContentTypeId = reset($result['data']);

                //add the new content type into the database if it doesn't exists
                if ($result['isEmpty']) {
                    $query = "INSERT INTO ContentType (content_type) VALUES (:newContentType)";
                    $queryVariables = array(
                        ':newContentType' => $entityContentType
                    );
                    $this->databaseHandler->query($query, $queryVariables);
                    $entityContentTypeId = $this->databaseHandler->getLastInsertId();
                }

                //add the new entity content
                $query = "INSERT INTO EntityContent (content_type_id, entity_id, creator_id, created_on) VALUES (:contentTypeId, :entityId, :creatorId, :createdOn)";
                $queryVariables = array(
                    ':contentTypeId' => $entityContentTypeId,
                    ':entityId' => $entityId,
                    ':creatorId' => $creatorId,
                    ':createdOn' => $createdOn
                );
                $result = $this->databaseHandler->query($query, $queryVariables);

                if (!$result['isSuccess']) {
                    throw New Exception('>>> Could not add new entity content to entity');
                }

                //return the entity content id
                return $this->databaseHandler->getLastInsertId();
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function addNewDescription($entityId, $description, $creatorId = NULL, $createdOn = NULL) {
            try {
                $this->addNewTextBody($entityId, 'description', $description, $creatorId, $createdOn);
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function addNewBackground($entityId, $background, $creatorId = NULL, $createdOn = NULL) {
            try {
                $this->addNewTextBody($entityId, 'background', $background, $creatorId, $createdOn);
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function addNewStakeholder($entityId, $stakeholder, $creatorId = NULL, $createdOn = NULL) {
            try {
                $this->addNewTextBody($entityId, 'stakeholder', $stakeholder, $creatorId, $createdOn);
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function addNewExistingSolution($entityId, $existingSolution, $creatorId = NULL, $createdOn = NULL) {
            try {
                $this->addNewTextBody($entityId, 'existing solution', $existingSolution, $creatorId, $createdOn);
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function addNewMarketAnalysis($entityId, $marketAnalysis, $creatorId = NULL, $createdOn = NULL) {
            try {
                $this->addNewTextBody($entityId, 'market analysis', $marketAnalysis, $creatorId, $createdOn);
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function addNewOtherInformation($entityId, $otherInformation, $creatorId = NULL, $createdOn = NULL) {
            try {
                $this->addNewTextBody($entityId, 'other', $otherInformation, $creatorId, $createdOn);
            } catch (Exception $error) {
                throw $error;
            }
        }

        private function addNewTextBody($entityId, $textBodyType, $textBody, $creatorId = NULL, $createdOn = NULL) {
            try {
                $entityContentId = $this->addNewEntityContent($entityId, 'text body', $creatorId, $createdOn);
                $query = "INSERT INTO TextBody (entity_content_id, text_body) VALUES (:entityContentId, :textBody)";
                $queryVariables = array(
                    ':entityContentId' => $entityContentId,
                    ':textBody' => $textBody
                );
                $result = $this->databaseHandler->query($query, $queryVariables);

                $query = "UPDATE TextBody SET text_body_type_id = (SELECT id FROM TextBodyType WHERE (text_body_type = :textBodyType)) WHERE (entity_content_id = :entityContentId)";
                $queryVariables = array(
                    ':entityContentId' => $entityContentId,
                    ':textBodyType' => $textBodyType
                );
                $result = $this->databaseHandler->query($query, $queryVariables);

                if (!$result['isSuccess']) {
                    throw New Exception('>>> Could not add new text body (' . $textBodyType . ') to entity');
                }
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function addNewRole($entityId, $role, $assigneeIds = [], $creatorId = NULL, $createdOn = NULL) {
            try {
                $roleId = $this->addNewEntityContent($entityId, 'role', $creatorId, $createdOn);
                $query = "INSERT INTO Role (entity_content_id, role) VALUES (:entityContentId, :role)";
                $queryVariables = array(
                    ':entityContentId' => $roleId,
                    ':role' => $role
                );
                $result = $this->databaseHandler->query($query, $queryVariables);

                if (!$result['isSuccess']) {
                    throw New Exception('>>> Could not add new role to entity');
                }

                //TODO: figure out why executing a multiple values insert fail with the error "SQLSTATE[HY093]: Invalid parameter number"
                //Reason: PDO doesn't let you reuse a named parameter. See: http://stackoverflow.com/questions/21777516/pdo-bindparam-with-multiple-named-parameters
                //add the assignees to the role
                if (!empty($assigneeIds)) {
                    // $queryVariables = array(
                    //     ':roleId' => $roleId
                    // );

                    // //create the values of the query
                    // $values = '';
                    // foreach ($assigneeIds as $index => $assigneeId) {
                    //     $assigneeKey = ':assignee' . $index;
                    //     $values .= '(:roleId,' . $assigneeKey . '),';
                    //     $queryVariables[$assigneeKey] = $assigneeId;
                    // }

                    // //remove the last comma in the query
                    // if (endsWith($values, ',')) {
                    //     $values = substr($values, 0, -1);
                    // }

                    // $query = "INSERT INTO RoleAssignee (role_id, assignee_id) VALUES " . $values;
                    // $result = $this->databaseHandler->query($query, $queryVariables);

                    // if (!$result['isSuccess']) {
                    //     throw New Exception('>>> Could not add new assignee to role');
                    // }

                    foreach ($assigneeIds as $assigneeId) {
                        $query = "INSERT INTO RoleAssignee (role_id, assignee_id) VALUES (:roleId, :assigneeId)";
                        $queryVariables = array(
                            ':roleId' => $roleId,
                            ':assigneeId' => $assigneeId
                        );
                        $result = $this->databaseHandler->query($query, $queryVariables);

                        if (!$result['isSuccess']) {
                            throw new Exception('>>> Could not add new assignee to role');
                        }
                    }
                }

                return $roleId;
            } catch (Exception $error) {
                throw $error;
            }
        }

        //TODO: implement or how is this method used?
        private function saveAttachmentToServer($entityId, $attachmentDataURL, $attachmentProperties) {
            try {

            } catch (Exception $error) {
                throw $error;
            }
        }

        public function addNewAttachment($attachmentId, $entityId) {
            try {
                $query = "INSERT IGNORE INTO EntityAttachment (entity_id, attachment_id) VALUES (:entityId, :attachmentId)";
                $queryVariables = array(
                    ':entityId' => $entityId,
                    ':attachmentId' => $attachmentId
                );
                $result = $this->databaseHandler->query($query, $queryVariables);

                return $result['isSuccess'];
            } catch (Exception $error) {
                throw $error;
            }
        }

        public static function defaultAttachmentsModel() {
            return array(
                'images' => array(),
                'others' => array()
            );
        }

        public function defaultAttachmentModel() {
            return array(
                'id' => NULL,
                'name' => NULL,
                'path' => NULL,
                'size' => 0,
                'formattedSize' => 0
            );
        }

        public function getAttachments($entityId) {
            try {
                //get all attachment ids associated to this entity
                $query = "SELECT attachment_id FROM EntityAttachment WHERE entity_id = :entityId";
                $queryVariables = array(
                    ':entityId' => $entityId
                );
                $results = $this->databaseHandler->queryByColumn($query, $queryVariables);

                //get all attachments from the attacment ids
                $attachmentIds = $results['data'];
                $attachments = $this->attachmentHandler->getAttachments($attachmentIds);

                return $attachments;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function removeAttachmentsFromDatabase($attachmentIds, $entityId) {
            try {
                if (is_int($attachmentIds)) {
                    $attachmentIds = array($attachmentIds);
                }

                if (empty($attachmentIds)) {
                    return TRUE;
                }

                //format the named parameters and query variables
                $parameters = '';
                $queryVariables = array();
                foreach ($attachmentIds as $index => $attachmentId) {
                    $entityIdParameterName = ':entityId' . $index;
                    $queryVariables[$entityIdParameterName] = $entityId;

                    $attachmentIdParameterName = ':attachmentId' . $index;
                    $queryVariables[$attachmentIdParameterName] = $attachmentId;
                    $parameters .= '(' . $entityIdParameterName . ',' . $attachmentIdParameterName . '),';
                }
                $parameters = substr($parameters, 0, -1);

                //perform the query - remove the attachments from this entity
                $query = "DELETE IGNORE FROM EntityAttachment WHERE (entity_id, attachment_id) IN (" . $parameters . ")";
                $result = $this->databaseHandler->query($query, $queryVariables);

                return $result['isSuccess'];
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function getDescription($entityId) {
            try {
                return $this->getTextBody($entityId, 'description');
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function getBackground($entityId) {
            try {
                return $this->getTextBody($entityId, 'background');
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function getStakeholder($entityId) {
            try {
                return $this->getTextBody($entityId, 'stakeholder');
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function getExistingSolution($entityId) {
            try {
                return $this->getTextBody($entityId, 'existing solution');
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function getMarketAnalysis($entityId) {
            try {
                return $this->getTextBody($entityId, 'market analysis');
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function getOtherInformation($entityId) {
            try {
                return $this->getTextBody($entityId, 'other');
            } catch (Exception $error) {
                throw $error;
            }
        }

        private function getTextBody($entityId, $textBodyType) {
            try {
                $query = "SELECT EntityContent.id, TextBody.text_body FROM EntityContent INNER JOIN ContentType ON (EntityContent.content_type_id = ContentType.id) INNER JOIN TextBody ON (EntityContent.id = TextBody.entity_content_id) INNER JOIN TextBodyType ON (TextBody.text_body_type_id = TextBodyType.id) WHERE (EntityContent.entity_id = :entityId) AND (ContentType.content_type = :contentType) AND (TextBodyType.text_body_type = :textBodyType)";
                $queryVariables = array(
                    ':entityId' => $entityId,
                    ':contentType' => 'text body',
                    ':textBodyType' => $textBodyType
                );
                $result = $this->databaseHandler->query($query, $queryVariables);

                if (!$result['isSuccess']) {
                    throw New Exception('>>> Could not get text body (' . $textBodyType . ')');
                }

                $textBody = NULL;
                if (!$result['isEmpty']) {
                    $entityContent = reset($result['data']);
                    $textBody = $this->createEntityContentModel($entityContent['id']);
                    $textBody['value'] = $entityContent['textBody'];
                }

                return $textBody;
            } catch (Exception $error) {
                throw $error;
            }
        }

        private function updateTextBody($entityContentId, $textBody) {
            try {
                $query = "UPDATE TextBody SET text_body = :textBody WHERE (entity_content_id = :entityContentId)";
                $queryVariables = array(
                    ':entityContentId' => $entityContentId,
                    ':textBody' => $textBody
                );
                $this->databaseHandler->query($query, $queryVariables);
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function getTeamMembers($entityId) {
            try {
                $teamMembers = array();

                $query = "SELECT user_id FROM EntityTeamMember WHERE (entity_id = :entityId)";
                $queryVariables = array(
                    ':entityId' => $entityId
                );
                $result = $this->databaseHandler->queryByColumn($query, $queryVariables);

                if (!$result['isEmpty']) {
                    foreach ($result['data'] as $teamMemberId) {
                        $teamMembers[] = $this->userHandler->createBasicUserProfileModel($teamMemberId);
                    }
                }

                return $teamMembers;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function getRoles($entityId) {
            try {
                $query = "SELECT EntityContent.id, Role.role FROM EntityContent INNER JOIN ContentType ON (EntityContent.content_type_id = ContentType.id) RIGHT JOIN Role ON (EntityContent.id = Role.entity_content_id) WHERE (EntityContent.entity_id = :entityId) AND (ContentType.content_type = :contentType)";
                $queryVariables = array(
                    ':entityId' => $entityId,
                    ':contentType' => 'role'
                );
                $result = $this->databaseHandler->query($query, $queryVariables);

                $roles = array();
                if (!$result['isEmpty']) {
                    foreach ($result['data'] as $entityContent) {
                        $entityContentModel = $this->createEntityContentModel($entityContent['id']);
                        $role = $entityContent['role'];

                        $queryRoleAssignees = "SELECT assignee_id FROM RoleAssignee WHERE (role_id = :roleId)";
                        $queryVariables = array(
                            ':roleId' => $entityContent['id']
                        );
                        $result = $this->databaseHandler->queryByColumn($queryRoleAssignees, $queryVariables);

                        $roleAssignees = array_map(function($roleAssigneeId) {
                            return $this->userHandler->createBasicUserProfileModel($roleAssigneeId);
                        }, $result['data']);

                        $entityContentModel['value'] = $this->createRoleModel($role, $roleAssignees);
                        $roles[] = $entityContentModel;
                    }
                }

                return $roles;
            } catch (Exception $error) {
                throw $error;
            }
        }

        private function createRoleModel($role = NULL, $roleAssignees = []) {
            try {
                return array(
                    'role' => $role,
                    'assignees' => $roleAssignees
                );
            } catch (Exception $error) {
                throw $error;
            }
        }

        protected function removeEntityContent($entityContentId) {
            try {
                $queryVariables = array(
                    ':entityContentId' => $entityContentId
                );

                //TODO: this might not be necessary if cascade delete is on
                // $query = "SELECT ContentType.content_type FROM EntityContent INNER JOIN ContentType ON (EntityContent.content_type_id = ContentType.id) WHERE (EntityContent.id = :entityContentId)";
                // $result = $this->databaseHandler->queryByColumn($query, $queryVariables);
                // $entityContentType = reset($result['data']);

                $query = "DELETE FROM EntityContent WHERE (id = :entityContentId)";
                $this->databaseHandler->query($query, $queryVariables);

                // switch ($entityContentType) {
                // }
            } catch (Exception $error) {
                throw $error;
            }
        }

        private function removeTeamMember($teamMemberId, $entityId) {
            try {
                $query = "DELETE FROM EntityTeamMember WHERE (entity_id, user_id) = (:entityId, :userId)";
                $queryVariables = array(
                    ':entityId' => $entityId,
                    ':userId' => $teamMemberId
                );
                $this->databaseHandler->query($query, $queryVariables);
            } catch (Exception $error) {
                throw $error;
            }
        }

        protected function updateEntity($entityId, $entityContents, $userId) {
            try {
                if (isset($entityContents['edited'])) {
                    $editedContents = $entityContents['edited'];
                    if (!empty($editedContents)) {
                        if (isset($editedContents['categories'])) {
                            foreach ($editedContents['categories'] as $category) {
                                $query = "UPDATE EntityCategory SET category_id = :categoryId WHERE (id = :entityCategoryId)";
                                $queryVariables = array(
                                    ':entityCategoryId' => $category['id'],
                                    ':categoryId' => $category['value']
                                );
                                $this->databaseHandler->query($query, $queryVariables);
                            }
                        }

                        if (isset($editedContents['description'])) {
                            $this->updateTextBody($editedContents['description']['id'], $editedContents['description']['value']);
                        }

                        if (isset($editedContents['background'])) {
                            $this->updateTextBody($editedContents['background']['id'], $editedContents['background']['value']);
                        }

                        if (isset($editedContents['stakeholder'])) {
                            $this->updateTextBody($editedContents['stakeholder']['id'], $editedContents['stakeholder']['value']);
                        }

                        if (isset($editedContents['existing-solution'])) {
                            $this->updateTextBody($editedContents['existing-solution']['id'], $editedContents['existing-solution']['value']);
                        }

                        if (isset($editedContents['market-analysis'])) {
                            $this->updateTextBody($editedContents['market-analysis']['id'], $editedContents['market-analysis']['value']);
                        }

                        if (isset($editedContents['other-information'])) {
                            $this->updateTextBody($editedContents['other-information']['id'], $editedContents['other-information']['value']);
                        }

                        //TODO: to be tested
                        if (isset($editedContents['roles'])) {
                            $roleQuery = "UPDATE Role SET role = :role WHERE (entity_content_id = :entityContentId)";
                            foreach ($editedContents['roles'] as $role) {
                                //update the role
                                $queryVariables = array(
                                    ':entityContentId' => $role['id'],
                                    ':role' => $role['value']['role']
                                );
                                $this->databaseHandler->query($roleQuery, $queryVariables);

                                //update the role assignees
                                $queryExistingAssignees = "SELECT assignee_id FROM RoleAssignee WHERE role_id = :roleId";
                                $queryVariables = array(
                                    ':roleId' => $role['id']
                                );
                                $result = $this->databaseHandler->queryByColumn($queryExistingAssignees, $queryVariables);
                                $oldRoleAssignees = $result['data'];
                                $newRoleAssignees = $role['value']['assignees'];

                                //compare new assignees with existing assignees
                                $roleAssigneesToRemove = array_diff($oldRoleAssignees, $newRoleAssignees);
                                if (!empty($roleAssigneesToRemove)) {
                                    $parameters = $this->databaseHandler->createMultipleMarkers(count($roleAssigneesToRemove));
                                    $removeAssigneesQuery = "DELETE FROM RoleAssignee WHERE (role_id = ?) AND (assignee_id IN " . $parameters . ")";
                                    $queryVariables = array_merge(array($role['id']), $roleAssigneesToRemove);
                                    $this->databaseHandler->query($removeAssigneesQuery, $queryVariables);
                                }

                                $roleAssigneesToAdd = array_diff($newRoleAssignees, $oldRoleAssignees);
                                if (!empty($roleAssigneesToAdd)) {
                                    $addAssigneesQuery = "INSERT IGNORE INTO RoleAssignee (role_id, assignee_id) VALUES (:roleId, :assigneeId)";
                                    foreach ($roleAssigneesToAdd as $roleAssigneeId) {
                                        $queryVariables = array(
                                            ':roleId' => $role['id'],
                                            ':assigneeId' => $roleAssigneeId
                                        );
                                        $this->databaseHandler->query($addAssigneesQuery, $queryVariables);
                                    }
                                }
                            }
                        }
                    }
                }

                if (isset($entityContents['removed'])) {
                    $removedContents = $entityContents['removed'];
                    if (!empty($removedContents)) {
                        if (isset($removedContents['description'])) {
                            $this->removeEntityContent($removedContents['description']['id']);
                        }

                        if (isset($removedContents['background'])) {
                            $this->removeEntityContent($removedContents['background']['id']);
                        }

                        if (isset($removedContents['stakeholder'])) {
                            $this->removeEntityContent($removedContents['stakeholder']['id']);
                        }

                        if (isset($removedContents['existing-solution'])) {
                            $this->removeEntityContent($removedContents['existing-solution']['id']);
                        }

                        if (isset($removedContents['market-analysis'])) {
                            $this->removeEntityContent($removedContents['market-analysis']['id']);
                        }

                        if (isset($removedContents['other-information'])) {
                            $this->removeEntityContent($removedContents['other-information']['id']);
                        }

                        if (isset($removedContents['teamMembers'])) {
                            foreach ($removedContents['teamMembers'] as $teamMembers) {
                                $this->removeTeamMember($teamMembers['id'], $entityId);
                            }
                        }

                        if (isset($removedContents['roles'])) {
                            foreach ($removedContents['roles'] as $role) {
                                $this->removeEntityContent($role['id']);
                            }
                        }

                        if (isset($removedContents['attachments'])) {
                            //TODO: should we create our own plucking function?
                            //extract the attachment ids into an array to use for removing the attachments
                            $attachmentIds = array_map(function($attachment) {
                                return $attachment['id'];
                            }, $removedContents['attachments']);

                            //remove the attachments from the entity in the database
                            $isSuccess = $this->removeAttachmentsFromDatabase($attachmentIds, $entityId);

                            if ($isSuccess) {
                                //NOTE: here, we are assuming that Attachment and Organization has a 1:1 relation (i.e an attachment cannot be shared among multiple entities and organizations)
                                //FUTURE TODO: check for non-shared attachments

                                //remove the non-shared attachments from the file system
                                $this->attachmentHandler->removeAttachmentsFromFileSystem($attachmentIds);

                                //remove the non-shared attachments from the database
                                $this->attachmentHandler->removeAttachmentsFromDatabase($attachmentIds);
                            }
                        }
                    }
                }

                if (isset($entityContents['added'])) {
                    $addedContents = $entityContents['added'];
                    if (!empty($addedContents)) {
                        if (isset($addedContents['description'])) {
                            $this->addNewDescription($entityId, $addedContents['description']['value'], $userId);
                        }

                        if (isset($addedContents['background'])) {
                            $this->addNewBackground($entityId, $addedContents['background']['value'], $userId);
                        }

                        if (isset($addedContents['stakeholder'])) {
                            $this->addNewStakeholder($entityId, $addedContents['stakeholder']['value'], $userId);
                        }

                        if (isset($addedContents['existing-solution'])) {
                            $this->addNewExistingSolution($entityId, $addedContents['existing-solution']['value'], $userId);
                        }

                        if (isset($addedContents['market-analysis'])) {
                            $this->addNewMarketAnalysis($entityId, $addedContents['market-analysis']['value'], $userId);
                        }

                        if (isset($addedContents['other-information'])) {
                            $this->addNewOtherInformation($entityId, $addedContents['other-information']['value'], $userId);
                        }

                        if (isset($addedContents['teamMembers'])) {
                            foreach ($addedContents['teamMembers'] as $teamMember) {
                                $query = "INSERT INTO EntityTeamMember (entity_id, user_id) VALUES (:entityId, :userId)";
                                $queryVariables = array(
                                    ':entityId' => $entityId,
                                    ':userId' => $teamMember['id']
                                );
                                $this->databaseHandler->query($query, $queryVariables);
                            }
                        }

                        //TODO: this needs to be tested
                        if (isset($addedContents['roles'])) {
                            foreach ($addedContents['roles'] as $role) {
                                $this->addNewRole($entityId, $role['value']['role'], $role['value']['assignees'], $userId);
                            }
                        }

                        if (isset($addedContents['attachments'])) {
                            foreach ($addedContents['attachments'] as $attachment) {
                                $this->addNewAttachment($attachment['id'], $entityId);

                                //TODO: update the attachment name if it is different than the existing name
                                //NOTE: actually, this should only need to be done if it's for editted contents because added contents will only use the changed name
                                if (!is_null($attachment['value'])) {
                                    //1. rename the attachment/partial attachments in the file system
                                    //2. update the attachment record in the database
                                    debug($attachment['value']);
                                }
                            }
                        }
                    }
                }
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function listAllCategories($organizationModel) {
            try {
                $query = "SELECT * FROM Category WHERE id IN (SELECT category_id FROM OrganizationCategory WHERE organization_id = :organizationId) ORDER BY name ASC";
                $queryVariables = array(
                    ':organizationId' => $organizationModel['organizationId']
                );
                $result = $this->databaseHandler->query($query, $queryVariables);
                return $result['data'];
            } catch (Exception $error) {
                throw $error;
            }
        }
    }
?>