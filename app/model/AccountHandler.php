<?php
    if (!defined('__GRAVIT8__')) {
        $gravit8Directory = dirname(dirname(dirname(__File__)));
        define('__GRAVIT8__', $gravit8Directory);
    }

    require_once __GRAVIT8__ . '/app/encryptions/PasswordHandler.php';
    require_once __GRAVIT8__ . '/app/model/DatabaseHandler.php';
    require_once __GRAVIT8__ . '/app/model/OrganizationHandler.php';
    require_once __GRAVIT8__ . '/app/model/UserHandler.php';

    Class AccountHandler {
        //constants
        //NOTE: PHP doesn't support enumerations by default
        const UNSUCCESSFUL = -1;
        const SUCCESS = 0;
        const ACCOUNT_ALREADY_EXISTS = 1;
        const ACCOUNT_DOES_NOT_EXISTS = 2;
        const INVALID_SIGNUP_CODE = 3;
        const INVALID_RESET_KEY = 4;

        private $passwordHandler;
        private $databaseHandler;
        private $organizationHandler;
        private $userHandler;

        public function __construct() {
            $this->passwordHandler = PasswordHandler::getInstance();
            $this->passwordHandler->setKeyStretchingIterations(pow(2, 17));

            $this->databaseHandler = DatabaseHandler::getInstance();
            $this->organizationHandler = OrganizationHandler::getInstance();
            $this->userHandler = UserHandler::getInstance();
        }

        public function createAccount($firstName, $lastName, $email, $password, $signupKey, $organizationModel) {
            try {
                //check if account exists
                $query = "SELECT account_name FROM User WHERE account_name = :accountName";
                $queryVariables = array(
                    ':accountName' => $email
                );
                $result = $this->databaseHandler->query($query, $queryVariables);
                $isNewAccount = $result['isEmpty'];

                //check if signup key is available
                $query = "SELECT * FROM SignupKey WHERE (signup_key = :signupKey) AND (used = FALSE)";
                $queryVariables = array(
                    ':signupKey' => $signupKey
                );
                $result = $this->databaseHandler->query($query, $queryVariables);
                $signupKeyExists = !$result['isEmpty'];
                $signupKeyUsed = $signupKeyExists ? reset($result['data'])['used'] : TRUE;
                $signupKeyIsAvailable = $signupKeyExists && !$signupKeyUsed;

                if ($isNewAccount && $signupKeyIsAvailable) {
                    //update signup key status
                    $query = "UPDATE SignupKey SET used = TRUE, reserved = TRUE WHERE signup_key = :signupKey";
                    $queryVariables = array(
                        ':signupKey' => $signupKey
                    );
                    $this->databaseHandler->query($query, $queryVariables);

                    //encrypt password
                    $encryptedPassword = $this->passwordHandler->encryptPassword($password);
                    $encodedPassword = $this->passwordHandler->encodePassword($encryptedPassword);

                    //create new user
                    $query = "INSERT INTO User (account_name, hash, salt) VALUES (:accountName, :hash, :salt)";
                    $queryVariables = array(
                        ':accountName' => $email,
                        ':hash' => $encodedPassword['hash'],
                        ':salt' => $encodedPassword['salt']
                    );
                    $this->databaseHandler->query($query, $queryVariables);
                    $userId = $this->databaseHandler->getLastInsertId();

                    //create user profile
                    $query = "INSERT INTO UserProfile (user_id, first_name, last_name, title) VALUES (:userId, :firstName, :lastName, :title)";
                    $queryVariables = array(
                        ':userId' => $userId,
                        ':firstName' => $firstName,
                        ':lastName' => $lastName,
                        ':title' => 'Participant'
                    );
                    $this->databaseHandler->query($query, $queryVariables);

                    //add user to given organization as a regular user
                    $query = "SELECT id FROM UserAccountType WHERE account_type = :accountType";
                    $queryVariables = array(
                        ':accountType' => 'regular'
                    );
                    $result = $this->databaseHandler->queryByColumn($query, $queryVariables);
                    $userAccountTypeId = reset($result['data']);

                    $query = "INSERT INTO UserOrganization (user_id, organization_id, organization_group_id, user_account_type_id) VALUES (:userId, :organizationId, :organizationGroupId, :userAccountTypeId)";
                    $queryVariables = array(
                        ':userId' => $userId,
                        ':organizationId' => $organizationModel['organizationId'],
                        ':organizationGroupId' => $organizationModel['organizationGroupId'],
                        ':userAccountTypeId' => $userAccountTypeId
                    );
                    $this->databaseHandler->query($query, $queryVariables);

                    //add user's email as a contact
                    $profileAttributeId = $this->userHandler->addNewProfileAttribute($userId, 'contact');

                    $query = "SELECT id FROM ContactType WHERE contact_type = 'email'";
                    $result = $this->databaseHandler->queryByColumn($query);
                    $contactTypeId = reset($result['data']);

                    $query = "INSERT INTO UserContact (profile_attribute_id, contact_type_id, contact) VALUES (:profileAttributeId, :contactTypeId, :contact)";
                    $queryVariables = array(
                        ':profileAttributeId' => $profileAttributeId,
                        ':contactTypeId' => $contactTypeId,
                        ':contact' => $email
                    );
                    $this->databaseHandler->query($query, $queryVariables);

                    return array(
                        'status' => self::SUCCESS,
                        'userId' => $userId
                    );
                } else if (!$isNewAccount) {
                    return array(
                        'status' => self::ACCOUNT_ALREADY_EXISTS
                    );
                } else if (!$signupKeyIsAvailable) {
                    return array(
                        'status' => self::INVALID_SIGNUP_CODE
                    );
                } else {
                    return array(
                        'status' => FALSE
                    );
                }
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function resetPassword($accountName, $newPassword, $resetKey) {
            try {
                $resetPasswordResult = array(
                    'status' => self::UNSUCCESSFUL,
                    'userId' => NULL
                );

                //verify that the account exists
                $doesAccountExists = $this->doesAccountExists($accountName);
                if (!$doesAccountExists) {
                    $resetPasswordResult['status'] = self::ACCOUNT_DOES_NOT_EXISTS;
                    return $resetPasswordResult;
                }

                //verify that the unique reset key is valid
                $query = "SELECT * FROM SignupKey WHERE (signup_key = :resetKey) AND (used = FALSE)";
                $queryVariables = array(
                    ':resetKey' => $resetKey
                );
                $result = $this->databaseHandler->query($query, $queryVariables);
                if (!$result['isSuccess'] || $result['isEmpty']) {
                    $resetPasswordResult['status'] = self::INVALID_RESET_KEY;
                    return $resetPasswordResult;
                }

                //update used unique reset key
                $query = "UPDATE SignupKey SET used = TRUE WHERE signup_key = :resetKey";
                $result = $this->databaseHandler->query($query, $queryVariables);
                if (!$result['isSuccess']) {
                    throw new Exception('>>>> Failed to update the unique reset key for account: ' . $accountName);
                }

                //get the user id
                $query = "SELECT id FROM User WHERE account_name = :accountName";
                $queryVariables = array(
                    ':accountName' => $accountName
                );
                $result = $this->databaseHandler->queryByColumn($query, $queryVariables);
                if (!$result['isSuccess']) {
                    throw new Exception('>>>> Failed to retrieve the user id for account: ' . $accountName);
                }
                $userId = reset($result['data']);

                //update the password
                $newEncryptedPassword = $this->passwordHandler->encryptPassword($newPassword);
                $encodedPassword = $this->passwordHandler->encodePassword($newEncryptedPassword);
                $query = "UPDATE User SET hash = :hash, salt = :salt WHERE id = :userId";
                $queryVariables = array(
                    ':hash' => $encodedPassword['hash'],
                    ':salt' => $encodedPassword['salt'],
                    ':userId' => $userId
                );
                $result = $this->databaseHandler->query($query, $queryVariables);

                //reset password failed
                if (!$result['isSuccess']) {
                    throw new Exception('>>>> Failed to reset password for account: ' . $accountName);
                }

                $resetPasswordResult['status'] = self::SUCCESS;
                $resetPasswordResult['userId'] = $userId;
                return $resetPasswordResult;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function verifyAccount($accountName, $password) {
            try {
                $verificationResults = array(
                    'isValidAccount' => FALSE,
                    'userId' => NULL
                );

                if ($this->doesAccountExists($accountName)) {
                    $query = "SELECT id, hash, salt FROM User WHERE account_name = :accountName";
                    $queryVariables = array(
                        ':accountName' => $accountName
                    );
                    $result = $this->databaseHandler->query($query, $queryVariables);

                    $user = reset($result['data']);
                    $verificationResults['userId'] = $user['id'];

                    //verify password
                    $encodedPassword = array(
                        'hash' => $user['hash'],
                        'salt' => $user['salt']
                    );
                    $verificationResults['isValidAccount'] = $this->verifyPassword($password, $encodedPassword);
                }

                return $verificationResults;
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function doesAccountExists($accountName) {
            try {
                $query = "SELECT * FROM User WHERE account_name = :accountName";
                $queryVariables = array(
                    ':accountName' => $accountName
                );
                $result = $this->databaseHandler->query($query, $queryVariables);

                if (!$result['isSuccess']) {
                    throw new Exception('>>>> Failed to verify if user account exists: ' . $accountName);
                }

                return (!$result['isEmpty']);
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function verifyPassword($password, $encodedPassword) {
            try {
                //$expectedPassword is an associative array with keys: (hash, salt) for the encoded password
                $expectedPassword = $this->passwordHandler->decodePassword($encodedPassword);
                $predictedPassword = $this->passwordHandler->encryptPassword($password, $expectedPassword['salt']);

                return ($predictedPassword['hash'] === $expectedPassword['hash']);
            } catch (Exception $error) {
                throw $error;
            }
        }

        public function confirmPassword($password, $retypedPassword) {
            try {
                return ($password === $retypedPassword);
            } catch (Exception $error) {
                throw $error;
            }
        }
    }
?>