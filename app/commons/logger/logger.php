<?php
    if (!defined('__GRAVIT8__')) {
        $gravit8Directory = dirname(__File__, 4);
        define('__GRAVIT8__', $gravit8Directory);
    }

    require_once __GRAVIT8__ . '/app/commons/utilities.php';

    function debugRoutes($routes) {
        foreach ($routes as $i=>$route) {
            debug('routes[' . $i . ']: ' . $route);
        }
        debug($_SERVER['REQUEST_METHOD']);
    }

    //wrapper for calling error_log instead of standard echo or prints to log messages; messages are logged in the ErrorLog destination in the Apache httpd.conf file
    function debug($message) {
        $formattedMessage = print_r($message, TRUE);
        error_log($formattedMessage);
    }

    //exception log
    function debugError($error) {
        //debug messages and errors are logged in logs/error.log
        $errorMessage = '>>> {Line ' . $error->getLine() . ' in ' . $error->getFile() . '}   ' . $error->getMessage();
        debug($errorMessage);

        debug('>>> Stack Trace:');
        $traceStack = $error->getTrace();
        $traceIndex = 0;
        foreach ($traceStack as $trace) {
            $traceReasonPrefix = '';
            if (array_key_exists('class', $trace)) {
                $traceReasonPrefix = $trace['class'] . $trace['type'];
            }

            $arguments = '';
            if (array_key_exists('args', $trace)) {
                $arguments = recursiveImplode($trace['args'], ', ');
            }

            $traceReason = $traceReasonPrefix . $trace['function'] . '(' . $arguments . ')';
            $traceMessage = '    >>> ' . $traceIndex . ' {Line ' . $trace['line'] . ' in ' . $trace['file'] . '}   ' . $traceReason;
            debug($traceMessage);
            $traceIndex++;
        }
        debug('    >>> ' . $traceIndex . ' {catch statement}');
    }
?>