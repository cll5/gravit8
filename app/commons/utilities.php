<?php
    $UTC_TIMEZONE = new DateTimeZone('UTC');
    function getTimeStamp($dateTimeString = NULL, $format = 'Y-m-d H:i:s') {
        global $UTC_TIMEZONE;
        $timeStamp = new DateTime($dateTimeString, $UTC_TIMEZONE);
        return $timeStamp->format($format);
    }

    function getInitials($fullName) {
        try {
            $names = preg_split('/\b[\s]+\b/', strtoupper($fullName));
            return array_reduce($names, function($initials, $name) {
                return $initials . substr($name, 0, 1);
            }, '');
        } catch (Exception $error) {
            throw $error;
        }
    }

    function doesFileExists($filePath, $isAbsolutePath = FALSE) {
        return (($isAbsolutePath) ? file_exists($filePath) : file_exists(getAbsolutePath($filePath)));
    }

    function getAbsolutePath($relativePath) {
        if (!startsWith($relativePath, DIRECTORY_SEPARATOR)) {
            $relativePath = DIRECTORY_SEPARATOR . $relativePath;
        }

        return ($_SERVER['DOCUMENT_ROOT'] . $relativePath);
    }

    function startsWith($string, $pattern) {
        $length = strlen($pattern);
        return (substr($string, 0, $length) === $pattern);
    }

    function endsWith($string, $pattern) {
        $length = strlen($pattern);
        return (substr($string, -$length) === $pattern);
    }

    function expandKeywordsWithWildcard($keywords) {
        $expandedKeywords = preg_replace('/\s+/', '* ', $keywords);
        if (is_null($expandedKeywords)) {
            $expandedKeywords = $keywords;
        }

        if (!endsWith($expandedKeywords, '*')) {
            $expandedKeywords .= '*';
        }

        return $expandedKeywords;
    }

    //random character generator for image file names
    function generateFilename($suffix) {
        return md5(microtime() . $suffix, false);
    }

    //returns the size of a file in bytes or -1 if there is an error
    function getFilesize($filepath) {
        try {
            $filesize = filesize($filepath);
            return ($filesize !== FALSE) ? $filesize : -1;
        } catch (Exception $error) {
            throw $error;
        }
    }

    //formats a file size in bytes to a human readable format
    function formatBytes($filesize) {
        try {
            $units = ['B', 'kB', 'MB', 'GB', 'TB'];
            $unitsLength = count($units);
            $divider = 1024;

            $bytes = (float) $filesize;
            $index = 0;
            for ($index = 0; ($bytes >= $divider) && ($index < $unitsLength); $index++) {
                $bytes /= $divider;
            }

            $formattedBytes = number_format($bytes, 2) . ' ' . $units[$index];
            return $formattedBytes;
        } catch (Exception $error) {
            throw $error;
        }
    }

    //one level plucking function
    //see: http://stackoverflow.com/questions/10659875/in-php-is-there-a-function-that-returns-an-array-made-up-of-the-value-of-a-key
    function pluck($key, $data) {
        return array_reduce($data, function($result, $array) use($key) {
            isset($array[$key]) && $result[] = $array[$key];
            return $result;
        }, array());
    }

    //multi level array strings join
    function recursiveImplode($nestedArray, $separator = ', ') {
        $result = '[';
        foreach ($nestedArray as $element) {
            $result .= !is_array($element) ? $element : recursiveImplode($element, $separator);
            $result .= $separator;
        }
        $result = substr($result, 0, -strlen($separator)) . ']';
        return $result;
    }

    function isAssociativeArray($array) {
        return count(array_filter(array_keys($array), 'is_string')) > 0;
    }

    function mapFieldsToKeys($results) {
        return array_map(function($result) {
            foreach ($result as $field => $value) {
                $key = fieldToKey($field);
                if ($key !== $field) {
                    $result[$key] = $result[$field];
                    unset($result[$field]);
                }
            }
            return $result;
        }, $results);
    }

    function mapKeysToFields($results) {
        return array_map(function($result) {
            foreach ($result as $key => $value) {
                $field = keyToField($key);
                if ($field !== $key) {
                    $result[$field] = $result[$key];
                    unset($result[$key]);
                }
            }
            return $result;
        }, $results);
    }

    function fieldToKey($field) {
        $key = str_replace('_', ' ', $field);
        $key = trim($key);
        $key = ucwords($key);
        $key = str_replace(' ', '', $key);
        $key[0] = strtolower($key[0]);
        return $key;
    }

    function keyToField($key) {
        $field = preg_replace('/([A-Z])/g', '_$1', $key);
        $field = strtolower($field);
        return $field;
    }

    function parseJSON(&$jsonObject) {
        $json = json_decode($jsonObject['json'], TRUE);
        debug(json_last_error_msg());
        return $json;
    }

    function formatJSON($success = TRUE, $data = NULL) {
        $response = array(
            'success' => $success,
            'data' => $data
        );
        $json = json_encode($response);
        debug(json_last_error_msg());
        return $json;
    }

    function createEmptyObject() {
        return new stdClass;
    }

    function isEmptyObject($value) {
        return ($value instanceof stdClass) ? TRUE : FALSE;
    }
?>