<?php
    $anyFilter = new Twig_SimpleFilter('any', function($array) {
        return array_reduce($array, function($carry, $element) {
            return ($carry || $element);
        }, FALSE);
    });
?>