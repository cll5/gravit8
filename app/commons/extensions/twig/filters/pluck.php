<?php
    //see: http://twig.sensiolabs.org/doc/advanced.html #}
    //and: http://stackoverflow.com/questions/13277444/using-a-custom-function-in-twig
    $pluckFilter = new Twig_SimpleFilter('pluck', function($array, $key) {
        return pluck($key, $array);
    });
?>