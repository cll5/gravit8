<?php
    if (!defined('__GRAVIT8__')) {
        $gravit8Directory = dirname(dirname(dirname(__File__)));
        define('__GRAVIT8__', $gravit8Directory);
    }

    require_once __GRAVIT8__ . '/app/commons/interfaces/Singleton.php';

    class PasswordHandler implements Singleton {
        private static $instance;
        private $hashFunction;
        private $keyStretchingIterations;
        private $hashLength;
        private $saltLength;
        private $useStrongCryptography;

        private function __construct($hashAlgorithm, $keyStretchingIterations) {
            $this->hashFunction = $hashAlgorithm;
            $this->keyStretchingIterations = $keyStretchingIterations;
            $this->hashLength = strlen(hash($this->hashFunction, "", true));
            $this->saltLength = $this->hashLength;
            $this->useStrongCryptography = true;
        }

        //for singleton classes, see: http://stackoverflow.com/questions/7968154/singleton-pattern-in-php
        public static function getInstance($hashAlgorithm = 'sha512', $keyStretchingIterations = 131072) {
            if (self::$instance === NULL) {
                self::$instance = new PasswordHandler($hashAlgorithm, $keyStretchingIterations);
            }

            return self::$instance;
        }

        public function setKeyStretchingIterations($iterations) {
            $this->keyStretchingIterations = $iterations;
            return $this;
        }

        //see: https://crackstation.net/hashing-security.htm#normalhashing
        public function encryptPassword($password, $salt = NULL) {
            try {
                //create a new salt if it is not provided
                if (is_null($salt)) {
                    $salt = openssl_random_pseudo_bytes($this->saltLength, $this->useStrongCryptography);
                }

                $algorithm = $this->hashFunction;
                $iterations = $this->keyStretchingIterations;
                $hashLength = $this->hashLength;
                $hash = $this->pbkdf2($algorithm, $password, $salt, $iterations, $hashLength, true);

                return array(
                    'hash' => $hash,
                    'salt' => $salt
                );
            } catch(Exception $error) {
                throw $error;
            }
        }

        public function encodePassword($encryptedPassword) {
            return array(
                'hash' => base64_encode($encryptedPassword['hash']),
                'salt' => base64_encode($encryptedPassword['salt'])
            );
        }

        public function decodePassword($encodedPassword) {
            return array(
                'hash' => base64_decode($encodedPassword['hash']),
                'salt' => base64_decode($encodedPassword['salt'])
            );
        }

        //key stretching hash, see: https://defuse.ca/php-pbkdf2.htm
        private function pbkdf2($algorithm, $password, $salt, $count, $key_length, $raw_output = false) {
            $algorithm = strtolower($algorithm);
            if (!in_array($algorithm, hash_algos(), true)) {
                trigger_error('PBKDF2 ERROR: Invalid hash algorithm.', E_USER_ERROR);
            }

            if ($count <= 0 || $key_length <= 0) {
                trigger_error('PBKDF2 ERROR: Invalid parameters.', E_USER_ERROR);
            }

            if (function_exists("hash_pbkdf2")) {
                // The output length is in NIBBLES (4-bits) if $raw_output is false!
                if (!$raw_output) {
                    $key_length = $key_length * 2;
                }
                return hash_pbkdf2($algorithm, $password, $salt, $count, $key_length, $raw_output);
            }

            $hash_length = strlen(hash($algorithm, "", true));
            $block_count = ceil($key_length / $hash_length);

            $output = "";
            for ($i = 1; $i <= $block_count; $i++) {
                // $i encoded as 4 bytes, big endian.
                $last = $salt . pack("N", $i);
                // first iteration
                $last = $xorsum = hash_hmac($algorithm, $last, $password, true);
                // perform the other $count - 1 iterations
                for ($j = 1; $j < $count; $j++) {
                    $xorsum ^= ($last = hash_hmac($algorithm, $last, $password, true));
                }
                $output .= $xorsum;
            }

            if ($raw_output) {
                return substr($output, 0, $key_length);
            } else {
                return bin2hex(substr($output, 0, $key_length));
            }
        }
    }
?>