var gulp = require("gulp");
var concat = require("gulp-concat");
var jshint = require("gulp-jshint");
var uglify = require("gulp-uglify");
var rename = require("gulp-rename");
var sass = require("gulp-sass");
var watch = require("gulp-watch");

var sourceFiles = {
    javascripts: [
        "javascripts/**/*.js",
        "!javascripts/vendor/fontFaceObserver.js",
        "!javascripts/**/*.min.js",
        "!javascripts/**/gravit8.js",
        "!javascripts/**/*.initialization.js"
    ],
    css: [
        "sass/**/*.scss"
    ]
};

//checks for javascript errors
gulp.task("jshint", function() {
    return gulp.src(sourceFiles.javascripts)
               .pipe(jshint({bitwise: true, curly: true, freeze: false, sub: true, shadow: true}))
               .pipe(jshint.reporter("default"));
});

//combine and minify javascripts to gravit8.js and gravit8.min.js
gulp.task("javascripts", function() {
    return gulp.src([
        "javascripts/gravit8/commons/utilities.js",
        "!javascripts/gravit8/commons/loadFonts.js",
        "!javascripts/gravit8/custom/**/*.js",
        "javascripts/gravit8/main.js",
        "javascripts/gravit8/commons/attachmentHandler.js",
        "javascripts/gravit8/commons/browseHistoryHandler.js",
        "javascripts/gravit8/commons/throttleEventDetector.js",
        "javascripts/gravit8/commons/scrollbarHandler.js",
        "javascripts/gravit8/commons/*.js",
        "javascripts/gravit8/eventHandlers/*.js",
        "javascripts/gravit8/entityHandlers/*.js",
        "javascripts/gravit8/initialization.js"
    ]).pipe(concat("gravit8.js"))
      .pipe(gulp.dest("javascripts/gravit8/"))
      .pipe(uglify())
      .pipe(rename({extname: ".min.js"}))
      .pipe(gulp.dest("javascripts/gravit8/"));
});

//combine and minify sass to gravit8.min.css
gulp.task("css", function() {
    return gulp.src(sourceFiles.css)
               .pipe(sass({outputStyle: "compressed"}).on("error", sass.logError))
               .pipe(rename({extname: ".min.css"}))
               .pipe(gulp.dest("stylesheets/"));
});

//watch for changes in the source files
gulp.task("watch", function() {
    gulp.watch(sourceFiles.css, {ignoreInitial: false}, ["css"]);
    gulp.watch(sourceFiles.javascripts, {ignoreInitial: false}, ["jshint", "javascripts"]);
});

//default is to run build task
gulp.task("default", ["jshint", "javascripts", "css", "watch"]);