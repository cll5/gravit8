DROP PROCEDURE IF EXISTS PopulateOrganizationGroup;
DELIMITER ;;
CREATE PROCEDURE PopulateOrganizationGroup()
BEGIN
DECLARE n INT DEFAULT 0;
DECLARE i INT DEFAULT 0;
DECLARE org_id INT DEFAULT 0;
DECLARE org_group_id INT DEFAULT 0;
SELECT COUNT(*) FROM Organization INTO n;
SET i = 0;
WHILE i < n DO
    -- create a public organization group for this organization
    SELECT id FROM Organization LIMIT i, 1 INTO org_id;
    INSERT IGNORE INTO OrganizationGroup (organization_id, organization_group) VALUES (org_id, 'public');

    -- configure the organization to point to the public group as default
    SET org_group_id = 0;
    SELECT LAST_INSERT_ID() INTO org_group_id;
    IF (org_group_id > 0) 
    THEN INSERT IGNORE INTO OrganizationDefaultGroup (organization_id, organization_group_id) VALUES (org_id, org_group_id);
    END IF;

    -- configure the organization to have unlimited voting capacity as default
    INSERT IGNORE INTO EntityVoteLimit (organization_id, maximum_vote) VALUES (org_id, NULL);
    SET i = i + 1;
END WHILE;
End;
;;
DELIMITER ;

LOAD DATA LOCAL INFILE './data/referenceTables/organization/organizations.csv' IGNORE 
INTO TABLE Organization 
FIELDS TERMINATED BY ',' ESCAPED BY '\\' ENCLOSED BY '"' 
LINES TERMINATED BY '\n' 
(@column1, @column2) SET
organization_domain = @column1,
organization = @column2;

CALL PopulateOrganizationGroup();