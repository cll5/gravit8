# NOTE: MySQL doesn't support LOAD DATA INFILE inside a stored procedure, so we are using user-defined (global) variables instead of local variables inside a stored procedure
# Actually, we'll run into a syntax error if we use user-defined variables too for some odd reason...

# batches of signup codes can be randomly generated from https://www.random.org/strings/?mode=advanced
# NOTE: 10,000 strings generated from May 9, 2016 are used for the first batch of signup codes. Future batches should use a different date as the seed.
# the signup codes are located in data/keys/signupCodes.csv

# or see with MySQL syntax: http://dev.mysql.com/doc/refman/5.7/en/load-data.html
# in the documentation, there are talks about an output method (LOAD DATA OUTFILE) to dump all table data into external files
# NOTE: if LOCAL is set, make sure mysql is started with the option --local-infile=1 (security permissions), and that the file path is relative to where your current directory is
LOAD DATA LOCAL INFILE './data/keys/signupKeys.csv' IGNORE 
INTO TABLE SignupKey 
FIELDS TERMINATED BY ',' ESCAPED BY '\\' ENCLOSED BY '"' 
LINES TERMINATED BY '\n'
(@column1) SET signup_key = @column1;