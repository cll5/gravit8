USE gravit8;

SET autocommit = 0;

-- create top level shared data tables
source tables/category/category.sql;
source tables/attachment/attachment.sql;

-- create organization related data tables
source tables/organization/organization.sql;
source tables/organization/organizationGroup.sql;
source tables/organization/organizationAttachment.sql;
source tables/organization/organizationCategory.sql;

-- create user related data tables
source tables/user/user.sql;
source tables/user/userOrganization.sql;
source tables/user/userProfileAttribute.sql;
source tables/user/profileAttributes/contact.sql;
source tables/user/profileAttributes/biography.sql;

-- create entity related data tables
source tables/entity/entity.sql;
source tables/entity/properties/entityCategory.sql;
source tables/entity/properties/entityLiked.sql;
source tables/entity/properties/entityVoted.sql;
source tables/entity/properties/entityComment.sql;
source tables/entity/properties/entityTeamMember.sql;
source tables/entity/entityContent.sql;
source tables/entity/contents/textBody.sql;
source tables/entity/contents/role.sql;
source tables/entity/contents/weburl.sql;
source tables/entity/entityOrganization.sql;
source tables/entity/types/idea.sql;
source tables/entity/types/pitch.sql;
source tables/entity/types/project.sql;
source tables/entity/entityAttachment.sql;

-- create signup key related data tables
source tables/signupKey/signupKey.sql;

-- fill out lookup tables with predefined values
-- source populateOrganization.sql;
-- source populateReferenceTables.sql;
-- source populateSignupKeys.sql;

COMMIT;
SET autocommit = 1;