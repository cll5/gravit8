# NOTE: The lines termination is OS dependent (unix for \n, dos/windows for \r\n, mac for \r)

# LOCAL INFILE means files that are located on the mysql client side (i.e. relative to where you started the mysql client)
# otherwise, files are checked on the server's data directory
LOAD DATA LOCAL INFILE './data/referenceTables/user/accountTypes.csv' IGNORE 
INTO TABLE UserAccountType 
FIELDS TERMINATED BY ',' ESCAPED BY '\\' ENCLOSED BY '"' 
LINES TERMINATED BY '\n' 
(@column1) SET account_type = @column1;

LOAD DATA LOCAL INFILE './data/referenceTables/user/profileAttributeTypes.csv' IGNORE 
INTO TABLE ProfileAttributeType 
FIELDS TERMINATED BY ',' ESCAPED BY '\\' ENCLOSED BY '"' 
LINES TERMINATED BY '\n' 
(@column1) SET profile_attribute_type = @column1;

LOAD DATA LOCAL INFILE './data/referenceTables/user/contactTypes.csv' IGNORE 
INTO TABLE ContactType 
FIELDS TERMINATED BY ',' ESCAPED BY '\\' ENCLOSED BY '"' 
LINES TERMINATED BY '\n' 
(@column1) SET contact_type = @column1;

LOAD DATA LOCAL INFILE './data/referenceTables/entity/entityTypes.csv' IGNORE 
INTO TABLE EntityType 
FIELDS TERMINATED BY ',' ESCAPED BY '\\' ENCLOSED BY '"' 
LINES TERMINATED BY '\n' 
(@column1) SET entity_type = @column1;

LOAD DATA LOCAL INFILE './data/referenceTables/entity/contentTypes.csv' IGNORE 
INTO TABLE ContentType 
FIELDS TERMINATED BY ',' ESCAPED BY '\\' ENCLOSED BY '"' 
LINES TERMINATED BY '\n' 
(@column1) SET content_type = @column1;

LOAD DATA LOCAL INFILE './data/referenceTables/entity/textBodyTypes.csv' IGNORE 
INTO TABLE TextBodyType 
FIELDS TERMINATED BY ',' ESCAPED BY '\\' ENCLOSED BY '"' 
LINES TERMINATED BY '\n' 
(@column1) SET text_body_type = @column1;

LOAD DATA LOCAL INFILE './data/referenceTables/attachment/attachmentTypes.csv' IGNORE 
INTO TABLE AttachmentType 
FIELDS TERMINATED BY ',' ESCAPED BY '\\' ENCLOSED BY '"' 
LINES TERMINATED BY '\n' 
(@column1) SET attachment_type = @column1;

LOAD DATA LOCAL INFILE './data/referenceTables/category/categories.csv' IGNORE 
INTO TABLE Category 
FIELDS TERMINATED BY ',' ESCAPED BY '\\' ENCLOSED BY '"' 
LINES TERMINATED BY '\n' 
(@column1) SET name = @column1;