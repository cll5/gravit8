CREATE TABLE IF NOT EXISTS Pitch (
    entity_id INT UNSIGNED,
    title TINYTEXT NULL DEFAULT NULL,
    pitch TEXT NULL DEFAULT NULL,

    PRIMARY KEY (entity_id),
    FULLTEXT (title),
    FULLTEXT (pitch),
    FULLTEXT (title, pitch),
    FOREIGN KEY (entity_id) REFERENCES Entity(id) ON UPDATE CASCADE ON DELETE CASCADE
);