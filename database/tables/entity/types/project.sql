CREATE TABLE IF NOT EXISTS Project (
    entity_id INT UNSIGNED,
    title TINYTEXT NULL DEFAULT NULL,
    summary TEXT NULL DEFAULT NULL,

    PRIMARY KEY (entity_id),
    FULLTEXT (title),
    FULLTEXT (summary),
    FULLTEXT (title, summary),
    FOREIGN KEY (entity_id) REFERENCES Entity(id) ON UPDATE CASCADE ON DELETE CASCADE
);