CREATE TABLE IF NOT EXISTS Idea (
    entity_id INT UNSIGNED,
    title TINYTEXT NULL DEFAULT NULL,
    idea TEXT NULL DEFAULT NULL,

    PRIMARY KEY (entity_id),
    FULLTEXT (title),
    FULLTEXT (idea),
    FULLTEXT (title, idea),
    FOREIGN KEY (entity_id) REFERENCES Entity(id) ON UPDATE CASCADE ON DELETE CASCADE
);