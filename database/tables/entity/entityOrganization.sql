CREATE TABLE IF NOT EXISTS EntityOrganization (
    entity_id INT UNSIGNED,
    organization_id INT UNSIGNED,
    organization_group_id INT UNSIGNED,

    PRIMARY KEY (entity_id, organization_id, organization_group_id),
    INDEX (entity_id),
    INDEX (organization_id),
    INDEX (organization_id, organization_group_id),
    FOREIGN KEY (entity_id) REFERENCES Entity(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (organization_id) REFERENCES Organization(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (organization_group_id) REFERENCES OrganizationGroup(id) ON UPDATE CASCADE ON DELETE CASCADE
);