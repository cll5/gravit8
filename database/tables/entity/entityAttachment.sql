CREATE TABLE IF NOT EXISTS EntityAttachment (
    entity_id INT UNSIGNED,
    attachment_id INT UNSIGNED,

    PRIMARY KEY (entity_id, attachment_id),
    INDEX (entity_id),
    INDEX (attachment_id),
    FOREIGN KEY (entity_id) REFERENCES Entity(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (attachment_id) REFERENCES Attachment(id) ON UPDATE CASCADE ON DELETE CASCADE
);