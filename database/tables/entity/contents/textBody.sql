CREATE TABLE IF NOT EXISTS TextBodyType (
    id INT UNSIGNED AUTO_INCREMENT,
    text_body_type VARCHAR(255),

    PRIMARY KEY (id),
    UNIQUE KEY (text_body_type)
);

CREATE TABLE IF NOT EXISTS TextBody (
    entity_content_id INT UNSIGNED,
    text_body_type_id INT UNSIGNED NULL,
    text_body TEXT,

    PRIMARY KEY (entity_content_id),
    FOREIGN KEY (entity_content_id) REFERENCES EntityContent(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (text_body_type_id) REFERENCES TextBodyType(id) ON UPDATE CASCADE ON DELETE SET NULL
);