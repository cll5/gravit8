CREATE TABLE IF NOT EXISTS WebURL (
    entity_content_id INT UNSIGNED,
    url_caption TINYTEXT NULL,
    url TEXT,

    PRIMARY KEY (entity_content_id),
    FOREIGN KEY (entity_content_id) REFERENCES EntityContent(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS EntityContentWebURL (
    entity_content_id INT UNSIGNED,
    web_url_id INT UNSIGNED,

    PRIMARY KEY (entity_content_id, web_url_id),
    FOREIGN KEY (entity_content_id) REFERENCES EntityContent(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (web_url_id) REFERENCES WebURL(entity_content_id) ON UPDATE CASCADE ON DELETE CASCADE
);