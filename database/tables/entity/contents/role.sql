CREATE TABLE IF NOT EXISTS Role (
    entity_content_id INT UNSIGNED,
    role TINYTEXT,

    PRIMARY KEY (entity_content_id),
    FULLTEXT (role),
    FOREIGN KEY (entity_content_id) REFERENCES EntityContent(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS RoleAssignee (
    role_id INT UNSIGNED,
    assignee_id INT UNSIGNED,

    PRIMARY KEY (role_id, assignee_id),
    INDEX (role_id),
    INDEX (assignee_id),
    FOREIGN KEY (role_id) REFERENCES Role(entity_content_id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (assignee_id) REFERENCES User(id) ON UPDATE CASCADE ON DELETE CASCADE
);