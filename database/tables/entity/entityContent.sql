CREATE TABLE IF NOT EXISTS ContentType (
    id INT UNSIGNED AUTO_INCREMENT,
    content_type VARCHAR(32),

    PRIMARY KEY (id),
    UNIQUE KEY (content_type)
);

CREATE TABLE IF NOT EXISTS EntityContent (
    id INT UNSIGNED AUTO_INCREMENT,
    content_type_id INT UNSIGNED NULL,
    entity_id INT UNSIGNED,
    creator_id INT UNSIGNED NULL,
    created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,

    PRIMARY KEY (id),
    INDEX (content_type_id),
    INDEX (entity_id),
    INDEX (creator_id),
    INDEX (created_on),
    FOREIGN KEY (content_type_id) REFERENCES ContentType(id) ON UPDATE CASCADE ON DELETE SET NULL,
    FOREIGN KEY (entity_id) REFERENCES Entity(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (creator_id) REFERENCES User(id) ON UPDATE CASCADE ON DELETE SET NULL
);