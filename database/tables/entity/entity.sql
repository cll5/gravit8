CREATE TABLE IF NOT EXISTS EntityType (
    id INT UNSIGNED AUTO_INCREMENT,
    entity_type VARCHAR(32),

    PRIMARY KEY (id),
    UNIQUE KEY (entity_type)
);

CREATE TABLE IF NOT EXISTS Entity (
    id INT UNSIGNED AUTO_INCREMENT,
    entity_type_id INT UNSIGNED NULL,
    creator_id INT UNSIGNED NULL,
    is_private BOOLEAN DEFAULT FALSE,
    created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,

    PRIMARY KEY (id),
    INDEX (entity_type_id),
    INDEX (creator_id),
    INDEX (is_private),
    INDEX (created_on),
    FOREIGN KEY (entity_type_id) REFERENCES EntityType(id) ON UPDATE CASCADE ON DELETE SET NULL,
    FOREIGN KEY (creator_id) REFERENCES User(id) ON UPDATE CASCADE ON DELETE SET NULL
);

CREATE TABLE IF NOT EXISTS EntityJourney (
    entity_id INT UNSIGNED NULL,
    parent_entity_id INT UNSIGNED NULL,

    UNIQUE KEY (parent_entity_id, entity_id),
    INDEX (entity_id),
    INDEX (parent_entity_id),
    FOREIGN KEY (entity_id) REFERENCES Entity(id) ON UPDATE CASCADE ON DELETE SET NULL,
    FOREIGN KEY (parent_entity_id) REFERENCES Entity(id) ON UPDATE CASCADE ON DELETE SET NULL
);