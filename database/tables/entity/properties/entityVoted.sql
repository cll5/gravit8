-- Definition: maximum_vote is unlimited if set to NULL
CREATE TABLE IF NOT EXISTS EntityVoteLimit (
    organization_id INT UNSIGNED,
    maximum_vote TINYINT UNSIGNED NULL,

    PRIMARY KEY (organization_id),
    FOREIGN KEY (organization_id) REFERENCES Organization(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS EntityVoted (
    entity_id INT UNSIGNED,
    user_id INT UNSIGNED,
    entity_type_id INT UNSIGNED,

    PRIMARY KEY (entity_id, user_id),
    INDEX (entity_id),
    INDEX (user_id),
    INDEX (entity_type_id),
    FOREIGN KEY (entity_id) REFERENCES Entity(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES User(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (entity_type_id) REFERENCES EntityType(id) ON UPDATE CASCADE ON DELETE CASCADE
);