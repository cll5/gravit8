CREATE TABLE IF NOT EXISTS EntityCategory (
    id INT UNSIGNED AUTO_INCREMENT,
    entity_id INT UNSIGNED NOT NULL,
    category_id INT UNSIGNED NOT NULL,

    PRIMARY KEY (id),
    UNIQUE KEY (entity_id, category_id),
    INDEX (entity_id),
    INDEX (category_id),
    FOREIGN KEY (entity_id) REFERENCES Entity(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (category_id) REFERENCES Category(id) ON UPDATE CASCADE ON DELETE CASCADE
);