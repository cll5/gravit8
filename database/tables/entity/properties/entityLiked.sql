CREATE TABLE IF NOT EXISTS EntityLiked (
    entity_id INT UNSIGNED,
    user_id INT UNSIGNED,

    PRIMARY KEY (entity_id, user_id),
    INDEX (entity_id),
    INDEX (user_id),
    FOREIGN KEY (entity_id) REFERENCES Entity(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES User(id) ON UPDATE CASCADE ON DELETE CASCADE
);