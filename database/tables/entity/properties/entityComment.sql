CREATE TABLE IF NOT EXISTS EntityComment (
    id INT UNSIGNED AUTO_INCREMENT,
    entity_id INT UNSIGNED,
    commenter_id INT UNSIGNED NULL,
    message TEXT,
    posted_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,

    PRIMARY KEY (id),
    INDEX (entity_id),
    INDEX (commenter_id),
    INDEX (posted_on),
    FOREIGN KEY (entity_id) REFERENCES Entity(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (commenter_id) REFERENCES User(id) ON UPDATE CASCADE ON DELETE SET NULL
);