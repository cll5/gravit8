CREATE TABLE IF NOT EXISTS AttachmentType (
    id INT UNSIGNED AUTO_INCREMENT,
    attachment_type VARCHAR(32),

    PRIMARY KEY (id),
    UNIQUE KEY (attachment_type)
);

CREATE TABLE IF NOT EXISTS Attachment (
    id INT UNSIGNED AUTO_INCREMENT,
    type_id INT UNSIGNED NULL,
    name TINYTEXT NULL,
    extension TINYTEXT NULL,
    size INT UNSIGNED,
    total_parts INT UNSIGNED DEFAULT 1,
    uploaded BOOLEAN DEFAULT FALSE,
    checksum CHAR(40),

    PRIMARY KEY (id),
    INDEX (type_id),
    INDEX (checksum),
    FULLTEXT (name),
    FOREIGN KEY (type_id) REFERENCES AttachmentType(id) ON UPDATE CASCADE ON DELETE SET NULL
);

CREATE TABLE IF NOT EXISTS PartialAttachment (
    attachment_id INT UNSIGNED,
    part INT UNSIGNED,
    uploaded BOOLEAN DEFAULT FALSE,
    checksum CHAR(40),

    PRIMARY KEY (attachment_id, part),
    INDEX (attachment_id, uploaded),
    INDEX (attachment_id),
    INDEX (checksum),
    FOREIGN KEY (attachment_id) REFERENCES Attachment(id) ON UPDATE CASCADE ON DELETE CASCADE
);