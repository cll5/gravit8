CREATE TABLE IF NOT EXISTS Organization (
    id INT UNSIGNED AUTO_INCREMENT,
    organization_domain VARCHAR(64),
    organization VARCHAR(64),

    PRIMARY KEY (id),
    UNIQUE KEY (organization_domain)
);