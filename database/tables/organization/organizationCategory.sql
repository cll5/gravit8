CREATE TABLE IF NOT EXISTS OrganizationCategory (
    organization_id INT UNSIGNED,
    category_id INT UNSIGNED,

    PRIMARY KEY (organization_id, category_id),
    INDEX (organization_id),
    FOREIGN KEY (organization_id) REFERENCES Organization(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (category_id) REFERENCES Category(id) ON UPDATE CASCADE ON DELETE CASCADE
);