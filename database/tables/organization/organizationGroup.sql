CREATE TABLE IF NOT EXISTS OrganizationGroup (
    id INT UNSIGNED AUTO_INCREMENT,
    organization_id INT UNSIGNED,
    organization_group VARCHAR(255) DEFAULT 'public',

    PRIMARY KEY (id),
    INDEX (organization_id),
    INDEX (organization_group),
    UNIQUE KEY (organization_id, organization_group),
    FOREIGN KEY (organization_id) REFERENCES Organization(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS OrganizationDefaultGroup (
    organization_id INT UNSIGNED,
    organization_group_id INT UNSIGNED,

    PRIMARY KEY (organization_id),
    UNIQUE KEY (organization_id, organization_group_id),
    FOREIGN KEY (organization_id) REFERENCES Organization(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (organization_group_id) REFERENCES OrganizationGroup(id) ON UPDATE CASCADE ON DELETE CASCADE
);