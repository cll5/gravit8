CREATE TABLE IF NOT EXISTS OrganizationAttachment (
    organization_id INT UNSIGNED,
    attachment_id INT UNSIGNED,

    PRIMARY KEY (organization_id, attachment_id),
    INDEX (organization_id),
    INDEX (attachment_id),
    FOREIGN KEY (organization_id) REFERENCES Organization(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (attachment_id) REFERENCES Attachment(id) ON UPDATE CASCADE ON DELETE CASCADE
);