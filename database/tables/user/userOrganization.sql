CREATE TABLE IF NOT EXISTS UserAccountType (
    id INT UNSIGNED AUTO_INCREMENT,
    account_type VARCHAR(32),

    PRIMARY KEY (id),
    UNIQUE KEY (account_type)
);

CREATE TABLE IF NOT EXISTS UserOrganization (
    user_id INT UNSIGNED,
    organization_id INT UNSIGNED,
    organization_group_id INT UNSIGNED NULL,
    user_account_type_id INT UNSIGNED NULL,

    UNIQUE KEY (user_id, organization_id, organization_group_id),
    INDEX (user_id),
    INDEX (organization_id),
    INDEX (organization_id, organization_group_id),
    INDEX (user_account_type_id),
    FOREIGN KEY (user_id) REFERENCES User(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (organization_id) REFERENCES Organization(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (organization_group_id) REFERENCES OrganizationGroup(id) ON UPDATE CASCADE ON DELETE SET NULL,
    FOREIGN KEY (user_account_type_id) REFERENCES UserAccountType(id) ON UPDATE CASCADE ON DELETE SET NULL
);