CREATE TABLE IF NOT EXISTS ContactType (
    id INT UNSIGNED AUTO_INCREMENT,
    contact_type VARCHAR(32),

    PRIMARY KEY (id),
    UNIQUE KEY (contact_type)
);

CREATE TABLE IF NOT EXISTS UserContact (
    profile_attribute_id INT UNSIGNED,
    contact_type_id INT UNSIGNED NULL,
    contact TINYTEXT NOT NULL,

    INDEX (profile_attribute_id),
    INDEX (profile_attribute_id, contact_type_id),
    FOREIGN KEY (profile_attribute_id) REFERENCES UserProfileAttribute(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (contact_type_id) REFERENCES ContactType(id) ON UPDATE CASCADE ON DELETE SET NULL
);