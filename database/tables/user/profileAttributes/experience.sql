CREATE TABLE IF NOT EXISTS Experience (
    profile_attribute_id INT UNSIGNED,
    experience TEXT,
    start_time TIMESTAMP NULL DEFAULT NULL,
    end_time TIMESTAMP NULL DEFAULT NULL,

    INDEX (profile_attribute_id),
    FOREIGN KEY (profile_attribute_id) REFERENCES UserProfileAttribute(id) ON UPDATE CASCADE ON DELETE CASCADE
);