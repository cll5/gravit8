CREATE TABLE IF NOT EXISTS Biography (
    profile_attribute_id INT UNSIGNED,
    biography TEXT,

    PRIMARY KEY (profile_attribute_id),
    FOREIGN KEY (profile_attribute_id) REFERENCES UserProfileAttribute(id) ON UPDATE CASCADE ON DELETE CASCADE
);