CREATE TABLE IF NOT EXISTS User (
    id INT UNSIGNED AUTO_INCREMENT,
    account_name VARCHAR(255) NOT NULL,   # this is the account login name
    hash CHAR(88) NOT NULL,
    salt CHAR(88) NOT NULL,

    PRIMARY KEY (id),
    UNIQUE KEY (account_name)
);

CREATE TABLE IF NOT EXISTS UserProfile (
    user_id INT UNSIGNED,
    first_name TINYTEXT,
    last_name TINYTEXT,
    title TINYTEXT NULL DEFAULT NULL,
    image TINYTEXT NULL DEFAULT NULL,

    PRIMARY KEY (user_id),
    FULLTEXT INDEX (first_name, last_name, title),
    FOREIGN KEY (user_id) REFERENCES User(id) ON UPDATE CASCADE ON DELETE CASCADE
);

/* user portfolio
CREATE TABLE IF NOT EXISTS UserPortfolio (
    user_id INT UNSIGNED,
    entity_id INT UNSIGNED,
    is_publishable BOOLEAN DEFAULT FALSE,

    PRIMARY KEY (user_id, entity_id),
    FOREIGN KEY (user_id) REFERENCES User(id) ON UPDATE CASCADE ON DELETE CASCADE, # really delete all the user's past work if they decide to quit Gravit8?
    FOREIGN KEY (entity_id) REFERENCES Entity(id) ON UPDATE CASCADE ON DELETE CASCADE
);
*/

/* user account setting data
CREATE TABLE IF NOT EXISTS UserAccountSetting (
    user_id INT UNSIGNED,

    PRIMARY KEY (user_id),
    FOREIGN KEY (user_id) REFERENCES User(id) ON UPDATE CASCADE ON DELETE CASCADE
);
*/