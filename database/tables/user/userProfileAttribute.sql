CREATE TABLE IF NOT EXISTS ProfileAttributeType (
    id INT UNSIGNED AUTO_INCREMENT,
    profile_attribute_type VARCHAR(32),

    PRIMARY KEY (id),
    UNIQUE KEY (profile_attribute_type)
);

CREATE TABLE IF NOT EXISTS UserProfileAttribute (
    id INT UNSIGNED AUTO_INCREMENT,
    user_id INT UNSIGNED,
    profile_attribute_type_id INT UNSIGNED NULL,

    PRIMARY KEY (id),
    INDEX (user_id),
    INDEX (user_id, profile_attribute_type_id),
    FOREIGN KEY (user_id) REFERENCES User(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (profile_attribute_type_id) REFERENCES ProfileAttributeType(id) ON UPDATE CASCADE ON DELETE SET NULL
);