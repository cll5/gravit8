CREATE TABLE IF NOT EXISTS SignupKey (
    signup_key CHAR(8),
    used BOOLEAN DEFAULT FALSE,
    reserved BOOLEAN DEFAULT FALSE,

    PRIMARY KEY (signup_key),
    INDEX (used)
);