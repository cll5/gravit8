USE gravit8;
SET autocommit = 0;

#identify the index names of fulltext indices
#SELECT TABLE_SCHEMA, INDEX_NAME, COLUMN_NAME FROM information_schema.statistics WHERE (TABLE_NAME = 'UserProfile') AND (INDEX_TYPE LIKE 'FULLTEXT%');

#TODO: update the full text minimum character length to 3 and rebuild the full text index
#see: https://www.sitepoint.com/community/t/how-to-rebuild-a-fulltext-index/3960
#and: https://dev.mysql.com/doc/refman/5.7/en/fulltext-fine-tuning.html

-- LOAD DATA LOCAL INFILE './data/referenceTables/entity/contentTypes.csv' IGNORE 
-- INTO TABLE ContentType 
-- FIELDS TERMINATED BY ',' ESCAPED BY '\\' ENCLOSED BY '"' 
-- LINES TERMINATED BY '\r\n' 
-- (@column1) SET content_type = @column1;

-- LOAD DATA LOCAL INFILE './data/referenceTables/entity/attachmentTypes.csv' IGNORE 
-- INTO TABLE AttachmentType 
-- FIELDS TERMINATED BY ',' ESCAPED BY '\\' ENCLOSED BY '"' 
-- LINES TERMINATED BY '\r\n' 
-- (@column1) SET attachment_type = @column1;

-- CREATE TABLE IF NOT EXISTS AttachmentType (
--     id INT UNSIGNED AUTO_INCREMENT,
--     attachment_type VARCHAR(32),

--     PRIMARY KEY (id),
--     UNIQUE KEY (attachment_type)
-- );

-- CREATE TABLE IF NOT EXISTS Attachment (
--     entity_content_id INT UNSIGNED,
--     attachment_type_id INT UNSIGNED NULL,
--     attachment_name TINYTEXT NULL,
--     attachment_path TEXT,

--     PRIMARY KEY (entity_content_id),
--     INDEX (attachment_type_id),
--     FULLTEXT (attachment_name),
--     FOREIGN KEY (entity_content_id) REFERENCES EntityContent(id) ON UPDATE CASCADE ON DELETE CASCADE,
--     FOREIGN KEY (attachment_type_id) REFERENCES AttachmentType(id) ON UPDATE CASCADE ON DELETE SET NULL
-- );

-- DROP PROCEDURE IF EXISTS CopyAttachmentToAttachmentTemp;
-- DELIMITER ;;
-- CREATE PROCEDURE CopyAttachmentToAttachmentTemp()
-- BEGIN
-- DECLARE numberOfRows INT DEFAULT 0;
-- DECLARE i INT DEFAULT 0;
-- DECLARE attachmentId INT DEFAULT 0;
-- DECLARE newAttachmentId INT DEFAULT 0;
-- DECLARE entityId INT DEFAULT 0;
-- SELECT COUNT(*) FROM Attachment INTO numberOfRows;
-- SET i = 0;
-- WHILE i < numberOfRows DO
--     SELECT entity_content_id FROM Attachment LIMIT i, 1 INTO attachmentId;
--     INSERT IGNORE INTO AttachmentTemp (type_id, name, extension, size, total_parts, uploaded, checksum) SELECT attachment_type_id, attachment_name, NULL, 0, 1, TRUE, attachment_path FROM Attachment WHERE (entity_content_id = attachmentId);
--     SET newAttachmentId = LAST_INSERT_ID();

--     SELECT entity_id FROM EntityContent WHERE (id = attachmentId) INTO entityId;
--     INSERT IGNORE INTO EntityAttachment (entity_id, attachment_id) VALUE (entityId, newAttachmentId);

--     INSERT IGNORE INTO OrganizationAttachment (organization_id, attachment_id) SELECT organization_id, newAttachmentId FROM EntityOrganization WHERE (entity_id = entityId);

--     SET i = i + 1;
-- END WHILE;
-- End;
-- ;;
-- DELIMITER ;

-- CREATE TABLE IF NOT EXISTS AttachmentTemp (
--     id INT UNSIGNED AUTO_INCREMENT,
--     type_id INT UNSIGNED NULL,
--     name TINYTEXT NULL,
--     extension TINYTEXT NULL,
--     size INT UNSIGNED,
--     total_parts INT UNSIGNED DEFAULT 1,
--     uploaded BOOLEAN DEFAULT FALSE,
--     checksum CHAR(40),

--     PRIMARY KEY (id),
--     INDEX (type_id),
--     INDEX (checksum),
--     FULLTEXT (name),
--     FOREIGN KEY (type_id) REFERENCES AttachmentType(id) ON UPDATE CASCADE ON DELETE SET NULL
-- );

-- CREATE TABLE IF NOT EXISTS PartialAttachment (
--     attachment_id INT UNSIGNED,
--     part INT UNSIGNED,
--     uploaded BOOLEAN DEFAULT FALSE,
--     checksum CHAR(40),

--     PRIMARY KEY (attachment_id, part),
--     INDEX (attachment_id, uploaded),
--     INDEX (attachment_id),
--     INDEX (checksum),
--     FOREIGN KEY (attachment_id) REFERENCES AttachmentTemp(id) ON UPDATE CASCADE ON DELETE CASCADE
-- );

-- CREATE TABLE IF NOT EXISTS EntityAttachment (
--     entity_id INT UNSIGNED,
--     attachment_id INT UNSIGNED,

--     PRIMARY KEY (entity_id, attachment_id),
--     INDEX (entity_id),
--     INDEX (attachment_id),
--     FOREIGN KEY (entity_id) REFERENCES Entity(id) ON UPDATE CASCADE ON DELETE CASCADE,
--     FOREIGN KEY (attachment_id) REFERENCES AttachmentTemp(id) ON UPDATE CASCADE ON DELETE CASCADE
-- );

-- CREATE TABLE IF NOT EXISTS OrganizationAttachment (
--     organization_id INT UNSIGNED,
--     attachment_id INT UNSIGNED,

--     PRIMARY KEY (organization_id, attachment_id),
--     INDEX (organization_id),
--     INDEX (attachment_id),
--     FOREIGN KEY (organization_id) REFERENCES Organization(id) ON UPDATE CASCADE ON DELETE CASCADE,
--     FOREIGN KEY (attachment_id) REFERENCES AttachmentTemp(id) ON UPDATE CASCADE ON DELETE CASCADE
-- );

-- CALL CopyAttachmentToAttachmentTemp();

-- DELETE FROM EntityContent WHERE id IN (SELECT entity_content_id FROM Attachment);
-- DROP TABLE IF EXISTS Attachment;
-- RENAME TABLE AttachmentTemp TO Attachment;

-- ALTER TABLE Category CHANGE COLUMN category name VARCHAR(64);

CREATE TABLE IF NOT EXISTS OrganizationDefaultGroup (
    organization_id INT UNSIGNED,
    organization_group_id INT UNSIGNED,

    PRIMARY KEY (organization_id),
    UNIQUE KEY (organization_id, organization_group_id),
    FOREIGN KEY (organization_id) REFERENCES Organization(id) ON UPDATE CASCADE ON DELETE CASCADE,
    FOREIGN KEY (organization_group_id) REFERENCES OrganizationGroup(id) ON UPDATE CASCADE ON DELETE CASCADE
);

INSERT INTO OrganizationDefaultGroup (organization_id, organization_group_id) SELECT organization_id, id FROM OrganizationGroup WHERE (organization_group = 'public');

COMMIT;
SET autocommit = 1;