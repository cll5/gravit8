DROP PROCEDURE IF EXISTS addUserToOrganizationGroup;
DELIMITER ;;
CREATE PROCEDURE addUserToOrganizationGroup()
BEGIN
DECLARE n INT DEFAULT 0;
DECLARE i INT DEFAULT 0;
DECLARE userId INT;
DECLARE userEmail TINYTEXT;

DECLARE orgId INT DEFAULT 5;
DECLARE orgGroupId INT DEFAULT 39;
DECLARE userAccountTypeId INT DEFAULT 2;

SELECT COUNT(*) FROM referenceUsers INTO n;
SET i = 0;
WHILE i < n DO
    -- get a new user email
    SELECT email FROM referenceUsers LIMIT i, 1 INTO userEmail;

    -- check if user account exists
    SET userId = 0;
    SELECT id FROM User WHERE (LOWER(account_name) = LOWER(userEmail)) INTO userId;

    -- add the existing user to the specify organization group as a regular user
    IF (userId > 0) 
    THEN INSERT IGNORE INTO UserOrganization (user_id, organization_id, organization_group_id, user_account_type_id) VALUE (userId, orgId, orgGroupId, userAccountTypeId);
    END IF;

    SET i = i + 1;
END WHILE;
End;
;;
DELIMITER ;


CREATE TEMPORARY TABLE IF NOT EXISTS referenceUsers (
    first_name TINYTEXT,
    last_name TINYTEXT,
    email TINYTEXT
);

LOAD DATA LOCAL INFILE '~/Desktop/newusers.csv' IGNORE 
INTO TABLE referenceUsers 
FIELDS TERMINATED BY ',' ESCAPED BY '\\' ENCLOSED BY '"' 
LINES TERMINATED BY '\r\n' 
(@column1, @column2, @column3) SET
first_name = @column1,
last_name = @column2,
email = @column3;

CALL addUserToOrganizationGroup();