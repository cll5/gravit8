SELECT 'id', 'category'
UNION ALL
SELECT id, category
FROM ProjectCategoryKeys 
INTO OUTFILE '/var/lib/mysql-files/projectCategoryKeys.csv'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
ESCAPED BY '\\'
LINES
STARTING BY ''
TERMINATED BY '\n';