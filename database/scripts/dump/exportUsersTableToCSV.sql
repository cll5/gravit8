SELECT 'id', 'email', 'hash', 'salt', 'name', 'title', 'bio', 'image'
UNION ALL
SELECT id, email, hash, salt, name, title, bio, image
FROM Users 
INTO OUTFILE '/var/lib/mysql-files/users.csv'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
ESCAPED BY '\\'
LINES
STARTING BY ''
TERMINATED BY '\n';