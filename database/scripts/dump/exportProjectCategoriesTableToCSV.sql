SELECT 'project_id', 'project_category'
UNION ALL
SELECT project_id, project_category
FROM ProjectCategories 
INTO OUTFILE '/var/lib/mysql-files/projectCategories.csv'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
ESCAPED BY '\\'
LINES
STARTING BY ''
TERMINATED BY '\n';