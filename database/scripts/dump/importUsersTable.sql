# Account name and password
LOAD DATA INFILE '/var/www/html/database/data/backup/users.csv' 
IGNORE
INTO TABLE User
FIELDS TERMINATED BY ',' ESCAPED BY '\\' ENCLOSED BY '"'
LINES STARTING BY '' TERMINATED BY '\n'
IGNORE 1 LINES
(@column1, @column2, @column3, @column4, @column5, @column6, @column7, @column8)
SET
account_name = @column2,
hash = @column3,
salt = @column4;


# Name, title, image
-- LOAD DATA INFILE '/var/www/html/database/data/backup/users.csv' 
-- IGNORE
-- INTO TABLE UserProfile
-- FIELDS TERMINATED BY ',' ESCAPED BY '\\' ENCLOSED BY '"'
-- LINES STARTING BY '' TERMINATED BY '\n'
-- IGNORE 1 LINES
-- (@column1, @column2, @column3, @column4, @column5, @column6, @column7, @column8)
-- SET
-- first_name = @column5,
-- title = @column6,
-- image = @column8;


# Biography
-- LOAD DATA INFILE '/var/www/html/database/data/backup/users.csv' 
-- IGNORE
-- INTO TABLE Biography
-- FIELDS TERMINATED BY ',' ESCAPED BY '\\' ENCLOSED BY '"'
-- LINES STARTING BY '' TERMINATED BY '\n'
-- IGNORE 1 LINES
-- (@column1, @column2, @column3, @column4, @column5, @column6, @column7, @column8)
-- SET
-- biography = @column7;