CREATE TEMPORARY TABLE IF NOT EXISTS ReservedKeys AS (
SELECT signup_key 
FROM SignupKey 
WHERE (used = FALSE) AND (reserved = FALSE) 
ORDER BY signup_key ASC 
LIMIT 0, 150
);

UPDATE SignupKey SET reserved = TRUE WHERE signup_key IN (SELECT * FROM ReservedKeys);

SELECT * FROM ReservedKeys 
INTO OUTFILE '/var/lib/mysql-files/keys.csv'
LINES
STARTING BY ''
TERMINATED BY '\n';