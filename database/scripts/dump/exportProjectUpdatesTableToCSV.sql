SELECT 'id', 'project_id', 'contributor_id', 'event_type', 'message', 'updated_on'
UNION ALL
SELECT id, project_id, contributor_id, event_type, message, updated_on
FROM ProjectUpdates 
INTO OUTFILE '/var/lib/mysql-files/projectUpdates.csv'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
ESCAPED BY '\\'
LINES
STARTING BY ''
TERMINATED BY '\n';