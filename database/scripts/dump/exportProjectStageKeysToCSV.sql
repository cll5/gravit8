SELECT 'id', 'stage'
UNION ALL
SELECT id, stage
FROM ProjectStageKeys 
INTO OUTFILE '/var/lib/mysql-files/projectStageKeys.csv'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
ESCAPED BY '\\'
LINES
STARTING BY ''
TERMINATED BY '\n';