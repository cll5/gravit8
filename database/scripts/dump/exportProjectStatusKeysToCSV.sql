SELECT 'id', 'status'
UNION ALL
SELECT id, status
FROM ProjectStatusKeys 
INTO OUTFILE '/var/lib/mysql-files/projectStatusKeys.csv'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
ESCAPED BY '\\'
LINES
STARTING BY ''
TERMINATED BY '\n';