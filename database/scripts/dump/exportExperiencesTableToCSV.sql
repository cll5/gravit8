SELECT 'id', 'user_id', 'title', 'description', 'duration'
UNION ALL
SELECT id, user_id, title, description, duration
FROM Experiences 
INTO OUTFILE '/var/lib/mysql-files/experiences.csv'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
ESCAPED BY '\\'
LINES
STARTING BY ''
TERMINATED BY '\n';