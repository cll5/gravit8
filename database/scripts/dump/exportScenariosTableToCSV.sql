SELECT 'id', 'user_id', 'project_id', 'scenario_before', 'scenario_after', 'contributed_on', 'iteration', 'is_official'
UNION ALL
SELECT id, user_id, project_id, scenario_before, scenario_after, contributed_on, iteration, is_official
FROM Scenarios 
INTO OUTFILE '/var/lib/mysql-files/scenarios.csv'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
ESCAPED BY '\\'
LINES
STARTING BY ''
TERMINATED BY '\n';