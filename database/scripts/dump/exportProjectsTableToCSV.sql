SELECT 'id', 'champion_id', 'idea_title', 'solution_title', 'idea_one_liner', 'solution_one_liner', 'idea_description', 'solution_description', 'is_team_formed', 'created_on', 'release_date', 'stage', 'iteration', 'status'
UNION ALL
SELECT id, champion_id, idea_title, solution_title, idea_one_liner, solution_one_liner, idea_description, solution_description, is_team_formed, created_on, release_date, stage, iteration, status
FROM Projects 
INTO OUTFILE '/var/lib/mysql-files/projects.csv'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
ESCAPED BY '\\'
LINES
STARTING BY ''
TERMINATED BY '\n';