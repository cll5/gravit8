SELECT 'id', 'event_type'
UNION ALL
SELECT id, event_type
FROM EventTypeKeys 
INTO OUTFILE '/var/lib/mysql-files/eventTypeKeys.csv'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
ESCAPED BY '\\'
LINES
STARTING BY ''
TERMINATED BY '\n';