USE gravit8;

source scripts/dump/exportEventTypeKeysToCSV.sql;
source scripts/dump/exportExperiencesTableToCSV.sql;
source scripts/dump/exportProjectCategoriesTableToCSV.sql;
source scripts/dump/exportProjectCategoryKeysToCSV.sql;
source scripts/dump/exportProjectsTableToCSV.sql;
source scripts/dump/exportProjectStageKeysToCSV.sql;
source scripts/dump/exportProjectStatusKeysToCSV.sql;
source scripts/dump/exportProjectUpdatesTableToCSV.sql;
source scripts/dump/exportScenariosTableToCSV.sql;
source scripts/dump/exportUsersTableToCSV.sql;