<?php
    /**
     * GIT DEPLOYMENT SCRIPT
     *
     * Used for automatically deploying websites via github or bitbucket, more deets here:
     *
     *      https://gist.github.com/1809044
     */

    // Run the commands for output
    $output = '';

    //NOTE: see http://stackoverflow.com/questions/33315767/bitbucket-webhook-not-sending-payload
    //TODO: figure out why the above site's method doesn't work here
    if (($_SERVER['REQUEST_METHOD'] === 'GET') || ($_SERVER['REQUEST_METHOD'] === 'POST')) {
        // The commands
        $commands = array(
            'whoami',
            'git status',
            'git pull -v',
            'git log -6 --pretty=oneline'
        );

        foreach ($commands AS $command){
            // Run it
            $tmp = shell_exec($command . ' 2>&1');
            // Output
            $output .= "<span style=\"color: #6BE234;\">\$</span> <span style=\"color: #729FCF;\">{$command}\n</span>";
            $output .= htmlentities(trim($tmp)) . "\n";
        }
    }

    // Make it pretty for manual user access (and why not?)
?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title>GIT DEPLOYMENT SCRIPT</title>
</head>
<body style="background-color: #000000; color: #FFFFFF; font-weight: bold; padding: 0 10px;">
<pre>
 .  ____  .    ____________________________
 |/      \|   |                            |
[| <span style="color: #FF0000;">&hearts;    &hearts;</span> |]  | Git Deployment Script v0.1 |
 |___==___|  /              &copy; oodavid 2012 |
              |____________________________|

<?php echo $output; ?>
</pre>
</body>
</html>