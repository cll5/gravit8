gravit8.Idea = (function() {
    var organizationInstance = gravit8.Organization.getInstance();
    var entityHandler = gravit8.Entity.getInstance();
    var likeHandler = new LikeHandler();
    var voteHandler = new VoteHandler();

    var templates = {
        cardTemplate: _.template($("#idea-card-template").html()),
        listTemplate: _.template($("#idea-list-template").html())
    };

    return {
        createNewIdea: function(event) {
            //form validation for required contents
            var ideaForm = $("#create-idea-form");
            var validRequiredContents = entityHandler.validateRequiredContents(ideaForm);

            //don't submit if the required contents are not valid
            if (!validRequiredContents) {
                return;
            }

            var title = $("textarea[name = 'title']").val().trim();
            var idea = $("textarea[name = 'idea']").val().trim();
            var categoryId = +$("select[name = 'categories'] option:selected").val();

            var entityContentData = entityHandler.dataModels.newEntityData(title, idea, false, [categoryId]);

            $("#create-idea [data-content-added]").each(function(index, optionalContent) {
                var content = $(optionalContent);
                var contentName = optionalContent.getAttribute("name");

                switch (contentName) {
                    case "description":
                    case "background":
                    case "stakeholder":
                    case "existing-solution":
                    case "market-analysis":
                    case "other-information":
                        var text = content.val().trim();
                        if (text.length > 0) {
                            entityContentData[contentName] = text;
                        }
                        break;
                }
            });

            $.ajax({
                type: "POST",
                url: "/submit/new/idea",
                data: utilities.formatJSON(entityContentData),
                success: function(response) {
                    if (response.success) {
                        window.location.href = "/idea/" + response.data.entityId;
                    }
                },
                dataType: "json"
            });
        },
        updateIdeaContents: function(event) {
            var route = window.location.pathname;
            var entityId = route.match(/\/(\d+)/);
            if (entityId) {
                //form validation
                var form = $("#edit-idea-form");
                var validRequiredContents = entityHandler.validateRequiredContents(form);

                //don't submit if the required contents are not valid
                if (!validRequiredContents) {
                    return;
                }

                //get the initial entity contents and keep track of all the changes
                var initialEntityData = event.data.initialEntityData;
                var entityData = entityHandler.updateEntityData($("#edit-idea"), initialEntityData);

                //submit the changes
                if (_.isEmpty(entityData.edited) && _.isEmpty(entityData.removed) && _.isEmpty(entityData.added)) {
                    window.location.href = route;
                } else {
                    var entity = {
                        "id": +entityId.pop(),
                        "contents": entityData
                    };

                    $.ajax({
                        type: "POST",
                        url: "/update/idea",
                        data: utilities.formatJSON(entity),
                        success: function() {
                            window.location.href = route;
                        },
                        error: function(request, status, error) {
                            switch (request.status) {
                                case 412:
                                    //retry?
                                    break;

                                case 500:
                                    //update failed
                                    break;

                                default:
                                    //generic error
                            }
                        },
                        dataType: "json"
                    });
                }
            } else {
                //entity id doesn't exist
            }
        },
        loadMoreOldIdeas: function(event) {
            var entitiesContainer = $("#browse-view [data-container = 'ideas']");
            var loadedIds = entitiesContainer.find(".card-view[data-entity-id]").map(function(index, idea) {
                return +idea.getAttribute("data-entity-id");
            }).get();
            referenceId = (loadedIds.length > 0) ? _.min(loadedIds) : 0;

            var requestData = {
                "id": referenceId,
                "lookupDirection": "oldest"
            };

            $.ajax({
                type: "GET",
                url: "/get/ideas",
                data: utilities.formatJSON(requestData),
                success: function(response) {
                    var numberOfEntities = response.data.entities.length;
                    if (numberOfEntities > 0) {
                        var cards = new Array(numberOfEntities);
                        var lists = new Array(numberOfEntities);
                        response.data.entities.forEach(function(idea, index) {
                            var card = $(templates.cardTemplate({idea: idea}));
                            card.find(".reaction-buttons .like-icon").on("click", likeHandler.likeCard);
                            card.find(".reaction-buttons .vote-icon").on("click", voteHandler.voteCard);

                            var list = $(templates.listTemplate({idea: idea}));
                            list.find(".reaction-buttons .like-icon").on("click", likeHandler.likeCard);
                            list.find(".reaction-buttons .vote-icon").on("click", voteHandler.voteCard);

                            if ($(".card-view-icon").hasClass("list-view-icon")) {
                                card.addClass("hidden");
                            } else {
                                list.addClass("hidden");
                            }

                            cards[index] = card;
                            lists[index] = list;
                        });
                        entitiesContainer.find(".invisible").first().before(cards);
                        entitiesContainer.find(".card-view").first().before(lists);

                        //resort the entities
                        $(".sort-by-options select[name = 'sort-by-options']").not(".hidden").trigger("change");
                    }
                },
                dataType: "json"
            });
        },
        loadMoreNewIdeas: function(event) {
            var entitiesContainer = $("#browse-view [data-container = 'ideas']");
            var loadedIds = entitiesContainer.find(".card-view[data-entity-id]").map(function(index, idea) {
                return +idea.getAttribute("data-entity-id");
            }).get();
            referenceId = (loadedIds.length > 0) ? _.max(loadedIds) : 0;

            var requestData = {
                "id": referenceId,
                "lookupDirection": "newest"
            };

            $.ajax({
                type: "GET",
                url: "/get/ideas",
                data: utilities.formatJSON(requestData),
                success: function(response) {
                    var numberOfEntities = response.data.entities.length;
                    if (numberOfEntities > 0) {
                        var cards = new Array(numberOfEntities);
                        var lists = new Array(numberOfEntities);
                        response.data.entities.forEach(function(idea, index) {
                            var card = $(templates.cardTemplate({idea: idea}));
                            card.find(".reaction-buttons .like-icon").on("click", likeHandler.likeCard);
                            card.find(".reaction-buttons .vote-icon").on("click", voteHandler.voteCard);

                            var list = $(templates.listTemplate({idea: idea}));
                            list.find(".reaction-buttons .like-icon").on("click", likeHandler.likeCard);
                            list.find(".reaction-buttons .vote-icon").on("click", voteHandler.voteCard);

                            if ($(".card-view-icon").hasClass("list-view-icon")) {
                                card.addClass("hidden");
                            } else {
                                list.addClass("hidden");
                            }

                            cards[index] = card;
                            lists[index] = list;
                        });
                        entitiesContainer.find(".item-container").first().before(cards);
                        entitiesContainer.find(".card-view").first().before(lists);

                        //resort the entities
                        $(".sort-by-options select[name = 'sort-by-options']").not(".hidden").trigger("change");

                        organizationInstance.refreshStatistics();
                    }
                },
                dataType: "json"
            });
        },
        refreshIdeas: function(event) {
            var entityIds = $("#browse-view [data-container = 'ideas'] .item-container.card-view[data-entity-id]").map(function(index, entity) {
                return +entity.getAttribute("data-entity-id");
            }).get();

            if (entityIds.length > 0) {
                var entities = {
                    "ids": entityIds
                };

                $.ajax({
                    type: "GET",
                    url: "/get/ideas",
                    data: utilities.formatJSON(entities),
                    success: function(response) {
                        if (response.data.entities.length > 0) {
                            response.data.entities.forEach(function(entity) {
                                var items = $("#browse-view [data-container = 'ideas'] .item-container[data-entity-id = " + entity.id + "]");
                                entityHandler.refreshData.refreshBasicEntity(items, entity);
                            });

                            //resort the entities
                            $(".sort-by-options select[name = 'sort-by-options']").not(".hidden").trigger("change");
                        }
                    },
                    dataType: "json"
                });
            }
        },
        refreshIdea: function(event) {
            var entityId = window.location.pathname.match(/\/(\d+)/);
            if (entityId) {
                $.ajax({
                    type: "GET",
                    url: "/get/idea/" + entityId.pop(),
                    success: function(entity) {
                        if (entity.data) {
                            entityHandler.refreshData.refreshEntityPage($("body"), entity.data);
                        }
                    },
                    dataType: "json"
                });
            }
        }
    };
});