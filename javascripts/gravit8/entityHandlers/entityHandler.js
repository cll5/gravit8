gravit8.Entity = (function() {
    var instance;

    function loadTemplates() {
        return {
            assignableRoleFormTemplate: _.template($("#add-role-form-template").html()),
            roleAssigneeTemplate: _.template($("#role-assignee-template").html()),
            teamMemberFormTemplate: _.template($("#add-team-member-form-template").html()),
            profileImageTemplate: _.template($("#profile-image-template").html()),
            descriptionFormTemplate: _.template($("#add-description-form-template").html()),
            backgroundFormTemplate: _.template($("#add-background-form-template").html()),
            stakeholderFormTemplate: _.template($("#add-stakeholder-form-template").html()),
            existingSolutionFormTemplate: _.template($("#add-existing-solution-form-template").html()),
            marketAnalysisFormTemplate: _.template($("#add-market-analysis-form-template").html()),
            otherInformationFormTemplate: _.template($("#add-other-information-form-template").html()),
            attachmentFormTemplate: _.template($("#add-attachment-form-template").html()),
            uploadedAttachmentFormTemplate: _.template($("#uploaded-attachment-template").html())
        };
    }

    function Entity() {
        //load other helpers
        var searchHandler = new SearchHandler();
        // var attachmentHandler = gravit8.AttachmentHandler.getInstance();

        var templates = loadTemplates();
        var dataModels = {
            newEntityData: function(title, oneLiner, setPrivate, categories) {
                var defaultCategoryId = +$("select[name = 'categories'] option:enabled").filter(":first").val();
                var defaultCategory = [defaultCategoryId];
                if (!defaultCategoryId) {
                    defaultCategory = [];
                }

                return {
                    setPrivate: setPrivate || false,
                    title: title || "No title provided",
                    oneLiner: oneLiner || "No one liner provided",
                    categories: (Array.isArray(categories) && (categories.length > 0)) ? categories : defaultCategory
                };
            },
            updateEntityData: function(editedContents, removedContents, addedContents) {
                return {
                    edited: editedContents || {},
                    removed: removedContents || {},
                    added: addedContents || {}
                };
            },
            initialEntityData: function(contentForm) {
                var entityContents = {};
                contentForm.find("[name]").each(function(index, entityContent) {
                    var field = $(entityContent);
                    var fieldName = entityContent.getAttribute("name");

                    switch (fieldName) {
                        case "role":
                            var role = field.val().trim();
                            var roleAssignees = field.siblings("ul").children("[name = 'role-assignee']").map(function(index, roleAssignee) {
                                return +roleAssignee.getAttribute("data-assignee-id");
                            }).get();
                            var roleData = dataModels.roleData(role, roleAssignees);

                            var contentId = +entityContent.getAttribute("data-entity-content-id");
                            if ("roles" in entityContents) {
                                entityContents["roles"][contentId] = roleData;
                            } else {
                                var roles = {};
                                roles[contentId] = roleData;
                                entityContents["roles"] = roles;
                            }
                            break;

                        case "role-assignee":
                            break;

                        default:
                            var contentId = +entityContent.getAttribute("data-entity-content-id");
                            var content = field.val().trim();
                            entityContents[fieldName] = dataModels.entityContentData(contentId, content);
                    }
                });

                return entityContents;
            },
            entityContentData: function(contentId, content) {
                return {
                    id: contentId || null,
                    value: content || null
                };
            },
            roleData: function(role, assignees) {
                return {
                    role: role,
                    assignees: Array.isArray(assignees) ? assignees : []
                };
            },
            teamMemberData: function(memberId) {
                return dataModels.entityContentData(memberId);
            }
        };

        var __helpers = {
            isRoleDataDifferent: function(roleDataA, roleDataB) {
                var isRoleDifferent = (roleDataA.role !== roleDataB.role);
                var isRoleAssigneesDifferent = !_.isEqual(_.sortBy(roleDataA.assignees), _.sortBy(roleDataB.assignees));
                return (isRoleDifferent || isRoleAssigneesDifferent);
            }
        };

        //UI interactions
        var __enableContentCard = function(contentCard) {
            contentCard.removeAttribute("data-module-disabled");
            $(contentCard).removeClass("disabled-module");
        };

        var __disableContentCard = function(contentCard) {
            contentCard.setAttribute("data-module-disabled", "");
            $(contentCard).addClass("disabled-module");
        };

        //change to __entityContentAdded
        var __newContentDetected = function(event) {
            if (!event.target.hasAttribute("data-content-added")) {
                event.target.setAttribute("data-content-added", "");
            }
        };

        var __entityContentEdited = function(event) {
            if (!event.target.hasAttribute("data-content-edited")) {
                event.target.setAttribute("data-content-edited", "");
            }
        };

        var __entityContentRemoved = function(event) {
            var form = $(event.target).closest(".entity-content").addClass("hidden");
            var field = form.find("[data-entity-content-id]").get(0);
            if (!field && form.find("[name = 'attachment']").exists()) {
                field = form.find("[name = 'attachment']").get(0);
            }
            if (!field.hasAttribute("data-content-removed")) {
                field.setAttribute("data-content-removed", "");
            }

            //we do not care if the entity content is edited when we are removing it
            if (field.hasAttribute("data-content-edited")) {
                field.removeAttribute("data-content-edited");
            }
        };

        var __hasContentChanged = function(content, initialContent) {
            return (content !== initialContent);
        };

        var __removeContent = function(event) {
            $(event.target).closest(".entity-content").remove();
        };

        var textareaHandler = function() {
            $("textarea").each(function() {
                this.setAttribute("style", "height:" + (this.scrollHeight) + "px; overflow-y: hidden;");
            }).on("input keypaste", function() {
                this.style.height = "";
                this.style.height = (this.scrollHeight) + "px";
            });
        };
        
        var __oneTimeUseTextBodyContent = function(cardId, formTemplate) {
            var card = document.getElementById(cardId);
            if (!card.hasAttribute("data-module-disabled")) {
                __disableContentCard(card);

                //render the form to the DOM
                var form = $(formTemplate());
                form.find("textarea").on("keydown paste", __newContentDetected);
                form.find(".cancel-remove-button button").on("click", null, card, function(event) {
                    __enableContentCard(event.data);
                }).on("click", __removeContent);

                textareaHandler();

                $(".create-buttons").before(form);
            }
        };

        var __newUploadAttachmentForm = function(attachment) {
            var uploadedAttachmentForm = $(templates.uploadedAttachmentFormTemplate({attachment: attachment}));
            uploadedAttachmentForm.find(".upload-button button").on("click", __newContentDetected);
            uploadedAttachmentForm.find(".cancel-remove-button button").on("click", __entityContentRemoved);
            $("#attachments").append(uploadedAttachmentForm);
        };

        var __renderAttachmentContent = function(event) {
            var card = document.getElementById("add-attachment-button");
            if (!card.hasAttribute("data-module-disabled")) {
                __disableContentCard(card);

                //render the attachment form to the DOM
                var attachmentForm = $(templates.attachmentFormTemplate());
                attachmentForm.find(".upload-button input[type = 'file']").on("change", null, {newUploadAttachmentForm: __newUploadAttachmentForm}, attachmentHandler.uploadAttachments);
                attachmentForm.find(".upload-button button").on("click", function(event) {
                    $(event.target).siblings("input[type = 'file']").trigger("click");
                });
                attachmentForm.find(".cancel-remove-button button").on("click", null, card, function(event) {
                    __enableContentCard(event.data);
                }).on("click", __removeContent);

                $(".create-buttons").before(attachmentForm);
            }
        };

        var requiredContentsAreValid;
        var __validateRequiredContent = function(index, requiredField) {
            switch (requiredField.tagName.toLowerCase()) {
                case "textarea":
                    var requiredContent = $(requiredField);
                    var requiredFormMessage = requiredContent.next(".invalid-message");
                    if (!requiredContent.val().trim()) {
                        requiredContent.focus();
                        requiredFormMessage.removeClass("hidden");
                        requiredContentsAreValid = false;
                    } else {
                        requiredFormMessage.addClass("hidden");
                    }
                    break;
            }
        };

        //TODO: how to pass in entityContentData to the each function?
        //from project, pitch, idea
        // var __includeOptionalContent = function(index, optionalContent) {
        //     console.log(entityContentData);
        //     var content = $(optionalContent);
        //     var contentName = optionalContent.getAttribute("name");

        //     switch (contentName) {
        //         case "role":
        //             var role = content.val().trim();
        //             if (role.length > 0) {
        //                 var roleData = createEntity.newRoleData(role);
        //                 roleData.assignees = content.parent().find("[name = 'role-assignee'] .selected").map(function(index, profileImage) {
        //                     return +$(profileImage).parent().attr("data-assignee-id");
        //                 }).get();

        //                 if (!("roles" in entityContentData)) {
        //                     entityContentData["roles"] = [roleData];
        //                 } else {
        //                     entityContentData["roles"].push(roleData);
        //                 }
        //             }
        //             break;

        //         case "description":
        //         case "background":
        //         case "stakeholder":
        //         case "existing-solution":
        //         case "market-analysis":
        //         case "other-information":
        //             var text = content.val().trim();
        //             if (text.length > 0) {
        //                 entityContentData[contentName] = text;
        //             }
        //             break;
        //     }
        // };

        var __filterTeamMembers = function() {
            return teamList;
        };

        //TODO: throttle the searching
        var __addTeamMemberHandler = function(event) {
            var teamMemberForm = $(templates.teamMemberFormTemplate());
            var methods = {
                filterPeople: __filterTeamMembers,
                selectResultHandler: __addPersonToTeam
            };
            teamMemberForm.find("textarea").on("keydown paste", __editTeamMemberHandler)
                                           .on("input paste", null, methods, searchHandler.searchPeopleSuggestionHandler);
            teamMemberForm.find(".cancel-remove-button button").on("click", __removeContent)
                                                               .on("click", __editTeamMemberHandler);

            $("#team-members").append(teamMemberForm);
        };

        var __editTeamMemberHandler = function(event) {
            if (!event.target.hasAttribute("data-content-removed")) {
                event.target.setAttribute("data-content-removed", "");
            }

            // we do not care if the entity content is edited when we are removing it
            if (event.target.hasAttribute("data-content-edited")) {
                event.target.removeAttribute("data-content-edited");
            }

            // update UI
            $(event.target).closest(".entity-content").find("[data-team-member-id]").empty().attr("data-team-member-id", '');

            // update team list
            __updateTeamList();
        };

        var teamList = [];
        var __updateTeamList = function() {
            teamList = $("#team-members [data-element-type = 'profile-image-container'][data-team-member-id != '']").map(function(index, profileImageContainer) {
                return +profileImageContainer.getAttribute("data-team-member-id");
            }).get();

            //trigger another event to update roles
            if ($("#roles").exists()) {
                __updateRoleAssignees();
            }
        };

        var __addPersonToTeam = function(event) {
            //check if person already exists in the team members list (this check is likely redundant since the duplicate check is already done in the database level)
            var person = event.data.person;
            if (!_.contains(teamList, person.id)) {
                //update the UI
                var profileImage = templates.profileImageTemplate({
                    person: person,
                    imageSizeClass: "profile-image-icon"
                });

                var contentEntity = $(event.target).closest(".entity-content");
                var teamMember = contentEntity.find("[data-element-type = 'profile-image-container']").empty().html(profileImage).attr("data-team-member-id", person.id).get(0);

                if (!teamMember.hasAttribute("data-content-added")) {
                    teamMember.setAttribute("data-content-added", "");
                }

                //NOTE: should this be abstracted to the search handler level?
                var personName = person.firstName + " " + person.lastName;
                contentEntity.find("[data-element-type = 'search-bar'] textarea").val(personName);

                //add the person.id to the team members list
                __updateTeamList();

                //include person to all roles
                if ($("#roles").exists()) {
                    __addPersonToRoleAssignees({data: person, profileImage: profileImage});
                }
            }
        };

        var __addAssignableRoleHandler = function(event) {
            var roleForm = $(templates.assignableRoleFormTemplate());
            roleForm.find("textarea").on("keydown paste", __newContentDetected);
            roleForm.find(".cancel-remove-button button").on("click", __removeContent);

            if (event.data && event.data.includeRoleAssignee) {
                var assignees = roleAssignees.map(function(roleAssignee) {
                    return __newRoleAssignee(roleAssignee);
                });
                roleForm.find("[data-element-type = 'role-assignees']").append(assignees);
            }

            $("#roles").append(roleForm);
        };

        var roleAssignees = [];
        var __addPersonToRoleAssignees = function(person) {
            roleAssignees.push(person);

            //update UI
            var assignee = __newRoleAssignee(person);
            $("#roles [data-element-type = 'role-assignees']").append(assignee);
        };

        var __newRoleAssignee = function(person) {
            var roleAssignee = $(templates.roleAssigneeTemplate({assignee: person.data}));
            roleAssignee.append(person.profileImage);
            roleAssignee.on("click", function(event) {
                if (event.target.hasAttribute("data-assignee-id")) {
                    $(event.target).find(".profile-image-icon").toggleClass("selected");
                } else {
                    $(event.target).toggleClass("selected");
                }
            }).on("click", __editRoleAssigneeHandler);
            return roleAssignee;
        };

        var __editRoleAssigneeHandler = function(event) {
            if (!$(event.target).closest(".entity-content").find("textarea").attr("data-content-edited")) {
                $(event.target).closest(".entity-content").find("textarea").attr("data-content-edited", "");
            }
        };

        var __updateRoleAssignees = function() {
            //update roleAssignees by removing removed team members
            roleAssignees = roleAssignees.filter(function(roleAssignee) {
                return _.contains(teamList, roleAssignee.data.id);
            });

            //update UI
            $("#roles [data-element-type = 'role-assignees'] [name = 'role-assignee']").filter(function(index, roleAssignee) {
                var assigneeId = +roleAssignee.getAttribute("data-assignee-id");
                return !_.contains(teamList, assigneeId);
            }).remove();
        };


        //TODO: this should probably be done in the HTML layer since it is related to contents
        var __addContentButtonUIUpdateHandler = function(index, text) {
            return (text == "+ Add Content") ? "✖ Cancel" : "+ Add Content";
        };


        var __packageEditedEntityContents = function(entityContents, initialEntityContents) {
            initialEntityContents = initialEntityContents || {};

            var editedEntityContents = {};
            entityContents.forEach(function(entityContent, index) {
                var field = $(entityContent);
                var fieldName = entityContent.getAttribute("name");

                switch (fieldName) {
                    case "title":
                        var text = field.val().trim();
                        if ((text.length > 0) && __hasContentChanged(text, initialEntityContents[fieldName].value)) {
                            editedEntityContents[fieldName] = dataModels.entityContentData(null, text);
                        }
                        break;

                    case "idea":
                    case "pitch":
                    case "summary":
                    case "one-liner":
                        var text = field.val().trim();
                        if ((text.length > 0) && __hasContentChanged(text, initialEntityContents[fieldName].value)) {
                            editedEntityContents["oneLiner"] = dataModels.entityContentData(null, text);
                        }
                        break;

                    case "categories":
                        if (entityContent.hasAttribute("data-entity-content-id")) {
                            var entityContentId = +entityContent.getAttribute("data-entity-content-id");
                            var categoryId = +field.val();
                            var category = dataModels.entityContentData(entityContentId, categoryId);
                            
                            if (!(fieldName in editedEntityContents)) {
                                editedEntityContents[fieldName] = [category];
                            } else {
                                editedEntityContents[fieldName].push(category);
                            }
                        }
                        break;

                    //TODO: to be completed
                    case "team-members":
                        editedEntityContents["teamMembers"] = [];
                        break;

                    case "role":
                        var role = field.val().trim();
                        var roleAssignees = field.parent().find("[name = 'role-assignee'] .selected").map(function(index, profileImage) {
                            return +$(profileImage).parent().attr("data-assignee-id");
                        }).get();
                        var roleData = dataModels.roleData(role, roleAssignees);

                        var entityContentId = +entityContent.getAttribute("data-entity-content-id");

                        //compare the role data to the initial values
                        if (("roles" in initialEntityContents) && (entityContentId in initialEntityContents["roles"])) {
                            var initialRoleData = initialEntityContents["roles"][entityContentId];

                            if (__helpers.isRoleDataDifferent(roleData, initialRoleData)) {
                                var entityContent = dataModels.entityContentData(entityContentId, roleData);
                                if ("roles" in editedEntityContents) {
                                    editedEntityContents["roles"].push(entityContent);
                                } else {
                                    editedEntityContents["roles"] = [entityContent];
                                }
                            }
                        }
                        break;

                    //TODO: to be completed
                    case "attachment":
                        editedEntityContents["attachment"] = [];
                        break;

                    case "description":
                    case "background":
                    case "stakeholder":
                    case "existing-solution":
                    case "market-analysis":
                    case "other-information":
                        var text = field.val().trim();
                        if (entityContent.hasAttribute("data-entity-content-id") && (text.length > 0) && __hasContentChanged(text, initialEntityContents[fieldName].value)) {
                            var entityContentId = +entityContent.getAttribute("data-entity-content-id");
                            editedEntityContents[fieldName] = dataModels.entityContentData(entityContentId, text);
                        }
                        break;
                }
            });

            return editedEntityContents;
        };

        var __packageRemovedEntityContents = function(entityContents, initialEntityContents) {
            initialEntityContents = initialEntityContents || {};

            var removedEntityContents = {};
            entityContents.forEach(function(entityContent, index) {
                var field = $(entityContent);
                var fieldName = entityContent.getAttribute("name");

                var entityContentId = +entityContent.getAttribute("data-entity-content-id");
                var entityContentData = dataModels.entityContentData(entityContentId);

                switch (fieldName) {
                    case "team-member":
                        if ("teamMembers" in removedEntityContents) {
                            removedEntityContents["teamMembers"].push(entityContentData);
                        } else {
                            removedEntityContents["teamMembers"] = [entityContentData];
                        }
                        break;

                    case "role":
                        if ("roles" in removedEntityContents) {
                            removedEntityContents["roles"].push(entityContentData);
                        } else {
                            removedEntityContents["roles"] = [entityContentData];
                        }
                        break;

                    case "attachment":
                        var attachmentId = +field.closest("[data-attachment-id]").attr("data-attachment-id");
                        entityContentData = dataModels.entityContentData(attachmentId);
                        if ("attachments" in removedEntityContents) {
                            removedEntityContents["attachments"].push(entityContentData);
                        } else {
                            removedEntityContents["attachments"] = [entityContentData];
                        }
                        break;

                    case "description":
                    case "background":
                    case "stakeholder":
                    case "existing-solution":
                    case "market-analysis":
                    case "other-information":
                        removedEntityContents[fieldName] = entityContentData;
                        break;
                }
            });

            return removedEntityContents;
        };

        var __packageAddedEntityContents = function(entityContents, initialEntityContents) {
            initialEntityContents = initialEntityContents || {};

            var addedEntityContents = {};
            entityContents.forEach(function(entityContent, index) {
                var field = $(entityContent);
                var fieldName = entityContent.getAttribute("name");

                switch (fieldName) {
                    case "attachment":
                        var attachmentId = +field.closest("[data-attachment-id]").attr("data-attachment-id");

                        //TODO: grab the changed name from a text box or something
                        var attachmentName = field.text().trim() || null;
                        if (attachmentId > 0) {
                            var entityContentData = dataModels.entityContentData(attachmentId, attachmentName);
                            if ("attachments" in addedEntityContents) {
                                addedEntityContents["attachments"].push(entityContentData);
                            } else {
                                addedEntityContents["attachments"] = [entityContentData];
                            }
                        }
                        break;

                    case "team-member":
                        var teamMemberId = +entityContent.getAttribute("data-team-member-id");
                        if (teamMemberId !== 0) {
                            var entityContentData = dataModels.entityContentData(teamMemberId);
                            if ("teamMembers" in addedEntityContents) {
                                addedEntityContents["teamMembers"].push(entityContentData);
                            } else {
                                addedEntityContents["teamMembers"] = [entityContentData];
                            }
                        }
                        break;

                    case "role":
                        var role = field.val().trim();
                        if (role.length > 0) {
                            var roleAssignees = field.parent().find("[name = 'role-assignee'] .selected").not("hidden").map(function(index, profileImage) {
                                return +$(profileImage).parent().attr("data-assignee-id");
                            }).get();

                            var roleData = dataModels.roleData(role, roleAssignees);
                            var entityContent = dataModels.entityContentData(null, roleData);
                            if ("roles" in addedEntityContents) {
                                addedEntityContents["roles"].push(entityContent);
                            } else {
                                addedEntityContents["roles"] = [entityContent];
                            }
                        }
                        break;

                    case "attachment":
                        break;

                    case "description":
                    case "background":
                    case "stakeholder":
                    case "existing-solution":
                    case "market-analysis":
                    case "other-information":
                        var text = field.val().trim();
                        if (text.length > 0) {
                            addedEntityContents[fieldName] = dataModels.entityContentData(null, text);
                        }
                        break;
                }
            });

            return addedEntityContents;
        };



        //refreshing data to UI functions
        var __refreshCard = function(cardView, entity) {
            cardView.find(".name").text(entity.title);

            var oneLiner = "";
            switch (entity.entityType) {
                case "idea":
                    oneLiner = entity.idea;
                    break;

                case "pitch":
                    oneLiner = entity.pitch;
                    break;

                case "project":
                    oneLiner = entity.summary;
                    break;
            }
            cardView.find(".one-liner").text(oneLiner);

            //update reaction buttons and counters
            var reactionCounters = cardView.find(".reaction-buttons [data-counter-type]");
            reactionCounters.filter("[data-counter-type = 'like']").text(entity.numberOfLikes);
            reactionCounters.filter("[data-counter-type = 'vote']").text(entity.numberOfVotes);

            //NOTE: the like icon only changes when the user clicks on it, so it doesn't make sense to constantly check for and update this
            // var likeIcon = cardView.find(".reaction-buttons .like-icon");
            // if (entity.liked) {
            //     likeIcon.addClass("liked");
            // } else {
            //     likeIcon.removeClass("liked");
            // }

            reactionCounters.filter("[data-counter-type = 'comment']").text(entity.numberOfComments);

            //update profile image
            var profileImages = cardView.find(".project-champion .profile-image-small");
            profileImages.not("[style]").text(entity.creator.initials);
            profileImages.filter("[style]").css("background-image", "url(" + entity.creator.imagePath + ")");

            var creatorFullname = entity.creator.firstName + " " + entity.creator.lastName;
            cardView.find(".project-champion .started-by .entity-creator-name").text(creatorFullname);

            var oldCategory = cardView.get(0).className.match(/([a-z0-9\-]*)-category/);
            if (oldCategory !== null) {
                oldCategory = oldCategory.pop();
                cardView.removeClass(oldCategory + "-category");
            }

            var newCategory = entity.categories[0].name.replace(/\s/g, '-');
            cardView.addClass(newCategory + "-category");

            var newCategoryName = entity.categories[0].name.replace("and", "&");
            cardView.find(".tag").text(newCategoryName);
        };

        var __refreshList = function(listView, entity) {
            listView.find(".name").text(entity.title);

            var oneLiner = "";
            switch (entity.entityType) {
                case "idea":
                    oneLiner = entity.idea;
                    break;

                case "pitch":
                    oneLiner = entity.pitch;
                    break;

                case "project":
                    oneLiner = entity.summary;
                    break;
            }
            listView.find(".one-liner").text(oneLiner);

            //update reaction buttons and counters
            var reactionCounters = listView.find(".reaction-buttons [data-counter-type]");
            reactionCounters.filter("[data-counter-type = 'like']").text(entity.numberOfLikes);
            reactionCounters.filter("[data-counter-type = 'vote']").text(entity.numberOfVotes);

            //NOTE: the like icon only changes when the user clicks on it, so it doesn't make sense to constantly check for and update this
            // var likeIcon = listView.find(".reaction-buttons .like-icon");
            // if (entity.liked) {
            //     likeIcon.addClass("liked");
            // } else {
            //     likeIcon.removeClass("liked");
            // }

            reactionCounters.filter("[data-counter-type = 'comment']").text(entity.numberOfComments);

            //update profile image
            var profileImages = listView.find(".project-champion .profile-image-small");
            profileImages.not("[style]").text(entity.creator.initials);
            profileImages.filter("[style]").css("background-image", "url(" + entity.creator.imagePath + ")");

            var creatorFullname = entity.creator.firstName + " " + entity.creator.lastName;
            listView.find(".project-champion .started-by .entity-creator-name").text(creatorFullname);

            var oldCategory = listView.get(0).className.match(/([a-z0-9\-]*)-category/);
            if (oldCategory !== null) {
                oldCategory = oldCategory.pop();
                listView.removeClass(oldCategory + "-category");
            }

            var newCategory = entity.categories[0].name.replace(/\s/g, '-');
            listView.addClass(newCategory + "-category");

            var newCategoryName = entity.categories[0].name.replace("and", "&");
            listView.find(".tag").text(newCategoryName);
        };

        var refreshDataMethods = {
            refreshBasicEntity: function(entity, data) {
                //update card view
                var cardView = entity.filter(".card-view");
                if (cardView.exists()) {
                    __refreshCard(cardView, data);
                }

                //update list view
                var listView = entity.filter(".list-view");
                if (listView.exists()) {
                    __refreshList(listView, data);
                }
            },
            refreshEntityPage: function(entity, data) {
                //TODO: to be implemented
                // console.log(data);

                //update the category
                if (data.categories.length > 0) {
                    var category = data.categories[0].name;
                    var categoryTag = entity.find(".category-tags .category-tag");

                    var oldCategory = categoryTag.text();
                    var newCategory = category.replace("and", "&");
                    if (newCategory !== oldCategory) {
                        var oldCategoryClass = oldCategory.replace("&", "and").replace(/\s/g, "-") + "-category";
                        categoryTag.removeClass(oldCategoryClass);

                        var newCategoryClass = category.replace(/\s/g, "-") + "-category";
                        categoryTag.addClass(newCategoryClass);
                        categoryTag.text(newCategory);
                    }
                }

                //update the creator
                if (data.creator.id !== null) {
                    //to be implemented
                }

                //update the title
                if (data.title !== null) {
                    var titleContainer = entity.find("[data-section = 'title']");
                    var oldTitle = titleContainer.text();
                    if (data.title !== oldTitle) {
                        titleContainer.text(data.title);

                        //update the last part of the breadcrumb
                        entity.find("[data-section = 'breadcrumb'] b").text(data.title);
                    }
                }

                //update the one liner (idea, pitch, or summary)
                if ((data.idea || data.pitch || data.summary) !== null) {
                    var oneLinerContainer = entity.find("[data-section = 'one-liner']");
                    var oldOneLiner = oneLinerContainer.text();
                    var newOneLiner = "";
                    switch (data.entityType) {
                        case "idea":
                            newOneLiner = data.idea;
                            break;

                        case "pitch":
                            newOneLiner = data.pitch;
                            break;

                        case "project":
                            newOneLiner = data.summary;
                            break;
                    }

                    if (newOneLiner !== oldOneLiner) {
                        oneLinerContainer.text(newOneLiner);
                    }
                }

                //update the likes counter
                var likeCounter = entity.find("[data-counter-type = 'like']");
                if (+likeCounter.text().match(/(\d+)/)[0] != data.numberOfLikes) {
                    likeCounter.text(data.numberOfLikes + " " + ((data.numberOfLikes != 1) ? "likes" : "like"));
                }

                //update the votes counter
                var voteCounter = entity.find("[data-counter-type = 'vote']");
                if (voteCounter.exists() && (+voteCounter.text().match(/(\d+)/)[0] != data.numberOfVotes)) {
                    voteCounter.text(data.numberOfVotes + " " + ((data.numberOfVotes != 1) ? "votes" : "vote"));
                }

                //NOTE: comment counter is updated from comment handler
                //update the other contents
                //update the optional contents
            }
        };

        return {
            dataModels: dataModels,
            addTeamMemberButtonHandler: __addTeamMemberHandler,
            addAssignableRoleButtonHandler: __addAssignableRoleHandler,
            addContentButtonHandler: function(event) {
                $(event.target).text(__addContentButtonUIUpdateHandler)
                               .parent()
                               .toggleClass("cancel-remove-button");

                var addContentModule = $(".add-content-module");
                if (addContentModule.hasClass("hidden")) { 
                    addContentModule.removeClass("hidden");
                } else {
                    addContentModule.addClass("hidden");
                }
            },
            descriptionCardHandler: function(event) {
                __oneTimeUseTextBodyContent("add-description-button", templates.descriptionFormTemplate);
            },
            backgroundCardHandler: function(event) {
                __oneTimeUseTextBodyContent("add-background-button", templates.backgroundFormTemplate);
            },
            stakeholderCardHandler: function(event) {
                __oneTimeUseTextBodyContent("add-stakeholder-button", templates.stakeholderFormTemplate);
            },
            existingSolutionCardHandler: function(event) {
                __oneTimeUseTextBodyContent("add-existing-solution-button", templates.existingSolutionFormTemplate);
            },
            marketAnalysisCardHandler: function(event) {
                __oneTimeUseTextBodyContent("add-market-analysis-button", templates.marketAnalysisFormTemplate);
            },
            otherInformationCardHandler: function(event) {
                __oneTimeUseTextBodyContent("add-other-information-button", templates.otherInformationFormTemplate);
            },
            attachmentCardHandler: __renderAttachmentContent,
            validateRequiredContents: function(contentForm) {
                requiredContentsAreValid = true;
                contentForm.find("[required]").reverse().each(__validateRequiredContent);
                return requiredContentsAreValid;
            },
            includeOptionalContents: function(optionalContents, entityContentData) {
                //TODO: how to pass in entityContentData to the each function?
                // var entityContentData = entityContentData;
                // optionalContents.each(__includeOptionalContent);
            },
            updateEntityData: function(contentForm, initialEntityData) {
                initialEntityData = initialEntityData || {};

                var entityContents = dataModels.updateEntityData();
                var editedEntityContents = contentForm.find("[data-content-edited]").get();

                entityContents["edited"] = __packageEditedEntityContents(editedEntityContents, initialEntityData);

                var removedEntityContents = contentForm.find("[data-content-removed]").get();
                entityContents["removed"] = __packageRemovedEntityContents(removedEntityContents, initialEntityData);

                var addedEntityContents = contentForm.find("[data-content-added]").get();
                entityContents["added"] = __packageAddedEntityContents(addedEntityContents, initialEntityData);

                return entityContents;
            },
            bindHandlersToEditForm: function(contentForm) {
                // populate team list
                $("#team-members div[data-team-member-id]").each(function(index, element) {
                    var id = +element.getAttribute("data-team-member-id");
                    teamList.push(id);
                });

                //TODO: where is teamMemberForm used?
                // search handler methods
                var teamMemberForm = $(templates.teamMemberFormTemplate());
                var methods = {
                    filterPeople: __filterTeamMembers,
                    selectResultHandler: __addPersonToTeam
                };
                teamMemberForm.find("textarea").on("keydown paste", __editTeamMemberHandler)
                                               .on("input paste", null, methods, searchHandler.searchPeopleSuggestionHandler);
                teamMemberForm.find(".cancel-remove-button button").on("click", __removeContent)
                                                                   .on("click", __editTeamMemberHandler);
                // populate role assignees
                // call AJAX to fetch data from the server
                // When the data gets returned, response would update the role assignees with the appropriate forms of data
                var people = {
                    "ids": teamList
                };

                $.ajax({
                    type: "GET",
                    url: "/get/people",
                    data: utilities.formatJSON(people),
                    success: function(response) {
                        response.data.people.forEach(function(person) {
                            var profileImage = templates.profileImageTemplate({
                                person: person,
                                imageSizeClass: "profile-image-icon"
                            });

                            var roleAssignee = {
                                data: person, 
                                profileImage: profileImage
                            };

                            roleAssignees.push(roleAssignee);
                        });
                    },
                    dataType: "json"
                });

                //detect editing on existing contents
                contentForm.find("textarea[name]").not("textarea[name='team-member']").on("keydown paste", __entityContentEdited);
                contentForm.find("select[name]").on("change", __entityContentEdited);

                //enable adding attachments
                var attachmentForm = contentForm.find("#attachment-section-body");
                var attachmentList = contentForm.find("#attachments");
                attachmentForm.find(".upload-button input[type = 'file']").on("change", function(event) {
                    if (event.target.files && event.target.files.length > 0) {
                        for (var index = 0; index < event.target.files.length; index++) {
                            var attachment = event.target.files[index];

                            var route = window.location.pathname;
                            var entityId = route.match(/\/(\d+)/);
                            entityId = entityId ? +entityId.pop() : null;
                            var attachmentHandler = new gravit8.AttachmentHandler(attachment, entityId);
                            var referenceHandler = {
                                attachmentHandler: attachmentHandler
                            };

                            var data = {
                                attachment: attachment,
                                formattedSize: utilities.formatBytes(attachment.size)
                            };
                            var anAttachment = $(templates.uploadedAttachmentFormTemplate(data));

                            //NOTE: does this belong here?
                            anAttachment.find("[name = 'attachment']").attr("data-content-added", "");

                            anAttachment.find(".cancel-remove-button button").on("click", null, referenceHandler, function(event) {
                                event.data.attachmentHandler.cancel();
                            }).on("click", __entityContentRemoved);
                            anAttachment.find(".pause-button button").on("click", null, referenceHandler, function(event) {
                                var pauseButton = $(event.target);
                                pauseButton.closest(".pause-button").addClass("hidden");
                                pauseButton.closest(".entity-content").find(".resume-button").removeClass("hidden");
                                event.data.attachmentHandler.pause();
                            });
                            anAttachment.find(".resume-button button").on("click", null, referenceHandler, function(event) {
                                var resumeButton = $(event.target);
                                resumeButton.closest(".resume-button").addClass("hidden");
                                resumeButton.closest(".entity-content").find(".pause-button").removeClass("hidden");
                                event.data.attachmentHandler.resume();
                            });
                            attachmentList.append(anAttachment);

                            //attachments that are larger than the size limit will be skipped
                            if (attachment.size > (100 * 1024 * 1024)) {
                                anAttachment.find(".status").text("(" + data.formattedSize + ") This attachment is too large to upload onto Gravit8 - it will be skipped. If it is a video, try uploading it onto Youtube, Vimeo, etc. and document the link.");
                                return;
                            }

                            attachmentHandler.setUITemplate(anAttachment);
                            attachmentHandler.setUserControl($(".user-controls"));
                            attachmentHandler.start();
                        }

                        //allow the same file(s) to be selected again sequentially
                        event.target.value = null;
                    }
                });
                attachmentForm.find(".upload-button button").on("click", function(event) {
                    $(event.target).siblings("input[type = 'file']").trigger("click");
                });

                //detect role assignee(s) change
                contentForm.find("#roles [name = 'role-assignee']").on("click", function(event) {
                    $(event.target).parentsUntil(".entity-content", "ul").siblings("textarea[name = 'role']").trigger("keydown");
                }).on("click", function(event) {
                    if (event.target.hasAttribute("data-assignee-id")) {
                        $(event.target).find(".profile-image-icon").toggleClass("selected");
                    } else {
                        $(event.target).toggleClass("selected");
                        $(event.target).siblings().toggleClass("selected");
                    }
                }).on("click", __editRoleAssigneeHandler);

                //remove button handling
                contentForm.find(".cancel-remove-button button").on("click", function(event) {
                    //update the UI - enable optional entity content cards and other UI
                    var fieldName = $(event.target).closest(".entity-content").find("[name]").attr("name");
                    switch (fieldName) {
                        case "team-member":
                            // remove logic for initial values
                            var teamMemberId = $(event.target).parent().siblings("[data-element-type = 'profile-image-container']").attr("data-team-member-id");
                            $("#roles").find("textarea").trigger("keydown");
                            $("#roles").find("li[data-assignee-id = '" + teamMemberId + "']").remove();
                            break;

                        case "role":
                            break;

                        case "attachment":
                            break;

                        case "description":
                        case "background":
                        case "stakeholder":
                        case "existing-solution":
                        case "market-analysis":
                        case "other-information":
                            var cardId = "add-" + fieldName + "-button";
                            var card = document.getElementById(cardId);
                            __enableContentCard(card);
                            break;
                    }
                }).on("click", __entityContentRemoved);
            },
            refreshData: refreshDataMethods
        };
    }

    return {
        getInstance: function() {
            if (!instance) {
                instance = new Entity();
            }

            return instance;
        }
    };
})();