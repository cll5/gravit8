gravit8.Pitch = (function() {
    var organizationInstance = gravit8.Organization.getInstance();
    var entityHandler = gravit8.Entity.getInstance();
    var likeHandler = new LikeHandler();
    var voteHandler = new VoteHandler();

    var templates = {
        cardTemplate: _.template($("#pitch-card-template").html()),
        listTemplate: _.template($("#pitch-list-template").html())
    };

    return {
        createNewPitch: function(event) {
            //form validation for required contents
            var pitchForm = $("#create-pitch-form");
            var validRequiredContents = entityHandler.validateRequiredContents(pitchForm);

            //don't submit if the required contents are not valid
            if (!validRequiredContents) {
                return;
            }

            var title = $("textarea[name = 'title']").val().trim();
            var pitch = $("textarea[name = 'pitch']").val().trim();
            var categoryId = +$("select[name = 'categories'] option:selected").val();

            var entityContentData = entityHandler.dataModels.newEntityData(title, pitch, false, [categoryId]);

            $("#create-pitch [data-content-added]").each(function(index, optionalContent) {
                var content = $(optionalContent);
                var contentName = optionalContent.getAttribute("name");

                switch (contentName) {
                    case "role":
                        var role = content.val().trim();
                        if (role.length > 0) {
                            var roleData = entityHandler.dataModels.roleData(role);
                            roleData.assignees = content.parent().find("[name = 'role-assignee'] .selected").map(function(index, profileImage) {
                                return +$(profileImage).parent().attr("data-assignee-id");
                            }).get();

                            if ("roles" in entityContentData) {
                                entityContentData["roles"].push(roleData);
                            } else {
                                entityContentData["roles"] = [roleData];
                            }
                        }
                        break;

                    case "description":
                    case "background":
                    case "stakeholder":
                    case "existing-solution":
                    case "market-analysis":
                    case "other-information":
                        var text = content.val().trim();
                        if (text.length > 0) {
                            entityContentData[contentName] = text;
                        }
                        break;
                }
            });

            $.ajax({
                type: "POST",
                url: "/submit/new/pitch",
                data: utilities.formatJSON(entityContentData),
                success: function(response) {
                    if (response.success) {
                        window.location.href = "/pitch/" + response.data.entityId;
                    }
                },
                dataType: "json"
            });
        },
        updatePitchContents: function(event) {
            var route = window.location.pathname;
            var entityId = route.match(/\/(\d+)/);
            if (entityId) {
                //form validation
                var form = $("#edit-pitch-form");
                var validRequiredContents = entityHandler.validateRequiredContents(form);

                //don't submit if the required contents are not valid
                if (!validRequiredContents) {
                    return;
                }

                //get the initial entity contents and keep track of all the changes
                var initialEntityData = event.data.initialEntityData;
                var entityData = entityHandler.updateEntityData($("#edit-pitch"), initialEntityData);

                //submit the changes
                if (_.isEmpty(entityData.edited) && _.isEmpty(entityData.removed) && _.isEmpty(entityData.added)) {
                    window.location.href = route;
                } else {
                    var entity = {
                        "id": +entityId.pop(),
                        "contents": entityData
                    };

                    $.ajax({
                        type: "POST",
                        url: "/update/pitch",
                        data: utilities.formatJSON(entity),
                        success: function(response) {
                            window.location.href = route;
                        },
                        error: function(request, status, error) {
                            switch (request.status) {
                                case 412:
                                    //retry?
                                    break;

                                case 500:
                                    //update failed
                                    break;

                                default:
                                    //generic error
                            }
                        },
                        dataType: "json"
                    });
                }
            } else {
                //entity id doesn't exist
            }
        },
        loadMoreOldPitches: function(event) {
            var entitiesContainer = $("#browse-view [data-container = 'pitches']");
            var loadedIds = entitiesContainer.find(".card-view[data-entity-id]").map(function(index, pitch) {
                return +pitch.getAttribute("data-entity-id");
            }).get();
            var referenceId = (loadedIds.length > 0) ? _.min(loadedIds) : 0;

            var requestData = {
                "id": referenceId,
                "lookupDirection": "oldest"
            };

            $.ajax({
                type: "GET",
                url: "/get/pitches",
                data: utilities.formatJSON(requestData),
                success: function(response) {
                    var numberOfEntities = response.data.entities.length;
                    if (numberOfEntities > 0) {
                        var cards = new Array(numberOfEntities);
                        var lists = new Array(numberOfEntities);
                        response.data.entities.forEach(function(pitch, index) {
                            var card = $(templates.cardTemplate({pitch: pitch}));
                            card.find(".reaction-buttons .like-icon").on("click", likeHandler.likeCard);
                            card.find(".reaction-buttons .vote-icon").on("click", voteHandler.voteCard);

                            var list = $(templates.listTemplate({pitch: pitch}));
                            list.find(".reaction-buttons .like-icon").on("click", likeHandler.likeCard);
                            list.find(".reaction-buttons .vote-icon").on("click", voteHandler.voteCard);

                            if ($(".card-view-icon").hasClass("list-view-icon")) {
                                card.addClass("hidden");
                            } else {
                                list.addClass("hidden");
                            }

                            cards[index] = card;
                            lists[index] = list;
                        });
                        entitiesContainer.find(".invisible").first().before(cards);
                        entitiesContainer.find(".card-view").first().before(lists);

                        //resort the entities
                        $(".sort-by-options select[name = 'sort-by-options']").not(".hidden").trigger("change");
                    }
                },
                dataType: "json"
            });
        },
        loadMoreNewPitches: function(event) {
            var entitiesContainer = $("#browse-view [data-container = 'pitches']");
            var loadedIds = entitiesContainer.find(".card-view[data-entity-id]").map(function(index, pitch) {
                return +pitch.getAttribute("data-entity-id");
            }).get();
            var referenceId = (loadedIds.length > 0) ? _.max(loadedIds) : 0;

            var requestData = {
                "id": referenceId,
                "lookupDirection": "newest"
            };

            $.ajax({
                type: "GET",
                url: "/get/pitches",
                data: utilities.formatJSON(requestData),
                success: function(response) {
                    var numberOfEntities = response.data.entities.length;
                    if (numberOfEntities > 0) {
                        var cards = new Array(numberOfEntities);
                        var lists = new Array(numberOfEntities);
                        response.data.entities.forEach(function(pitch, index) {
                            var card = $(templates.cardTemplate({pitch: pitch}));
                            card.find(".reaction-buttons .like-icon").on("click", likeHandler.likeCard);
                            card.find(".reaction-buttons .vote-icon").on("click", voteHandler.voteCard);

                            var list = $(templates.listTemplate({pitch: pitch}));
                            list.find(".reaction-buttons .like-icon").on("click", likeHandler.likeCard);
                            list.find(".reaction-buttons .vote-icon").on("click", voteHandler.voteCard);
                            
                            if ($(".card-view-icon").hasClass("list-view-icon")) {
                                card.addClass("hidden");
                            } else {
                                list.addClass("hidden");
                            }
                            cards[index] = card;
                            lists[index] = list;
                        });

                        entitiesContainer.find(".item-container").first().before(cards);
                        entitiesContainer.find(".card-view").first().before(lists);

                        //resort the entities
                        $(".sort-by-options select[name = 'sort-by-options']").not(".hidden").trigger("change");

                        organizationInstance.refreshStatistics();
                    }
                },
                dataType: "json"
            });
        },
        refreshPitches: function(event) {
            var entityIds = $("#browse-view [data-container = 'pitches'] .item-container.card-view[data-entity-id]").map(function(index, entity) {
                return +entity.getAttribute("data-entity-id");
            }).get();

            if (entityIds.length > 0) {
                var entities = {
                    "ids": entityIds
                };

                $.ajax({
                    type: "GET",
                    url: "/get/pitches",
                    data: utilities.formatJSON(entities),
                    success: function(response) {
                        if (response.data.entities.length > 0) {
                            response.data.entities.forEach(function(entity) {
                                var items = $("#browse-view [data-container = 'pitches'] .item-container[data-entity-id = " + entity.id + "]");
                                entityHandler.refreshData.refreshBasicEntity(items, entity);
                            });

                            //resort the entities
                            $(".sort-by-options select[name = 'sort-by-options']").not(".hidden").trigger("change");
                        }
                    },
                    dataType: "json"
                });
            }
        },
        refreshPitch: function(event) {
            var entityId = window.location.pathname.match(/\/(\d+)/);
            if (entityId) {
                $.ajax({
                    type: "GET",
                    url: "/get/pitch/" + entityId.pop(),
                    success: function(entity) {
                        if (entity.data) {
                            entityHandler.refreshData.refreshEntityPage($("body"), entity.data);
                        }
                    },
                    dataType: "json"
                });
            }
        }
    };
});