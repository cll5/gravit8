gravit8.Organization = (function() {
    var instance;

    function Organization() {
        var __refreshStatistics = function() {
            $.ajax({
                type: "GET",
                url: "/get/summary",
                success: function(response) {
                    var count = response.data.summary.count;
                    var dataCounts = $(".header-browse .browse-tabs a span");
                    dataCounts.filter("[data-count-idea]").text(count.idea);
                    dataCounts.filter("[data-count-pitch]").text(count.pitch);
                    dataCounts.filter("[data-count-project]").text(count.project);
                    dataCounts.filter("[data-count-user]").text(count.user);
                },
                dataType: "json"
            });
        };

        return {
            refreshStatistics: __refreshStatistics
        };
    }

    return {
        getInstance: function() {
            if (!instance) {
                instance = new Organization();
            }

            return instance;
        }
    };
})();