var SearchHandler = (function() {
    var MINIMUM_SEARCH_LENGTH = 3;
    var searchSuggestionTemplate = _.template($("#search-people-result-template").html());

    var __searchPeopleSuggestionHandler = function(event) {
        var selectResultHandler = event.data && event.data.selectResultHandler;
        var searchSuggestionsContainer = $(event.target).parents("[data-element-type = 'search-bar']").find(".search-suggestions ul");

        var keywords = $(event.target).val().trim();
        if (keywords.length < MINIMUM_SEARCH_LENGTH) {
            searchSuggestionsContainer.empty().addClass("hidden");
            return;
        }

        var searchParameters = {
            "keywords": keywords,
            "excludePeople": []
        };

        if (event.data && event.data.filterPeople) {
            searchParameters.excludePeople = event.data.filterPeople();
        }

        $.ajax({
            type: "POST",
            url: "/submit/search/people",
            data: utilities.formatJSON(searchParameters),
            success: function(response) {
                if (response.data.length > 0) {
                    var searchResults = new Array(response.data.length);
                    response.data.forEach(function(person, index) {
                        var searchResult = $(searchSuggestionTemplate({person: person}));

                        if (selectResultHandler) {
                            searchResult.on("click", null, {person: person}, selectResultHandler);
                        }

                        searchResult.on("click", function() {
                            searchSuggestionsContainer.empty();
                        });

                        searchResults[index] = searchResult;
                    });
                    searchSuggestionsContainer.html(searchResults).removeClass("hidden");
                }
            },
            dataType: "json"
        });
    };

    //TODO: to be implemented
    var __searchEntitySuggestionHandler = function(event) {};

    return {
        searchEntitySuggestionHandler: __searchEntitySuggestionHandler,
        searchPeopleSuggestionHandler: __searchPeopleSuggestionHandler
    };
});