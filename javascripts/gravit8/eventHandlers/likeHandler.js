var LikeHandler = (function() {
    var ENTITY_TABS = ["ideas", "pitches", "projects"];

    var updateLikeButton = function(index, text) {
        return (text === "Like") ? "Unlike" : "Like";
    };

    var updateVoteButton = function(index, text) {
        return (text === "Vote") ? "Unvote" : "Vote";
    };

    return {
        likeCard: function(event) {
            event.preventDefault();

            var entityView = $(this).parents("[data-entity-id]");
            var otherEntityView = entityView.siblings("[data-entity-id = " + entityView.attr("data-entity-id") + "]");
            var entityId = +entityView.attr("data-entity-id");
            var entity = {
                "id": entityId
            };

            $.ajax({
                type: "POST",
                url: "/like/entity",
                data: utilities.formatJSON(entity),
                success: function(response) {
                    if (response.success) {
                        entityView.add(otherEntityView)
                                  .find(".like-icon")
                                  .toggleClass("liked")
                                  .siblings("[data-counter-type = 'like']")
                                  .text(response.data.numberOfLikesForEntity);

                        entityView.find(".like-button button")
                                  .text(updateLikeButton);
                    }
                },
                dataType: "json"
            });

            return false;
        },
        likeEntity: function(event) {
            var entityId = +window.location.pathname.match(/\/(\d+)/).pop();
            if (entityId > 0) {
                var entity = {
                    "id": entityId
                };

                $.ajax({
                    type: "POST",
                    url: "/like/entity",
                    data: utilities.formatJSON(entity),
                    success: function(response) {
                        if (response.success) {
                            var numberOfLikes = response.data.numberOfLikesForEntity;
                            $(".reaction-buttons .like-icon").toggleClass("liked")
                                                             .siblings("[data-counter-type = 'like']")
                                                             .text(numberOfLikes + " " + ((numberOfLikes != 1) ? "likes" : "like"));
                        }
                    },
                    dataType: "json"
                });
            } else {
                //entity id not found, then what?
            }
            return false;
        }
    };
});

var VoteHandler = (function() {
    var updateVoteButton = function(index, text) {
        return (text === "Vote") ? "Unvote" : "Vote";
    };

    return {
        voteCard: function(event) {
            event.preventDefault();

            var entityView = $(this).parents("[data-entity-id]");
            var otherEntityView = entityView.siblings("[data-entity-id = " + entityView.attr("data-entity-id") + "]");
            var entityId = +entityView.attr("data-entity-id");
            var entity = {
                "id": entityId
            };

            $.ajax({
                type: "POST",
                url: "/vote/entity",
                data: utilities.formatJSON(entity),
                success: function(response) {
                    if (response.success) {
                        entityView.add(otherEntityView)
                                  .find(".reaction-buttons .vote-icon")
                                  .toggleClass("voted")
                                  .siblings("[data-counter-type = 'vote']")
                                  .text(response.data.numberOfVotesForEntity);

                        entityView.find(".vote-button button")
                                  .text(updateVoteButton);

                        //update the vote summary bar
                        var userVoteCapacity = response.data.voteCapacity[response.data.entityType];
                        var voteSummaryBars = $("#vote-summary [data-entity-type = '" + response.data.entityType + "']");
                        voteSummaryBars.find("span").text(userVoteCapacity.votesUsed);
                        if (userVoteCapacity.votesRemaining === 0) {
                            voteSummaryBars.addClass("limit-reached");
                        } else {
                            voteSummaryBars.removeClass("limit-reached");
                        }
                    } else {
                        //you're out of votes, show some warning?
                        // $("#voting-status-bar").addClass("limit-reached");
                    }
                },
                dataType: "json"
            });

            return false;
        },
        voteEntity: function(event) {
            var entityId = +window.location.pathname.match(/\/(\d+)/).pop();
            if (entityId > 0) {
                var entity = {
                    "id": entityId
                };

                $.ajax({
                    type: "POST",
                    url: "/vote/entity",
                    data: utilities.formatJSON(entity),
                    success: function(response) {
                        if (response.success) {
                            var numberOfVotes = response.data.numberOfVotesForEntity;
                            $(".reaction-buttons .vote-icon").toggleClass("voted")
                                                             .siblings("[data-counter-type = 'vote']")
                                                             .text(numberOfVotes + " " + ((numberOfVotes != 1) ? "votes" : "vote"));

                            //update the vote summary bar
                            var userVoteCapacity = response.data.voteCapacity[response.data.entityType];
                            var voteSummaryBars = $("#vote-summary [data-entity-type = '" + response.data.entityType + "']");
                            voteSummaryBars.find("span").text(userVoteCapacity.votesUsed);
                            if (userVoteCapacity.votesRemaining === 0) {
                                voteSummaryBars.addClass("limit-reached");
                            } else {
                                voteSummaryBars.removeClass("limit-reached");
                            }
                        } else {
                            //you're out of votes, show the vote counter and warning
                            // $("#voting-status-bar").addClass("limit-reached");
                        }
                    },
                    dataType: "json"
                });
            } else {
                //entity id not found, then what?
            }
            return false;
        }
    };
});