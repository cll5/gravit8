var CommentHandler = (function() {
    var commentTemplate = _.template($("#comment-template").html());

    var postCommentHandler = function(event) {
        var entityId = +window.location.pathname.match(/\d+/).shift();

        var field = $("#comment-control textarea[name = 'comment']");
        var message = field.val().trim();

        var defaultLatestCommentId = 0;
        var latestComment = $("#comments li:first");
        var latestCommentId = (latestComment.length > 0) ? +latestComment.attr("data-comment-id") : defaultLatestCommentId;

        if (!_.isEmpty(message) && (entityId > 0)) {
            var comment = {
                "entityId": entityId,
                "message": message,
                "latestCommentId": latestCommentId
            };

            $.ajax({
                type: "POST",
                url: "/submit/comment",
                data: utilities.formatJSON(comment),
                success: function(response) {
                    if (response.success) {
                        var newComments = "";
                        response.data.comments.forEach(function(comment) {
                            newComments += commentTemplate({comment: comment});
                        });
                        $("#comments ul").prepend(newComments);

                        //clear comment box
                        field.val("");

                        var commentControlContent = "Comments (" + response.data.numberOfComments + ")";
                        $("#number-of-comments").text(commentControlContent);

                        var commentCounterContent = response.data.numberOfComments + " " + ((response.data.numberOfComments != 1) ? "comments" : "comment");
                        $(".comment-icon ~ p[data-counter-type = 'comment']").text(commentCounterContent);
                    }
                },
                dataType: "json"
            });
        }
    };

    var loadOldCommentsHandler = function(event) {
        var entityId = +window.location.pathname.match(/\d+/).shift();

        var defaultOldestCommentId = 0;
        var oldestComment = $("#comments li:last");
        var referenceId = (oldestComment.length > 0) ? +oldestComment.attr("data-comment-id") : defaultOldestCommentId;

        var requestData = {
            "entityId": entityId,
            "referenceId": referenceId,
            "lookupDirection": "oldest"
        };

        $.ajax({
            type: "GET",
            url: "/get/comments",
            data: utilities.formatJSON(requestData),
            success: function(response) {
                var oldComments = "";
                response.data.comments.forEach(function(comment) {
                    oldComments += commentTemplate({comment: comment});
                });
                $("#comments ul").append(oldComments);

                var commentControlContent = "Comments (" + response.data.numberOfComments + ")";
                $("#number-of-comments").text(commentControlContent);

                var commentCounterContent = response.data.numberOfComments + " " + ((response.data.numberOfComments != 1) ? "comments" : "comment");
                $(".comment-icon ~ p[data-counter-type = 'comment']").text(commentCounterContent);
            },
            dataType: "json"
        });
    };

    var loadNewCommentsHandler = function(event) {
        var entityId = +window.location.pathname.match(/\d+/).shift();

        var defaultNewestCommentId = 0;
        var newestComment = $("#comments li:first");
        var referenceId = (newestComment.length > 0) ? +newestComment.attr("data-comment-id") : defaultNewestCommentId;

        var requestData = {
            "entityId": entityId,
            "referenceId": referenceId,
            "lookupDirection": "newest"
        };

        $.ajax({
            type: "GET",
            url: "/get/comments",
            data: utilities.formatJSON(requestData),
            success: function(response) {
                var newComments = "";
                response.data.comments.forEach(function(comment) {
                    newComments += commentTemplate({comment: comment});
                });
                $("#comments ul").prepend(newComments);

                var commentControlContent = "Comments (" + response.data.numberOfComments + ")";
                $("#number-of-comments").text(commentControlContent);

                var commentCounterContent = response.data.numberOfComments + " " + ((response.data.numberOfComments != 1) ? "comments" : "comment");
                $(".comment-icon ~ p[data-counter-type = 'comment']").text(commentCounterContent);
            },
            dataType: "json"
        });
    };

    return {
        postComment: postCommentHandler,
        loadOldComments: loadOldCommentsHandler,
        loadNewComments: loadNewCommentsHandler
    };
});