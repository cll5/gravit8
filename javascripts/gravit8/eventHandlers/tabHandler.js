var TabHandler = (function() {
    var SCROLL_ANIMATION_PERIOD = 500;
    var BASE_HEIGHT = $(".navigation-bar").height() + $("#help-header").height();
    var PADDING = 20;
    var OFFSET = BASE_HEIGHT + PADDING;

    return {
        goToByScroll: function(selector) {
            var yCoordinate = $(selector).offset().top - OFFSET;
            $('html, body').animate({
                scrollTop: yCoordinate
            }, SCROLL_ANIMATION_PERIOD);
        }
    };
});