$(document).ready(function() {
	$("#testimonial-selector li").on("click", function(event) {
		var oldSelector = $("#testimonial-selector li.selected");
		var newSelector = $(event.target);
		var oldSelectorIndex = $("#testimonial-selector li").index(oldSelector) + 1;
		var newSelectorIndex = $("#testimonial-selector li").index(newSelector) + 1;

		oldSelector.removeClass("selected");
		newSelector.addClass("selected");

		$("#testimonial-container .testimonial:nth-child(" + oldSelectorIndex + ")").fadeTo(300, 0);
		$("#testimonial-container .testimonial:nth-child(" + oldSelectorIndex + ")").addClass("hidden");
		$("#testimonial-container .testimonial:nth-child(" + newSelectorIndex + ")").removeClass("hidden");
		$("#testimonial-container .testimonial:nth-child(" + newSelectorIndex + ")").fadeTo(300, 1);
	});
});