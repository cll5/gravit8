//To-Do: Split password validation and error messaging in the future

(function() {
    var signupIntroduction = $("#signup-introduction");
    //NOTE: due to the fact that there is no Web API to directly determine the state of the caps lock key, 
    //      we will always assume the initlal state of the caps lock key is not ON, but the state will only
    //      get updated when the person pressed a letter (because the caps lock state is inferred by comparing 
    //      if the letter is capital or not)
    var capsLockState = false;
    var capsLockWarningMessage = $("#caps-lock-feedback");

    //determine the state of caps lock
    //see: http://stackoverflow.com/questions/348792/how-do-you-tell-if-caps-lock-is-on-using-javascript
    //see: http://code.stephenmorley.org/javascript/detecting-the-caps-lock-key/
    var determineCapsLockState = function(event) {
        var isMac = /Mac/.test(window.navigator.platform);

        //NOTE: this is ridiculous, there isn't one consistent API to get the key's unicode
        //see: https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent
        var keyCode = event.keyCode || event.charCode || event.which;
        var character = String.fromCharCode(keyCode);
        var isLetter = /[A-Za-z]/.test(character);

        if (isLetter && ((!event.shiftKey && (character === character.toUpperCase())) ||
                         (isMac && event.shiftKey && (character === character.toUpperCase())) ||
                         (event.shiftKey && (character === character.toLowerCase())))) {
            capsLockState = true;
        }

        if (capsLockState) {
            capsLockWarningMessage.removeClass("hidden");
            passwordNotMatchFeedbackMessage.addClass("hidden");
            signupIntroduction.addClass("hidden");

        } else {
            capsLockWarningMessage.addClass("hidden");
            signupIntroduction.removeClass("hidden");
        }
    };

    //determine if the caps lock key is pressed
    var CAPS_LOCK_KEY = 20;
    var checkForCapsLockIsPressed = function(event) {
        var keyCode = event.keyCode || event.charCode || event.which;
        var key = event.char || event.key;
        if ((keyCode === CAPS_LOCK_KEY) || (key === "CapsLock")) {
            //caps lock key (key code is 20) is pressed
            capsLockState = !capsLockState;
            if (capsLockState) {
                capsLockWarningMessage.removeClass("hidden");
                passwordNotMatchFeedbackMessage.addClass("hidden");
                signupIntroduction.addClass("hidden");
            } else {
                capsLockWarningMessage.addClass("hidden");
                signupIntroduction.removeClass("hidden");
            }
        }
    };

    // var documentRoot = $("html");
    // documentRoot.on("keypress", determineCapsLockState);
    // documentRoot.on("keydown", checkForCapsLockIsPressed);

    //determine if passwords match
    var passwordInput = $("#password");
    var confirmPasswordInput = $("#confirm-password");
    if (confirmPasswordInput) {
        //check for matching passwords
        var passwordNotMatchFeedbackMessage = $("#password-not-match-feedback");
        var checkForMatchingPassword = function(event) {
            if (confirmPasswordInput.val() != passwordInput.val()) {
                passwordNotMatchFeedbackMessage.removeClass("hidden");
                capsLockWarningMessage.addClass("hidden");
                signupIntroduction.addClass("hidden");

            } else {
                passwordNotMatchFeedbackMessage.addClass("hidden");
                signupIntroduction.removeClass("hidden");
            }
        };

        var previousTimeoutHandler;
        var triggerCheckForMatchingPassword = function(event) {
            if (previousTimeoutHandler) {
                clearTimeout(previousTimeoutHandler);
            }
            previousTimeoutHandler = setTimeout(checkForMatchingPassword, 800);
        };

        confirmPasswordInput.on("change", checkForMatchingPassword);
        confirmPasswordInput.on("keydown", triggerCheckForMatchingPassword);
    }

    //email code error (?) - some error state function
    var emailCodeError = $(".email-code-error");
    if (emailCodeError) {
        $("#landing-page form").on("input", function(event) {
            emailCodeError.addClass("hidden");
        });
    }
})();