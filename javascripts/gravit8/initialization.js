$(document).ready(function() {
    var SCROLLBAR_DETECTION_PERIOD = 500;
    var CARD_REFRESH_PERIOD = 5000;
    var COMMENT_REFRESH_PERIOD = 7500;
    var LIKE_COUNTER_REFRESH_PERIOD = 15000;

    var throttleEventDetector = new ThrottleEventDetector();
    var scrollbar = new ScrollbarHandler();
    var sortHandler = new SortHandler();
    var likeHandler = new LikeHandler();
    var voteHandler = new VoteHandler();
    var commentHandler = new CommentHandler();
    var entityInstance = gravit8.Entity.getInstance();
    var organizationInstance = gravit8.Organization.getInstance();

    $(window).on("scroll", function() {
        throttleEventDetector.isScrolling = true;
    });

    //textareas are auto-sized based on content in textarea
    var textareaHandler = function() {
        $("textarea").not("[name='team-member']").not("[name='role']").each(function() {
            this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px; overflow-y: hidden;');
        }).on('input keypaste', function() {
            this.style.height = '';
            this.style.height = (this.scrollHeight) + 'px';
        });
    };

    // $(window).on("drop dragend dragover", function(event) {
    //     event.preventDefault();
    // });

    var infiniteScrollHandler = function(eventHandler) {
        if (throttleEventDetector.isScrolling && scrollbar.isEndOfPage()) {
            throttleEventDetector.isScrolling = false;
            eventHandler();
        }
    };

    var hostname = window.location.hostname;
    var domain = hostname.split(".")[0];
    var route = window.location.pathname;

    //explore people page handler definitions
    var personCardTemplate = _.template($("#person-card-template").html());
    var loadMoreOldPeopleHandler = function() {
        var personIds = $("#browse-view [data-container = 'people'] .people-card:not(.invisible)").map(function(index, person) {
            return +person.getAttribute("href").match(/\/(\d+)/).pop();
        }).get();
        referenceId = _.isEmpty(personIds) ? 0 : _.min(personIds);

        var requestData = {
            "id": referenceId,
            "lookupDirection": "oldest"
        };

        $.ajax({
            type: "GET",
            url: "/get/people",
            data: utilities.formatJSON(requestData),
            success: function(response) {
                //render the batch people cards
                var peopleCards = "";
                response.data.people.forEach(function(person) {
                    peopleCards += personCardTemplate({person: person});
                });
                $("#browse-view [data-container = 'people'] .invisible:first").before(peopleCards);

                //resort the people
                $(".sort-by-options select[name = 'sort-by-options']").not(".hidden").trigger("change");
            },
            dataType: "json"
        });
    };

    var loadMoreNewPeopleHandler = function() {
        var personIds = $("#browse-view [data-container = 'people'] .people-card:not(.invisible)").map(function(index, person) {
            return +person.getAttribute("href").match(/\/(\d+)/).pop();
        }).get();
        referenceId = _.isEmpty(personIds) ? 0 : _.max(personIds);

        var requestData = {
            "id": referenceId,
            "lookupDirection": "newest"
        };

        $.ajax({
            type: "GET",
            url: "/get/people",
            data: utilities.formatJSON(requestData),
            success: function(response) {
                //render the batch people cards
                var peopleCards = "";
                response.data.people.forEach(function(person) {
                    peopleCards += personCardTemplate({person: person});
                });
                $("#browse-view [data-container = 'people'] .people-card:first").before(peopleCards);

                //resort the people
                $(".sort-by-options select[name = 'sort-by-options']").not(".hidden").trigger("change");

                organizationInstance.refreshStatistics();
            },
            dataType: "json"
        });
    };


    //route: /browse
    if (/^\/browse/i.test(route)) {
        var exploreRouteHandler = new ExploreRouteHandler();

        var idea = new gravit8.Idea();
        var pitch = new gravit8.Pitch();
        var project = new gravit8.Project();

        //update the history state when page loads on browse page
        if (window.history && window.history.state) {
            $(window).on("popstate", exploreRouteHandler.popStateHandler);
        } else if (window.history && !window.history.state) {
            var selectedTab = $(".browse-tabs a.selected").attr("data-container");
            exploreRouteHandler.updateState(route, selectedTab);
        }

        $(".card-view-icon").on("click", function(event) {
            var viewButton = $(event.target);
            viewButton.toggleClass("list-view-icon");

            if (viewButton.hasClass("list-view-icon")) {
                $("#browse-view .list-view").removeClass("hidden");
                $("#browse-view .card-view").addClass("hidden");
            } else {
                $("#browse-view .card-view").removeClass("hidden");
                $("#browse-view .list-view").addClass("hidden");
            }
        });

        var infiniteScrollThrottlingId;
        var timerThrottlingId;
        var refreshDataCountersThrottlingId;
        $("ul.browse-tabs a").on("click", function(event) {
            event.preventDefault();
            var tab = event.target.getAttribute("data-container");

            //show appropriate contents and update browse tabs
            $(".browse-tabs a").removeClass("selected")
                                .filter("[data-container = " + tab + "]")
                                .addClass("selected");

            $(".items-container[data-container]").addClass("hidden")
                                                .filter("[data-container = " + tab + "]")
                                                .removeClass("hidden");

            var newRoute = event.target.getAttribute("href");
            exploreRouteHandler.pushState(newRoute, tab);

            //switch to the specific set of sort options
            $(".sort-by-options select[name = 'sort-by-options']").addClass("hidden");
            $("#" + tab + "-sort-options").removeClass("hidden");

            //hide the list-/card-view icon if the people tab is selected
            if (tab === "people") {
                $(".card-view-icon").addClass("hidden");
            } else {
                $(".card-view-icon").removeClass("hidden");
            }
        }).on("click", function(event) {
            //explore page specific event handlers

            //clear up the infinite scroll and timer handler from the previous explore page
            window.clearInterval(infiniteScrollThrottlingId);
            window.clearInterval(timerThrottlingId);
            window.clearInterval(refreshDataCountersThrottlingId);

            var selected = this.getAttribute("data-container");
            switch (selected) {
                case "ideas":
                    infiniteScrollThrottlingId = window.setInterval(infiniteScrollHandler, SCROLLBAR_DETECTION_PERIOD, idea.loadMoreOldIdeas);
                    timerThrottlingId = window.setInterval(idea.loadMoreNewIdeas, CARD_REFRESH_PERIOD);
                    refreshDataCountersThrottlingId = window.setInterval(idea.refreshIdeas, CARD_REFRESH_PERIOD);
                    $("#vote-summary footer").addClass("hidden").filter("[data-entity-type = 'idea']").removeClass("hidden");
                    break;

                case "pitches":
                    infiniteScrollThrottlingId = window.setInterval(infiniteScrollHandler, SCROLLBAR_DETECTION_PERIOD, pitch.loadMoreOldPitches);
                    timerThrottlingId = window.setInterval(pitch.loadMoreNewPitches, CARD_REFRESH_PERIOD);
                    refreshDataCountersThrottlingId = window.setInterval(pitch.refreshPitches, CARD_REFRESH_PERIOD);
                    $("#vote-summary footer").addClass("hidden").filter("[data-entity-type = 'pitch']").removeClass("hidden");
                    break;

                case "projects":
                    infiniteScrollThrottlingId = window.setInterval(infiniteScrollHandler, SCROLLBAR_DETECTION_PERIOD, project.loadMoreOldProjects);
                    timerThrottlingId = window.setInterval(project.loadMoreNewProjects, CARD_REFRESH_PERIOD);
                    refreshDataCountersThrottlingId = window.setInterval(project.refreshProjects, CARD_REFRESH_PERIOD);
                    $("#vote-summary footer").addClass("hidden").filter("[data-entity-type = 'project']").removeClass("hidden");
                    break;

                case "people":
                    infiniteScrollThrottlingId = window.setInterval(infiniteScrollHandler, SCROLLBAR_DETECTION_PERIOD, loadMoreOldPeopleHandler);
                    timerThrottlingId = window.setInterval(loadMoreNewPeopleHandler, CARD_REFRESH_PERIOD);
                    $("#vote-summary footer").addClass("hidden");
                    break;
            }
        });

        //hide the list/card view icon when the people tab is selected on the explore page
        var initialTab = $("ul.browse-tabs a.selected").attr("data-container");
        if (initialTab === "people") {
            $(".card-view-icon").addClass("hidden");
        } else {
            $(".card-view-icon").removeClass("hidden");
        }


        //sorting cards, lists, or people
        $(".sort-by-options select[name = 'sort-by-options']").on("change", function(event) {
            var tab = this.id;
            var sortOption = $(this).val();
            var visibleItems = $("#browse-view .items-container").not(".hidden");

            if (tab === "people-sort-options") {
                //sort people
                var people = visibleItems.find(".people-card").not(".invisible").get();
                var sortedPeople = sortHandler.sortPeople(people, sortOption);
                visibleItems.find(".invisible").filter(":first").before(sortedPeople);
            } else {
                //sort cards
                var cards = visibleItems.find(".card-view").get();
                var sortedCards = sortHandler.sortCards(cards, sortOption);
                visibleItems.find(".invisible").filter(":first").before(sortedCards);

                //sort lists according to the order of the cards
                var lists = visibleItems.find(".list-view").get();
                var sortedLists = sortHandler.sortLists(sortedCards);
                visibleItems.find(".card-view").filter(":first").before(sortedLists);
            }
        });

        //search bar feature (disabled until functional)
        // var ENTER_KEY = 13;
        // $(".search-bar input[type = 'search']").on("keydown paste", function(event) {
        //     console.log(event);
        //     if (event.type === "keydown") {
        //         var keyCode = event.keyCode || event.charCode || event.which;
        //         var character = String.fromCharCode(keyCode);
        //         var isValidCharacter = /[A-Za-z0-9\~\`\!\@\#\$\%\^\&\*\(\)\_\-\+\=\{\}\[\]\"\'\:\;\|\/\\\?\>\.\<\,]/.test(character);
        //         console.log(character);

        //         if (keyCode === ENTER_KEY) {
        //         }
        //     }

        //     var keywords = $(this).val().trim();
        //     var searchRequest = {
        //         "keywords": keywords
        //     };

        //     var exploreTab = $(".browse-tabs a.selected").attr("data-container");
        //     var requestURL = "/submit/search/" + exploreTab;
        //     $.ajax({
        //         type: "POST",
        //         url: requestURL,
        //         data: JSON.stringify(searchRequest),
        //         success: function(response) {
        //             console.log(response);
        //         },
        //         dataType: "json"
        //     });
        // });

        //load ideas, pitches, projects, and people at the start
        idea.loadMoreNewIdeas();
        pitch.loadMoreNewPitches();
        project.loadMoreNewProjects();
        loadMoreNewPeopleHandler();

        var selected = $("ul.browse-tabs a.selected").attr("data-container");
        switch (selected) {
            case "ideas":
                infiniteScrollThrottlingId = window.setInterval(infiniteScrollHandler, SCROLLBAR_DETECTION_PERIOD, idea.loadMoreOldIdeas);
                timerThrottlingId = window.setInterval(idea.loadMoreNewIdeas, CARD_REFRESH_PERIOD);
                refreshDataCountersThrottlingId = window.setInterval(idea.refreshIdeas, CARD_REFRESH_PERIOD);
                $("#vote-summary footer").addClass("hidden").filter("[data-entity-type = 'idea']").removeClass("hidden");
                break;

            case "pitches":
                infiniteScrollThrottlingId = window.setInterval(infiniteScrollHandler, SCROLLBAR_DETECTION_PERIOD, pitch.loadMoreOldPitches);
                timerThrottlingId = window.setInterval(pitch.loadMoreNewPitches, CARD_REFRESH_PERIOD);
                refreshDataCountersThrottlingId = window.setInterval(pitch.refreshPitches, CARD_REFRESH_PERIOD);
                // $("#voting-status-bar").removeClass("hidden");
                $("#vote-summary footer").addClass("hidden").filter("[data-entity-type = 'pitch']").removeClass("hidden");
                break;

            case "projects":
                infiniteScrollThrottlingId = window.setInterval(infiniteScrollHandler, SCROLLBAR_DETECTION_PERIOD, project.loadMoreOldProjects);
                timerThrottlingId = window.setInterval(project.loadMoreNewProjects, CARD_REFRESH_PERIOD);
                refreshDataCountersThrottlingId = window.setInterval(project.refreshProjects, CARD_REFRESH_PERIOD);
                $("#vote-summary footer").addClass("hidden").filter("[data-entity-type = 'project']").removeClass("hidden");
                break;

            case "people":
                infiniteScrollThrottlingId = window.setInterval(infiniteScrollHandler, SCROLLBAR_DETECTION_PERIOD, loadMoreOldPeopleHandler);
                timerThrottlingId = window.setInterval(loadMoreNewPeopleHandler, CARD_REFRESH_PERIOD);
                $("#vote-summary footer").addClass("hidden");
                break;
        }
    }

    //route: /create/idea
    else if (/^\/create\/idea/i.test(route)) {
        //ping server every 10 mins to keep the session alive
        window.setInterval(function() {
            $.ajax({url: "/ping"});
        }, 600000);

        textareaHandler();

        //add content button handler
        $(".create-buttons .add-content").on("click", entityInstance.addContentButtonHandler);

        //add optional content handlers
        $("#add-description-button").on("click", entityInstance.descriptionCardHandler);
        $("#add-background-button").on("click", entityInstance.backgroundCardHandler);
        $("#add-stakeholder-button").on("click", entityInstance.stakeholderCardHandler);
        $("#add-existing-solution-button").on("click", entityInstance.existingSolutionCardHandler);
        $("#add-market-analysis-button").on("click", entityInstance.marketAnalysisCardHandler);
        $("#add-other-information-button").on("click", entityInstance.otherInformationCardHandler);
        $("#add-attachment-button").on("click", entityInstance.attachmentCardHandler);

        var idea = new gravit8.Idea();
        $("#submit").on("click", idea.createNewIdea);
    }

    //route: /create/pitch
    else if (/^\/create\/pitch/i.test(route)) {
        //ping server every 10 mins to keep the session alive
        window.setInterval(function() {
            $.ajax({url: "/ping"});
        }, 600000);

        textareaHandler();

        $(".add-role-button button").on("click", entityInstance.addAssignableRoleButtonHandler);

        //add content button handler
        $(".create-buttons .add-content").on("click", entityInstance.addContentButtonHandler);

        //add optional content handlers
        $("#add-description-button").on("click", entityInstance.descriptionCardHandler);
        $("#add-background-button").on("click", entityInstance.backgroundCardHandler);
        $("#add-stakeholder-button").on("click", entityInstance.stakeholderCardHandler);
        $("#add-existing-solution-button").on("click", entityInstance.existingSolutionCardHandler);
        $("#add-market-analysis-button").on("click", entityInstance.marketAnalysisCardHandler);
        $("#add-other-information-button").on("click", entityInstance.otherInformationCardHandler);
        $("#add-attachment-button").on("click", entityInstance.attachmentCardHandler);

        var pitch = new gravit8.Pitch();
        $("#submit").on("click", pitch.createNewPitch);

        //back button handler
        $(".back-submit .back-button button").on("click", function() {
            history.go(-1);
        });
    }

    //route: /create/project
    else if (/^\/create\/project/i.test(route)) {
        //ping server every 10 mins to keep the session alive
        window.setInterval(function() {
            $.ajax({url: "/ping"});
        }, 600000);

        textareaHandler();

        $(".add-team-member-button button").on("click", entityInstance.addTeamMemberButtonHandler);
        $(".add-role-button button").on("click", null, {includeRoleAssignee: true}, entityInstance.addAssignableRoleButtonHandler);

        //enable adding attachments
        var uploadedAttachmentFormTemplate = _.template($("#uploaded-attachment-template").html());
        var attachmentForm = $("#attachment-section-body");
        var attachmentList = $("#attachments");
        attachmentForm.find(".upload-button input[type = 'file']").on("change", function(event) {
            if (event.target.files && event.target.files.length > 0) {
                for (var index = 0; index < event.target.files.length; index++) {
                    var attachment = event.target.files[index];
                    var attachmentHandler = new gravit8.AttachmentHandler(attachment);
                    var referenceHandler = {
                        attachmentHandler: attachmentHandler
                    };

                    var data = {
                        attachment: attachment,
                        formattedSize: utilities.formatBytes(attachment.size)
                    };
                    var anAttachment = $(uploadedAttachmentFormTemplate(data));
                    anAttachment.find(".cancel-remove-button button").on("click", null, referenceHandler, function(event) {
                        event.data.attachmentHandler.cancel();
                        $(event.target).closest(".entity-content").remove();
                    });
                    anAttachment.find(".pause-button button").on("click", null, referenceHandler, function(event) {
                        var pauseButton = $(event.target);
                        pauseButton.closest(".pause-button").addClass("hidden");
                        pauseButton.closest(".entity-content").find(".resume-button").removeClass("hidden");
                        event.data.attachmentHandler.pause();
                    });
                    anAttachment.find(".resume-button button").on("click", null, referenceHandler, function(event) {
                        var resumeButton = $(event.target);
                        resumeButton.closest(".resume-button").addClass("hidden");
                        resumeButton.closest(".entity-content").find(".pause-button").removeClass("hidden");
                        event.data.attachmentHandler.resume();
                    });
                    attachmentList.append(anAttachment);

                    //attachments that are larger than the size limit will be skipped
                    if (attachment.size > (100 * 1024 * 1024)) {
                        anAttachment.find(".status").text("(" + data.formattedSize + ") This attachment is too large to upload onto Gravit8 - it will be skipped. If it is a video, try uploading it onto Youtube, Vimeo, etc. and document the link.");
                        return;
                    }

                    attachmentHandler.setUITemplate(anAttachment);
                    attachmentHandler.setUserControl($(".user-controls"));
                    attachmentHandler.start();
                }

                //allow the same file(s) to be selected again sequentially
                event.target.value = null;
            }
        });
        attachmentForm.find(".upload-button button").on("click", function(event) {
            $(event.target).siblings("input[type = 'file']").trigger("click");
        });

        //if the back button is pressed, make sure any uploaded attachments are discarded from the server
        $(".user-controls .back-button").find("a, button").on("click", function(event) {
            $("#attachments [data-attachment-id]").each(function(index, attachment) {
                var attachmentId = +attachment.getAttribute("data-attachment-id");
                if (attachmentId > 0) {
                    $.ajax({
                        type: "POST",
                        url: "/remove/attachment",
                        data: utilities.formatJSON({attachmentId: attachmentId}),
                        success: function(response) {
                            if (response.success) {
                                //then what?
                            }
                        },
                        dataType: "json"
                    });
                }
            });
        });

        //add content button handler
        $(".create-buttons .add-content").on("click", entityInstance.addContentButtonHandler);

        //add optional content handlers
        $("#add-description-button").on("click", entityInstance.descriptionCardHandler);
        $("#add-background-button").on("click", entityInstance.backgroundCardHandler);
        $("#add-stakeholder-button").on("click", entityInstance.stakeholderCardHandler);
        $("#add-existing-solution-button").on("click", entityInstance.existingSolutionCardHandler);
        $("#add-market-analysis-button").on("click", entityInstance.marketAnalysisCardHandler);
        $("#add-other-information-button").on("click", entityInstance.otherInformationCardHandler);
        // $("#add-attachment-button").on("click", entityInstance.attachmentCardHandler);

        //submit the new project
        var project = new gravit8.Project();
        $("#submit").on("click", project.createNewProject);
    }

    //route: /idea/(id)
    else if (/^\/idea\/(\d+)/i.test(route)) {
        var idea = new gravit8.Idea();

        textareaHandler();

        //keep track of initial values to use for differing prior to sending the updates to the server
        var initialEntityData = entityInstance.dataModels.initialEntityData($("#edit-idea"));

        //like button and like counter handling
        $(".reaction-buttons .like-icon").on("click", likeHandler.likeEntity);

        //constant data refreshing
        var entityRefreshThrottlingId = window.setInterval(idea.refreshIdea, CARD_REFRESH_PERIOD);

        //comment handling
        $("#comment-control button").on("click", commentHandler.postComment);
        var infiniteScrollThrottlingId = window.setInterval(infiniteScrollHandler, SCROLLBAR_DETECTION_PERIOD, commentHandler.loadOldComments);
        var updateCommentThrottlingId = window.setInterval(commentHandler.loadNewComments, COMMENT_REFRESH_PERIOD);
        commentHandler.loadNewComments();

        //switch to edit view
        $("#edit-entity-button").on("click", function(event) {
            //switch over to published idea view
            $("#published-idea").addClass("hidden");
            $("#edit-idea").removeClass("hidden");

            textareaHandler();

            //switch over to edit idea button
            $(this).parent().addClass("hidden");
            $(".user-controls .back-submit").removeClass("hidden");

            window.clearInterval(entityRefreshThrottlingId);
        });

        //switch to public view
        $("#cancel-edit-idea-button").on("click", function(event) {
            //switch over to published idea view
            $("#edit-idea").addClass("hidden");
            $("#published-idea").removeClass("hidden");

            textareaHandler();

            //switch over to edit idea button
            $("#edit-entity-button").parent().removeClass("hidden");
            $(".user-controls .back-submit").addClass("hidden");

            entityRefreshThrottlingId = window.setInterval(idea.refreshIdea, CARD_REFRESH_PERIOD);
        });

        entityInstance.bindHandlersToEditForm($("#edit-idea-form"));

        //add entity content modules
        $(".create-buttons .add-content").on("click", entityInstance.addContentButtonHandler);

        //add optional content handlers
        $("#add-description-button").on("click", entityInstance.descriptionCardHandler);
        $("#add-background-button").on("click", entityInstance.backgroundCardHandler);
        $("#add-stakeholder-button").on("click", entityInstance.stakeholderCardHandler);
        $("#add-existing-solution-button").on("click", entityInstance.existingSolutionCardHandler);
        $("#add-market-analysis-button").on("click", entityInstance.marketAnalysisCardHandler);
        $("#add-other-information-button").on("click", entityInstance.otherInformationCardHandler);
        $("#add-attachment-button").on("click", entityInstance.attachmentCardHandler);

        //submit changes
        $("#submit-edits").on("click", null, {initialEntityData: initialEntityData}, idea.updateIdeaContents);
    }

    //route: /pitch/(id)
    else if (/^\/pitch\/(\d+)/i.test(route)) {
        var pitch = new gravit8.Pitch();

        textareaHandler();

        //keep track of initial values to use for differing prior to sending the updates to the server
        var initialEntityData = entityInstance.dataModels.initialEntityData($("#edit-pitch"));

        //constant data refreshing
        var entityRefreshThrottlingId = window.setInterval(pitch.refreshPitch, CARD_REFRESH_PERIOD);

        //comment handling
        $("#comment-control button").on("click", commentHandler.postComment);
        var infiniteScrollThrottlingId = window.setInterval(infiniteScrollHandler, SCROLLBAR_DETECTION_PERIOD, commentHandler.loadOldComments);
        var updateCommentThrottlingId = window.setInterval(commentHandler.loadNewComments, COMMENT_REFRESH_PERIOD);
        commentHandler.loadNewComments();

        //like button and like counter handling
        $(".reaction-buttons .like-icon").on("click", likeHandler.likeEntity);

        //vote button and vote counter handling
        $(".reaction-buttons .vote-icon").on("click", voteHandler.voteEntity);

        //edit pitch handling
        $("#edit-entity-button").on("click", function() {
            //switch edit button to cancel and submit changes buttons
            $(this).parent(".closed-button")
                   .add(".like-button") //TODO FIX: this should be .closed-button to generalize, but it also targets the children of .back-submit
                   .addClass("hidden")
                   .siblings(".back-submit")
                   .removeClass("hidden");

            //switch to edit pitch page
            $("#published-pitch").addClass("hidden");
            $("#edit-pitch").removeClass("hidden");

            textareaHandler();

            window.clearInterval(entityRefreshThrottlingId);
        });

        $("#cancel-edit-entity-button").on("click", function() {
            //switch cancel and submit changes buttons to edit button
            $(this).parents(".back-submit")
                   .addClass("hidden")
                   .siblings(".closed-button")
                   .removeClass("hidden");

            //switch to publish pitch page
            $("#edit-pitch").addClass("hidden");
            $("#published-pitch").removeClass("hidden");

            textareaHandler();

            //constant data refreshing
            var entityRefreshThrottlingId = window.setInterval(pitch.refreshPitch, CARD_REFRESH_PERIOD);
        });

        entityInstance.bindHandlersToEditForm($("#edit-pitch-form"));

        //add roles
        $(".add-role-button button").on("click", entityInstance.addAssignableRoleButtonHandler);

        //add entity content modules
        $(".create-buttons .add-content").on("click", entityInstance.addContentButtonHandler);

        //add optional content handlers
        $("#add-description-button").on("click", entityInstance.descriptionCardHandler);
        $("#add-background-button").on("click", entityInstance.backgroundCardHandler);
        $("#add-stakeholder-button").on("click", entityInstance.stakeholderCardHandler);
        $("#add-existing-solution-button").on("click", entityInstance.existingSolutionCardHandler);
        $("#add-market-analysis-button").on("click", entityInstance.marketAnalysisCardHandler);
        $("#add-other-information-button").on("click", entityInstance.otherInformationCardHandler);
        $("#add-attachment-button").on("click", entityInstance.attachmentCardHandler);

        //submit changes
        $("#submit-edits").on("click", null, {initialEntityData: initialEntityData}, pitch.updatePitchContents);
    }

    //route: /project/(id)
    else if (/^\/project\/(\d+)/i.test(route)) {
        var project = new gravit8.Project();

        textareaHandler();

        //keep track of initial values to use for differing prior to sending the updates to the server
        var initialEntityData = entityInstance.dataModels.initialEntityData($("#edit-project"));

        //like button and like counter handling
        $(".reaction-buttons .like-icon").on("click", likeHandler.likeEntity);

        //vote button and vote counter handling
        $(".reaction-buttons .vote-icon").on("click", voteHandler.voteEntity);

        $(".user-controls .like-button button").on("click", likeHandler.likeEntity);

        //constant data refreshing
        var entityRefreshThrottlingId = window.setInterval(project.refreshProject, CARD_REFRESH_PERIOD);

        //gallery functionality
        $("#gallery-container li:not(:first-child)").addClass("hidden");
        $("#gallery-selector li:first-child").addClass("selected");

        $("#gallery-selector li").on("click", function(event) {
            var oldSelector = $("#gallery-selector li.selected");
            var newSelector = $(event.target);
            var oldSelectorIndex = $("#gallery-selector li").index(oldSelector) + 1;
            var newSelectorIndex = $("#gallery-selector li").index(newSelector) + 1;

            oldSelector.removeClass("selected");
            newSelector.addClass("selected");
            
            $("#gallery-container li:nth-child(" + oldSelectorIndex + ")").addClass("hidden");
            $("#gallery-container li:nth-child(" + newSelectorIndex + ")").removeClass("hidden");
        });

        //comment handling
        $("#comment-control button").on("click", commentHandler.postComment);
        var infiniteScrollThrottlingId = window.setInterval(infiniteScrollHandler, SCROLLBAR_DETECTION_PERIOD, commentHandler.loadOldComments);
        var updateCommentThrottlingId = window.setInterval(commentHandler.loadNewComments, COMMENT_REFRESH_PERIOD);
        commentHandler.loadNewComments();

        //edit project handling
        $("#edit-entity-button").on("click", function() {
            //switch edit button to cancel and submit changes buttons
            $(this).parent(".closed-button")
                   .add(".like-button") //TODO FIX: this should be .closed-button to generalize, but it also targets the children of .back-submit
                   .addClass("hidden")
                   .siblings(".back-submit")
                   .removeClass("hidden");

            //switch to edit project page
            $("#published-project").addClass("hidden");
            $("#edit-project").removeClass("hidden");

            textareaHandler();

            window.clearInterval(entityRefreshThrottlingId);
        });

        $("#cancel-edit-entity-button").on("click", function() {
            //switch cancel and submit changes buttons to edit button
            $(this).parents(".back-submit")
                   .addClass("hidden")
                   .siblings(".closed-button")
                   .removeClass("hidden");

            //switch to publish project page
            $("#edit-project").addClass("hidden");
            $("#published-project").removeClass("hidden");

            textareaHandler();

            //constant data refreshing
            var entityRefreshThrottlingId = window.setInterval(project.refreshProject, CARD_REFRESH_PERIOD);
        });

        entityInstance.bindHandlersToEditForm($("#edit-project-form"));

        //add team members and roles
        $(".add-team-member-button button").on("click", entityInstance.addTeamMemberButtonHandler);
        $(".add-role-button button").on("click", null, {includeRoleAssignee: true}, entityInstance.addAssignableRoleButtonHandler);

        //add entity content modules
        $(".create-buttons .add-content").on("click", entityInstance.addContentButtonHandler);

        //add optional content handlers
        $("#add-description-button").on("click", entityInstance.descriptionCardHandler);
        $("#add-background-button").on("click", entityInstance.backgroundCardHandler);
        $("#add-stakeholder-button").on("click", entityInstance.stakeholderCardHandler);
        $("#add-existing-solution-button").on("click", entityInstance.existingSolutionCardHandler);
        $("#add-market-analysis-button").on("click", entityInstance.marketAnalysisCardHandler);
        $("#add-other-information-button").on("click", entityInstance.otherInformationCardHandler);
        // $("#add-attachment-button").on("click", entityInstance.attachmentCardHandler);

        //submit changes
        $("#submit-edits").on("click", null, {initialEntityData: initialEntityData}, project.updateProjectContents);
    }

    //route: /people/(id)
    else if (/^\/people\/(?:\d+)/i.test(route)) {
        var contactTypes;
        var defaultContactTypeId;
        $.ajax({
            type: "GET",
            url: "/get/list/contact-types",
            success: function(response) {
                contactTypes = response.data.contactTypes;
                defaultContactTypeId = response.data.contactTypes[0].id;
            },
            dataType: "json"
        });

        var personId = +route.match(/\/(\d+)/).pop();
        var loadMoreOldPortfolioHandler = function(event) {
            //TODO: to be implemented
        };

        var ideaCardTemplate = _.template($("#idea-card-template").html());
        var pitchCardTemplate = _.template($("#pitch-card-template").html());
        var projectCardTemplate = _.template($("#project-card-template").html());
        var loadMoreNewPortfolioHandler = function(event) {
            var person = {
                "id": personId
            };

            $.ajax({
                type: "GET",
                url: "/get/portfolios",
                data: utilities.formatJSON(person),
                success: function(response) {
                    if (response.data.started.length > 0) {
                        var portfolios = response.data.started.map(function(entity) {
                            var entityCard;
                            switch (entity.entityType) {
                                case "idea":
                                    entityCard = $(ideaCardTemplate({idea: entity}));
                                    // entityCard.find(".reaction-buttons .like-icon").on("click", likeHandler.likeCard);
                                    // entityCard.find(".reaction-buttons .vote-icon").on("click", voteHandler.voteCard);
                                    break;

                                case "pitch":
                                    entityCard = $(pitchCardTemplate({pitch: entity}));
                                    // entityCard.find(".reaction-buttons .like-icon").on("click", likeHandler.likeCard);
                                    // entityCard.find(".reaction-buttons .vote-icon").on("click", voteHandler.voteCard);
                                    break;

                                case "project":
                                    entityCard = $(projectCardTemplate({entity: entity}));
                                    // entityCard.find(".reaction-buttons .like-icon").on("click", likeHandler.likeCard);
                                    // entityCard.find(".reaction-buttons .vote-icon").on("click", voteHandler.voteCard);
                                    break;
                            }
                            return entityCard;
                        });

                        $("#published-profile .profile-portfolio .item-container.invisible:first").before(portfolios);

                        //swap warning message and portfolio cards
                        $("#published-profile .profile-portfolio .warning-message").addClass("hidden");
                        $("#published-profile .profile-portfolio .items-container").removeClass("hidden");
                    } else {
                        //swap warning message and portfolio cards
                        $("#published-profile .profile-portfolio .warning-message").removeClass("hidden");
                        $("#published-profile .profile-portfolio .items-container").addClass("hidden");
                    }
                },
                dataType: "json"
            });
        };

        //setup infinite scroll handling
        // var infiniteScrollThrottlingId = window.setInterval(infiniteScrollHandler, SCROLLBAR_DETECTION_PERIOD, loadMoreOldPortfolioHandler);
        loadMoreNewPortfolioHandler();

        //changing between publish and edit views
        $("#edit-profile-button").on("click", function(event) {
            //switch over to edit profile view
            $("#edit-profile").removeClass("hidden");
            $("#published-profile").addClass("hidden");

            //switch over to back-submit buttons
            $(this).parent().addClass("hidden");
            $(".user-controls .back-submit").removeClass("hidden");
        });

        $("#cancel-edit-profile-button").on("click", function(event) {
            //switch over to published profile view
            $("#edit-profile").addClass("hidden");
            $("#published-profile").removeClass("hidden");

            //switch over to edit profile button
            $("#edit-profile-button").parent().removeClass("hidden");
            $(".user-controls .back-submit").addClass("hidden");
        });

        //add new profile attribute
        $("#edit-profile .add-content").on("click", entityInstance.addContentButtonHandler);

        var removeButtonHandler = function() {
            $(this).parents(".profile-attribute").remove();
        };

        var removedProfileAttributes = {};
        $("#edit-profile-form .cancel-remove-button button").on("click", function(event) {
            var field = $(this).parents(".profile-attribute").find("textarea[data-profile-attribute-id]");
            var fieldName = field.attr("name");
            var profileAttributeId = +field.attr("data-profile-attribute-id");

            //check if biography is removed
            if (fieldName === "biography") {
                $("#edit-profile #add-profile-biography-button")[0].removeAttribute("data-module-disabled");
                $("#edit-profile #add-profile-biography-button").removeClass("disabled-module");
            }

            //keep a record of what profile attribute has been removed
            var profileAttribute = {"id": profileAttributeId};
            switch (fieldName) {
                case "biography":
                    removedProfileAttributes["biography"] = profileAttribute;
                    break;

                case "contact":
                    if (!("contacts" in removedProfileAttributes)) {
                        removedProfileAttributes["contacts"] = [profileAttribute];
                    } else {
                        removedProfileAttributes["contacts"].push(profileAttribute);
                    }
                    break;
            }
        }).on("click", removeButtonHandler);

        //detect changes to fields
        var newProfileAttributeDetectionHandler = function(event) {
            if (!event.target.hasAttribute("data-content-added")) {
                event.target.setAttribute("data-content-added", "");
            }
        };
        var editDetectionHandler = function(event) {
            if (!event.target.hasAttribute("data-content-edited")) {
                event.target.setAttribute("data-content-edited", "");
            }
        };
        $("#edit-profile-form textarea").on("keydown paste", editDetectionHandler);
        $("#edit-profile-form select[name = 'categories']").on("change", function() {
            $(this).parent("form").siblings("textarea").attr("data-content-edited", "");
        });

        //add new biography
        //TODO: a user should only be able to have one biography at all time
        $("#edit-profile #add-profile-biography-button").on("click", function(event) {
            if (!this.hasAttribute("data-module-disabled")) {
                this.setAttribute("data-module-disabled", "");
                $(this).addClass("disabled-module");

                //create the biography form template and attach event handlers to it
                var rawTemplate = $("#add-profile-biography-form-template").html();
                var profileBiographyForm = $(_.template(rawTemplate)());
                profileBiographyForm.find("textarea").on("keydown paste", newProfileAttributeDetectionHandler);
                profileBiographyForm.find(".cancel-remove-button button").on("click", null, this, function(event) {
                    event.data.removeAttribute("data-module-disabled");
                    $(event.data).removeClass("disabled-module");
                }).on("click", removeButtonHandler);

                //add the biography form to the DOM
                $("#edit-profile-form ~ .create-buttons").before(profileBiographyForm);
            }
        });

        //add new contact
        $("#edit-profile #add-profile-contact-button").on("click", function(event) {
            //create the contact form template and attach event handlers to it
            var rawTemplate = $("#add-profile-contact-form-template").html();
            var template = _.template(rawTemplate);
            var profileContactForm = $(template({contactTypes: contactTypes}));
            profileContactForm.find("textarea").on("keydown paste", newProfileAttributeDetectionHandler);
            profileContactForm.find(".cancel-remove-button button").on("click", removeButtonHandler);

            //NOTE: Joey, what does this random regex supposed to do?
            // profileContactForm.find("textarea").on("keydown paste", function(event) {
            //     var url = $(this).val();
            //     var match = /^([a-z][a-z0-9\*\-\.]*):\/\/(?:(?:(?:[\w\.\-\+!$&'\(\)*\+,;=]|%[0-9a-f]{2})+:)*(?:[\w\.\-\+%!$&'\(\)*\+,;=]|%[0-9a-f]{2})+@)?(?:(?:[a-z0-9\-\.]|%[0-9a-f]{2})+|(?:\[(?:[0-9a-f]{0,4}:)*(?:[0-9a-f]{0,4})\]))(?::[0-9]+)?(?:[\/|\?](?:[\w#!:\.\?\+=&@!$'~*,;\/\(\)\[\]\-]|%[0-9a-f]{2})*)?$/;
            //     var protomatch = /^(https?|ftp):\/\/(.*)/;


            //     if (match.test(url)) {
            //     console.log(url + ' is valid');

            //     // if valid replace http, https, ftp etc with empty
            //     var b = url.replace(protomatch.test(url), '');
            //     console.log(b);

            //     } else { // IF INVALID
            //         console.log('not valid');
            //     }
            // });

            //add the contact form to the DOM
            $("#edit-profile-form ~ .create-buttons").before(profileContactForm);
        });



        //upload photo with upload button
        $("#upload-profile-image-button").on("click", function() {
            $(this).siblings("input[type = 'file']").click();
        });

        $("#upload-profile-image-button ~ input[type = 'file']").on("change", function() {
            if (this.files.length > 0) {
                prepareImageForUpload(this.files[0]);
            }
        });

        //drag and drop photo upload
        // $("#edit-profile-form div.section-body[data-dropzone]").on("drag", function(event) {
        //     event.preventDefault();
        //     console.log(event.type);
        //     // $(this).addClass("dropzone");

        // }).on("dragstart dragenter", function(event) {
        //     console.log(event.type);
        //     $(this).addClass("dropzone");

        // }).on("dragend", function(event) {
        //     console.log(event.type);
        //     $(this).removeClass("dropzone");

        // }).on("drop", function(event) {
        //     event.preventDefault();
        //     console.log(event.type);
        //     if (event.dataTransfer.files.length > 0) {
        //         prepareImageForUpload(event.dataTransfer.files[0]);
        //     }
        // });

        var MAX_IMAGE_SIZE = Math.pow(2, 17);    //128kb
        var prepareImageForUpload = function(image) {
            //file should be an image
            if (!/image.*/.test(image.type)) {
                //show warning
                return;
            }

            var reader = new FileReader();
            reader.onload = function(event) {
                var imageDataURL = event.target.result;

                //image should be within a certain size (128kb)
                if (image.size > MAX_IMAGE_SIZE) {
                    downSampleImage(imageDataURL, image.type);
                } else {
                    updateProfileImagePreview(imageDataURL);
                }
            };
            reader.readAsDataURL(image);
        };

        var downSampleImage = function(dataURL, imageType) {
            //NOTE: Bilinear interpolation - http://stackoverflow.com/questions/19262141/resize-image-with-javascript-canvas-smoothly
            var img = document.createElement("img");
            img.onload = function() {
                var canvas = document.createElement("canvas");
                var context = canvas.getContext("2d");

                if (img.width > img.height) {
                    canvas.width = Math.min(300, img.width);
                    canvas.height = canvas.width * img.height / img.width;
                } else {
                    canvas.height = Math.min(300, img.height);
                    canvas.width = canvas.height * img.width / img.height;
                }

                var virtualCanvas = document.createElement("canvas");
                var virtualContext = virtualCanvas.getContext("2d");
                virtualCanvas.width = 0.5 * img.width;
                virtualCanvas.height = 0.5 * img.height;
                virtualContext.drawImage(img, 0, 0, virtualCanvas.width, virtualCanvas.height);
                virtualContext.drawImage(virtualCanvas, 0, 0, 0.5 * virtualCanvas.width, 0.5 * virtualCanvas.height);

                context.drawImage(virtualCanvas, 0, 0, 0.5 * virtualCanvas.width, 0.5 * virtualCanvas.height, 0, 0, canvas.width, canvas.height);
                var imageDataURL = canvas.toDataURL(imageType);

                updateProfileImagePreview(imageDataURL);
            };
            img.src = dataURL;
        };

        var updateProfileImagePreview = function(imageDataURL) {
            $("#edit-profile-image-initials").addClass("hidden");
            $("#edit-profile-image-preview").css("background-image", "url(" + imageDataURL + ")")
                                            .removeClass("hidden");
            $("#edit-profile-image-preview ~ .button input[name = 'image']").attr("data-content-edited", "")
                                                                            .attr("value", imageDataURL);
        };

        //submit changes
        $("#edit-profile-submission").on("click", function(event) {
            var personId = +route.match(/\/(\d+)/).pop();
            if (personId > 0) {
                //form validation - a person should have a name
                var firstName = $("#edit-profile-form textarea[name = 'first-name']").val().trim();
                if (_.isEmpty(firstName) || $("#edit-profile-form textarea[name = 'first-name']").hasClass("required")) {
                    $("#edit-profile-form textarea[name = 'first-name']").on("keypress paste", function() {
                        $(this).off("keypress paste")
                               .on("paste", editDetectionHandler)
                               .removeClass("required")
                               .next().next(".invalid-message")
                               .addClass("hidden");
                    }).addClass("required")
                      .focus()
                      .next().next(".invalid-message")
                      .removeClass("hidden");
                    return;
                }

                //package the new and edited fields in a consistent packaging scheme here
                var profileAttributes = {
                    edited: {},
                    removed: removedProfileAttributes,
                    added: {}
                };

                //package the edited profile attributes
                $("#edit-profile-form [data-content-edited]").each(function(index, editedProfileAttribute) {
                    var field = $(editedProfileAttribute);
                    var fieldName = field.attr("name");

                    switch (fieldName) {
                        case "first-name":
                        case "last-name":
                        case "title":
                            profileAttributes["edited"][fieldName] = field.val().trim();
                            break;

                        case "image":
                            profileAttributes["edited"][fieldName] = field.attr("value");
                            break;

                        case "biography":
                            var biography = {
                                "id": +field.attr("data-profile-attribute-id"),
                                "value": field.val().trim()
                            };
                            profileAttributes["edited"][fieldName] = biography;
                            break;

                        case "contact":
                            var contactTypeId = field.siblings("form").find("select option:selected").val();
                            var contact = {
                                "id": +field.attr("data-profile-attribute-id"),
                                "value": field.val().trim(),
                                "contactTypeId": contactTypeId
                            };

                            if (!("contacts" in profileAttributes["edited"])) {
                                profileAttributes["edited"]["contacts"] = [contact];
                            } else {
                                profileAttributes["edited"]["contacts"].push(contact);
                            }
                    }
                });

                //package the new profile attributes
                $("#edit-profile .profile-attribute textarea[data-content-added]").each(function(index, addedProfileAttribute) {
                    var field = $(addedProfileAttribute);
                    switch (field.attr("name")) {
                        case "biography":
                            profileAttributes["added"]["biography"] = field.val().trim();
                            break;

                        case "contact":
                            var contactTypeId = field.siblings("form").find("select option:selected").val();
                            var contact = {
                                "contactTypeId": _.isEmpty(contactTypeId) ? defaultContactTypeId : contactTypeId,
                                "value": field.val().trim()
                            };

                            if (!("contacts" in profileAttributes["added"])) {
                                profileAttributes["added"]["contacts"] = [contact];
                            } else {
                                profileAttributes["added"]["contacts"].push(contact);
                            }
                            break;
                    }
                });

                //submit profile changes
                if (_.isEmpty(profileAttributes.edited) && _.isEmpty(profileAttributes.removed) && _.isEmpty(profileAttributes.added)) {
                    window.location.href = route;
                } else {
                    var profile = {
                        "id": personId,
                        "attributes": profileAttributes
                    };

                    $.ajax({
                        type: "POST",
                        url: "/update/profile",
                        data: utilities.formatJSON(profile),
                        success: function(response) {
                            if (response.success) {
                                window.location.href = route;
                            } else {
                                //inform user something went wrong?
                            }
                        },
                        dataType: "json"
                    });
                }
            } else {
                //can't get the person id
            }
        });
    }

    //route: /help
    else if (/^\/help/i.test(route)) {
        var tabHandler = new TabHandler();

        //tabs handler
        $("#help-header a").on("click", function(event) {
            var section = "#" + event.target.getAttribute("data-name");
            tabHandler.goToByScroll(section);

            $("#help-header a.selected").removeClass("selected");
            $(event.target).addClass("selected");
        });

        //back button handler
        $(".back-submit .back-button button").on("click", function() {
            history.go(-1);
        });

        //go to initial section
        var initialSection = "#" + $("#help-header a.selected").attr("data-name");
        tabHandler.goToByScroll(initialSection);
    }

    //navigation bar handling
    $(".navigation-bar #navigation-sidebar-icon").on("click tap touchstart", function(event) {
        event.preventDefault();
        $(event.target).toggleClass("selected");
        $("#navigation-sidebar").toggleClass("hidden");
    });
});