$(document).ready(function() {
    var SCROLLBAR_DETECTION_PERIOD = 1000;
    var VIEWPORT_RESIZE_DETECTION_PERIOD = 1000;
    var CARD_REFRESH_PERIOD = 15000;
    var COMMENT_REFRESH_PERIOD = 7500;
    var LIKE_COUNTER_REFRESH_PERIOD = 15000;

    var throttleEventDetector = new ThrottleEventDetector();
    var scrollbar = new ScrollbarHandler();
    var likeHandler = new LikeHandler();
    var commentHandler = new CommentHandler();
    var sortHandler = new SortHandler();

    var infiniteScrollHandler = function(eventHandler) {
        if (throttleEventDetector.isScrolling && scrollbar.isEndOfPage()) {
            throttleEventDetector.isScrolling = false;
            eventHandler();
        }
    };

    var viewportResizedHandler = function(eventHandler) {
        if (throttleEventDetector.viewportResized) {
            throttleEventDetector.viewportResized = false;
            eventHandler();
        }
    };

    var hostname = window.location.hostname;
    var domain = hostname.split(".")[0];
    var route = window.location.pathname;

    //explore people page handler definitions
    var personCardTemplate = _.template($("#person-card-template").html());
    var loadMoreOldPeopleHandler = function() {
        var personIds = $("#browse-view [data-container = 'people'] .people-card:not(.invisible)").map(function(index, person) {
            return +person.getAttribute("href").match(/\/(\d+)/).pop();
        }).get();
        var referenceId = _.isEmpty(personIds) ? 0 : _.min(personIds);

        var requestData = {
            "id": referenceId,
            "lookupDirection": "oldest"
        };

        $.ajax({
            type: "GET",
            url: "/get/people",
            data: utilities.formatJSON(requestData),
            success: function(response) {
                //render the batch people cards
                var peopleCards = "";
                response.data.people.forEach(function(person) {
                    peopleCards += personCardTemplate({person: person});
                });
                $("#browse-view [data-container = 'people'] .invisible").filter(":first").before(peopleCards);
                $(".sort-by-options select#people-sort-options").trigger("change");
            },
            dataType: "json"
        });
    };

    var loadMoreNewPeopleHandler = function() {
        var personIds = $("#browse-view [data-container = 'people'] .people-card:not(.invisible)").map(function(index, person) {
            return +person.getAttribute("href").match(/\/(\d+)/).pop();
        }).get();
        var referenceId = _.isEmpty(personIds) ? 0 : _.max(personIds);

        var requestData = {
            "id": referenceId,
            "lookupDirection": "newest"
        };

        $.ajax({
            type: "GET",
            url: "/get/people",
            data: utilities.formatJSON(requestData),
            success: function(response) {
                //render the batch people cards
                var peopleCards = "";
                response.data.people.forEach(function(person) {
                    peopleCards += personCardTemplate({person: person});
                });
                $("#browse-view [data-container = 'people'] .people-card").filter(":first").before(peopleCards);
                $(".sort-by-options select#people-sort-options").trigger("change");
            },
            dataType: "json"
        });
    };

    //explore pitch page handler definitions
    var pitchCardTemplate = _.template($("#pitch-card-template").html());
    var pitchListTemplate = _.template($("#pitch-list-template").html());

    var changePitchCategoryHandler = function(event) {
        var categorySelector = $(event.target);
        var entityId = +categorySelector.parents("[data-entity-id]").attr("data-entity-id");
        var categoryId = +categorySelector.val();
        var entityCategoryId = +categorySelector.attr("data-category-id");

        var categoryData = {
            "edited": {
                "categories": [{
                    "id": entityCategoryId,
                    "categoryId": categoryId
                }]
            }
        };

        var updateURL = "/submit/edit/pitch/" + entityId;
        $.ajax({
            type: "POST",
            url: updateURL,
            data: JSON.stringify(categoryData),
            success: function(response) {
                if (response.updateIsSuccessful) {
                    //update the list view
                    var categoryName = categorySelector.find(":selected").attr("label");
                    var oldCategoryColourClass = categorySelector.parents("[data-entity-id]").get(0).className.match(/([a-z0-9\-]*-project)/).join(" ");
                    var newCategoryColourClass = categoryName + "-category";
                    categorySelector.parents("[data-entity-id]")
                                    .removeClass(oldCategoryColourClass)
                                    .addClass(newCategoryColourClass);

                    //update the card view
                    $("#browse-view [data-container = 'pitches'] .card-view[data-entity-id = " + entityId + "]").removeClass(oldCategoryColourClass)
                                                                                                                .addClass(newCategoryColourClass)
                                                                                                                .find(".tag")
                                                                                                                .text(categoryName.replace("and", "&amp;").replace("-", " "));

                }
            },
            dataType: "json"
        });
    };

    var loadMoreOldPitchesHandler = function() {
        var oldestPitchIds = $("#browse-view [data-container = 'pitches'] .item-container[data-entity-id]").map(function(index, pitch) {
            return +pitch.getAttribute("data-entity-id");
        }).get();
        var oldestPitchId = _.isEmpty(oldestPitchIds) ? 0 : _.min(oldestPitchIds);

        $.ajax({
            type: "GET",
            url: "/get/pitches/o/" + oldestPitchId,
            success: function(response) {
                var pitchCards = new Array(response.pitches.length);
                var pitchLists = new Array(response.pitches.length);
                response.pitches.forEach(function(pitch, index) {
                    var pitchCard = $(pitchCardTemplate({pitch: pitch}));
                    pitchCard.find(".button.like-button button").on("click", likeHandler.likeCard);

                    var pitchList = $(pitchListTemplate({pitch: pitch}));
                    pitchList.find(".button.like-button button").on("click", likeHandler.likeCard);
                    pitchList.find("select[name = 'categories']").on("change", changePitchCategoryHandler);
                    pitchList.find("select[name = 'categories']").val(pitch.categories[0].categoryId);

                    if ($(".card-view-icon").hasClass("list-view-icon")) {
                        pitchCard.addClass("hidden");
                    } else {
                        pitchList.addClass("hidden");
                    }
                    pitchCards[index] = pitchCard;
                    pitchLists[index] = pitchList;
                });
                $("#browse-view [data-container = 'pitches'] .invisible").filter(":first").before(pitchCards);
                $("#browse-view [data-container = 'pitches'] .card-view").filter(":first").before(pitchLists);
                $(".sort-by-options select#pitches-sort-options").trigger("change");
            },
            dataType: "json"
        });
    };

    var loadMoreNewPitchesHandler = function() {
        var newestPitchIds = $("#browse-view [data-container = 'pitches'] .item-container[data-entity-id]").map(function(index, pitch) {
            return +pitch.getAttribute("data-entity-id");
        }).get();
        var newestPitchId = _.isEmpty(newestPitchIds) ? 0 : _.max(newestPitchIds);

        $.ajax({
            type: "GET",
            url: "/get/pitches/n/" + newestPitchId,
            success: function(response) {
                var pitchCards = new Array(response.pitches.length);
                var pitchLists = new Array(response.pitches.length);
                response.pitches.forEach(function(pitch, index) {
                    var pitchCard = $(pitchCardTemplate({pitch: pitch}));
                    pitchCard.find(".button.like-button button").on("click", likeHandler.likeCard);

                    var pitchList = $(pitchListTemplate({pitch: pitch}));
                    pitchList.find(".button.like-button button").on("click", likeHandler.likeCard);
                    pitchList.find("select[name = 'categories']").on("change", changePitchCategoryHandler);
                    pitchList.find("select[name = 'categories']").val(pitch.categories[0].categoryId);

                    if ($(".card-view-icon").hasClass("list-view-icon")) {
                        pitchCard.addClass("hidden");
                    } else {
                        pitchList.addClass("hidden");
                    }
                    pitchCards[index] = pitchCard;
                    pitchLists[index] = pitchList;
                });

                $("#browse-view [data-container = 'pitches'] .invisible").filter(":first").before(pitchCards);
                $("#browse-view [data-container = 'pitches'] .card-view").filter(":first").before(pitchLists);
                $(".sort-by-options select#pitches-sort-options").trigger("change");
            },
            dataType: "json"
        });
    };

    var refreshPitchCard = function(cardView, entity) {
        cardView.find(".name").text(entity.title);
        cardView.find(".one-liner").text(entity.pitch);
        cardView.find(".reaction-buttons [data-counter-type = 'like']").text(entity.numberOfLikes);
        cardView.find(".reaction-buttons [data-counter-type = 'comment']").text(entity.numberOfComments);
        cardView.find(".project-champion .profile-image-small:not([style])").text(entity.creator.initials);
        cardView.find(".project-champion .profile-image-small[style]").css("background-image", "url(" + entity.creator.imagePath + ")");

        var creatorFullname = entity.creator.firstName + " " + entity.creator.lastName;
        cardView.find(".project-champion .started-by .entity-creator-name").text(creatorFullname);

        var oldCategory = cardView.get(0).className.match(/([a-z0-9\-]*)-category/);
        if (oldCategory != null) {
            oldCategory = oldCategory.pop();
            cardView.removeClass(oldCategory + "-category");
        }

        var newCategory = entity.categories[0].name.replace(/\s/g, '-');
        cardView.addClass(newCategory + "-category");

        var newCategoryName = entity.categories[0].name.replace("and", "&amp;");
        cardView.find(".tag").text(newCategoryName);
    };

    var refreshPitchList = function(listView, entity) {
        listView.find(".name").text(entity.title);
        listView.find(".one-liner").text(entity.pitch);
        listView.find(".project-champion .profile-image-small:not([style])").text(entity.creator.initials);
        listView.find(".project-champion .profile-image-small[style]").css("background-image", "url(" + entity.creator.imagePath + ")");

        var creatorFullname = entity.creator.firstName + " " + entity.creator.lastName;
        listView.find(".project-champion .started-by .entity-creator-name").text(creatorFullname);

        listView.find("select[name = 'categories']").val(entity.categories[0].categoryId);

        var oldCategory = listView.get(0).className.match(/([a-z0-9\-]*)-category/);
        if (oldCategory != null) {
            oldCategory = oldCategory.pop();
            listView.removeClass(oldCategory + "-category");
        }

        var newCategory = entity.categories[0].name.replace(/\s/g, '-');
        listView.addClass(newCategory + "-category");
    };

    var refreshPitchView = function(pitch, data) {
        //update card view
        var cardView = pitch.filter(".card-view");
        if (cardView.exists()) {
            refreshPitchCard(cardView, data);
        }

        //update list view
        var listView = pitch.filter(".list-view");
        if (listView.exists()) {
            refreshPitchList(listView, data);
        }
    };

    //FUTURE TODO: lower the performance by only updating the cards that are in the viewport
    var ENTITY_TABS = ["pitches", "projects"];
    var refreshPitches = function(event) {
        var tab = $(".browse-tabs .selected").attr("data-container");

        //TODO: maybe this if case can become buggy if projects is added?
        if (_.include(ENTITY_TABS, tab)) {
            var entityIds = "";
            $("#browse-view [data-container = '" + tab + "'] .item-container.card-view[data-entity-id]").each(function(index, entity) {
                var entityId = entity.getAttribute("data-entity-id");
                entityIds += (entityId + ",");
            });
            entityIds = entityIds.replace(/,$/, "");

            if (entityIds.length > 0) {
                var requestURL = "/get/pitches?ids=" + entityIds;
                $.ajax({
                    type: "GET",
                    url: requestURL,
                    success: function(response) {
                        response.pitches.forEach(function(entity) {
                            var items = $("#browse-view [data-container = '" + tab + "'] .item-container[data-entity-id = " + entity.id + "]");
                            refreshPitchView(items, entity);
                        });
                    },
                    dataType: "json"
                });
            }
        }
    };

    var addContentButtonUpdate = function(index, text) {
        return (text == "+ Add Content") ? "✖ Cancel" : "+ Add Content";
    };

    //route: /browse
    if (/^\/browse/i.test(route)) {
        var exploreRouteHandler = new ExploreRouteHandler();

        //update the history state when page loads on browse page
        if (window.history && window.history.state) {
            $(window).on("popstate", exploreRouteHandler.popStateHandler);
        } else if (window.history && !window.history.state) {
            var selectedTab = $(".browse-tabs a.selected").attr("data-container");
            exploreRouteHandler.updateState(route, selectedTab);
        }

        $(".card-view-icon").on("click", function(event) {
            var viewButton = $(event.target);
            viewButton.toggleClass("list-view-icon");

            if (viewButton.hasClass("list-view-icon")) {
                $("#browse-view .items-container:not(.hidden) .list-view").removeClass("hidden");
                $("#browse-view .items-container:not(.hidden) .card-view").addClass("hidden");
            } else {
                $("#browse-view .items-container:not(.hidden) .card-view").removeClass("hidden");
                $("#browse-view .items-container:not(.hidden) .list-view").addClass("hidden");
            }
        });

        var infiniteScrollThrottlingId;
        var timerThrottlingId;
        var refreshPitchesTimerId;
        $("ul.browse-tabs a").on("click", function(event) {
            event.preventDefault();
            var tab = event.target.getAttribute("data-container");

            //show appropriate contents and update browse tabs
            $(".browse-tabs a").removeClass("selected")
                                .filter("[data-container = " + tab + "]")
                                .addClass("selected");

            $(".items-container[data-container]").addClass("hidden")
                                                .filter("[data-container = " + tab + "]")
                                                .removeClass("hidden");

            var newRoute = event.target.getAttribute("href");
            exploreRouteHandler.pushState(newRoute, tab);

            //switch to the specific set of sort options
            $(".sort-by-options select[name = 'sort-by-options']").addClass("hidden");
            $("#" + tab + "-sort-options").removeClass("hidden");
        }).on("click", function(event) {
            //explore page specific event handlers

            //clear up the infinite scroll and timer handler from the previous explore page
            window.clearInterval(infiniteScrollThrottlingId);
            window.clearInterval(timerThrottlingId);
            window.clearInterval(refreshPitchesTimerId);

            var selected = this.getAttribute("data-container");
            switch (selected) {
                case "pitches":
                    infiniteScrollThrottlingId = window.setInterval(infiniteScrollHandler, SCROLLBAR_DETECTION_PERIOD, loadMoreOldPitchesHandler);
                    timerThrottlingId = window.setInterval(loadMoreNewPitchesHandler, CARD_REFRESH_PERIOD);
                    refreshPitchesTimerId = window.setInterval(refreshPitches, CARD_REFRESH_PERIOD);
                    $("#voting-status-bar").removeClass("hidden");
                    break;

                case "projects":
                    $("#voting-status-bar").addClass("hidden");
                    break;

                case "people":
                    infiniteScrollThrottlingId = window.setInterval(infiniteScrollHandler, SCROLLBAR_DETECTION_PERIOD, loadMoreOldPeopleHandler);
                    timerThrottlingId = window.setInterval(loadMoreNewPeopleHandler, CARD_REFRESH_PERIOD);
                    $("#voting-status-bar").addClass("hidden");
                    break;
            }
        });

        //sorting feature
        //NOTE: Javascript default sort method is not stable. You will see the effect if there are more than 10 cards in the explore page.
        //sort functions
        var sortByEntityTitleAlphabetical = function(a, b) {
            var titleA = $(a).find(".name").text();
            var titleB = $(b).find(".name").text();
            return (titleA < titleB) ? -1 : (titleA > titleB);
        };

        var sortByEntityTitleReverse = function(a, b) {
            return sortByEntityTitleAlphabetical(b, a);
        };

        var sortByEntityCategory = function(a, b) {
            var categoryA = $(a).find("h4:first").text();
            var categoryB = $(b).find("h4:first").text();
            if (categoryA == categoryB) {
                return sortByEntityTitleAlphabetical(a, b);
            } else if (categoryA == "selected") {
                return -1;
            } else if (categoryB == "selected") {
                return 1;
            } else if ((categoryA == "selected") && (categoryB == "posted")) {
                return -1;
            } else if ((categoryA == "posted") && (categoryB == "selected")) {
                return 1;
            } else if (categoryA == "presented") {
                return 1;
            } else if (categoryB == "presented") {
                return -1;
            }
        };

        var sortByMostLikedEntity = function(a, b) {
            var likesA = +$(a).find(".reaction-buttons p[data-counter-type = 'like']").text();
            var likesB = +$(b).find(".reaction-buttons p[data-counter-type = 'like']").text();
            return (likesB - likesA);
        };

        var sortByLeastLikedEntity = function(a, b) {
            return sortByMostLikedEntity(b, a);
        };

        var sortByMostEngagedEntity = function(a, b) {
            var engagesA = +$(a).find(".reaction-buttons p[data-counter-type = 'comment']").text();
            var engagesB = +$(b).find(".reaction-buttons p[data-counter-type = 'comment']").text();
            return (engagesB - engagesA);
        };

        var sortByLeastEngagedEntity = function(a, b) {
            return sortByMostEngagedEntity(b, a);
        };

        var sortByNewestEntity = function(a, b) {
            var idA = +a.pathname.match(/\/(\d+)/).pop();
            var idB = +b.pathname.match(/\/(\d+)/).pop();
            return (idB - idA);
        };

        var sortByOldestEntity = function(a, b) {
            return sortByNewestEntity(b, a);
        };

        var sortByPersonNameAlphabetical = function(a, b) {
            var nameA = $(a).find("h6.person-name").text();
            var nameB = $(b).find("h6.person-name").text();
            return (nameA < nameB) ? -1 : (nameA > nameB);
        };

        var sortByPersonNameReverse = function(a, b) {
            return sortByPersonNameAlphabetical(b, a);
        };

        var sortByNewestPerson = function(a, b) {
            var idA = +a.pathname.match(/\/(\d+)/).pop();
            var idB = +b.pathname.match(/\/(\d+)/).pop();
            return (idB - idA);
        };

        var sortByOldestPerson = function(a, b) {
            return sortByNewestPerson(b, a);
        };

        var sortListByCard = function(card) {
            var entityId = card.getAttribute("data-entity-id");
            return $("#browse-view .items-container:not(.hidden) .list-view[data-entity-id = " + entityId + "]").get(0);
        };

        //actual sorting
        $(".sort-by-options select[name = 'sort-by-options']").on("change", function(event) {
            var tab = this.id;
            var sortOption = $(this).val();
            var cards = $("#browse-view .items-container:not(.hidden) .card-view").get();
            var lists = $("#browse-view .items-container:not(.hidden) .list-view").get();
            var people = $("#browse-view .items-container:not(.hidden) .people-card:not(.invisible)").get();

            switch (sortOption) {
                //TODO: string sorting doesn't seem to work as one might expect. Figure out why and fix it
                case "entity-title-in-alphabetical":
                    var sortedCards = sortHandler.mergeSort(cards, sortByEntityTitleAlphabetical);
                    if (lists.length) {
                        var sortedLists = sortedCards.map(sortListByCard);
                    }
                    break;

                case "entity-title-in-reverse":
                    // var sortedCards = cards.sort(sortByEntityTitleReverse);
                    var sortedCards = sortHandler.mergeSort(cards, sortByEntityTitleReverse);
                    if (lists.length) {
                        var sortedLists = sortedCards.map(sortListByCard);
                    }
                    break;

                case "entity-category":
                    var sortedCards = sortHandler.mergeSort(cards, sortByEntityCategory);
                    if (lists.length) {
                        var sortedLists = sortedCards.map(sortListByCard);
                    }
                    break;

                case "most-liked-entity":
                    // var sortedCards = cards.sort(sortByMostLikedEntity);
                    var sortedCards = sortHandler.mergeSort(cards, sortByMostLikedEntity);
                    if (lists.length) {
                        var sortedLists = sortedCards.map(sortListByCard);
                    }
                    break;

                case "least-liked-entity":
                    // var sortedCards = cards.sort(sortByLeastLikedEntity);
                    var sortedCards = sortHandler.mergeSort(cards, sortByLeastLikedEntity);
                    if (lists.length) {
                        var sortedLists = sortedCards.map(sortListByCard);
                    }
                    break;

                case "most-engaged-entity":
                    // var sortedCards = cards.sort(sortByMostEngagedEntity);
                    var sortedCards = sortHandler.mergeSort(cards, sortByMostEngagedEntity);
                    if (lists.length) {
                        var sortedLists = sortedCards.map(sortListByCard);
                    }
                    break;

                case "least-engaged-entity":
                    // var sortedCards = cards.sort(sortByLeastEngagedEntity);
                    var sortedCards = sortHandler.mergeSort(cards, sortByLeastEngagedEntity);
                    if (lists.length) {
                        var sortedLists = sortedCards.map(sortListByCard);
                    }
                    break;

                case "newest-entity":
                    // var sortedCards = cards.sort(sortByNewestEntity);
                    var sortedCards = sortHandler.mergeSort(cards, sortByNewestEntity);
                    if (lists.length) {
                        var sortedLists = sortedCards.map(sortListByCard);
                    }
                    break;

                case "oldest-entity":
                    // var sortedCards = cards.sort(sortByOldestEntity);
                    var sortedCards = sortHandler.mergeSort(cards, sortByOldestEntity);
                    if (lists.length) {
                        var sortedLists = sortedCards.map(sortListByCard);
                    }
                    break;

                case "person-name-in-alphabetical":
                    // var sortedPeople = people.sort(sortByPersonNameAlphabetical);
                    var sortedPeople = sortHandler.mergeSort(people, sortByPersonNameAlphabetical);
                    break;

                case "person-name-in-reverse":
                    // var sortedPeople = people.sort(sortByPersonNameReverse);
                    var sortedPeople = sortHandler.mergeSort(people, sortByPersonNameReverse);
                    break;

                case "newest-person":
                    // var sortedPeople = people.sort(sortByNewestPerson);
                    var sortedPeople = sortHandler.mergeSort(people, sortByNewestPerson);
                    break;

                case "oldest-person":
                    // var sortedPeople = people.sort(sortByOldestPerson);
                    var sortedPeople = sortHandler.mergeSort(people, sortByOldestPerson);
                    break;
            }

            if (cards.length) {
                $("#browse-view .items-container:not(.hidden) .invisible").filter(":first").before(sortedCards);
            }

            if (lists.length) {
                $("#browse-view .items-container:not(.hidden) .card-view").filter(":first").before(sortedLists);
            }

            if (people.length) {
                $("#browse-view .items-container:not(.hidden) .invisible").filter(":first").before(sortedPeople);
            }
        });

        //search bar feature (disabled until functional)
        // var ENTER_KEY = 13;
        // $(".search-bar input[type = 'search']").on("keydown paste", function(event) {
        //     console.log(event);
        //     if (event.type === "keydown") {
        //         var keyCode = event.keyCode || event.charCode || event.which;
        //         var character = String.fromCharCode(keyCode);
        //         var isValidCharacter = /[A-Za-z0-9\~\`\!\@\#\$\%\^\&\*\(\)\_\-\+\=\{\}\[\]\"\'\:\;\|\/\\\?\>\.\<\,]/.test(character);
        //         console.log(character);

        //         if (keyCode === ENTER_KEY) {
        //         }
        //     }

        //     var keywords = $(this).val().trim();
        //     var searchRequest = {
        //         "keywords": keywords
        //     };

        //     var exploreTab = $(".browse-tabs a.selected").attr("data-container");
        //     var requestURL = "/submit/search/" + exploreTab;
        //     $.ajax({
        //         type: "POST",
        //         url: requestURL,
        //         data: JSON.stringify(searchRequest),
        //         success: function(response) {
        //             console.log(response);
        //         },
        //         dataType: "json"
        //     });
        // });

        //load pitches, projects, and people at the start
        loadMoreNewPitchesHandler();
        loadMoreNewPeopleHandler();

        var selected = $("ul.browse-tabs a.selected").attr("data-container");
        switch (selected) {
            case "pitches":
                infiniteScrollThrottlingId = window.setInterval(infiniteScrollHandler, SCROLLBAR_DETECTION_PERIOD, loadMoreOldPitchesHandler);
                timerThrottlingId = window.setInterval(loadMoreNewPitchesHandler, CARD_REFRESH_PERIOD);
                refreshPitchesTimerId = window.setInterval(refreshPitches, CARD_REFRESH_PERIOD);
                $("#voting-status-bar").removeClass("hidden");
                break;

            case "projects":
                $("#voting-status-bar").addClass("hidden");
                break;

            case "people":
                infiniteScrollThrottlingId = window.setInterval(infiniteScrollHandler, SCROLLBAR_DETECTION_PERIOD, loadMoreOldPeopleHandler);
                timerThrottlingId = window.setInterval(loadMoreNewPeopleHandler, CARD_REFRESH_PERIOD);
                $("#voting-status-bar").addClass("hidden");
                break;
        }
    }

    //route: /create/pitch
    else if (/^\/create\/pitch/i.test(route)) {
        //have the first category id ready as default category id
        // var defaultCategoryId = +$("#create-pitch-form select[name = 'categories'] option:not([disabled]):first").val();
        var defaultCategoryId;
        $.ajax({
            type: "GET",
            url: "/get/list/categories",
            success: function(response) {
                defaultCategoryId = response.data.categories[0].id;
            },
            dataType: "json"
        });

        $("#submit").on("click", function(event) {
            var pitchTitle = $("#create-pitch-form textarea[name = 'title']").val().trim();
            var pitch = $("#create-pitch-form textarea[name = 'pitch']").val().trim();

            //form validation - a pitch should have a title
            if (_.isEmpty(pitchTitle) || $("#create-pitch-form textarea[name = 'title']").hasClass("required")) {
                $("#create-pitch-form textarea[name = 'title']").on("keypress paste", function() {
                    $(this).off("keypress paste")
                           .removeClass("required")
                           .next(".invalid-message")
                           .addClass("hidden");
                }).addClass("required")
                  .focus()
                  .next(".invalid-message")
                  .removeClass("hidden");
                return;
            }

            //form validation - a pitch should have the pitch
            if (_.isEmpty(pitch) || $("#create-pitch-form textarea[name = 'pitch']").hasClass("required")) {
                $("#create-pitch-form textarea[name = 'pitch']").on("keypress paste", function() {
                    $(this).off("keypress paste")
                           .removeClass("required")
                           .next(".invalid-message")
                           .addClass("hidden");
                }).addClass("required")
                  .focus()
                  .next(".invalid-message")
                  .removeClass("hidden");
                return;
            }

            var entityContents = {
                "setPrivate": false,
                "title": pitchTitle,
                "pitch": pitch,
                "categories": [defaultCategoryId]
            };

            $("#create-pitch textarea[name]").each(function(index, addedContent) {
                var field = $(addedContent);
                var fieldName = field.attr("name");

                switch (fieldName) {
                    case "description":
                    case "background":
                    case "stakeholder":
                    case "existing-solution":
                    case "market-analysis":
                    case "other-information":
                        var text = field.val().trim();
                        if (!_.isEmpty(text)) {
                            entityContents[fieldName] = text;
                        }
                        break;
                }
            });

            $.ajax({
                type: "POST",
                url: "/submit/new/pitch",
                data: JSON.stringify(entityContents),
                success: function(response) {
                    if (response.pitchIsCreated && /^(\d+)$/g.test(response.pitchId)) {
                        window.location.href = "/pitch/" + response.pitchId;
                    } else {
                        //something went wrong and pitch was not created
                    }
                },
                dataType: "json"
            });
        });
    }

    //route: /create/project
    else if (/^\/create\/project/i.test(route)) {

    }

    //route: /pitch/(id)
    else if (/^\/pitch\/(\d+)/i.test(route)) {
        //load up the dynamic templates for the idea page
        var addDescriptionFormTemplate = _.template($("#add-description-form-template").html());
        var addRoleFormTemplate = _.template($("#add-pitch-role-form-template").html());

        //comment handling
        $("#comment-control button").on("click", commentHandler.postComment);
        var infiniteScrollThrottlingId = window.setInterval(infiniteScrollHandler, SCROLLBAR_DETECTION_PERIOD, commentHandler.loadOldComments);
        var updateCommentThrottlingId = window.setInterval(commentHandler.loadNewComments, COMMENT_REFRESH_PERIOD);
        commentHandler.loadNewComments();

        //like button and like counter handling
        $(".like-button button").on("click", likeHandler.likeEntity);
        var updateLikeCountersThrottlingId = window.setInterval(null, LIKE_COUNTER_REFRESH_PERIOD);

        //edit pitch handling
        $("#edit-entity-button").on("click", function() {
            //switch edit button to cancel and submit changes buttons
            $(this).parent(".closed-button")
                   .add(".like-button") //TODO FIX: this should be .closed-button to generalize, but it also targets the children of .back-submit
                   .addClass("hidden")
                   .siblings(".back-submit")
                   .removeClass("hidden");

            //switch to edit pitch page
            $("#published-pitch").addClass("hidden");
            $("#edit-pitch").removeClass("hidden");
        });

        $("#cancel-edit-entity-button").on("click", function() {
            //switch cancel and submit changes buttons to edit button
            $(this).parents(".back-submit")
                   .addClass("hidden")
                   .siblings(".closed-button")
                   .removeClass("hidden");

            //switch to publish pitch page
            $("#edit-pitch").addClass("hidden");
            $("#published-pitch").removeClass("hidden");
        });

        //edit existing entity contents handling
        var initialEntityContents = {};
        $("#edit-pitch-form textarea[name]").each(function(index, entityContent) {
            var field = $(entityContent);
            var fieldName = field.attr("name");
            initialEntityContents[fieldName] = field.val().trim();
        });

        //submit changes
        $("#submit-edits").on("click", function() {
            var entityId = route.match(/\/(\d+)/);
            if (entityId) {
                //form validation - a pitch should have a title
                var pitchTitle = $("#edit-pitch-form textarea[name = 'title']").val().trim();
                if (_.isEmpty(pitchTitle) || $("#edit-pitch-form textarea[name = 'title']").hasClass("required")) {
                    $("#edit-pitch-form textarea[name = 'title']").on("keypress paste", function() {
                        $(this).off("keypress paste")
                               .removeClass("required")
                               .next(".invalid-message")
                               .addClass("hidden");
                    }).addClass("required")
                      .focus()
                      .next(".invalid-message")
                      .removeClass("hidden");
                    return;
                }

                //form validation - a pitch should have the pitch
                var pitch = $("#edit-pitch-form textarea[name = 'pitch']").val().trim();
                if (_.isEmpty(pitch) || $("#edit-pitch-form textarea[name = 'pitch']").hasClass("required")) {
                    $("#edit-pitch-form textarea[name = 'pitch']").on("keypress paste", function() {
                        $(this).off("keypress paste")
                               .removeClass("required")
                               .next(".invalid-message")
                               .addClass("hidden");
                    }).addClass("required")
                      .focus()
                      .next(".invalid-message")
                      .removeClass("hidden");
                    return;
                }

                //package the new and edited fields in a consistent packaging scheme here
                var entityContents = {
                    edited: {},
                    removed: {},
                    added: {}
                };

                $("#edit-pitch-form textarea[name]").each(function(index, entityContent) {
                    var field = $(entityContent);
                    var fieldName = field.attr("name");
                    switch (fieldName) {
                        case "title":
                        case "pitch":
                            var text = field.val().trim();
                            if (initialEntityContents[fieldName] !== text) {
                                entityContents["edited"][fieldName] = text;
                            }
                            break;

                        case "description":
                        case "background":
                        case "stakeholder":
                        case "existing-solution":
                        case "market-analysis":
                        case "other-information":
                            var entityContentId = +field.attr("data-entity-content-id");
                            var text = field.val().trim();

                            if (_.isEmpty(text) && (entityContentId > 0)) {
                                entityContents["removed"][fieldName] = {"id": entityContentId};

                            } else if (initialEntityContents[fieldName] !== text) {
                                if (_.isEmpty(initialEntityContents[fieldName]) && (entityContentId == 0)) {
                                    entityContents["added"][fieldName] = text;
                                } else {
                                    entityContents["edited"][fieldName] = {
                                        "id": entityContentId,
                                        "value": text
                                    };
                                }
                            }
                            break;
                    }
                });

                //submit changes
                if (_.isEmpty(entityContents.edited) && _.isEmpty(entityContents.removed) && _.isEmpty(entityContents.added)) {
                    window.location.href = route;
                } else {
                    var submissionURL = "/submit/edit/pitch/" + entityId.pop();
                    $.ajax({
                        type: "POST",
                        url: submissionURL,
                        data: JSON.stringify(entityContents),
                        success: function(response) {
                            if (response.updateIsSuccessful) {
                                window.location.href = route;
                            } else {
                                //show a warning that the edits are not successful on the backend
                            }
                        },
                        dataType: "json"
                    });
                }
            }
        });
    }

    //route: /project/(id)
    else if (/^\/project\/(\d+)/i.test(route)) {
        //comment handling
        $("#comment-control button").on("click", commentHandler.postComment);
        var infiniteScrollThrottlingId = window.setInterval(infiniteScrollHandler, SCROLLBAR_DETECTION_PERIOD, commentHandler.loadOldComments);
        var updateCommentThrottlingId = window.setInterval(commentHandler.loadNewComments, COMMENT_REFRESH_PERIOD);
        commentHandler.loadNewComments();
    }

    //route: /people/(id)
    else if (/^\/people\/(?:\d+)/i.test(route)) {
        var contactTypes;
        var defaultContactTypeId;
        $.ajax({
            type: "GET",
            url: "/get/list/contact-types",
            success: function(response) {
                contactTypes = response.data.contactTypes;
                defaultContactTypeId = response.data.contactTypes[0].id;
            },
            dataType: "json"
        });

        var personId = +route.match(/\/(\d+)/).pop();
        var loadMoreOldPortfolioHandler = function(event) {
            //TODO: to be implemented
        };

        var loadMoreNewPortfolioHandler = function(event) {
            var person = {
                "id": personId
            };

            $.ajax({
                type: "GET",
                url: "/get/portfolios",
                data: utilities.formatJSON(person),
                success: function(response) {
                    if (response.data.started.length > 0) {
                        var portfolio = new Array(response.data.started.length);
                        response.data.started.forEach(function(entity, index) {
                            var entityCard;
                            switch (entity.entityType) {
                                case "pitch":
                                    entityCard = $(pitchCardTemplate({pitch: entity}));
                                    entityCard.find(".button.like-button button").on("click", likeHandler.likeCard);
                            }
                            portfolio[index] = entityCard;
                        });

                        $("#published-profile .profile-portfolio .item-container.invisible:first").before(portfolio);

                        //swap warning message and portfolio cards
                        $("#published-profile .profile-portfolio .warning-message").addClass("hidden");
                        $("#published-profile .profile-portfolio .items-container").removeClass("hidden");
                    } else {
                        //swap warning message and portfolio cards
                        $("#published-profile .profile-portfolio .warning-message").removeClass("hidden");
                        $("#published-profile .profile-portfolio .items-container").addClass("hidden");
                    }
                },
                dataType: "json"
            });
        };

        //setup infinite scroll handling
        // var infiniteScrollThrottlingId = window.setInterval(infiniteScrollHandler, SCROLLBAR_DETECTION_PERIOD, loadMoreOldPortfolioHandler);
        loadMoreNewPortfolioHandler();

        //changing between publish and edit views
        $("#edit-profile-button").on("click", function(event) {
            //switch over to edit profile view
            $(".help-link").addClass("hidden");
            $("#edit-profile").removeClass("hidden");
            $("#published-profile").addClass("hidden");

            //switch over to back-submit buttons
            $(this).parent().addClass("hidden");
            $(".user-controls .back-submit").removeClass("hidden");
        });

        $("#cancel-edit-profile-button").on("click", function(event) {
            //switch over to published profile view
            $("#edit-profile").addClass("hidden");
            $("#published-profile").removeClass("hidden");

            //switch over to edit profile button
            $("#edit-profile-button").parent().removeClass("hidden");
            $(".user-controls .back-submit").addClass("hidden");
            $(".help-link").removeClass("hidden");
        });

        //add new profile attribute
        $("#edit-profile .add-content").on("click", function(event) {
            $(event.target).text(addContentButtonUpdate);
            $(event.target).parent().toggleClass("cancel-remove-button");

            var addContentModule = $("#edit-profile .add-content-module");
            if (addContentModule.hasClass("hidden")) {
                addContentModule.removeClass("hidden");
            } else {
                addContentModule.addClass("hidden");
            }
        });

        var removeButtonHandler = function() {
            $(this).parents(".profile-attribute").remove();
        };

        var removedProfileAttributes = {};
        $("#edit-profile-form .cancel-remove-button button").on("click", function(event) {
            var field = $(this).parents(".profile-attribute").find("textarea[data-profile-attribute-id]");
            var fieldName = field.attr("name");
            var profileAttributeId = +field.attr("data-profile-attribute-id");

            //check if biography is removed
            if (fieldName === "biography") {
                $("#edit-profile #add-profile-biography-button")[0].removeAttribute("data-module-disabled");
                $("#edit-profile #add-profile-biography-button").removeClass("disabled-module");
            }

            //keep a record of what profile attribute has been removed
            var profileAttribute = {"id": profileAttributeId};
            switch (fieldName) {
                case "biography":
                    removedProfileAttributes["biography"] = profileAttribute;
                    break;

                case "contact":
                    if (!("contacts" in removedProfileAttributes)) {
                        removedProfileAttributes["contacts"] = [profileAttribute];
                    } else {
                        removedProfileAttributes["contacts"].push(profileAttribute);
                    }
                    break;
            }
        }).on("click", removeButtonHandler);

        //detect changes to fields
        var newProfileAttributeDetectionHandler = function() {
            if (!this.hasAttribute("data-added")) {
                this.setAttribute("data-added", "");
            }
        };
        var editDetectionHandler = function() {
            if (!this.hasAttribute("data-edited")) {
                this.setAttribute("data-edited", "");
            }
        };
        $("#edit-profile-form textarea").on("keydown paste", editDetectionHandler);
        $("#edit-profile-form select[name = 'categories']").on("change", function() {
            $(this).parent("form").siblings("textarea").attr("data-edited", "");
        });

        //add new biography
        //TODO: a user should only be able to have one biography at all time
        $("#edit-profile #add-profile-biography-button").on("click", function(event) {
            if (!this.hasAttribute("data-module-disabled")) {
                this.setAttribute("data-module-disabled", "");
                $(this).addClass("disabled-module");

                //create the biography form template and attach event handlers to it
                var rawTemplate = $("#add-profile-biography-form-template").html();
                var profileBiographyForm = $(_.template(rawTemplate)());
                profileBiographyForm.find("textarea").on("keydown paste", newProfileAttributeDetectionHandler);
                profileBiographyForm.find(".cancel-remove-button button").on("click", null, this, function(event) {
                    event.data.removeAttribute("data-module-disabled");
                    $(event.data).removeClass("disabled-module");
                }).on("click", removeButtonHandler);

                //add the biography form to the DOM
                $("#edit-profile-form ~ .create-buttons").before(profileBiographyForm);
            };
        });

        //add new contact
        $("#edit-profile #add-profile-contact-button").on("click", function(event) {
            //create the contact form template and attach event handlers to it
            var rawTemplate = $("#add-profile-contact-form-template").html();
            var template = _.template(rawTemplate);
            var profileContactForm = $(template({contactTypes: contactTypes}));
            profileContactForm.find("textarea").on("keydown paste", newProfileAttributeDetectionHandler);
            profileContactForm.find(".cancel-remove-button button").on("click", removeButtonHandler);

            //add the contact form to the DOM
            $("#edit-profile-form ~ .create-buttons").before(profileContactForm);
        });

        //upload photo with upload button
        $("#upload-profile-image-button").on("click", function() {
            $(this).siblings("input[type = 'file']").click();
        });

        $("#upload-profile-image-button ~ input[type = 'file']").on("change", function() {
            if (this.files.length > 0) {
                prepareImageForUpload(this.files[0]);
            }
        });

        //drag and drop photo upload
        // $("#edit-profile-form div.section-body[data-dropzone]").on("drag", function(event) {
        //     event.preventDefault();
        //     console.log(event.type);
        //     // $(this).addClass("dropzone");

        // }).on("dragstart dragenter", function(event) {
        //     console.log(event.type);
        //     $(this).addClass("dropzone");

        // }).on("dragend", function(event) {
        //     console.log(event.type);
        //     $(this).removeClass("dropzone");

        // }).on("drop", function(event) {
        //     event.preventDefault();
        //     console.log(event.type);
        //     if (event.dataTransfer.files.length > 0) {
        //         prepareImageForUpload(event.dataTransfer.files[0]);
        //     }
        // });

        var MAX_IMAGE_SIZE = Math.pow(2, 17);    //128kb
        var prepareImageForUpload = function(image) {
            //file should be an image
            if (!/image.*/.test(image.type)) {
                //show warning
                return;
            }

            var reader = new FileReader();
            reader.onload = function(event) {
                var imageDataURL = event.target.result;

                //image should be within a certain size (128kb)
                if (image.size > MAX_IMAGE_SIZE) {
                    downSampleImage(imageDataURL, image.type);
                } else {
                    updateProfileImagePreview(imageDataURL);
                }
            };
            reader.readAsDataURL(image);
        };

        var downSampleImage = function(dataURL, imageType) {
            //NOTE: Bilinear interpolation - http://stackoverflow.com/questions/19262141/resize-image-with-javascript-canvas-smoothly
            var img = document.createElement("img");
            img.onload = function() {
                var canvas = document.createElement("canvas");
                var context = canvas.getContext("2d");

                if (img.width > img.height) {
                    canvas.width = Math.min(300, img.width);
                    canvas.height = canvas.width * img.height / img.width;
                } else {
                    canvas.height = Math.min(300, img.height);
                    canvas.width = canvas.height * img.width / img.height;
                }

                var virtualCanvas = document.createElement("canvas");
                var virtualContext = virtualCanvas.getContext("2d");
                virtualCanvas.width = 0.5 * img.width;
                virtualCanvas.height = 0.5 * img.height;
                virtualContext.drawImage(img, 0, 0, virtualCanvas.width, virtualCanvas.height);
                virtualContext.drawImage(virtualCanvas, 0, 0, 0.5 * virtualCanvas.width, 0.5 * virtualCanvas.height);

                context.drawImage(virtualCanvas, 0, 0, 0.5 * virtualCanvas.width, 0.5 * virtualCanvas.height, 0, 0, canvas.width, canvas.height);
                var imageDataURL = canvas.toDataURL(imageType);

                updateProfileImagePreview(imageDataURL);
            };
            img.src = dataURL;
        };

        var updateProfileImagePreview = function(imageDataURL) {
            $("#edit-profile-image-initials").addClass("hidden");
            $("#edit-profile-image-preview").css("background-image", "url(" + imageDataURL + ")")
                                            .removeClass("hidden");
            $("#edit-profile-image-preview ~ .button input[name = 'image']").attr("data-edited", "")
                                                                            .attr("value", imageDataURL);
        };

        //submit changes
        $("#edit-profile-submission").on("click", function(event) {
            var personId = +route.match(/\/(\d+)/).pop();
            if (personId > 0) {
                //form validation - a person should have a name
                var firstName = $("#edit-profile-form textarea[name = 'first-name']").val().trim();
                if (_.isEmpty(firstName) || $("#edit-profile-form textarea[name = 'first-name']").hasClass("required")) {
                    $("#edit-profile-form textarea[name = 'first-name']").on("keypress paste", function() {
                        $(this).off("keypress paste")
                               .on("paste", editDetectionHandler)
                               .removeClass("required")
                               .next().next(".invalid-message")
                               .addClass("hidden");
                    }).addClass("required")
                      .focus()
                      .next().next(".invalid-message")
                      .removeClass("hidden");
                    return;
                }

                //package the new and edited fields in a consistent packaging scheme here
                var profileAttributes = {
                    edited: {},
                    removed: removedProfileAttributes,
                    added: {}
                };

                //package the edited profile attributes
                $("#edit-profile-form [data-edited]").each(function(index, editedProfileAttribute) {
                    var field = $(editedProfileAttribute);
                    var fieldName = field.attr("name");

                    switch (fieldName) {
                        case "first-name":
                        case "last-name":
                        case "title":
                            profileAttributes["edited"][fieldName] = field.val().trim();
                            break;

                        case "image":
                            profileAttributes["edited"][fieldName] = field.attr("value");
                            break;

                        case "biography":
                            var biography = {
                                "id": +field.attr("data-profile-attribute-id"),
                                "value": field.val().trim()
                            };
                            profileAttributes["edited"][fieldName] = biography;
                            break;

                        case "contact":
                            var contactTypeId = field.siblings("form").find("select option:selected").val();
                            var contact = {
                                "id": +field.attr("data-profile-attribute-id"),
                                "value": field.val().trim(),
                                "contactTypeId": contactTypeId
                            };

                            if (!("contacts" in profileAttributes["edited"])) {
                                profileAttributes["edited"]["contacts"] = [contact];
                            } else {
                                profileAttributes["edited"]["contacts"].push(contact);
                            }
                    }
                });

                //package the new profile attributes
                $("#edit-profile .profile-attribute textarea[data-added]").each(function(index, addedProfileAttribute) {
                    var field = $(addedProfileAttribute);
                    switch (field.attr("name")) {
                        case "biography":
                            profileAttributes["added"]["biography"] = field.val().trim();
                            break;

                        case "contact":
                            var contactTypeId = field.siblings("form").find("select option:selected").val();
                            var contact = {
                                "contactTypeId": _.isEmpty(contactTypeId) ? defaultContactTypeId : contactTypeId,
                                "value": field.val().trim()
                            };

                            if (!("contacts" in profileAttributes["added"])) {
                                profileAttributes["added"]["contacts"] = [contact];
                            } else {
                                profileAttributes["added"]["contacts"].push(contact);
                            }
                            break;
                    }
                });

                //submit profile changes
                if (_.isEmpty(profileAttributes.edited) && _.isEmpty(profileAttributes.removed) && _.isEmpty(profileAttributes.added)) {
                    window.location.href = route;
                } else {
                    var profile = {
                        "id": personId,
                        "attributes": profileAttributes
                    };

                    $.ajax({
                        type: "POST",
                        url: "/update/profile",
                        data: utilities.formatJSON(profile),
                        success: function(response) {
                            if (response.success) {
                                window.location.href = route;
                            } else {
                                //inform user something went wrong?
                            }
                        },
                        dataType: "json"
                    });
                }
            } else {
                //can't get the person id
            }
        });
    }

    //route: /help
    else if (/^\/help/i.test(route)) {
        var tabHandler = new TabHandler();

        //tabs handler
        $("#help-header a").on("click", function(event) {
            var section = "#" + event.target.getAttribute("data-name");
            tabHandler.goToByScroll(section);

            $("#help-header a.selected").removeClass("selected");
            $(event.target).addClass("selected");
        });

        //back button handler
        $(".back-submit .back-button button").on("click", function() {
            history.go(-1);
        });

        //go to initial section
        var initialSection = "#" + $("#help-header a.selected").attr("data-name");
        tabHandler.goToByScroll(initialSection);
    }



    //setup general event handlers
    //navigation bar handling
    $(".navigation-bar #navigation-sidebar-icon").on("click tap touchstart", function(event) {
        event.preventDefault()
        $(event.target).toggleClass("selected");
        $("#navigation-sidebar").toggleClass("hidden");
    });

    $(window).on("scroll", function() {
        throttleEventDetector.isScrolling = true;
    }).on("resize orientationchange", function() {
        throttleEventDetector.viewportResized = true;
    });

    window.setInterval(viewportResizedHandler, VIEWPORT_RESIZE_DETECTION_PERIOD, scrollbar.updateMaxScrollTop);

    // $(window).on("drop dragend dragover", function(event) {
    //     event.preventDefault();
    // });
});

//force the browser to calculate the viewport and scroll heights to allow the infinite scroll handler work properly
$(window).on("load", function() {
    $(window).trigger("resize");
});

//force the browser to reset the scroll bar to the top of the page in the next page load
$(window).on('beforeunload', function() {
    $(window).scrollTop(0);
});