var utilities = (function() {
    var formatBytes = function(attachmentSize) {
        var units = ["B", "kB", "MB", "GB", "TB"];
        var divider = 1024;
        var bytes = attachmentSize;

        var index = 0;
        for (index = 0; (bytes >= divider) && (index < units.length); index++) {
            bytes /= divider;
        }

        var formattedBytes = bytes.toFixed(2) + " " + units[index];
        return formattedBytes;
    };

    var formatJSON = function(dataModel) {
        return {
            "json": JSON.stringify(dataModel)
        };
    };

    return {
        formatBytes: formatBytes,
        formatJSON: formatJSON
    };
})();
var gravit8 = {};
gravit8.AttachmentHandler = function(attachment, entityId) {
    this.entityId = +entityId;
    this.attachment = attachment;

    //setting up some options
    this.PARTIAL_ATTACHMENT_SIZE = 512 * 1024;
    this.rangeStart = 0;
    this.rangeEnd = this.PARTIAL_ATTACHMENT_SIZE;
    this.totalParts = Math.max(Math.ceil(this.attachment.size / this.PARTIAL_ATTACHMENT_SIZE), 1);
    this.sliceMethod = "slice";
    if ("mozSlice" in this.attachment) {
        this.sliceMethod = "mozSlice";
    } else if ("webkitSlice" in this.attachment) {
        this.sliceMethod = "webkitSlice";
    }

    //resume/pause upload control flag
    this.isPaused = false;

    //for the AJAX request
    this.uploadRequest = new XMLHttpRequest();
    this.uploadRequest.responseType = "json";
    this.uploadRequest.timeout = 5000;
    this.numberOfRetries = 5;   //number of retries if upload fails for whatever reason

    //checksum to detect for data corruption during attachment transfer between client and server
    this.hash = new Rusha();
    this.hash.resetState();
    this.checksum = null;
    this.partialChecksum = null;

    //placeholder identifier for the attachment until a proper attachment identifier is created from the server
    this.attachmentId = "";

    //keep track of the corresponding UI element
    this.uiTemplate = null;
    this.userControl = null;
};

gravit8.AttachmentHandler.prototype = {
    __allUploadsFinished: function() {
        //check if all attachments are done uploading
        return ($("#attachments [data-uploading]").length === 0);
    },
    __updateChecksum: function() {
        //update the checksum with the current chunk of data
        // var self = this;

        // var fileReader =  new FileReader();
        // fileReader.onload = function(event) {
        //     var partialAttachmentAsArrayBuffer = event.target.result;
        //     self.hash.append(partialAttachmentAsArrayBuffer);
        //     self.checksum = self.hash.digestFromArrayBuffer(partialAttachmentAsArrayBuffer);
        //     console.log(self.checksum + "\n");
        //     //should end with 3bfd8311b36ecb329589e4baf9aa9fe7be6f37a6
        // };

        // var partialAttachment = self.attachment[self.sliceMethod](self.rangeStart, self.rangeEnd);
        // fileReader.readAsArrayBuffer(partialAttachment);
    },
    __updateUploadProgress: function() {
        var percentage = (this.rangeEnd / this.attachment.size) * 100;
        if (this.attachment.size === 0) {
            percentage = 100;
        }
        var status = percentage.toFixed(2) + "% (" + utilities.formatBytes(this.rangeEnd) + " / " + utilities.formatBytes(this.attachment.size) + ")";
        this.uiTemplate.find(".status").html(status);
    },
    __getAttachmentExtension: function() {
        var candidateExtension = this.attachment.name.match(/\.(\w+)$/);
        var extension = (candidateExtension && (candidateExtension.length > 1)) ? candidateExtension[1] : "";
        return extension;
    },
    __retryToUpload: function() {
        if (this.numberOfRetries > 0) {
            var self = this;
            setTimeout(function() {
                self.numberOfRetries -= 1;
                self.__upload();
            }, 5000);
        } else {
            //number of retries is exhausted, then what?
            this.pause();
            this.uiTemplate.removeAttr("data-uploading");
        }
    },
    __upload: function() {
        var self = this;
        var chunk;

        //this is what happens when the partial attachment is received by the server
        self.uploadRequest.onload = function(event) {
            if ((event.target.status === 200) && (event.target.response.success)) {
                var response = event.target.response.data;

                //update the attachment id for the UI
                if (!self.attachmentId && response.attachmentId) {
                    self.attachmentId = response.attachmentId;
                    self.uiTemplate.attr("data-attachment-id", response.attachmentId);
                }

                //update UI: update upload progress
                self.__updateUploadProgress();

                if (self.rangeEnd === self.attachment.size) {
                    self.__onUploadComplete();
                    return;
                }

                self.rangeStart = self.rangeEnd;
                self.rangeEnd += self.PARTIAL_ATTACHMENT_SIZE;
                if (!self.isPaused) {
                    self.__upload();
                }
            } else {
                self.__retryToUpload();
            }
        };

        self.uploadRequest.onerror = function(event) {
            self.__retryToUpload();
        };
        self.uploadRequest.ontimeout = function(event) {
            self.__retryToUpload();
        };
        self.uploadRequest.onabort = function(event) {};

        //prepare the next partial attachment to upload
        //prevent index out of bound
        if (self.rangeEnd > self.attachment.size) {
            self.rangeEnd = self.attachment.size;
        }

        self.part = Math.max(Math.ceil(self.rangeEnd / self.PARTIAL_ATTACHMENT_SIZE), 1);
        var partialAttachment = self.attachment[self.sliceMethod](self.rangeStart, self.rangeEnd);

        //TODO: use a web worker to read the file in chunks?
        //update the checksum with the current chunk of data
        var fileReader =  new FileReader();
        fileReader.onload = function(event) {
            //update checksums
            var partialAttachmentAsArrayBuffer = event.target.result;

            var rusha = new Rusha();
            self.partialChecksum = rusha.digestFromArrayBuffer(partialAttachmentAsArrayBuffer);

            //progressively update the checksum
            self.hash.append(partialAttachmentAsArrayBuffer);
            if (self.rangeEnd === self.attachment.size) {
                self.checksum = self.hash.end();
            }

            setTimeout(function() {
                // self.uploadRequest.open("PUT", "/", true);
                self.uploadRequest.open("POST", "/submit/attachment", true);

                //we ought to upload the partial attachment as a binary file
                self.uploadRequest.overrideMimeType("application/octet-stream");

                //include information about this partial attachment to the HTTP header
                self.uploadRequest.setRequestHeader("Content-Attachment-Name", self.attachment.name);
                self.uploadRequest.setRequestHeader("Content-Attachment-Extension", self.__getAttachmentExtension());
                self.uploadRequest.setRequestHeader("Content-Attachment-Type", self.attachment.type);
                self.uploadRequest.setRequestHeader("Content-Attachment-Size", self.attachment.size.toString());
                self.uploadRequest.setRequestHeader("Content-Attachment-Checksum", self.checksum);
                self.uploadRequest.setRequestHeader("Content-Attachment-Id", self.attachmentId);
                self.uploadRequest.setRequestHeader("Content-Attachment-Part", self.part.toString());
                self.uploadRequest.setRequestHeader("Content-Attachment-Partial-Checksum", self.partialChecksum);
                self.uploadRequest.setRequestHeader("Content-Attachment-Total-Parts", self.totalParts.toString());

                if (self.entityId > 0) {
                    self.uploadRequest.setRequestHeader("Content-Entity-Id", self.entityId.toString());
                }

                //send the partial attachment to the server
                self.uploadRequest.send(partialAttachment);
            }, 0);
        };

        if (self.rangeEnd <= self.attachment.size) {
            fileReader.readAsArrayBuffer(partialAttachment);
        }
    },
    __onUploadComplete: function() {
        this.uiTemplate.removeAttr("data-uploading");

        //update UI: hide the pause and resume upload buttons
        this.uiTemplate.find(".pause-button, .resume-button").addClass("hidden");

        //enable the button again
        if (this.__allUploadsFinished()) {
            this.__enableUserControl();
        }
    },
    __startIfAttachmentDoesNotExistsOnServer: function() {
        var self = this;

        //update UI
        var status = "preparing to upload... please wait";
        self.uiTemplate.find(".status").html(status);

        var reader = new FileReader();
        reader.onload = function(event) {
            var rusha = new Rusha();
            self.checksum = rusha.digestFromArrayBuffer(event.target.result);

            var attachment = {
                "checksum": self.checksum
            };

            $.ajax({
                type: "GET",
                url: "/get/attachment",
                data: utilities.formatJSON(attachment),
                success: function(response) {
                    //update UI - enable remove button
                    self.uiTemplate.find(".cancel-remove-button").removeClass("hidden");

                    if (response.data.exists) {
                        var attachment = response.data.attachment;

                        //update attachment id
                        self.attachmentId = attachment.id;
                        self.uiTemplate.attr("data-attachment-id", attachment.id);

                        //TODO: attachment name can be custom to entity and organization
                        //TODO: checksum isn't unique, just fast to hash; maybe use sha256 for unique fingerprint
                        self.attachment.name = attachment.name;
                        self.attachment.size = attachment.size;
                        self.rangeEnd = attachment.size;

                        //enable submit button
                        self.uiTemplate.removeAttr("data-uploading");
                        if (self.__allUploadsFinished()) {
                            self.__enableUserControl();
                        }

                        //update UI
                        self.__updateUploadProgress();
                    } else {
                        self.__upload();
                    }
                },
                dataType: "json"
            });
        };

        reader.readAsArrayBuffer(self.attachment);
    },
    start: function() {
        if (this.__allUploadsFinished()) {
            this.__disableUserControl();
        }
        this.uiTemplate.attr("data-uploading", "");
        this.__startIfAttachmentDoesNotExistsOnServer();
    },
    pause: function() {
        this.isPaused = true;
    },
    resume: function() {
        this.isPaused = false;
        this.__upload();
    },
    cancel: function() {
        this.isPaused = true;
        this.uiTemplate.removeAttr("data-uploading");

        //enable the button again
        if (this.__allUploadsFinished()) {
            this.__enableUserControl();
        }

        if (this.attachmentId > 0) {
            var data = {
                attachmentId: this.attachmentId
            };
            if (this.entityId > 0) {
                data.entityId = this.entityId;
            }

            $.ajax({
                type: "POST",
                url: "/remove/attachment",
                data: utilities.formatJSON(data),
                success: function(response) {
                    if (response.success) {
                        //then what?
                    }
                },
                dataType: "json"
            });
        }
    },
    getAttachmentExtension: function() {
        return this.__getAttachmentExtension();
    },
    setUITemplate: function(attachmentTemplate) {
        this.uiTemplate = attachmentTemplate;
    },
    setUserControl: function(userControl) {
        this.userControl = userControl;
    },
    __disableUserControl: function() {
        //disable the buttons
        this.userControl.find("button").attr("disabled", "");

        //disble the anchor tag for the back button
        //see placeholder hyperlink: http://stackoverflow.com/questions/5292343/is-an-anchor-tag-without-the-href-attribute-safe
        var route = this.userControl.find(".back-button a").attr("href");
        this.userControl.find(".back-button a").attr("data-route", route).removeAttr("href");
    },
    __enableUserControl: function() {
        //enable the buttons
        this.userControl.find("button").removeAttr("disabled");

        //enable the anchor tag for the back button
        var route = this.userControl.find(".back-button a").attr("data-route");
        this.userControl.find(".back-button a").attr("href", route).removeAttr("data-route");
    }
};

//drag and drop photo upload
// $("#edit-profile-form div.section-body[data-dropzone]").on("drag", function(event) {
//     event.preventDefault();
//     console.log(event.type);
//     // $(this).addClass("dropzone");

// }).on("dragstart dragenter", function(event) {
//     console.log(event.type);
//     $(this).addClass("dropzone");

// }).on("dragend", function(event) {
//     console.log(event.type);
//     $(this).removeClass("dropzone");

// }).on("drop", function(event) {
//     event.preventDefault();
//     console.log(event.type);
//     if (event.dataTransfer.files.length > 0) {
//         prepareImageForUpload(event.dataTransfer.files[0]);
//     }
// });
var ThrottleEventDetector = (function() {
    var isScrolling = false;
    var viewportResized = false;
    var searchKeywordChanged = false;

    return {
        isScrolling: isScrolling,
        viewportResized: viewportResized,
        searchKeywordChanged: searchKeywordChanged
    };
});
var ScrollbarHandler = (function() {
    var viewportHeight = 0;
    var scrollbarThreshold = 0.80;

    //detecting if scrolled to the bottom, see: https://developer.mozilla.org/en-US/docs/Web/API/Element/scrollHeight
    //determining viewport height: http://stackoverflow.com/questions/1248081/get-the-browser-viewport-dimensions-with-javascript
    var isEndOfPage = function() {
        var contentHeight = document.documentElement.scrollHeight || document.body.scrollHeight || 0;
        contentHeight = Math.floor(scrollbarThreshold * contentHeight);

        var y = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;

        var scrollDelta = (viewportHeight !== 0) ? (contentHeight - y) : 0;
        return (scrollDelta <= viewportHeight);
    };

    var updateMaxScrollTop = function() {
        viewportHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    };

    return {
        isEndOfPage: isEndOfPage,
        updateMaxScrollTop: updateMaxScrollTop
    };
});
var BrowserHistoryHandler = (function() {
    //NOTE: solution is html5 pushState API - https://developer.mozilla.org/en-US/docs/Web/API/History_API
    var pushState = function(route, state) {
        //defaults
        route = route || "#";
        state = state || {};

        if (window.history) {
            window.history.pushState(state, "", route);
        } else {
            //update the url with the a tag's href, but don't trigger page redirect
            //see: http://stackoverflow.com/questions/846954/change-url-and-redirect-using-jquery
            window.location.replace(route);
        }
    };

    var updateState = function(route, state) {
        //defaults
        route = route || "#";
        state = state || {};

        if (window.history) {
            window.history.replaceState(state, "", route);
        }
    };

    return {
        pushState: pushState,
        updateState: updateState
    };
});

var ExploreRouteHandler = (function() {
    var browserHistoryHandler = new BrowserHistoryHandler();

    function createBrowseTabState(selectedTab) {
        return {
            selectedBrowseTab: selectedTab
        };
    }

    var popStateHandler = function(event) {
        var selectedTab = window.history.state.selectedBrowseTab;

        //show appropriate contents and update browse tabs
        $(".browse-tabs a").removeClass("selected")
                            .filter("[data-container = " + selectedTab + "]")
                            .addClass("selected");

        $(".items-container[data-container]").addClass("hidden")
                                            .filter("[data-container = " + selectedTab + "]")
                                            .removeClass("hidden");
    };

    var pushState = function(route, selectedTab) {
        var state = createBrowseTabState(selectedTab);
        browserHistoryHandler.pushState(route, state);
    };

    var updateState = function(route, selectedTab) {
        var state = createBrowseTabState(selectedTab);
        browserHistoryHandler.updateState(route, state);
    };

    return {
        pushState: pushState,
        updateState: updateState,
        popStateHandler: popStateHandler
    };
});
var SortHandler = (function() {
    var sortingMethods = new SortingMethods();

    //merge sort implementation: http://stackoverflow.com/questions/1427608/fast-stable-sorting-algorithm-implementation-in-javascript
    var mergeSort = function(array, comparator) {
        if (!comparator) {
            comparator = function(a, b) {
                if (a < b) {
                    return -1;
                } else if (a > b) {
                    return 1;
                } else {
                    return 0;
                }
            };
        }

        return __mergeSort(array, 0, array.length, comparator);
    };

    var __mergeSort = function(array, startIndex, endIndex, comparator) {
        var arrayLength = endIndex - startIndex;
        if (arrayLength < 2) {
            return;
        }

        var middleIndex = startIndex + Math.floor(arrayLength / 2);
        __mergeSort(array, startIndex, middleIndex, comparator);
        __mergeSort(array, middleIndex, endIndex, comparator);
        __mergeArrays(array, startIndex, middleIndex, endIndex, comparator);

        return array;
    };

    var __mergeArrays = function(array, startIndex, middleIndex, endIndex, comparator) {
        for (; startIndex < middleIndex; ++startIndex) {
            if (comparator(array[startIndex], array[middleIndex]) > 0) {
                var temporary = array[startIndex];
                array[startIndex] = array[middleIndex];
                __insert(array, middleIndex, endIndex, temporary, comparator);
            }
        }
    };

    Array.prototype.swap = function(a, b) {
        var temporary = this[a];
        this[a] = this[b];
        this[b] = temporary;
    };

    var __insert = function(array, startIndex, endIndex, temporary, comparator) {
        while ((startIndex + 1 < endIndex) && (comparator(array[startIndex + 1], temporary) < 0)) {
            array.swap(startIndex, startIndex + 1);
            ++startIndex;
        }
        array[startIndex] = temporary;
    };

    return {
        mergeSort: mergeSort,
        sortCards: function(cards, sortOption) {
            return mergeSort(cards, sortingMethods.sortBy[sortOption]);
        },
        sortLists: function(sortedCards) {
            return !sortedCards ? [] : sortedCards.map(sortingMethods.sortListByCard);
        },
        sortPeople: function(people, sortOption) {
            return mergeSort(people, sortingMethods.sortBy[sortOption]);
        },
        resort: function() {
            $(".sort-by-options select[name = 'sort-by-options']").not(".hidden").trigger("change");
        }
    };
});

var SortingMethods = (function() {
    var __sortByEntityTitleAlphabetical = function(a, b) {
        var titleA = $(a).find(".name").text().toLowerCase();
        var titleB = $(b).find(".name").text().toLowerCase();
        return (titleA < titleB) ? -1 : (titleA > titleB);
    };

    var __sortByEntityCategory = function(a, b) {
        var categoryA = $(a).find("h4:first").text();
        var categoryB = $(b).find("h4:first").text();
        if (categoryA == categoryB) {
            return __sortByEntityTitleAlphabetical(a, b);
        } else if (categoryA == "selected") {
            return -1;
        } else if (categoryB == "selected") {
            return 1;
        } else if ((categoryA == "selected") && (categoryB == "posted")) {
            return -1;
        } else if ((categoryA == "posted") && (categoryB == "selected")) {
            return 1;
        } else if (categoryA == "presented") {
            return 1;
        } else if (categoryB == "presented") {
            return -1;
        } else if (categoryA < categoryB) {
            return -1;
        } else if (categoryA > categoryB) {
            return 1;
        }
    };

    var __sortByMostLikedEntity = function(a, b) {
        var likesA = +$(a).find(".reaction-buttons [data-counter-type = 'like']").text();
        var likesB = +$(b).find(".reaction-buttons [data-counter-type = 'like']").text();
        return (likesB - likesA);
    };

    var __sortByMostVotedEntity = function(a, b) {
        var votesA = +$(a).find(".reaction-buttons [data-counter-type = 'vote']").text();
        var votesB = +$(b).find(".reaction-buttons [data-counter-type = 'vote']").text();
        return (votesB - votesA);
    };

    var __sortByMostComments = function(a, b) {
        var commentsA = +$(a).find(".reaction-buttons [data-counter-type = 'comment']").text();
        var commentsB = +$(b).find(".reaction-buttons [data-counter-type = 'comment']").text();
        return (commentsB - commentsA);
    };

    var __sortByNewestEntity = function(a, b) {
        var idA = +a.pathname.match(/\/(\d+)/).pop();
        var idB = +b.pathname.match(/\/(\d+)/).pop();
        return (idB - idA);
    };

    var __sortByPersonNameAlphabetical = function(a, b) {
        var nameA = $(a).find("h6.person-name").text().toLowerCase();
        var nameB = $(b).find("h6.person-name").text().toLowerCase();
        return (nameA < nameB) ? -1 : (nameA > nameB);
    };

    var __sortByNewestPerson = function(a, b) {
        var idA = +a.pathname.match(/\/(\d+)/).pop();
        var idB = +b.pathname.match(/\/(\d+)/).pop();
        return (idB - idA);
    };

    var __sortListByCard = function(card) {
        var entityId = card.getAttribute("data-entity-id");
        return $("#browse-view .items-container").not(".hidden").find(".list-view[data-entity-id = " + entityId + "]").get(0);
    };

    return {
        sortBy: {
            "entity-title-in-alphabetical": __sortByEntityTitleAlphabetical,
            "entity-title-in-reverse": function(a, b) {
                return __sortByEntityTitleAlphabetical(b, a);
            },
            "entity-category": __sortByEntityCategory,
            "most-liked-entity": __sortByMostLikedEntity,
            "least-liked-entity": function(a, b) {
                return __sortByMostLikedEntity(b, a);
            },
            "most-voted-entity": __sortByMostVotedEntity,
            "least-voted-entity": function(a, b) {
                return __sortByMostVotedEntity(b, a);
            },
            "most-commented-entity": __sortByMostComments,
            "least-commented-entity": function(a, b) {
                return __sortByMostComments(b, a);
            },
            "newest-entity": __sortByNewestEntity,
            "oldest-entity": function(a, b) {
                return __sortByNewestEntity(b, a);
            },
            "person-name-in-alphabetical": __sortByPersonNameAlphabetical,
            "person-name-in-reverse": function(a, b) {
                return __sortByPersonNameAlphabetical(b, a);
            },
            "newest-person": __sortByNewestPerson,
            "oldest-person": function(a, b) {
                return __sortByNewestPerson(b, a);
            }
        },
        sortListByCard: __sortListByCard
    };
});
var CommentHandler = (function() {
    var commentTemplate = _.template($("#comment-template").html());

    var postCommentHandler = function(event) {
        var entityId = +window.location.pathname.match(/\d+/).shift();

        var field = $("#comment-control textarea[name = 'comment']");
        var message = field.val().trim();

        var defaultLatestCommentId = 0;
        var latestComment = $("#comments li:first");
        var latestCommentId = (latestComment.length > 0) ? +latestComment.attr("data-comment-id") : defaultLatestCommentId;

        if (!_.isEmpty(message) && (entityId > 0)) {
            var comment = {
                "entityId": entityId,
                "message": message,
                "latestCommentId": latestCommentId
            };

            $.ajax({
                type: "POST",
                url: "/submit/comment",
                data: utilities.formatJSON(comment),
                success: function(response) {
                    if (response.success) {
                        var newComments = "";
                        response.data.comments.forEach(function(comment) {
                            newComments += commentTemplate({comment: comment});
                        });
                        $("#comments ul").prepend(newComments);

                        //clear comment box
                        field.val("");

                        var commentControlContent = "Comments (" + response.data.numberOfComments + ")";
                        $("#number-of-comments").text(commentControlContent);

                        var commentCounterContent = response.data.numberOfComments + " " + ((response.data.numberOfComments != 1) ? "comments" : "comment");
                        $(".comment-icon ~ p[data-counter-type = 'comment']").text(commentCounterContent);
                    }
                },
                dataType: "json"
            });
        }
    };

    var loadOldCommentsHandler = function(event) {
        var entityId = +window.location.pathname.match(/\d+/).shift();

        var defaultOldestCommentId = 0;
        var oldestComment = $("#comments li:last");
        var referenceId = (oldestComment.length > 0) ? +oldestComment.attr("data-comment-id") : defaultOldestCommentId;

        var requestData = {
            "entityId": entityId,
            "referenceId": referenceId,
            "lookupDirection": "oldest"
        };

        $.ajax({
            type: "GET",
            url: "/get/comments",
            data: utilities.formatJSON(requestData),
            success: function(response) {
                var oldComments = "";
                response.data.comments.forEach(function(comment) {
                    oldComments += commentTemplate({comment: comment});
                });
                $("#comments ul").append(oldComments);

                var commentControlContent = "Comments (" + response.data.numberOfComments + ")";
                $("#number-of-comments").text(commentControlContent);

                var commentCounterContent = response.data.numberOfComments + " " + ((response.data.numberOfComments != 1) ? "comments" : "comment");
                $(".comment-icon ~ p[data-counter-type = 'comment']").text(commentCounterContent);
            },
            dataType: "json"
        });
    };

    var loadNewCommentsHandler = function(event) {
        var entityId = +window.location.pathname.match(/\d+/).shift();

        var defaultNewestCommentId = 0;
        var newestComment = $("#comments li:first");
        var referenceId = (newestComment.length > 0) ? +newestComment.attr("data-comment-id") : defaultNewestCommentId;

        var requestData = {
            "entityId": entityId,
            "referenceId": referenceId,
            "lookupDirection": "newest"
        };

        $.ajax({
            type: "GET",
            url: "/get/comments",
            data: utilities.formatJSON(requestData),
            success: function(response) {
                var newComments = "";
                response.data.comments.forEach(function(comment) {
                    newComments += commentTemplate({comment: comment});
                });
                $("#comments ul").prepend(newComments);

                var commentControlContent = "Comments (" + response.data.numberOfComments + ")";
                $("#number-of-comments").text(commentControlContent);

                var commentCounterContent = response.data.numberOfComments + " " + ((response.data.numberOfComments != 1) ? "comments" : "comment");
                $(".comment-icon ~ p[data-counter-type = 'comment']").text(commentCounterContent);
            },
            dataType: "json"
        });
    };

    return {
        postComment: postCommentHandler,
        loadOldComments: loadOldCommentsHandler,
        loadNewComments: loadNewCommentsHandler
    };
});
var LikeHandler = (function() {
    var ENTITY_TABS = ["ideas", "pitches", "projects"];

    var updateLikeButton = function(index, text) {
        return (text === "Like") ? "Unlike" : "Like";
    };

    var updateVoteButton = function(index, text) {
        return (text === "Vote") ? "Unvote" : "Vote";
    };

    return {
        likeCard: function(event) {
            event.preventDefault();

            var entityView = $(this).parents("[data-entity-id]");
            var otherEntityView = entityView.siblings("[data-entity-id = " + entityView.attr("data-entity-id") + "]");
            var entityId = +entityView.attr("data-entity-id");
            var entity = {
                "id": entityId
            };

            $.ajax({
                type: "POST",
                url: "/like/entity",
                data: utilities.formatJSON(entity),
                success: function(response) {
                    if (response.success) {
                        entityView.add(otherEntityView)
                                  .find(".like-icon")
                                  .toggleClass("liked")
                                  .siblings("[data-counter-type = 'like']")
                                  .text(response.data.numberOfLikesForEntity);

                        entityView.find(".like-button button")
                                  .text(updateLikeButton);
                    }
                },
                dataType: "json"
            });

            return false;
        },
        likeEntity: function(event) {
            var entityId = +window.location.pathname.match(/\/(\d+)/).pop();
            if (entityId > 0) {
                var entity = {
                    "id": entityId
                };

                $.ajax({
                    type: "POST",
                    url: "/like/entity",
                    data: utilities.formatJSON(entity),
                    success: function(response) {
                        if (response.success) {
                            var numberOfLikes = response.data.numberOfLikesForEntity;
                            $(".reaction-buttons .like-icon").toggleClass("liked")
                                                             .siblings("[data-counter-type = 'like']")
                                                             .text(numberOfLikes + " " + ((numberOfLikes != 1) ? "likes" : "like"));
                        }
                    },
                    dataType: "json"
                });
            } else {
                //entity id not found, then what?
            }
            return false;
        }
    };
});

var VoteHandler = (function() {
    var updateVoteButton = function(index, text) {
        return (text === "Vote") ? "Unvote" : "Vote";
    };

    return {
        voteCard: function(event) {
            event.preventDefault();

            var entityView = $(this).parents("[data-entity-id]");
            var otherEntityView = entityView.siblings("[data-entity-id = " + entityView.attr("data-entity-id") + "]");
            var entityId = +entityView.attr("data-entity-id");
            var entity = {
                "id": entityId
            };

            $.ajax({
                type: "POST",
                url: "/vote/entity",
                data: utilities.formatJSON(entity),
                success: function(response) {
                    if (response.success) {
                        entityView.add(otherEntityView)
                                  .find(".reaction-buttons .vote-icon")
                                  .toggleClass("voted")
                                  .siblings("[data-counter-type = 'vote']")
                                  .text(response.data.numberOfVotesForEntity);

                        entityView.find(".vote-button button")
                                  .text(updateVoteButton);

                        //update the vote summary bar
                        var userVoteCapacity = response.data.voteCapacity[response.data.entityType];
                        var voteSummaryBars = $("#vote-summary [data-entity-type = '" + response.data.entityType + "']");
                        voteSummaryBars.find("span").text(userVoteCapacity.votesUsed);
                        if (userVoteCapacity.votesRemaining === 0) {
                            voteSummaryBars.addClass("limit-reached");
                        } else {
                            voteSummaryBars.removeClass("limit-reached");
                        }
                    } else {
                        //you're out of votes, show some warning?
                        // $("#voting-status-bar").addClass("limit-reached");
                    }
                },
                dataType: "json"
            });

            return false;
        },
        voteEntity: function(event) {
            var entityId = +window.location.pathname.match(/\/(\d+)/).pop();
            if (entityId > 0) {
                var entity = {
                    "id": entityId
                };

                $.ajax({
                    type: "POST",
                    url: "/vote/entity",
                    data: utilities.formatJSON(entity),
                    success: function(response) {
                        if (response.success) {
                            var numberOfVotes = response.data.numberOfVotesForEntity;
                            $(".reaction-buttons .vote-icon").toggleClass("voted")
                                                             .siblings("[data-counter-type = 'vote']")
                                                             .text(numberOfVotes + " " + ((numberOfVotes != 1) ? "votes" : "vote"));

                            //update the vote summary bar
                            var userVoteCapacity = response.data.voteCapacity[response.data.entityType];
                            var voteSummaryBars = $("#vote-summary [data-entity-type = '" + response.data.entityType + "']");
                            voteSummaryBars.find("span").text(userVoteCapacity.votesUsed);
                            if (userVoteCapacity.votesRemaining === 0) {
                                voteSummaryBars.addClass("limit-reached");
                            } else {
                                voteSummaryBars.removeClass("limit-reached");
                            }
                        } else {
                            //you're out of votes, show the vote counter and warning
                            // $("#voting-status-bar").addClass("limit-reached");
                        }
                    },
                    dataType: "json"
                });
            } else {
                //entity id not found, then what?
            }
            return false;
        }
    };
});
var SearchHandler = (function() {
    var MINIMUM_SEARCH_LENGTH = 3;
    var searchSuggestionTemplate = _.template($("#search-people-result-template").html());

    var __searchPeopleSuggestionHandler = function(event) {
        var selectResultHandler = event.data && event.data.selectResultHandler;
        var searchSuggestionsContainer = $(event.target).parents("[data-element-type = 'search-bar']").find(".search-suggestions ul");

        var keywords = $(event.target).val().trim();
        if (keywords.length < MINIMUM_SEARCH_LENGTH) {
            searchSuggestionsContainer.empty().addClass("hidden");
            return;
        }

        var searchParameters = {
            "keywords": keywords,
            "excludePeople": []
        };

        if (event.data && event.data.filterPeople) {
            searchParameters.excludePeople = event.data.filterPeople();
        }

        $.ajax({
            type: "POST",
            url: "/submit/search/people",
            data: utilities.formatJSON(searchParameters),
            success: function(response) {
                if (response.data.length > 0) {
                    var searchResults = new Array(response.data.length);
                    response.data.forEach(function(person, index) {
                        var searchResult = $(searchSuggestionTemplate({person: person}));

                        if (selectResultHandler) {
                            searchResult.on("click", null, {person: person}, selectResultHandler);
                        }

                        searchResult.on("click", function() {
                            searchSuggestionsContainer.empty();
                        });

                        searchResults[index] = searchResult;
                    });
                    searchSuggestionsContainer.html(searchResults).removeClass("hidden");
                }
            },
            dataType: "json"
        });
    };

    //TODO: to be implemented
    var __searchEntitySuggestionHandler = function(event) {};

    return {
        searchEntitySuggestionHandler: __searchEntitySuggestionHandler,
        searchPeopleSuggestionHandler: __searchPeopleSuggestionHandler
    };
});
var TabHandler = (function() {
    var SCROLL_ANIMATION_PERIOD = 500;
    var BASE_HEIGHT = $(".navigation-bar").height() + $("#help-header").height();
    var PADDING = 20;
    var OFFSET = BASE_HEIGHT + PADDING;

    return {
        goToByScroll: function(selector) {
            var yCoordinate = $(selector).offset().top - OFFSET;
            $('html, body').animate({
                scrollTop: yCoordinate
            }, SCROLL_ANIMATION_PERIOD);
        }
    };
});
gravit8.Entity = (function() {
    var instance;

    function loadTemplates() {
        return {
            assignableRoleFormTemplate: _.template($("#add-role-form-template").html()),
            roleAssigneeTemplate: _.template($("#role-assignee-template").html()),
            teamMemberFormTemplate: _.template($("#add-team-member-form-template").html()),
            profileImageTemplate: _.template($("#profile-image-template").html()),
            descriptionFormTemplate: _.template($("#add-description-form-template").html()),
            backgroundFormTemplate: _.template($("#add-background-form-template").html()),
            stakeholderFormTemplate: _.template($("#add-stakeholder-form-template").html()),
            existingSolutionFormTemplate: _.template($("#add-existing-solution-form-template").html()),
            marketAnalysisFormTemplate: _.template($("#add-market-analysis-form-template").html()),
            otherInformationFormTemplate: _.template($("#add-other-information-form-template").html()),
            attachmentFormTemplate: _.template($("#add-attachment-form-template").html()),
            uploadedAttachmentFormTemplate: _.template($("#uploaded-attachment-template").html())
        };
    }

    function Entity() {
        //load other helpers
        var searchHandler = new SearchHandler();
        // var attachmentHandler = gravit8.AttachmentHandler.getInstance();

        var templates = loadTemplates();
        var dataModels = {
            newEntityData: function(title, oneLiner, setPrivate, categories) {
                var defaultCategoryId = +$("select[name = 'categories'] option:enabled").filter(":first").val();
                var defaultCategory = [defaultCategoryId];
                if (!defaultCategoryId) {
                    defaultCategory = [];
                }

                return {
                    setPrivate: setPrivate || false,
                    title: title || "No title provided",
                    oneLiner: oneLiner || "No one liner provided",
                    categories: (Array.isArray(categories) && (categories.length > 0)) ? categories : defaultCategory
                };
            },
            updateEntityData: function(editedContents, removedContents, addedContents) {
                return {
                    edited: editedContents || {},
                    removed: removedContents || {},
                    added: addedContents || {}
                };
            },
            initialEntityData: function(contentForm) {
                var entityContents = {};
                contentForm.find("[name]").each(function(index, entityContent) {
                    var field = $(entityContent);
                    var fieldName = entityContent.getAttribute("name");

                    switch (fieldName) {
                        case "role":
                            var role = field.val().trim();
                            var roleAssignees = field.siblings("ul").children("[name = 'role-assignee']").map(function(index, roleAssignee) {
                                return +roleAssignee.getAttribute("data-assignee-id");
                            }).get();
                            var roleData = dataModels.roleData(role, roleAssignees);

                            var contentId = +entityContent.getAttribute("data-entity-content-id");
                            if ("roles" in entityContents) {
                                entityContents["roles"][contentId] = roleData;
                            } else {
                                var roles = {};
                                roles[contentId] = roleData;
                                entityContents["roles"] = roles;
                            }
                            break;

                        case "role-assignee":
                            break;

                        default:
                            var contentId = +entityContent.getAttribute("data-entity-content-id");
                            var content = field.val().trim();
                            entityContents[fieldName] = dataModels.entityContentData(contentId, content);
                    }
                });

                return entityContents;
            },
            entityContentData: function(contentId, content) {
                return {
                    id: contentId || null,
                    value: content || null
                };
            },
            roleData: function(role, assignees) {
                return {
                    role: role,
                    assignees: Array.isArray(assignees) ? assignees : []
                };
            },
            teamMemberData: function(memberId) {
                return dataModels.entityContentData(memberId);
            }
        };

        var __helpers = {
            isRoleDataDifferent: function(roleDataA, roleDataB) {
                var isRoleDifferent = (roleDataA.role !== roleDataB.role);
                var isRoleAssigneesDifferent = !_.isEqual(_.sortBy(roleDataA.assignees), _.sortBy(roleDataB.assignees));
                return (isRoleDifferent || isRoleAssigneesDifferent);
            }
        };

        //UI interactions
        var __enableContentCard = function(contentCard) {
            contentCard.removeAttribute("data-module-disabled");
            $(contentCard).removeClass("disabled-module");
        };

        var __disableContentCard = function(contentCard) {
            contentCard.setAttribute("data-module-disabled", "");
            $(contentCard).addClass("disabled-module");
        };

        //change to __entityContentAdded
        var __newContentDetected = function(event) {
            if (!event.target.hasAttribute("data-content-added")) {
                event.target.setAttribute("data-content-added", "");
            }
        };

        var __entityContentEdited = function(event) {
            if (!event.target.hasAttribute("data-content-edited")) {
                event.target.setAttribute("data-content-edited", "");
            }
        };

        var __entityContentRemoved = function(event) {
            var form = $(event.target).closest(".entity-content").addClass("hidden");
            var field = form.find("[data-entity-content-id]").get(0);
            if (!field && form.find("[name = 'attachment']").exists()) {
                field = form.find("[name = 'attachment']").get(0);
            }
            if (!field.hasAttribute("data-content-removed")) {
                field.setAttribute("data-content-removed", "");
            }

            //we do not care if the entity content is edited when we are removing it
            if (field.hasAttribute("data-content-edited")) {
                field.removeAttribute("data-content-edited");
            }
        };

        var __hasContentChanged = function(content, initialContent) {
            return (content !== initialContent);
        };

        var __removeContent = function(event) {
            $(event.target).closest(".entity-content").remove();
        };

        var textareaHandler = function() {
            $("textarea").each(function() {
                this.setAttribute("style", "height:" + (this.scrollHeight) + "px; overflow-y: hidden;");
            }).on("input keypaste", function() {
                this.style.height = "";
                this.style.height = (this.scrollHeight) + "px";
            });
        };
        
        var __oneTimeUseTextBodyContent = function(cardId, formTemplate) {
            var card = document.getElementById(cardId);
            if (!card.hasAttribute("data-module-disabled")) {
                __disableContentCard(card);

                //render the form to the DOM
                var form = $(formTemplate());
                form.find("textarea").on("keydown paste", __newContentDetected);
                form.find(".cancel-remove-button button").on("click", null, card, function(event) {
                    __enableContentCard(event.data);
                }).on("click", __removeContent);

                textareaHandler();

                $(".create-buttons").before(form);
            }
        };

        var __newUploadAttachmentForm = function(attachment) {
            var uploadedAttachmentForm = $(templates.uploadedAttachmentFormTemplate({attachment: attachment}));
            uploadedAttachmentForm.find(".upload-button button").on("click", __newContentDetected);
            uploadedAttachmentForm.find(".cancel-remove-button button").on("click", __entityContentRemoved);
            $("#attachments").append(uploadedAttachmentForm);
        };

        var __renderAttachmentContent = function(event) {
            var card = document.getElementById("add-attachment-button");
            if (!card.hasAttribute("data-module-disabled")) {
                __disableContentCard(card);

                //render the attachment form to the DOM
                var attachmentForm = $(templates.attachmentFormTemplate());
                attachmentForm.find(".upload-button input[type = 'file']").on("change", null, {newUploadAttachmentForm: __newUploadAttachmentForm}, attachmentHandler.uploadAttachments);
                attachmentForm.find(".upload-button button").on("click", function(event) {
                    $(event.target).siblings("input[type = 'file']").trigger("click");
                });
                attachmentForm.find(".cancel-remove-button button").on("click", null, card, function(event) {
                    __enableContentCard(event.data);
                }).on("click", __removeContent);

                $(".create-buttons").before(attachmentForm);
            }
        };

        var requiredContentsAreValid;
        var __validateRequiredContent = function(index, requiredField) {
            switch (requiredField.tagName.toLowerCase()) {
                case "textarea":
                    var requiredContent = $(requiredField);
                    var requiredFormMessage = requiredContent.next(".invalid-message");
                    if (!requiredContent.val().trim()) {
                        requiredContent.focus();
                        requiredFormMessage.removeClass("hidden");
                        requiredContentsAreValid = false;
                    } else {
                        requiredFormMessage.addClass("hidden");
                    }
                    break;
            }
        };

        //TODO: how to pass in entityContentData to the each function?
        //from project, pitch, idea
        // var __includeOptionalContent = function(index, optionalContent) {
        //     console.log(entityContentData);
        //     var content = $(optionalContent);
        //     var contentName = optionalContent.getAttribute("name");

        //     switch (contentName) {
        //         case "role":
        //             var role = content.val().trim();
        //             if (role.length > 0) {
        //                 var roleData = createEntity.newRoleData(role);
        //                 roleData.assignees = content.parent().find("[name = 'role-assignee'] .selected").map(function(index, profileImage) {
        //                     return +$(profileImage).parent().attr("data-assignee-id");
        //                 }).get();

        //                 if (!("roles" in entityContentData)) {
        //                     entityContentData["roles"] = [roleData];
        //                 } else {
        //                     entityContentData["roles"].push(roleData);
        //                 }
        //             }
        //             break;

        //         case "description":
        //         case "background":
        //         case "stakeholder":
        //         case "existing-solution":
        //         case "market-analysis":
        //         case "other-information":
        //             var text = content.val().trim();
        //             if (text.length > 0) {
        //                 entityContentData[contentName] = text;
        //             }
        //             break;
        //     }
        // };

        var __filterTeamMembers = function() {
            return teamList;
        };

        //TODO: throttle the searching
        var __addTeamMemberHandler = function(event) {
            var teamMemberForm = $(templates.teamMemberFormTemplate());
            var methods = {
                filterPeople: __filterTeamMembers,
                selectResultHandler: __addPersonToTeam
            };
            teamMemberForm.find("textarea").on("keydown paste", __editTeamMemberHandler)
                                           .on("input paste", null, methods, searchHandler.searchPeopleSuggestionHandler);
            teamMemberForm.find(".cancel-remove-button button").on("click", __removeContent)
                                                               .on("click", __editTeamMemberHandler);

            $("#team-members").append(teamMemberForm);
        };

        var __editTeamMemberHandler = function(event) {
            if (!event.target.hasAttribute("data-content-removed")) {
                event.target.setAttribute("data-content-removed", "");
            }

            // we do not care if the entity content is edited when we are removing it
            if (event.target.hasAttribute("data-content-edited")) {
                event.target.removeAttribute("data-content-edited");
            }

            // update UI
            $(event.target).closest(".entity-content").find("[data-team-member-id]").empty().attr("data-team-member-id", '');

            // update team list
            __updateTeamList();
        };

        var teamList = [];
        var __updateTeamList = function() {
            teamList = $("#team-members [data-element-type = 'profile-image-container'][data-team-member-id != '']").map(function(index, profileImageContainer) {
                return +profileImageContainer.getAttribute("data-team-member-id");
            }).get();

            //trigger another event to update roles
            if ($("#roles").exists()) {
                __updateRoleAssignees();
            }
        };

        var __addPersonToTeam = function(event) {
            //check if person already exists in the team members list (this check is likely redundant since the duplicate check is already done in the database level)
            var person = event.data.person;
            if (!_.contains(teamList, person.id)) {
                //update the UI
                var profileImage = templates.profileImageTemplate({
                    person: person,
                    imageSizeClass: "profile-image-icon"
                });

                var contentEntity = $(event.target).closest(".entity-content");
                var teamMember = contentEntity.find("[data-element-type = 'profile-image-container']").empty().html(profileImage).attr("data-team-member-id", person.id).get(0);

                if (!teamMember.hasAttribute("data-content-added")) {
                    teamMember.setAttribute("data-content-added", "");
                }

                //NOTE: should this be abstracted to the search handler level?
                var personName = person.firstName + " " + person.lastName;
                contentEntity.find("[data-element-type = 'search-bar'] textarea").val(personName);

                //add the person.id to the team members list
                __updateTeamList();

                //include person to all roles
                if ($("#roles").exists()) {
                    __addPersonToRoleAssignees({data: person, profileImage: profileImage});
                }
            }
        };

        var __addAssignableRoleHandler = function(event) {
            var roleForm = $(templates.assignableRoleFormTemplate());
            roleForm.find("textarea").on("keydown paste", __newContentDetected);
            roleForm.find(".cancel-remove-button button").on("click", __removeContent);

            if (event.data && event.data.includeRoleAssignee) {
                var assignees = roleAssignees.map(function(roleAssignee) {
                    return __newRoleAssignee(roleAssignee);
                });
                roleForm.find("[data-element-type = 'role-assignees']").append(assignees);
            }

            $("#roles").append(roleForm);
        };

        var roleAssignees = [];
        var __addPersonToRoleAssignees = function(person) {
            roleAssignees.push(person);

            //update UI
            var assignee = __newRoleAssignee(person);
            $("#roles [data-element-type = 'role-assignees']").append(assignee);
        };

        var __newRoleAssignee = function(person) {
            var roleAssignee = $(templates.roleAssigneeTemplate({assignee: person.data}));
            roleAssignee.append(person.profileImage);
            roleAssignee.on("click", function(event) {
                if (event.target.hasAttribute("data-assignee-id")) {
                    $(event.target).find(".profile-image-icon").toggleClass("selected");
                } else {
                    $(event.target).toggleClass("selected");
                }
            }).on("click", __editRoleAssigneeHandler);
            return roleAssignee;
        };

        var __editRoleAssigneeHandler = function(event) {
            if (!$(event.target).closest(".entity-content").find("textarea").attr("data-content-edited")) {
                $(event.target).closest(".entity-content").find("textarea").attr("data-content-edited", "");
            }
        };

        var __updateRoleAssignees = function() {
            //update roleAssignees by removing removed team members
            roleAssignees = roleAssignees.filter(function(roleAssignee) {
                return _.contains(teamList, roleAssignee.data.id);
            });

            //update UI
            $("#roles [data-element-type = 'role-assignees'] [name = 'role-assignee']").filter(function(index, roleAssignee) {
                var assigneeId = +roleAssignee.getAttribute("data-assignee-id");
                return !_.contains(teamList, assigneeId);
            }).remove();
        };


        //TODO: this should probably be done in the HTML layer since it is related to contents
        var __addContentButtonUIUpdateHandler = function(index, text) {
            return (text == "+ Add Content") ? "✖ Cancel" : "+ Add Content";
        };


        var __packageEditedEntityContents = function(entityContents, initialEntityContents) {
            initialEntityContents = initialEntityContents || {};

            var editedEntityContents = {};
            entityContents.forEach(function(entityContent, index) {
                var field = $(entityContent);
                var fieldName = entityContent.getAttribute("name");

                switch (fieldName) {
                    case "title":
                        var text = field.val().trim();
                        if ((text.length > 0) && __hasContentChanged(text, initialEntityContents[fieldName].value)) {
                            editedEntityContents[fieldName] = dataModels.entityContentData(null, text);
                        }
                        break;

                    case "idea":
                    case "pitch":
                    case "summary":
                    case "one-liner":
                        var text = field.val().trim();
                        if ((text.length > 0) && __hasContentChanged(text, initialEntityContents[fieldName].value)) {
                            editedEntityContents["oneLiner"] = dataModels.entityContentData(null, text);
                        }
                        break;

                    case "categories":
                        if (entityContent.hasAttribute("data-entity-content-id")) {
                            var entityContentId = +entityContent.getAttribute("data-entity-content-id");
                            var categoryId = +field.val();
                            var category = dataModels.entityContentData(entityContentId, categoryId);
                            
                            if (!(fieldName in editedEntityContents)) {
                                editedEntityContents[fieldName] = [category];
                            } else {
                                editedEntityContents[fieldName].push(category);
                            }
                        }
                        break;

                    //TODO: to be completed
                    case "team-members":
                        editedEntityContents["teamMembers"] = [];
                        break;

                    case "role":
                        var role = field.val().trim();
                        var roleAssignees = field.parent().find("[name = 'role-assignee'] .selected").map(function(index, profileImage) {
                            return +$(profileImage).parent().attr("data-assignee-id");
                        }).get();
                        var roleData = dataModels.roleData(role, roleAssignees);

                        var entityContentId = +entityContent.getAttribute("data-entity-content-id");

                        //compare the role data to the initial values
                        if (("roles" in initialEntityContents) && (entityContentId in initialEntityContents["roles"])) {
                            var initialRoleData = initialEntityContents["roles"][entityContentId];

                            if (__helpers.isRoleDataDifferent(roleData, initialRoleData)) {
                                var entityContent = dataModels.entityContentData(entityContentId, roleData);
                                if ("roles" in editedEntityContents) {
                                    editedEntityContents["roles"].push(entityContent);
                                } else {
                                    editedEntityContents["roles"] = [entityContent];
                                }
                            }
                        }
                        break;

                    //TODO: to be completed
                    case "attachment":
                        editedEntityContents["attachment"] = [];
                        break;

                    case "description":
                    case "background":
                    case "stakeholder":
                    case "existing-solution":
                    case "market-analysis":
                    case "other-information":
                        var text = field.val().trim();
                        if (entityContent.hasAttribute("data-entity-content-id") && (text.length > 0) && __hasContentChanged(text, initialEntityContents[fieldName].value)) {
                            var entityContentId = +entityContent.getAttribute("data-entity-content-id");
                            editedEntityContents[fieldName] = dataModels.entityContentData(entityContentId, text);
                        }
                        break;
                }
            });

            return editedEntityContents;
        };

        var __packageRemovedEntityContents = function(entityContents, initialEntityContents) {
            initialEntityContents = initialEntityContents || {};

            var removedEntityContents = {};
            entityContents.forEach(function(entityContent, index) {
                var field = $(entityContent);
                var fieldName = entityContent.getAttribute("name");

                var entityContentId = +entityContent.getAttribute("data-entity-content-id");
                var entityContentData = dataModels.entityContentData(entityContentId);

                switch (fieldName) {
                    case "team-member":
                        if ("teamMembers" in removedEntityContents) {
                            removedEntityContents["teamMembers"].push(entityContentData);
                        } else {
                            removedEntityContents["teamMembers"] = [entityContentData];
                        }
                        break;

                    case "role":
                        if ("roles" in removedEntityContents) {
                            removedEntityContents["roles"].push(entityContentData);
                        } else {
                            removedEntityContents["roles"] = [entityContentData];
                        }
                        break;

                    case "attachment":
                        var attachmentId = +field.closest("[data-attachment-id]").attr("data-attachment-id");
                        entityContentData = dataModels.entityContentData(attachmentId);
                        if ("attachments" in removedEntityContents) {
                            removedEntityContents["attachments"].push(entityContentData);
                        } else {
                            removedEntityContents["attachments"] = [entityContentData];
                        }
                        break;

                    case "description":
                    case "background":
                    case "stakeholder":
                    case "existing-solution":
                    case "market-analysis":
                    case "other-information":
                        removedEntityContents[fieldName] = entityContentData;
                        break;
                }
            });

            return removedEntityContents;
        };

        var __packageAddedEntityContents = function(entityContents, initialEntityContents) {
            initialEntityContents = initialEntityContents || {};

            var addedEntityContents = {};
            entityContents.forEach(function(entityContent, index) {
                var field = $(entityContent);
                var fieldName = entityContent.getAttribute("name");

                switch (fieldName) {
                    case "attachment":
                        var attachmentId = +field.closest("[data-attachment-id]").attr("data-attachment-id");

                        //TODO: grab the changed name from a text box or something
                        var attachmentName = field.text().trim() || null;
                        if (attachmentId > 0) {
                            var entityContentData = dataModels.entityContentData(attachmentId, attachmentName);
                            if ("attachments" in addedEntityContents) {
                                addedEntityContents["attachments"].push(entityContentData);
                            } else {
                                addedEntityContents["attachments"] = [entityContentData];
                            }
                        }
                        break;

                    case "team-member":
                        var teamMemberId = +entityContent.getAttribute("data-team-member-id");
                        if (teamMemberId !== 0) {
                            var entityContentData = dataModels.entityContentData(teamMemberId);
                            if ("teamMembers" in addedEntityContents) {
                                addedEntityContents["teamMembers"].push(entityContentData);
                            } else {
                                addedEntityContents["teamMembers"] = [entityContentData];
                            }
                        }
                        break;

                    case "role":
                        var role = field.val().trim();
                        if (role.length > 0) {
                            var roleAssignees = field.parent().find("[name = 'role-assignee'] .selected").not("hidden").map(function(index, profileImage) {
                                return +$(profileImage).parent().attr("data-assignee-id");
                            }).get();

                            var roleData = dataModels.roleData(role, roleAssignees);
                            var entityContent = dataModels.entityContentData(null, roleData);
                            if ("roles" in addedEntityContents) {
                                addedEntityContents["roles"].push(entityContent);
                            } else {
                                addedEntityContents["roles"] = [entityContent];
                            }
                        }
                        break;

                    case "attachment":
                        break;

                    case "description":
                    case "background":
                    case "stakeholder":
                    case "existing-solution":
                    case "market-analysis":
                    case "other-information":
                        var text = field.val().trim();
                        if (text.length > 0) {
                            addedEntityContents[fieldName] = dataModels.entityContentData(null, text);
                        }
                        break;
                }
            });

            return addedEntityContents;
        };



        //refreshing data to UI functions
        var __refreshCard = function(cardView, entity) {
            cardView.find(".name").text(entity.title);

            var oneLiner = "";
            switch (entity.entityType) {
                case "idea":
                    oneLiner = entity.idea;
                    break;

                case "pitch":
                    oneLiner = entity.pitch;
                    break;

                case "project":
                    oneLiner = entity.summary;
                    break;
            }
            cardView.find(".one-liner").text(oneLiner);

            //update reaction buttons and counters
            var reactionCounters = cardView.find(".reaction-buttons [data-counter-type]");
            reactionCounters.filter("[data-counter-type = 'like']").text(entity.numberOfLikes);
            reactionCounters.filter("[data-counter-type = 'vote']").text(entity.numberOfVotes);

            //NOTE: the like icon only changes when the user clicks on it, so it doesn't make sense to constantly check for and update this
            // var likeIcon = cardView.find(".reaction-buttons .like-icon");
            // if (entity.liked) {
            //     likeIcon.addClass("liked");
            // } else {
            //     likeIcon.removeClass("liked");
            // }

            reactionCounters.filter("[data-counter-type = 'comment']").text(entity.numberOfComments);

            //update profile image
            var profileImages = cardView.find(".project-champion .profile-image-small");
            profileImages.not("[style]").text(entity.creator.initials);
            profileImages.filter("[style]").css("background-image", "url(" + entity.creator.imagePath + ")");

            var creatorFullname = entity.creator.firstName + " " + entity.creator.lastName;
            cardView.find(".project-champion .started-by .entity-creator-name").text(creatorFullname);

            var oldCategory = cardView.get(0).className.match(/([a-z0-9\-]*)-category/);
            if (oldCategory !== null) {
                oldCategory = oldCategory.pop();
                cardView.removeClass(oldCategory + "-category");
            }

            var newCategory = entity.categories[0].name.replace(/\s/g, '-');
            cardView.addClass(newCategory + "-category");

            var newCategoryName = entity.categories[0].name.replace("and", "&");
            cardView.find(".tag").text(newCategoryName);
        };

        var __refreshList = function(listView, entity) {
            listView.find(".name").text(entity.title);

            var oneLiner = "";
            switch (entity.entityType) {
                case "idea":
                    oneLiner = entity.idea;
                    break;

                case "pitch":
                    oneLiner = entity.pitch;
                    break;

                case "project":
                    oneLiner = entity.summary;
                    break;
            }
            listView.find(".one-liner").text(oneLiner);

            //update reaction buttons and counters
            var reactionCounters = listView.find(".reaction-buttons [data-counter-type]");
            reactionCounters.filter("[data-counter-type = 'like']").text(entity.numberOfLikes);
            reactionCounters.filter("[data-counter-type = 'vote']").text(entity.numberOfVotes);

            //NOTE: the like icon only changes when the user clicks on it, so it doesn't make sense to constantly check for and update this
            // var likeIcon = listView.find(".reaction-buttons .like-icon");
            // if (entity.liked) {
            //     likeIcon.addClass("liked");
            // } else {
            //     likeIcon.removeClass("liked");
            // }

            reactionCounters.filter("[data-counter-type = 'comment']").text(entity.numberOfComments);

            //update profile image
            var profileImages = listView.find(".project-champion .profile-image-small");
            profileImages.not("[style]").text(entity.creator.initials);
            profileImages.filter("[style]").css("background-image", "url(" + entity.creator.imagePath + ")");

            var creatorFullname = entity.creator.firstName + " " + entity.creator.lastName;
            listView.find(".project-champion .started-by .entity-creator-name").text(creatorFullname);

            var oldCategory = listView.get(0).className.match(/([a-z0-9\-]*)-category/);
            if (oldCategory !== null) {
                oldCategory = oldCategory.pop();
                listView.removeClass(oldCategory + "-category");
            }

            var newCategory = entity.categories[0].name.replace(/\s/g, '-');
            listView.addClass(newCategory + "-category");

            var newCategoryName = entity.categories[0].name.replace("and", "&");
            listView.find(".tag").text(newCategoryName);
        };

        var refreshDataMethods = {
            refreshBasicEntity: function(entity, data) {
                //update card view
                var cardView = entity.filter(".card-view");
                if (cardView.exists()) {
                    __refreshCard(cardView, data);
                }

                //update list view
                var listView = entity.filter(".list-view");
                if (listView.exists()) {
                    __refreshList(listView, data);
                }
            },
            refreshEntityPage: function(entity, data) {
                //TODO: to be implemented
                // console.log(data);

                //update the category
                if (data.categories.length > 0) {
                    var category = data.categories[0].name;
                    var categoryTag = entity.find(".category-tags .category-tag");

                    var oldCategory = categoryTag.text();
                    var newCategory = category.replace("and", "&");
                    if (newCategory !== oldCategory) {
                        var oldCategoryClass = oldCategory.replace("&", "and").replace(/\s/g, "-") + "-category";
                        categoryTag.removeClass(oldCategoryClass);

                        var newCategoryClass = category.replace(/\s/g, "-") + "-category";
                        categoryTag.addClass(newCategoryClass);
                        categoryTag.text(newCategory);
                    }
                }

                //update the creator
                if (data.creator.id !== null) {
                    //to be implemented
                }

                //update the title
                if (data.title !== null) {
                    var titleContainer = entity.find("[data-section = 'title']");
                    var oldTitle = titleContainer.text();
                    if (data.title !== oldTitle) {
                        titleContainer.text(data.title);

                        //update the last part of the breadcrumb
                        entity.find("[data-section = 'breadcrumb'] b").text(data.title);
                    }
                }

                //update the one liner (idea, pitch, or summary)
                if ((data.idea || data.pitch || data.summary) !== null) {
                    var oneLinerContainer = entity.find("[data-section = 'one-liner']");
                    var oldOneLiner = oneLinerContainer.text();
                    var newOneLiner = "";
                    switch (data.entityType) {
                        case "idea":
                            newOneLiner = data.idea;
                            break;

                        case "pitch":
                            newOneLiner = data.pitch;
                            break;

                        case "project":
                            newOneLiner = data.summary;
                            break;
                    }

                    if (newOneLiner !== oldOneLiner) {
                        oneLinerContainer.text(newOneLiner);
                    }
                }

                //update the likes counter
                var likeCounter = entity.find("[data-counter-type = 'like']");
                if (+likeCounter.text().match(/(\d+)/)[0] != data.numberOfLikes) {
                    likeCounter.text(data.numberOfLikes + " " + ((data.numberOfLikes != 1) ? "likes" : "like"));
                }

                //update the votes counter
                var voteCounter = entity.find("[data-counter-type = 'vote']");
                if (voteCounter.exists() && (+voteCounter.text().match(/(\d+)/)[0] != data.numberOfVotes)) {
                    voteCounter.text(data.numberOfVotes + " " + ((data.numberOfVotes != 1) ? "votes" : "vote"));
                }

                //NOTE: comment counter is updated from comment handler
                //update the other contents
                //update the optional contents
            }
        };

        return {
            dataModels: dataModels,
            addTeamMemberButtonHandler: __addTeamMemberHandler,
            addAssignableRoleButtonHandler: __addAssignableRoleHandler,
            addContentButtonHandler: function(event) {
                $(event.target).text(__addContentButtonUIUpdateHandler)
                               .parent()
                               .toggleClass("cancel-remove-button");

                var addContentModule = $(".add-content-module");
                if (addContentModule.hasClass("hidden")) { 
                    addContentModule.removeClass("hidden");
                } else {
                    addContentModule.addClass("hidden");
                }
            },
            descriptionCardHandler: function(event) {
                __oneTimeUseTextBodyContent("add-description-button", templates.descriptionFormTemplate);
            },
            backgroundCardHandler: function(event) {
                __oneTimeUseTextBodyContent("add-background-button", templates.backgroundFormTemplate);
            },
            stakeholderCardHandler: function(event) {
                __oneTimeUseTextBodyContent("add-stakeholder-button", templates.stakeholderFormTemplate);
            },
            existingSolutionCardHandler: function(event) {
                __oneTimeUseTextBodyContent("add-existing-solution-button", templates.existingSolutionFormTemplate);
            },
            marketAnalysisCardHandler: function(event) {
                __oneTimeUseTextBodyContent("add-market-analysis-button", templates.marketAnalysisFormTemplate);
            },
            otherInformationCardHandler: function(event) {
                __oneTimeUseTextBodyContent("add-other-information-button", templates.otherInformationFormTemplate);
            },
            attachmentCardHandler: __renderAttachmentContent,
            validateRequiredContents: function(contentForm) {
                requiredContentsAreValid = true;
                contentForm.find("[required]").reverse().each(__validateRequiredContent);
                return requiredContentsAreValid;
            },
            includeOptionalContents: function(optionalContents, entityContentData) {
                //TODO: how to pass in entityContentData to the each function?
                // var entityContentData = entityContentData;
                // optionalContents.each(__includeOptionalContent);
            },
            updateEntityData: function(contentForm, initialEntityData) {
                initialEntityData = initialEntityData || {};

                var entityContents = dataModels.updateEntityData();
                var editedEntityContents = contentForm.find("[data-content-edited]").get();

                entityContents["edited"] = __packageEditedEntityContents(editedEntityContents, initialEntityData);

                var removedEntityContents = contentForm.find("[data-content-removed]").get();
                entityContents["removed"] = __packageRemovedEntityContents(removedEntityContents, initialEntityData);

                var addedEntityContents = contentForm.find("[data-content-added]").get();
                entityContents["added"] = __packageAddedEntityContents(addedEntityContents, initialEntityData);

                return entityContents;
            },
            bindHandlersToEditForm: function(contentForm) {
                // populate team list
                $("#team-members div[data-team-member-id]").each(function(index, element) {
                    var id = +element.getAttribute("data-team-member-id");
                    teamList.push(id);
                });

                //TODO: where is teamMemberForm used?
                // search handler methods
                var teamMemberForm = $(templates.teamMemberFormTemplate());
                var methods = {
                    filterPeople: __filterTeamMembers,
                    selectResultHandler: __addPersonToTeam
                };
                teamMemberForm.find("textarea").on("keydown paste", __editTeamMemberHandler)
                                               .on("input paste", null, methods, searchHandler.searchPeopleSuggestionHandler);
                teamMemberForm.find(".cancel-remove-button button").on("click", __removeContent)
                                                                   .on("click", __editTeamMemberHandler);
                // populate role assignees
                // call AJAX to fetch data from the server
                // When the data gets returned, response would update the role assignees with the appropriate forms of data
                var people = {
                    "ids": teamList
                };

                $.ajax({
                    type: "GET",
                    url: "/get/people",
                    data: utilities.formatJSON(people),
                    success: function(response) {
                        response.data.people.forEach(function(person) {
                            var profileImage = templates.profileImageTemplate({
                                person: person,
                                imageSizeClass: "profile-image-icon"
                            });

                            var roleAssignee = {
                                data: person, 
                                profileImage: profileImage
                            };

                            roleAssignees.push(roleAssignee);
                        });
                    },
                    dataType: "json"
                });

                //detect editing on existing contents
                contentForm.find("textarea[name]").not("textarea[name='team-member']").on("keydown paste", __entityContentEdited);
                contentForm.find("select[name]").on("change", __entityContentEdited);

                //enable adding attachments
                var attachmentForm = contentForm.find("#attachment-section-body");
                var attachmentList = contentForm.find("#attachments");
                attachmentForm.find(".upload-button input[type = 'file']").on("change", function(event) {
                    if (event.target.files && event.target.files.length > 0) {
                        for (var index = 0; index < event.target.files.length; index++) {
                            var attachment = event.target.files[index];

                            var route = window.location.pathname;
                            var entityId = route.match(/\/(\d+)/);
                            entityId = entityId ? +entityId.pop() : null;
                            var attachmentHandler = new gravit8.AttachmentHandler(attachment, entityId);
                            var referenceHandler = {
                                attachmentHandler: attachmentHandler
                            };

                            var data = {
                                attachment: attachment,
                                formattedSize: utilities.formatBytes(attachment.size)
                            };
                            var anAttachment = $(templates.uploadedAttachmentFormTemplate(data));

                            //NOTE: does this belong here?
                            anAttachment.find("[name = 'attachment']").attr("data-content-added", "");

                            anAttachment.find(".cancel-remove-button button").on("click", null, referenceHandler, function(event) {
                                event.data.attachmentHandler.cancel();
                            }).on("click", __entityContentRemoved);
                            anAttachment.find(".pause-button button").on("click", null, referenceHandler, function(event) {
                                var pauseButton = $(event.target);
                                pauseButton.closest(".pause-button").addClass("hidden");
                                pauseButton.closest(".entity-content").find(".resume-button").removeClass("hidden");
                                event.data.attachmentHandler.pause();
                            });
                            anAttachment.find(".resume-button button").on("click", null, referenceHandler, function(event) {
                                var resumeButton = $(event.target);
                                resumeButton.closest(".resume-button").addClass("hidden");
                                resumeButton.closest(".entity-content").find(".pause-button").removeClass("hidden");
                                event.data.attachmentHandler.resume();
                            });
                            attachmentList.append(anAttachment);

                            //attachments that are larger than the size limit will be skipped
                            if (attachment.size > (100 * 1024 * 1024)) {
                                anAttachment.find(".status").text("(" + data.formattedSize + ") This attachment is too large to upload onto Gravit8 - it will be skipped. If it is a video, try uploading it onto Youtube, Vimeo, etc. and document the link.");
                                return;
                            }

                            attachmentHandler.setUITemplate(anAttachment);
                            attachmentHandler.setUserControl($(".user-controls"));
                            attachmentHandler.start();
                        }

                        //allow the same file(s) to be selected again sequentially
                        event.target.value = null;
                    }
                });
                attachmentForm.find(".upload-button button").on("click", function(event) {
                    $(event.target).siblings("input[type = 'file']").trigger("click");
                });

                //detect role assignee(s) change
                contentForm.find("#roles [name = 'role-assignee']").on("click", function(event) {
                    $(event.target).parentsUntil(".entity-content", "ul").siblings("textarea[name = 'role']").trigger("keydown");
                }).on("click", function(event) {
                    if (event.target.hasAttribute("data-assignee-id")) {
                        $(event.target).find(".profile-image-icon").toggleClass("selected");
                    } else {
                        $(event.target).toggleClass("selected");
                        $(event.target).siblings().toggleClass("selected");
                    }
                }).on("click", __editRoleAssigneeHandler);

                //remove button handling
                contentForm.find(".cancel-remove-button button").on("click", function(event) {
                    //update the UI - enable optional entity content cards and other UI
                    var fieldName = $(event.target).closest(".entity-content").find("[name]").attr("name");
                    switch (fieldName) {
                        case "team-member":
                            // remove logic for initial values
                            var teamMemberId = $(event.target).parent().siblings("[data-element-type = 'profile-image-container']").attr("data-team-member-id");
                            $("#roles").find("textarea").trigger("keydown");
                            $("#roles").find("li[data-assignee-id = '" + teamMemberId + "']").remove();
                            break;

                        case "role":
                            break;

                        case "attachment":
                            break;

                        case "description":
                        case "background":
                        case "stakeholder":
                        case "existing-solution":
                        case "market-analysis":
                        case "other-information":
                            var cardId = "add-" + fieldName + "-button";
                            var card = document.getElementById(cardId);
                            __enableContentCard(card);
                            break;
                    }
                }).on("click", __entityContentRemoved);
            },
            refreshData: refreshDataMethods
        };
    }

    return {
        getInstance: function() {
            if (!instance) {
                instance = new Entity();
            }

            return instance;
        }
    };
})();
gravit8.Idea = (function() {
    var organizationInstance = gravit8.Organization.getInstance();
    var entityHandler = gravit8.Entity.getInstance();
    var likeHandler = new LikeHandler();
    var voteHandler = new VoteHandler();

    var templates = {
        cardTemplate: _.template($("#idea-card-template").html()),
        listTemplate: _.template($("#idea-list-template").html())
    };

    return {
        createNewIdea: function(event) {
            //form validation for required contents
            var ideaForm = $("#create-idea-form");
            var validRequiredContents = entityHandler.validateRequiredContents(ideaForm);

            //don't submit if the required contents are not valid
            if (!validRequiredContents) {
                return;
            }

            var title = $("textarea[name = 'title']").val().trim();
            var idea = $("textarea[name = 'idea']").val().trim();
            var categoryId = +$("select[name = 'categories'] option:selected").val();

            var entityContentData = entityHandler.dataModels.newEntityData(title, idea, false, [categoryId]);

            $("#create-idea [data-content-added]").each(function(index, optionalContent) {
                var content = $(optionalContent);
                var contentName = optionalContent.getAttribute("name");

                switch (contentName) {
                    case "description":
                    case "background":
                    case "stakeholder":
                    case "existing-solution":
                    case "market-analysis":
                    case "other-information":
                        var text = content.val().trim();
                        if (text.length > 0) {
                            entityContentData[contentName] = text;
                        }
                        break;
                }
            });

            $.ajax({
                type: "POST",
                url: "/submit/new/idea",
                data: utilities.formatJSON(entityContentData),
                success: function(response) {
                    if (response.success) {
                        window.location.href = "/idea/" + response.data.entityId;
                    }
                },
                dataType: "json"
            });
        },
        updateIdeaContents: function(event) {
            var route = window.location.pathname;
            var entityId = route.match(/\/(\d+)/);
            if (entityId) {
                //form validation
                var form = $("#edit-idea-form");
                var validRequiredContents = entityHandler.validateRequiredContents(form);

                //don't submit if the required contents are not valid
                if (!validRequiredContents) {
                    return;
                }

                //get the initial entity contents and keep track of all the changes
                var initialEntityData = event.data.initialEntityData;
                var entityData = entityHandler.updateEntityData($("#edit-idea"), initialEntityData);

                //submit the changes
                if (_.isEmpty(entityData.edited) && _.isEmpty(entityData.removed) && _.isEmpty(entityData.added)) {
                    window.location.href = route;
                } else {
                    var entity = {
                        "id": +entityId.pop(),
                        "contents": entityData
                    };

                    $.ajax({
                        type: "POST",
                        url: "/update/idea",
                        data: utilities.formatJSON(entity),
                        success: function() {
                            window.location.href = route;
                        },
                        error: function(request, status, error) {
                            switch (request.status) {
                                case 412:
                                    //retry?
                                    break;

                                case 500:
                                    //update failed
                                    break;

                                default:
                                    //generic error
                            }
                        },
                        dataType: "json"
                    });
                }
            } else {
                //entity id doesn't exist
            }
        },
        loadMoreOldIdeas: function(event) {
            var entitiesContainer = $("#browse-view [data-container = 'ideas']");
            var loadedIds = entitiesContainer.find(".card-view[data-entity-id]").map(function(index, idea) {
                return +idea.getAttribute("data-entity-id");
            }).get();
            referenceId = (loadedIds.length > 0) ? _.min(loadedIds) : 0;

            var requestData = {
                "id": referenceId,
                "lookupDirection": "oldest"
            };

            $.ajax({
                type: "GET",
                url: "/get/ideas",
                data: utilities.formatJSON(requestData),
                success: function(response) {
                    var numberOfEntities = response.data.entities.length;
                    if (numberOfEntities > 0) {
                        var cards = new Array(numberOfEntities);
                        var lists = new Array(numberOfEntities);
                        response.data.entities.forEach(function(idea, index) {
                            var card = $(templates.cardTemplate({idea: idea}));
                            card.find(".reaction-buttons .like-icon").on("click", likeHandler.likeCard);
                            card.find(".reaction-buttons .vote-icon").on("click", voteHandler.voteCard);

                            var list = $(templates.listTemplate({idea: idea}));
                            list.find(".reaction-buttons .like-icon").on("click", likeHandler.likeCard);
                            list.find(".reaction-buttons .vote-icon").on("click", voteHandler.voteCard);

                            if ($(".card-view-icon").hasClass("list-view-icon")) {
                                card.addClass("hidden");
                            } else {
                                list.addClass("hidden");
                            }

                            cards[index] = card;
                            lists[index] = list;
                        });
                        entitiesContainer.find(".invisible").first().before(cards);
                        entitiesContainer.find(".card-view").first().before(lists);

                        //resort the entities
                        $(".sort-by-options select[name = 'sort-by-options']").not(".hidden").trigger("change");
                    }
                },
                dataType: "json"
            });
        },
        loadMoreNewIdeas: function(event) {
            var entitiesContainer = $("#browse-view [data-container = 'ideas']");
            var loadedIds = entitiesContainer.find(".card-view[data-entity-id]").map(function(index, idea) {
                return +idea.getAttribute("data-entity-id");
            }).get();
            referenceId = (loadedIds.length > 0) ? _.max(loadedIds) : 0;

            var requestData = {
                "id": referenceId,
                "lookupDirection": "newest"
            };

            $.ajax({
                type: "GET",
                url: "/get/ideas",
                data: utilities.formatJSON(requestData),
                success: function(response) {
                    var numberOfEntities = response.data.entities.length;
                    if (numberOfEntities > 0) {
                        var cards = new Array(numberOfEntities);
                        var lists = new Array(numberOfEntities);
                        response.data.entities.forEach(function(idea, index) {
                            var card = $(templates.cardTemplate({idea: idea}));
                            card.find(".reaction-buttons .like-icon").on("click", likeHandler.likeCard);
                            card.find(".reaction-buttons .vote-icon").on("click", voteHandler.voteCard);

                            var list = $(templates.listTemplate({idea: idea}));
                            list.find(".reaction-buttons .like-icon").on("click", likeHandler.likeCard);
                            list.find(".reaction-buttons .vote-icon").on("click", voteHandler.voteCard);

                            if ($(".card-view-icon").hasClass("list-view-icon")) {
                                card.addClass("hidden");
                            } else {
                                list.addClass("hidden");
                            }

                            cards[index] = card;
                            lists[index] = list;
                        });
                        entitiesContainer.find(".item-container").first().before(cards);
                        entitiesContainer.find(".card-view").first().before(lists);

                        //resort the entities
                        $(".sort-by-options select[name = 'sort-by-options']").not(".hidden").trigger("change");

                        organizationInstance.refreshStatistics();
                    }
                },
                dataType: "json"
            });
        },
        refreshIdeas: function(event) {
            var entityIds = $("#browse-view [data-container = 'ideas'] .item-container.card-view[data-entity-id]").map(function(index, entity) {
                return +entity.getAttribute("data-entity-id");
            }).get();

            if (entityIds.length > 0) {
                var entities = {
                    "ids": entityIds
                };

                $.ajax({
                    type: "GET",
                    url: "/get/ideas",
                    data: utilities.formatJSON(entities),
                    success: function(response) {
                        if (response.data.entities.length > 0) {
                            response.data.entities.forEach(function(entity) {
                                var items = $("#browse-view [data-container = 'ideas'] .item-container[data-entity-id = " + entity.id + "]");
                                entityHandler.refreshData.refreshBasicEntity(items, entity);
                            });

                            //resort the entities
                            $(".sort-by-options select[name = 'sort-by-options']").not(".hidden").trigger("change");
                        }
                    },
                    dataType: "json"
                });
            }
        },
        refreshIdea: function(event) {
            var entityId = window.location.pathname.match(/\/(\d+)/);
            if (entityId) {
                $.ajax({
                    type: "GET",
                    url: "/get/idea/" + entityId.pop(),
                    success: function(entity) {
                        if (entity.data) {
                            entityHandler.refreshData.refreshEntityPage($("body"), entity.data);
                        }
                    },
                    dataType: "json"
                });
            }
        }
    };
});
gravit8.Organization = (function() {
    var instance;

    function Organization() {
        var __refreshStatistics = function() {
            $.ajax({
                type: "GET",
                url: "/get/summary",
                success: function(response) {
                    var count = response.data.summary.count;
                    var dataCounts = $(".header-browse .browse-tabs a span");
                    dataCounts.filter("[data-count-idea]").text(count.idea);
                    dataCounts.filter("[data-count-pitch]").text(count.pitch);
                    dataCounts.filter("[data-count-project]").text(count.project);
                    dataCounts.filter("[data-count-user]").text(count.user);
                },
                dataType: "json"
            });
        };

        return {
            refreshStatistics: __refreshStatistics
        };
    }

    return {
        getInstance: function() {
            if (!instance) {
                instance = new Organization();
            }

            return instance;
        }
    };
})();
gravit8.People = (function() {
    return {
    
    };
});
gravit8.Pitch = (function() {
    var organizationInstance = gravit8.Organization.getInstance();
    var entityHandler = gravit8.Entity.getInstance();
    var likeHandler = new LikeHandler();
    var voteHandler = new VoteHandler();

    var templates = {
        cardTemplate: _.template($("#pitch-card-template").html()),
        listTemplate: _.template($("#pitch-list-template").html())
    };

    return {
        createNewPitch: function(event) {
            //form validation for required contents
            var pitchForm = $("#create-pitch-form");
            var validRequiredContents = entityHandler.validateRequiredContents(pitchForm);

            //don't submit if the required contents are not valid
            if (!validRequiredContents) {
                return;
            }

            var title = $("textarea[name = 'title']").val().trim();
            var pitch = $("textarea[name = 'pitch']").val().trim();
            var categoryId = +$("select[name = 'categories'] option:selected").val();

            var entityContentData = entityHandler.dataModels.newEntityData(title, pitch, false, [categoryId]);

            $("#create-pitch [data-content-added]").each(function(index, optionalContent) {
                var content = $(optionalContent);
                var contentName = optionalContent.getAttribute("name");

                switch (contentName) {
                    case "role":
                        var role = content.val().trim();
                        if (role.length > 0) {
                            var roleData = entityHandler.dataModels.roleData(role);
                            roleData.assignees = content.parent().find("[name = 'role-assignee'] .selected").map(function(index, profileImage) {
                                return +$(profileImage).parent().attr("data-assignee-id");
                            }).get();

                            if ("roles" in entityContentData) {
                                entityContentData["roles"].push(roleData);
                            } else {
                                entityContentData["roles"] = [roleData];
                            }
                        }
                        break;

                    case "description":
                    case "background":
                    case "stakeholder":
                    case "existing-solution":
                    case "market-analysis":
                    case "other-information":
                        var text = content.val().trim();
                        if (text.length > 0) {
                            entityContentData[contentName] = text;
                        }
                        break;
                }
            });

            $.ajax({
                type: "POST",
                url: "/submit/new/pitch",
                data: utilities.formatJSON(entityContentData),
                success: function(response) {
                    if (response.success) {
                        window.location.href = "/pitch/" + response.data.entityId;
                    }
                },
                dataType: "json"
            });
        },
        updatePitchContents: function(event) {
            var route = window.location.pathname;
            var entityId = route.match(/\/(\d+)/);
            if (entityId) {
                //form validation
                var form = $("#edit-pitch-form");
                var validRequiredContents = entityHandler.validateRequiredContents(form);

                //don't submit if the required contents are not valid
                if (!validRequiredContents) {
                    return;
                }

                //get the initial entity contents and keep track of all the changes
                var initialEntityData = event.data.initialEntityData;
                var entityData = entityHandler.updateEntityData($("#edit-pitch"), initialEntityData);

                //submit the changes
                if (_.isEmpty(entityData.edited) && _.isEmpty(entityData.removed) && _.isEmpty(entityData.added)) {
                    window.location.href = route;
                } else {
                    var entity = {
                        "id": +entityId.pop(),
                        "contents": entityData
                    };

                    $.ajax({
                        type: "POST",
                        url: "/update/pitch",
                        data: utilities.formatJSON(entity),
                        success: function(response) {
                            window.location.href = route;
                        },
                        error: function(request, status, error) {
                            switch (request.status) {
                                case 412:
                                    //retry?
                                    break;

                                case 500:
                                    //update failed
                                    break;

                                default:
                                    //generic error
                            }
                        },
                        dataType: "json"
                    });
                }
            } else {
                //entity id doesn't exist
            }
        },
        loadMoreOldPitches: function(event) {
            var entitiesContainer = $("#browse-view [data-container = 'pitches']");
            var loadedIds = entitiesContainer.find(".card-view[data-entity-id]").map(function(index, pitch) {
                return +pitch.getAttribute("data-entity-id");
            }).get();
            var referenceId = (loadedIds.length > 0) ? _.min(loadedIds) : 0;

            var requestData = {
                "id": referenceId,
                "lookupDirection": "oldest"
            };

            $.ajax({
                type: "GET",
                url: "/get/pitches",
                data: utilities.formatJSON(requestData),
                success: function(response) {
                    var numberOfEntities = response.data.entities.length;
                    if (numberOfEntities > 0) {
                        var cards = new Array(numberOfEntities);
                        var lists = new Array(numberOfEntities);
                        response.data.entities.forEach(function(pitch, index) {
                            var card = $(templates.cardTemplate({pitch: pitch}));
                            card.find(".reaction-buttons .like-icon").on("click", likeHandler.likeCard);
                            card.find(".reaction-buttons .vote-icon").on("click", voteHandler.voteCard);

                            var list = $(templates.listTemplate({pitch: pitch}));
                            list.find(".reaction-buttons .like-icon").on("click", likeHandler.likeCard);
                            list.find(".reaction-buttons .vote-icon").on("click", voteHandler.voteCard);

                            if ($(".card-view-icon").hasClass("list-view-icon")) {
                                card.addClass("hidden");
                            } else {
                                list.addClass("hidden");
                            }

                            cards[index] = card;
                            lists[index] = list;
                        });
                        entitiesContainer.find(".invisible").first().before(cards);
                        entitiesContainer.find(".card-view").first().before(lists);

                        //resort the entities
                        $(".sort-by-options select[name = 'sort-by-options']").not(".hidden").trigger("change");
                    }
                },
                dataType: "json"
            });
        },
        loadMoreNewPitches: function(event) {
            var entitiesContainer = $("#browse-view [data-container = 'pitches']");
            var loadedIds = entitiesContainer.find(".card-view[data-entity-id]").map(function(index, pitch) {
                return +pitch.getAttribute("data-entity-id");
            }).get();
            var referenceId = (loadedIds.length > 0) ? _.max(loadedIds) : 0;

            var requestData = {
                "id": referenceId,
                "lookupDirection": "newest"
            };

            $.ajax({
                type: "GET",
                url: "/get/pitches",
                data: utilities.formatJSON(requestData),
                success: function(response) {
                    var numberOfEntities = response.data.entities.length;
                    if (numberOfEntities > 0) {
                        var cards = new Array(numberOfEntities);
                        var lists = new Array(numberOfEntities);
                        response.data.entities.forEach(function(pitch, index) {
                            var card = $(templates.cardTemplate({pitch: pitch}));
                            card.find(".reaction-buttons .like-icon").on("click", likeHandler.likeCard);
                            card.find(".reaction-buttons .vote-icon").on("click", voteHandler.voteCard);

                            var list = $(templates.listTemplate({pitch: pitch}));
                            list.find(".reaction-buttons .like-icon").on("click", likeHandler.likeCard);
                            list.find(".reaction-buttons .vote-icon").on("click", voteHandler.voteCard);
                            
                            if ($(".card-view-icon").hasClass("list-view-icon")) {
                                card.addClass("hidden");
                            } else {
                                list.addClass("hidden");
                            }
                            cards[index] = card;
                            lists[index] = list;
                        });

                        entitiesContainer.find(".item-container").first().before(cards);
                        entitiesContainer.find(".card-view").first().before(lists);

                        //resort the entities
                        $(".sort-by-options select[name = 'sort-by-options']").not(".hidden").trigger("change");

                        organizationInstance.refreshStatistics();
                    }
                },
                dataType: "json"
            });
        },
        refreshPitches: function(event) {
            var entityIds = $("#browse-view [data-container = 'pitches'] .item-container.card-view[data-entity-id]").map(function(index, entity) {
                return +entity.getAttribute("data-entity-id");
            }).get();

            if (entityIds.length > 0) {
                var entities = {
                    "ids": entityIds
                };

                $.ajax({
                    type: "GET",
                    url: "/get/pitches",
                    data: utilities.formatJSON(entities),
                    success: function(response) {
                        if (response.data.entities.length > 0) {
                            response.data.entities.forEach(function(entity) {
                                var items = $("#browse-view [data-container = 'pitches'] .item-container[data-entity-id = " + entity.id + "]");
                                entityHandler.refreshData.refreshBasicEntity(items, entity);
                            });

                            //resort the entities
                            $(".sort-by-options select[name = 'sort-by-options']").not(".hidden").trigger("change");
                        }
                    },
                    dataType: "json"
                });
            }
        },
        refreshPitch: function(event) {
            var entityId = window.location.pathname.match(/\/(\d+)/);
            if (entityId) {
                $.ajax({
                    type: "GET",
                    url: "/get/pitch/" + entityId.pop(),
                    success: function(entity) {
                        if (entity.data) {
                            entityHandler.refreshData.refreshEntityPage($("body"), entity.data);
                        }
                    },
                    dataType: "json"
                });
            }
        }
    };
});
gravit8.Project = (function () {
    var organizationInstance = gravit8.Organization.getInstance();
    var entityHandler = gravit8.Entity.getInstance();
    var likeHandler = new LikeHandler();
    var voteHandler = new VoteHandler();

    var templates = {
        cardTemplate: _.template($("#project-card-template").html()),
        listTemplate: _.template($("#project-list-template").html())
    };

    return {
        createNewProject: function(event) {
            //form validation for required contents
            var projectForm = $("#create-project-form");
            var validRequiredContents = entityHandler.validateRequiredContents(projectForm);

            //don't submit if the required contents are not valid
            if (!validRequiredContents) {
                return;
            }

            //include default contents
            var title = projectForm.find("textarea[name = 'title']").val().trim();
            var summary = projectForm.find("textarea[name = 'summary']").val().trim();
            var categoryId = +projectForm.find("select[name = 'categories']").val();
            var entityContentData = entityHandler.dataModels.newEntityData(title, summary, false, [categoryId]);

            entityContentData["teamMembers"] = $("#team-members [data-element-type = 'profile-image-container'][data-team-member-id != '']").map(function(index, teamMember) {
                return +teamMember.getAttribute("data-team-member-id");
            }).get();

            //include optional contents (TODO: abstractable?)
            $("#create-project [name]").each(function(index, optionalContent) {
                var content = $(optionalContent);
                var contentName = optionalContent.getAttribute("name");

                switch (contentName) {
                    case "attachment":
                        var attachmentId = +content.closest("[data-attachment-id]").attr("data-attachment-id");
                        //TODO: attachment name should be changable by the user in a text box
                        var attachmentName = content.text().trim() || null;
                        if (attachmentId > 0) {
                            var attachmentContentData = entityHandler.dataModels.entityContentData(attachmentId, attachmentName);
                            if ("attachments" in entityContentData) {
                                entityContentData["attachments"].push(attachmentContentData);
                            } else {
                                entityContentData["attachments"] = [attachmentContentData];
                            }
                        }
                        break;

                    case "role":
                        var role = content.val().trim();
                        if (role.length > 0) {
                            var roleData = entityHandler.dataModels.roleData(role);
                            roleData.assignees = content.parent().find("[name = 'role-assignee'] .selected").map(function(index, profileImage) {
                                return +$(profileImage).parent().attr("data-assignee-id");
                            }).get();

                            if ("roles" in entityContentData) {
                                entityContentData["roles"].push(roleData);
                            } else {
                                entityContentData["roles"] = [roleData];
                            }
                        }
                        break;

                    case "description":
                    case "background":
                    case "stakeholder":
                    case "existing-solution":
                    case "market-analysis":
                    case "other-information":
                        var text = content.val().trim();
                        if (text.length > 0) {
                            entityContentData[contentName] = text;
                        }
                        break;
                }
            });

            //TODO: attempting to abstract the optional contents, but haven't figured out how to pass over entityContentData in the foreach callback
            // entity.includeOptionalContents($("#create-project textarea[name]"), entityContentData);

            $.ajax({
                type: "POST",
                url: "/submit/new/project",
                data: utilities.formatJSON(entityContentData),
                success: function(response) {
                    if (response.success) {
                        window.location.href = "/project/" + response.data.entityId;
                    }
                },
                dataType: "json"
            });
        },
        updateProjectContents: function(event) {
            var route = window.location.pathname;
            var entityId = route.match(/\/(\d+)/);
            if (entityId) {
                //form validation
                var form = $("#edit-project-form");
                var validRequiredContents = entityHandler.validateRequiredContents(form);

                //don't submit if the required contents are not valid
                if (!validRequiredContents) {
                    return;
                }

                //get the initial entity contents and keep track of all the changes
                var initialEntityData = event.data.initialEntityData;
                var entityData = entityHandler.updateEntityData($("#edit-project"), initialEntityData);

                //submit the changes
                if (_.isEmpty(entityData.edited) && _.isEmpty(entityData.removed) && _.isEmpty(entityData.added)) {
                    window.location.href = route;
                } else {
                    var entity = {
                        "id": +entityId.pop(),
                        "contents": entityData
                    };

                    $.ajax({
                        type: "POST",
                        url: "/update/project",
                        data: utilities.formatJSON(entity),
                        success: function() {
                            window.location.href = route;
                        },
                        error: function(request, status, error) {
                            switch (request.status) {
                                case 412:
                                    //retry?
                                    break;

                                case 500:
                                    //update failed
                                    break;

                                default:
                                    //generic error
                            }
                        },
                        dataType: "json"
                    });
                }
            } else {
                //entity id doesn't exist
            }
        },
        loadMoreOldProjects: function() {
            var entitiesContainer = $("#browse-view [data-container = 'projects']");
            var loadedIds = entitiesContainer.find(".card-view[data-entity-id]").map(function(index, card) {
                return +card.getAttribute("data-entity-id");
            }).get();
            var referenceId = (loadedIds.length > 0) ? _.min(loadedIds) : 0;

            var requestData = {
                "id": referenceId,
                "lookupDirection": "oldest"
            };

            $.ajax({
                type: "GET",
                url: "/get/projects",
                data: utilities.formatJSON(requestData),
                success: function(response) {
                    var numberOfEntities = response.data.entities.length;
                    if (numberOfEntities > 0) {
                        var cards = new Array(numberOfEntities);
                        var lists = new Array(numberOfEntities);
                        response.data.entities.forEach(function(project, index) {
                            var card = $(templates.cardTemplate({entity: project}));
                            card.find(".reaction-buttons .like-icon").on("click", likeHandler.likeCard);
                            card.find(".reaction-buttons .vote-icon").on("click", voteHandler.voteCard);

                            var list = $(templates.listTemplate({entity: project}));
                            list.find(".reaction-buttons .like-icon").on("click", likeHandler.likeCard);
                            list.find(".reaction-buttons .vote-icon").on("click", voteHandler.voteCard);

                            if ($(".card-view-icon").hasClass("list-view-icon")) {
                                card.addClass("hidden");
                            } else {
                                list.addClass("hidden");
                            }

                            cards[index] = card;
                            lists[index] = list;
                        });
                        entitiesContainer.find(".invisible").first().before(cards);
                        entitiesContainer.find(".card-view").first().before(lists);

                        //resort the entities
                        $(".sort-by-options select[name = 'sort-by-options']").not(".hidden").trigger("change");
                    }
                },
                dataType: "json"
            });
        },
        loadMoreNewProjects: function() {
            var entitiesContainer = $("#browse-view [data-container = 'projects']");
            var loadedIds = entitiesContainer.find(".card-view[data-entity-id]").map(function(index, card) {
                return +card.getAttribute("data-entity-id");
            }).get();
            var referenceId = (loadedIds.length > 0) ? _.max(loadedIds) : 0;

            var requestData = {
                "id": referenceId,
                "lookupDirection": "newest"
            };

            $.ajax({
                type: "GET",
                url: "/get/projects",
                data: utilities.formatJSON(requestData),
                success: function(response) {
                    var numberOfEntities = response.data.entities.length;
                    if (numberOfEntities > 0) {
                        var cards = new Array(numberOfEntities);
                        var lists = new Array(numberOfEntities);
                        response.data.entities.forEach(function(project, index) {
                            var card = $(templates.cardTemplate({entity: project}));
                            card.find(".reaction-buttons .like-icon").on("click", likeHandler.likeCard);
                            card.find(".reaction-buttons .vote-icon").on("click", voteHandler.voteCard);

                            var list = $(templates.listTemplate({entity: project}));
                            list.find(".reaction-buttons .like-icon").on("click", likeHandler.likeCard);
                            list.find(".reaction-buttons .vote-icon").on("click", voteHandler.voteCard);

                            if ($(".card-view-icon").hasClass("list-view-icon")) {
                                card.addClass("hidden");
                            } else {
                                list.addClass("hidden");
                            }

                            cards[index] = card;
                            lists[index] = list;
                        });
                        entitiesContainer.find(".item-container").first().before(cards);
                        entitiesContainer.find(".card-view").first().before(lists);

                        //resort the entities
                        $(".sort-by-options select[name = 'sort-by-options']").not(".hidden").trigger("change");

                        organizationInstance.refreshStatistics();
                    }
                },
                dataType: "json"
            });
        },
        refreshProjects: function(event) {
            var entityIds = $("#browse-view [data-container = 'projects'] .item-container.card-view[data-entity-id]").map(function(index, entity) {
                return +entity.getAttribute("data-entity-id");
            }).get();

            if (entityIds.length > 0) {
                var entities = {
                    "ids": entityIds
                };

                $.ajax({
                    type: "GET",
                    url: "/get/projects",
                    data: utilities.formatJSON(entities),
                    success: function(response) {
                        if (response.data.entities.length > 0) {
                            response.data.entities.forEach(function(entity) {
                                var items = $("#browse-view [data-container = 'projects'] .item-container[data-entity-id = " + entity.id + "]");
                                entityHandler.refreshData.refreshBasicEntity(items, entity);
                            });

                            //resort the entities
                            $(".sort-by-options select[name = 'sort-by-options']").not(".hidden").trigger("change");
                        }
                    },
                    dataType: "json"
                });
            }
        },
        refreshProject: function(event) {
            var entityId = window.location.pathname.match(/\/(\d+)/);
            if (entityId) {
                $.ajax({
                    type: "GET",
                    url: "/get/project/" + entityId.pop(),
                    success: function(entity) {
                        if (entity.data) {
                            entityHandler.refreshData.refreshEntityPage($("body"), entity.data);
                        }
                    },
                    dataType: "json"
                });
            }
        }
    };
});
$(document).ready(function() {
    var SCROLLBAR_DETECTION_PERIOD = 500;
    var CARD_REFRESH_PERIOD = 5000;
    var COMMENT_REFRESH_PERIOD = 7500;
    var LIKE_COUNTER_REFRESH_PERIOD = 15000;

    var throttleEventDetector = new ThrottleEventDetector();
    var scrollbar = new ScrollbarHandler();
    var sortHandler = new SortHandler();
    var likeHandler = new LikeHandler();
    var voteHandler = new VoteHandler();
    var commentHandler = new CommentHandler();
    var entityInstance = gravit8.Entity.getInstance();
    var organizationInstance = gravit8.Organization.getInstance();

    $(window).on("scroll", function() {
        throttleEventDetector.isScrolling = true;
    });

    //textareas are auto-sized based on content in textarea
    var textareaHandler = function() {
        $("textarea").not("[name='team-member']").not("[name='role']").each(function() {
            this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px; overflow-y: hidden;');
        }).on('input keypaste', function() {
            this.style.height = '';
            this.style.height = (this.scrollHeight) + 'px';
        });
    };

    // $(window).on("drop dragend dragover", function(event) {
    //     event.preventDefault();
    // });

    var infiniteScrollHandler = function(eventHandler) {
        if (throttleEventDetector.isScrolling && scrollbar.isEndOfPage()) {
            throttleEventDetector.isScrolling = false;
            eventHandler();
        }
    };

    var hostname = window.location.hostname;
    var domain = hostname.split(".")[0];
    var route = window.location.pathname;

    //explore people page handler definitions
    var personCardTemplate = _.template($("#person-card-template").html());
    var loadMoreOldPeopleHandler = function() {
        var personIds = $("#browse-view [data-container = 'people'] .people-card:not(.invisible)").map(function(index, person) {
            return +person.getAttribute("href").match(/\/(\d+)/).pop();
        }).get();
        referenceId = _.isEmpty(personIds) ? 0 : _.min(personIds);

        var requestData = {
            "id": referenceId,
            "lookupDirection": "oldest"
        };

        $.ajax({
            type: "GET",
            url: "/get/people",
            data: utilities.formatJSON(requestData),
            success: function(response) {
                //render the batch people cards
                var peopleCards = "";
                response.data.people.forEach(function(person) {
                    peopleCards += personCardTemplate({person: person});
                });
                $("#browse-view [data-container = 'people'] .invisible:first").before(peopleCards);

                //resort the people
                $(".sort-by-options select[name = 'sort-by-options']").not(".hidden").trigger("change");
            },
            dataType: "json"
        });
    };

    var loadMoreNewPeopleHandler = function() {
        var personIds = $("#browse-view [data-container = 'people'] .people-card:not(.invisible)").map(function(index, person) {
            return +person.getAttribute("href").match(/\/(\d+)/).pop();
        }).get();
        referenceId = _.isEmpty(personIds) ? 0 : _.max(personIds);

        var requestData = {
            "id": referenceId,
            "lookupDirection": "newest"
        };

        $.ajax({
            type: "GET",
            url: "/get/people",
            data: utilities.formatJSON(requestData),
            success: function(response) {
                //render the batch people cards
                var peopleCards = "";
                response.data.people.forEach(function(person) {
                    peopleCards += personCardTemplate({person: person});
                });
                $("#browse-view [data-container = 'people'] .people-card:first").before(peopleCards);

                //resort the people
                $(".sort-by-options select[name = 'sort-by-options']").not(".hidden").trigger("change");

                organizationInstance.refreshStatistics();
            },
            dataType: "json"
        });
    };


    //route: /browse
    if (/^\/browse/i.test(route)) {
        var exploreRouteHandler = new ExploreRouteHandler();

        var idea = new gravit8.Idea();
        var pitch = new gravit8.Pitch();
        var project = new gravit8.Project();

        //update the history state when page loads on browse page
        if (window.history && window.history.state) {
            $(window).on("popstate", exploreRouteHandler.popStateHandler);
        } else if (window.history && !window.history.state) {
            var selectedTab = $(".browse-tabs a.selected").attr("data-container");
            exploreRouteHandler.updateState(route, selectedTab);
        }

        $(".card-view-icon").on("click", function(event) {
            var viewButton = $(event.target);
            viewButton.toggleClass("list-view-icon");

            if (viewButton.hasClass("list-view-icon")) {
                $("#browse-view .list-view").removeClass("hidden");
                $("#browse-view .card-view").addClass("hidden");
            } else {
                $("#browse-view .card-view").removeClass("hidden");
                $("#browse-view .list-view").addClass("hidden");
            }
        });

        var infiniteScrollThrottlingId;
        var timerThrottlingId;
        var refreshDataCountersThrottlingId;
        $("ul.browse-tabs a").on("click", function(event) {
            event.preventDefault();
            var tab = event.target.getAttribute("data-container");

            //show appropriate contents and update browse tabs
            $(".browse-tabs a").removeClass("selected")
                                .filter("[data-container = " + tab + "]")
                                .addClass("selected");

            $(".items-container[data-container]").addClass("hidden")
                                                .filter("[data-container = " + tab + "]")
                                                .removeClass("hidden");

            var newRoute = event.target.getAttribute("href");
            exploreRouteHandler.pushState(newRoute, tab);

            //switch to the specific set of sort options
            $(".sort-by-options select[name = 'sort-by-options']").addClass("hidden");
            $("#" + tab + "-sort-options").removeClass("hidden");

            //hide the list-/card-view icon if the people tab is selected
            if (tab === "people") {
                $(".card-view-icon").addClass("hidden");
            } else {
                $(".card-view-icon").removeClass("hidden");
            }
        }).on("click", function(event) {
            //explore page specific event handlers

            //clear up the infinite scroll and timer handler from the previous explore page
            window.clearInterval(infiniteScrollThrottlingId);
            window.clearInterval(timerThrottlingId);
            window.clearInterval(refreshDataCountersThrottlingId);

            var selected = this.getAttribute("data-container");
            switch (selected) {
                case "ideas":
                    infiniteScrollThrottlingId = window.setInterval(infiniteScrollHandler, SCROLLBAR_DETECTION_PERIOD, idea.loadMoreOldIdeas);
                    timerThrottlingId = window.setInterval(idea.loadMoreNewIdeas, CARD_REFRESH_PERIOD);
                    refreshDataCountersThrottlingId = window.setInterval(idea.refreshIdeas, CARD_REFRESH_PERIOD);
                    $("#vote-summary footer").addClass("hidden").filter("[data-entity-type = 'idea']").removeClass("hidden");
                    break;

                case "pitches":
                    infiniteScrollThrottlingId = window.setInterval(infiniteScrollHandler, SCROLLBAR_DETECTION_PERIOD, pitch.loadMoreOldPitches);
                    timerThrottlingId = window.setInterval(pitch.loadMoreNewPitches, CARD_REFRESH_PERIOD);
                    refreshDataCountersThrottlingId = window.setInterval(pitch.refreshPitches, CARD_REFRESH_PERIOD);
                    $("#vote-summary footer").addClass("hidden").filter("[data-entity-type = 'pitch']").removeClass("hidden");
                    break;

                case "projects":
                    infiniteScrollThrottlingId = window.setInterval(infiniteScrollHandler, SCROLLBAR_DETECTION_PERIOD, project.loadMoreOldProjects);
                    timerThrottlingId = window.setInterval(project.loadMoreNewProjects, CARD_REFRESH_PERIOD);
                    refreshDataCountersThrottlingId = window.setInterval(project.refreshProjects, CARD_REFRESH_PERIOD);
                    $("#vote-summary footer").addClass("hidden").filter("[data-entity-type = 'project']").removeClass("hidden");
                    break;

                case "people":
                    infiniteScrollThrottlingId = window.setInterval(infiniteScrollHandler, SCROLLBAR_DETECTION_PERIOD, loadMoreOldPeopleHandler);
                    timerThrottlingId = window.setInterval(loadMoreNewPeopleHandler, CARD_REFRESH_PERIOD);
                    $("#vote-summary footer").addClass("hidden");
                    break;
            }
        });

        //hide the list/card view icon when the people tab is selected on the explore page
        var initialTab = $("ul.browse-tabs a.selected").attr("data-container");
        if (initialTab === "people") {
            $(".card-view-icon").addClass("hidden");
        } else {
            $(".card-view-icon").removeClass("hidden");
        }


        //sorting cards, lists, or people
        $(".sort-by-options select[name = 'sort-by-options']").on("change", function(event) {
            var tab = this.id;
            var sortOption = $(this).val();
            var visibleItems = $("#browse-view .items-container").not(".hidden");

            if (tab === "people-sort-options") {
                //sort people
                var people = visibleItems.find(".people-card").not(".invisible").get();
                var sortedPeople = sortHandler.sortPeople(people, sortOption);
                visibleItems.find(".invisible").filter(":first").before(sortedPeople);
            } else {
                //sort cards
                var cards = visibleItems.find(".card-view").get();
                var sortedCards = sortHandler.sortCards(cards, sortOption);
                visibleItems.find(".invisible").filter(":first").before(sortedCards);

                //sort lists according to the order of the cards
                var lists = visibleItems.find(".list-view").get();
                var sortedLists = sortHandler.sortLists(sortedCards);
                visibleItems.find(".card-view").filter(":first").before(sortedLists);
            }
        });

        //search bar feature (disabled until functional)
        // var ENTER_KEY = 13;
        // $(".search-bar input[type = 'search']").on("keydown paste", function(event) {
        //     console.log(event);
        //     if (event.type === "keydown") {
        //         var keyCode = event.keyCode || event.charCode || event.which;
        //         var character = String.fromCharCode(keyCode);
        //         var isValidCharacter = /[A-Za-z0-9\~\`\!\@\#\$\%\^\&\*\(\)\_\-\+\=\{\}\[\]\"\'\:\;\|\/\\\?\>\.\<\,]/.test(character);
        //         console.log(character);

        //         if (keyCode === ENTER_KEY) {
        //         }
        //     }

        //     var keywords = $(this).val().trim();
        //     var searchRequest = {
        //         "keywords": keywords
        //     };

        //     var exploreTab = $(".browse-tabs a.selected").attr("data-container");
        //     var requestURL = "/submit/search/" + exploreTab;
        //     $.ajax({
        //         type: "POST",
        //         url: requestURL,
        //         data: JSON.stringify(searchRequest),
        //         success: function(response) {
        //             console.log(response);
        //         },
        //         dataType: "json"
        //     });
        // });

        //load ideas, pitches, projects, and people at the start
        idea.loadMoreNewIdeas();
        pitch.loadMoreNewPitches();
        project.loadMoreNewProjects();
        loadMoreNewPeopleHandler();

        var selected = $("ul.browse-tabs a.selected").attr("data-container");
        switch (selected) {
            case "ideas":
                infiniteScrollThrottlingId = window.setInterval(infiniteScrollHandler, SCROLLBAR_DETECTION_PERIOD, idea.loadMoreOldIdeas);
                timerThrottlingId = window.setInterval(idea.loadMoreNewIdeas, CARD_REFRESH_PERIOD);
                refreshDataCountersThrottlingId = window.setInterval(idea.refreshIdeas, CARD_REFRESH_PERIOD);
                $("#vote-summary footer").addClass("hidden").filter("[data-entity-type = 'idea']").removeClass("hidden");
                break;

            case "pitches":
                infiniteScrollThrottlingId = window.setInterval(infiniteScrollHandler, SCROLLBAR_DETECTION_PERIOD, pitch.loadMoreOldPitches);
                timerThrottlingId = window.setInterval(pitch.loadMoreNewPitches, CARD_REFRESH_PERIOD);
                refreshDataCountersThrottlingId = window.setInterval(pitch.refreshPitches, CARD_REFRESH_PERIOD);
                // $("#voting-status-bar").removeClass("hidden");
                $("#vote-summary footer").addClass("hidden").filter("[data-entity-type = 'pitch']").removeClass("hidden");
                break;

            case "projects":
                infiniteScrollThrottlingId = window.setInterval(infiniteScrollHandler, SCROLLBAR_DETECTION_PERIOD, project.loadMoreOldProjects);
                timerThrottlingId = window.setInterval(project.loadMoreNewProjects, CARD_REFRESH_PERIOD);
                refreshDataCountersThrottlingId = window.setInterval(project.refreshProjects, CARD_REFRESH_PERIOD);
                $("#vote-summary footer").addClass("hidden").filter("[data-entity-type = 'project']").removeClass("hidden");
                break;

            case "people":
                infiniteScrollThrottlingId = window.setInterval(infiniteScrollHandler, SCROLLBAR_DETECTION_PERIOD, loadMoreOldPeopleHandler);
                timerThrottlingId = window.setInterval(loadMoreNewPeopleHandler, CARD_REFRESH_PERIOD);
                $("#vote-summary footer").addClass("hidden");
                break;
        }
    }

    //route: /create/idea
    else if (/^\/create\/idea/i.test(route)) {
        //ping server every 10 mins to keep the session alive
        window.setInterval(function() {
            $.ajax({url: "/ping"});
        }, 600000);

        textareaHandler();

        //add content button handler
        $(".create-buttons .add-content").on("click", entityInstance.addContentButtonHandler);

        //add optional content handlers
        $("#add-description-button").on("click", entityInstance.descriptionCardHandler);
        $("#add-background-button").on("click", entityInstance.backgroundCardHandler);
        $("#add-stakeholder-button").on("click", entityInstance.stakeholderCardHandler);
        $("#add-existing-solution-button").on("click", entityInstance.existingSolutionCardHandler);
        $("#add-market-analysis-button").on("click", entityInstance.marketAnalysisCardHandler);
        $("#add-other-information-button").on("click", entityInstance.otherInformationCardHandler);
        $("#add-attachment-button").on("click", entityInstance.attachmentCardHandler);

        var idea = new gravit8.Idea();
        $("#submit").on("click", idea.createNewIdea);
    }

    //route: /create/pitch
    else if (/^\/create\/pitch/i.test(route)) {
        //ping server every 10 mins to keep the session alive
        window.setInterval(function() {
            $.ajax({url: "/ping"});
        }, 600000);

        textareaHandler();

        $(".add-role-button button").on("click", entityInstance.addAssignableRoleButtonHandler);

        //add content button handler
        $(".create-buttons .add-content").on("click", entityInstance.addContentButtonHandler);

        //add optional content handlers
        $("#add-description-button").on("click", entityInstance.descriptionCardHandler);
        $("#add-background-button").on("click", entityInstance.backgroundCardHandler);
        $("#add-stakeholder-button").on("click", entityInstance.stakeholderCardHandler);
        $("#add-existing-solution-button").on("click", entityInstance.existingSolutionCardHandler);
        $("#add-market-analysis-button").on("click", entityInstance.marketAnalysisCardHandler);
        $("#add-other-information-button").on("click", entityInstance.otherInformationCardHandler);
        $("#add-attachment-button").on("click", entityInstance.attachmentCardHandler);

        var pitch = new gravit8.Pitch();
        $("#submit").on("click", pitch.createNewPitch);

        //back button handler
        $(".back-submit .back-button button").on("click", function() {
            history.go(-1);
        });
    }

    //route: /create/project
    else if (/^\/create\/project/i.test(route)) {
        //ping server every 10 mins to keep the session alive
        window.setInterval(function() {
            $.ajax({url: "/ping"});
        }, 600000);

        textareaHandler();

        $(".add-team-member-button button").on("click", entityInstance.addTeamMemberButtonHandler);
        $(".add-role-button button").on("click", null, {includeRoleAssignee: true}, entityInstance.addAssignableRoleButtonHandler);

        //enable adding attachments
        var uploadedAttachmentFormTemplate = _.template($("#uploaded-attachment-template").html());
        var attachmentForm = $("#attachment-section-body");
        var attachmentList = $("#attachments");
        attachmentForm.find(".upload-button input[type = 'file']").on("change", function(event) {
            if (event.target.files && event.target.files.length > 0) {
                for (var index = 0; index < event.target.files.length; index++) {
                    var attachment = event.target.files[index];
                    var attachmentHandler = new gravit8.AttachmentHandler(attachment);
                    var referenceHandler = {
                        attachmentHandler: attachmentHandler
                    };

                    var data = {
                        attachment: attachment,
                        formattedSize: utilities.formatBytes(attachment.size)
                    };
                    var anAttachment = $(uploadedAttachmentFormTemplate(data));
                    anAttachment.find(".cancel-remove-button button").on("click", null, referenceHandler, function(event) {
                        event.data.attachmentHandler.cancel();
                        $(event.target).closest(".entity-content").remove();
                    });
                    anAttachment.find(".pause-button button").on("click", null, referenceHandler, function(event) {
                        var pauseButton = $(event.target);
                        pauseButton.closest(".pause-button").addClass("hidden");
                        pauseButton.closest(".entity-content").find(".resume-button").removeClass("hidden");
                        event.data.attachmentHandler.pause();
                    });
                    anAttachment.find(".resume-button button").on("click", null, referenceHandler, function(event) {
                        var resumeButton = $(event.target);
                        resumeButton.closest(".resume-button").addClass("hidden");
                        resumeButton.closest(".entity-content").find(".pause-button").removeClass("hidden");
                        event.data.attachmentHandler.resume();
                    });
                    attachmentList.append(anAttachment);

                    //attachments that are larger than the size limit will be skipped
                    if (attachment.size > (100 * 1024 * 1024)) {
                        anAttachment.find(".status").text("(" + data.formattedSize + ") This attachment is too large to upload onto Gravit8 - it will be skipped. If it is a video, try uploading it onto Youtube, Vimeo, etc. and document the link.");
                        return;
                    }

                    attachmentHandler.setUITemplate(anAttachment);
                    attachmentHandler.setUserControl($(".user-controls"));
                    attachmentHandler.start();
                }

                //allow the same file(s) to be selected again sequentially
                event.target.value = null;
            }
        });
        attachmentForm.find(".upload-button button").on("click", function(event) {
            $(event.target).siblings("input[type = 'file']").trigger("click");
        });

        //if the back button is pressed, make sure any uploaded attachments are discarded from the server
        $(".user-controls .back-button").find("a, button").on("click", function(event) {
            $("#attachments [data-attachment-id]").each(function(index, attachment) {
                var attachmentId = +attachment.getAttribute("data-attachment-id");
                if (attachmentId > 0) {
                    $.ajax({
                        type: "POST",
                        url: "/remove/attachment",
                        data: utilities.formatJSON({attachmentId: attachmentId}),
                        success: function(response) {
                            if (response.success) {
                                //then what?
                            }
                        },
                        dataType: "json"
                    });
                }
            });
        });

        //add content button handler
        $(".create-buttons .add-content").on("click", entityInstance.addContentButtonHandler);

        //add optional content handlers
        $("#add-description-button").on("click", entityInstance.descriptionCardHandler);
        $("#add-background-button").on("click", entityInstance.backgroundCardHandler);
        $("#add-stakeholder-button").on("click", entityInstance.stakeholderCardHandler);
        $("#add-existing-solution-button").on("click", entityInstance.existingSolutionCardHandler);
        $("#add-market-analysis-button").on("click", entityInstance.marketAnalysisCardHandler);
        $("#add-other-information-button").on("click", entityInstance.otherInformationCardHandler);
        // $("#add-attachment-button").on("click", entityInstance.attachmentCardHandler);

        //submit the new project
        var project = new gravit8.Project();
        $("#submit").on("click", project.createNewProject);
    }

    //route: /idea/(id)
    else if (/^\/idea\/(\d+)/i.test(route)) {
        var idea = new gravit8.Idea();

        textareaHandler();

        //keep track of initial values to use for differing prior to sending the updates to the server
        var initialEntityData = entityInstance.dataModels.initialEntityData($("#edit-idea"));

        //like button and like counter handling
        $(".reaction-buttons .like-icon").on("click", likeHandler.likeEntity);

        //constant data refreshing
        var entityRefreshThrottlingId = window.setInterval(idea.refreshIdea, CARD_REFRESH_PERIOD);

        //comment handling
        $("#comment-control button").on("click", commentHandler.postComment);
        var infiniteScrollThrottlingId = window.setInterval(infiniteScrollHandler, SCROLLBAR_DETECTION_PERIOD, commentHandler.loadOldComments);
        var updateCommentThrottlingId = window.setInterval(commentHandler.loadNewComments, COMMENT_REFRESH_PERIOD);
        commentHandler.loadNewComments();

        //switch to edit view
        $("#edit-entity-button").on("click", function(event) {
            //switch over to published idea view
            $("#published-idea").addClass("hidden");
            $("#edit-idea").removeClass("hidden");

            textareaHandler();

            //switch over to edit idea button
            $(this).parent().addClass("hidden");
            $(".user-controls .back-submit").removeClass("hidden");

            window.clearInterval(entityRefreshThrottlingId);
        });

        //switch to public view
        $("#cancel-edit-idea-button").on("click", function(event) {
            //switch over to published idea view
            $("#edit-idea").addClass("hidden");
            $("#published-idea").removeClass("hidden");

            textareaHandler();

            //switch over to edit idea button
            $("#edit-entity-button").parent().removeClass("hidden");
            $(".user-controls .back-submit").addClass("hidden");

            entityRefreshThrottlingId = window.setInterval(idea.refreshIdea, CARD_REFRESH_PERIOD);
        });

        entityInstance.bindHandlersToEditForm($("#edit-idea-form"));

        //add entity content modules
        $(".create-buttons .add-content").on("click", entityInstance.addContentButtonHandler);

        //add optional content handlers
        $("#add-description-button").on("click", entityInstance.descriptionCardHandler);
        $("#add-background-button").on("click", entityInstance.backgroundCardHandler);
        $("#add-stakeholder-button").on("click", entityInstance.stakeholderCardHandler);
        $("#add-existing-solution-button").on("click", entityInstance.existingSolutionCardHandler);
        $("#add-market-analysis-button").on("click", entityInstance.marketAnalysisCardHandler);
        $("#add-other-information-button").on("click", entityInstance.otherInformationCardHandler);
        $("#add-attachment-button").on("click", entityInstance.attachmentCardHandler);

        //submit changes
        $("#submit-edits").on("click", null, {initialEntityData: initialEntityData}, idea.updateIdeaContents);
    }

    //route: /pitch/(id)
    else if (/^\/pitch\/(\d+)/i.test(route)) {
        var pitch = new gravit8.Pitch();

        textareaHandler();

        //keep track of initial values to use for differing prior to sending the updates to the server
        var initialEntityData = entityInstance.dataModels.initialEntityData($("#edit-pitch"));

        //constant data refreshing
        var entityRefreshThrottlingId = window.setInterval(pitch.refreshPitch, CARD_REFRESH_PERIOD);

        //comment handling
        $("#comment-control button").on("click", commentHandler.postComment);
        var infiniteScrollThrottlingId = window.setInterval(infiniteScrollHandler, SCROLLBAR_DETECTION_PERIOD, commentHandler.loadOldComments);
        var updateCommentThrottlingId = window.setInterval(commentHandler.loadNewComments, COMMENT_REFRESH_PERIOD);
        commentHandler.loadNewComments();

        //like button and like counter handling
        $(".reaction-buttons .like-icon").on("click", likeHandler.likeEntity);

        //vote button and vote counter handling
        $(".reaction-buttons .vote-icon").on("click", voteHandler.voteEntity);

        //edit pitch handling
        $("#edit-entity-button").on("click", function() {
            //switch edit button to cancel and submit changes buttons
            $(this).parent(".closed-button")
                   .add(".like-button") //TODO FIX: this should be .closed-button to generalize, but it also targets the children of .back-submit
                   .addClass("hidden")
                   .siblings(".back-submit")
                   .removeClass("hidden");

            //switch to edit pitch page
            $("#published-pitch").addClass("hidden");
            $("#edit-pitch").removeClass("hidden");

            textareaHandler();

            window.clearInterval(entityRefreshThrottlingId);
        });

        $("#cancel-edit-entity-button").on("click", function() {
            //switch cancel and submit changes buttons to edit button
            $(this).parents(".back-submit")
                   .addClass("hidden")
                   .siblings(".closed-button")
                   .removeClass("hidden");

            //switch to publish pitch page
            $("#edit-pitch").addClass("hidden");
            $("#published-pitch").removeClass("hidden");

            textareaHandler();

            //constant data refreshing
            var entityRefreshThrottlingId = window.setInterval(pitch.refreshPitch, CARD_REFRESH_PERIOD);
        });

        entityInstance.bindHandlersToEditForm($("#edit-pitch-form"));

        //add roles
        $(".add-role-button button").on("click", entityInstance.addAssignableRoleButtonHandler);

        //add entity content modules
        $(".create-buttons .add-content").on("click", entityInstance.addContentButtonHandler);

        //add optional content handlers
        $("#add-description-button").on("click", entityInstance.descriptionCardHandler);
        $("#add-background-button").on("click", entityInstance.backgroundCardHandler);
        $("#add-stakeholder-button").on("click", entityInstance.stakeholderCardHandler);
        $("#add-existing-solution-button").on("click", entityInstance.existingSolutionCardHandler);
        $("#add-market-analysis-button").on("click", entityInstance.marketAnalysisCardHandler);
        $("#add-other-information-button").on("click", entityInstance.otherInformationCardHandler);
        $("#add-attachment-button").on("click", entityInstance.attachmentCardHandler);

        //submit changes
        $("#submit-edits").on("click", null, {initialEntityData: initialEntityData}, pitch.updatePitchContents);
    }

    //route: /project/(id)
    else if (/^\/project\/(\d+)/i.test(route)) {
        var project = new gravit8.Project();

        textareaHandler();

        //keep track of initial values to use for differing prior to sending the updates to the server
        var initialEntityData = entityInstance.dataModels.initialEntityData($("#edit-project"));

        //like button and like counter handling
        $(".reaction-buttons .like-icon").on("click", likeHandler.likeEntity);

        //vote button and vote counter handling
        $(".reaction-buttons .vote-icon").on("click", voteHandler.voteEntity);

        $(".user-controls .like-button button").on("click", likeHandler.likeEntity);

        //constant data refreshing
        var entityRefreshThrottlingId = window.setInterval(project.refreshProject, CARD_REFRESH_PERIOD);

        //gallery functionality
        $("#gallery-container li:not(:first-child)").addClass("hidden");
        $("#gallery-selector li:first-child").addClass("selected");

        $("#gallery-selector li").on("click", function(event) {
            var oldSelector = $("#gallery-selector li.selected");
            var newSelector = $(event.target);
            var oldSelectorIndex = $("#gallery-selector li").index(oldSelector) + 1;
            var newSelectorIndex = $("#gallery-selector li").index(newSelector) + 1;

            oldSelector.removeClass("selected");
            newSelector.addClass("selected");
            
            $("#gallery-container li:nth-child(" + oldSelectorIndex + ")").addClass("hidden");
            $("#gallery-container li:nth-child(" + newSelectorIndex + ")").removeClass("hidden");
        });

        //comment handling
        $("#comment-control button").on("click", commentHandler.postComment);
        var infiniteScrollThrottlingId = window.setInterval(infiniteScrollHandler, SCROLLBAR_DETECTION_PERIOD, commentHandler.loadOldComments);
        var updateCommentThrottlingId = window.setInterval(commentHandler.loadNewComments, COMMENT_REFRESH_PERIOD);
        commentHandler.loadNewComments();

        //edit project handling
        $("#edit-entity-button").on("click", function() {
            //switch edit button to cancel and submit changes buttons
            $(this).parent(".closed-button")
                   .add(".like-button") //TODO FIX: this should be .closed-button to generalize, but it also targets the children of .back-submit
                   .addClass("hidden")
                   .siblings(".back-submit")
                   .removeClass("hidden");

            //switch to edit project page
            $("#published-project").addClass("hidden");
            $("#edit-project").removeClass("hidden");

            textareaHandler();

            window.clearInterval(entityRefreshThrottlingId);
        });

        $("#cancel-edit-entity-button").on("click", function() {
            //switch cancel and submit changes buttons to edit button
            $(this).parents(".back-submit")
                   .addClass("hidden")
                   .siblings(".closed-button")
                   .removeClass("hidden");

            //switch to publish project page
            $("#edit-project").addClass("hidden");
            $("#published-project").removeClass("hidden");

            textareaHandler();

            //constant data refreshing
            var entityRefreshThrottlingId = window.setInterval(project.refreshProject, CARD_REFRESH_PERIOD);
        });

        entityInstance.bindHandlersToEditForm($("#edit-project-form"));

        //add team members and roles
        $(".add-team-member-button button").on("click", entityInstance.addTeamMemberButtonHandler);
        $(".add-role-button button").on("click", null, {includeRoleAssignee: true}, entityInstance.addAssignableRoleButtonHandler);

        //add entity content modules
        $(".create-buttons .add-content").on("click", entityInstance.addContentButtonHandler);

        //add optional content handlers
        $("#add-description-button").on("click", entityInstance.descriptionCardHandler);
        $("#add-background-button").on("click", entityInstance.backgroundCardHandler);
        $("#add-stakeholder-button").on("click", entityInstance.stakeholderCardHandler);
        $("#add-existing-solution-button").on("click", entityInstance.existingSolutionCardHandler);
        $("#add-market-analysis-button").on("click", entityInstance.marketAnalysisCardHandler);
        $("#add-other-information-button").on("click", entityInstance.otherInformationCardHandler);
        // $("#add-attachment-button").on("click", entityInstance.attachmentCardHandler);

        //submit changes
        $("#submit-edits").on("click", null, {initialEntityData: initialEntityData}, project.updateProjectContents);
    }

    //route: /people/(id)
    else if (/^\/people\/(?:\d+)/i.test(route)) {
        var contactTypes;
        var defaultContactTypeId;
        $.ajax({
            type: "GET",
            url: "/get/list/contact-types",
            success: function(response) {
                contactTypes = response.data.contactTypes;
                defaultContactTypeId = response.data.contactTypes[0].id;
            },
            dataType: "json"
        });

        var personId = +route.match(/\/(\d+)/).pop();
        var loadMoreOldPortfolioHandler = function(event) {
            //TODO: to be implemented
        };

        var ideaCardTemplate = _.template($("#idea-card-template").html());
        var pitchCardTemplate = _.template($("#pitch-card-template").html());
        var projectCardTemplate = _.template($("#project-card-template").html());
        var loadMoreNewPortfolioHandler = function(event) {
            var person = {
                "id": personId
            };

            $.ajax({
                type: "GET",
                url: "/get/portfolios",
                data: utilities.formatJSON(person),
                success: function(response) {
                    if (response.data.started.length > 0) {
                        var portfolios = response.data.started.map(function(entity) {
                            var entityCard;
                            switch (entity.entityType) {
                                case "idea":
                                    entityCard = $(ideaCardTemplate({idea: entity}));
                                    // entityCard.find(".reaction-buttons .like-icon").on("click", likeHandler.likeCard);
                                    // entityCard.find(".reaction-buttons .vote-icon").on("click", voteHandler.voteCard);
                                    break;

                                case "pitch":
                                    entityCard = $(pitchCardTemplate({pitch: entity}));
                                    // entityCard.find(".reaction-buttons .like-icon").on("click", likeHandler.likeCard);
                                    // entityCard.find(".reaction-buttons .vote-icon").on("click", voteHandler.voteCard);
                                    break;

                                case "project":
                                    entityCard = $(projectCardTemplate({entity: entity}));
                                    // entityCard.find(".reaction-buttons .like-icon").on("click", likeHandler.likeCard);
                                    // entityCard.find(".reaction-buttons .vote-icon").on("click", voteHandler.voteCard);
                                    break;
                            }
                            return entityCard;
                        });

                        $("#published-profile .profile-portfolio .item-container.invisible:first").before(portfolios);

                        //swap warning message and portfolio cards
                        $("#published-profile .profile-portfolio .warning-message").addClass("hidden");
                        $("#published-profile .profile-portfolio .items-container").removeClass("hidden");
                    } else {
                        //swap warning message and portfolio cards
                        $("#published-profile .profile-portfolio .warning-message").removeClass("hidden");
                        $("#published-profile .profile-portfolio .items-container").addClass("hidden");
                    }
                },
                dataType: "json"
            });
        };

        //setup infinite scroll handling
        // var infiniteScrollThrottlingId = window.setInterval(infiniteScrollHandler, SCROLLBAR_DETECTION_PERIOD, loadMoreOldPortfolioHandler);
        loadMoreNewPortfolioHandler();

        //changing between publish and edit views
        $("#edit-profile-button").on("click", function(event) {
            //switch over to edit profile view
            $("#edit-profile").removeClass("hidden");
            $("#published-profile").addClass("hidden");

            //switch over to back-submit buttons
            $(this).parent().addClass("hidden");
            $(".user-controls .back-submit").removeClass("hidden");
        });

        $("#cancel-edit-profile-button").on("click", function(event) {
            //switch over to published profile view
            $("#edit-profile").addClass("hidden");
            $("#published-profile").removeClass("hidden");

            //switch over to edit profile button
            $("#edit-profile-button").parent().removeClass("hidden");
            $(".user-controls .back-submit").addClass("hidden");
        });

        //add new profile attribute
        $("#edit-profile .add-content").on("click", entityInstance.addContentButtonHandler);

        var removeButtonHandler = function() {
            $(this).parents(".profile-attribute").remove();
        };

        var removedProfileAttributes = {};
        $("#edit-profile-form .cancel-remove-button button").on("click", function(event) {
            var field = $(this).parents(".profile-attribute").find("textarea[data-profile-attribute-id]");
            var fieldName = field.attr("name");
            var profileAttributeId = +field.attr("data-profile-attribute-id");

            //check if biography is removed
            if (fieldName === "biography") {
                $("#edit-profile #add-profile-biography-button")[0].removeAttribute("data-module-disabled");
                $("#edit-profile #add-profile-biography-button").removeClass("disabled-module");
            }

            //keep a record of what profile attribute has been removed
            var profileAttribute = {"id": profileAttributeId};
            switch (fieldName) {
                case "biography":
                    removedProfileAttributes["biography"] = profileAttribute;
                    break;

                case "contact":
                    if (!("contacts" in removedProfileAttributes)) {
                        removedProfileAttributes["contacts"] = [profileAttribute];
                    } else {
                        removedProfileAttributes["contacts"].push(profileAttribute);
                    }
                    break;
            }
        }).on("click", removeButtonHandler);

        //detect changes to fields
        var newProfileAttributeDetectionHandler = function(event) {
            if (!event.target.hasAttribute("data-content-added")) {
                event.target.setAttribute("data-content-added", "");
            }
        };
        var editDetectionHandler = function(event) {
            if (!event.target.hasAttribute("data-content-edited")) {
                event.target.setAttribute("data-content-edited", "");
            }
        };
        $("#edit-profile-form textarea").on("keydown paste", editDetectionHandler);
        $("#edit-profile-form select[name = 'categories']").on("change", function() {
            $(this).parent("form").siblings("textarea").attr("data-content-edited", "");
        });

        //add new biography
        //TODO: a user should only be able to have one biography at all time
        $("#edit-profile #add-profile-biography-button").on("click", function(event) {
            if (!this.hasAttribute("data-module-disabled")) {
                this.setAttribute("data-module-disabled", "");
                $(this).addClass("disabled-module");

                //create the biography form template and attach event handlers to it
                var rawTemplate = $("#add-profile-biography-form-template").html();
                var profileBiographyForm = $(_.template(rawTemplate)());
                profileBiographyForm.find("textarea").on("keydown paste", newProfileAttributeDetectionHandler);
                profileBiographyForm.find(".cancel-remove-button button").on("click", null, this, function(event) {
                    event.data.removeAttribute("data-module-disabled");
                    $(event.data).removeClass("disabled-module");
                }).on("click", removeButtonHandler);

                //add the biography form to the DOM
                $("#edit-profile-form ~ .create-buttons").before(profileBiographyForm);
            }
        });

        //add new contact
        $("#edit-profile #add-profile-contact-button").on("click", function(event) {
            //create the contact form template and attach event handlers to it
            var rawTemplate = $("#add-profile-contact-form-template").html();
            var template = _.template(rawTemplate);
            var profileContactForm = $(template({contactTypes: contactTypes}));
            profileContactForm.find("textarea").on("keydown paste", newProfileAttributeDetectionHandler);
            profileContactForm.find(".cancel-remove-button button").on("click", removeButtonHandler);

            //NOTE: Joey, what does this random regex supposed to do?
            // profileContactForm.find("textarea").on("keydown paste", function(event) {
            //     var url = $(this).val();
            //     var match = /^([a-z][a-z0-9\*\-\.]*):\/\/(?:(?:(?:[\w\.\-\+!$&'\(\)*\+,;=]|%[0-9a-f]{2})+:)*(?:[\w\.\-\+%!$&'\(\)*\+,;=]|%[0-9a-f]{2})+@)?(?:(?:[a-z0-9\-\.]|%[0-9a-f]{2})+|(?:\[(?:[0-9a-f]{0,4}:)*(?:[0-9a-f]{0,4})\]))(?::[0-9]+)?(?:[\/|\?](?:[\w#!:\.\?\+=&@!$'~*,;\/\(\)\[\]\-]|%[0-9a-f]{2})*)?$/;
            //     var protomatch = /^(https?|ftp):\/\/(.*)/;


            //     if (match.test(url)) {
            //     console.log(url + ' is valid');

            //     // if valid replace http, https, ftp etc with empty
            //     var b = url.replace(protomatch.test(url), '');
            //     console.log(b);

            //     } else { // IF INVALID
            //         console.log('not valid');
            //     }
            // });

            //add the contact form to the DOM
            $("#edit-profile-form ~ .create-buttons").before(profileContactForm);
        });



        //upload photo with upload button
        $("#upload-profile-image-button").on("click", function() {
            $(this).siblings("input[type = 'file']").click();
        });

        $("#upload-profile-image-button ~ input[type = 'file']").on("change", function() {
            if (this.files.length > 0) {
                prepareImageForUpload(this.files[0]);
            }
        });

        //drag and drop photo upload
        // $("#edit-profile-form div.section-body[data-dropzone]").on("drag", function(event) {
        //     event.preventDefault();
        //     console.log(event.type);
        //     // $(this).addClass("dropzone");

        // }).on("dragstart dragenter", function(event) {
        //     console.log(event.type);
        //     $(this).addClass("dropzone");

        // }).on("dragend", function(event) {
        //     console.log(event.type);
        //     $(this).removeClass("dropzone");

        // }).on("drop", function(event) {
        //     event.preventDefault();
        //     console.log(event.type);
        //     if (event.dataTransfer.files.length > 0) {
        //         prepareImageForUpload(event.dataTransfer.files[0]);
        //     }
        // });

        var MAX_IMAGE_SIZE = Math.pow(2, 17);    //128kb
        var prepareImageForUpload = function(image) {
            //file should be an image
            if (!/image.*/.test(image.type)) {
                //show warning
                return;
            }

            var reader = new FileReader();
            reader.onload = function(event) {
                var imageDataURL = event.target.result;

                //image should be within a certain size (128kb)
                if (image.size > MAX_IMAGE_SIZE) {
                    downSampleImage(imageDataURL, image.type);
                } else {
                    updateProfileImagePreview(imageDataURL);
                }
            };
            reader.readAsDataURL(image);
        };

        var downSampleImage = function(dataURL, imageType) {
            //NOTE: Bilinear interpolation - http://stackoverflow.com/questions/19262141/resize-image-with-javascript-canvas-smoothly
            var img = document.createElement("img");
            img.onload = function() {
                var canvas = document.createElement("canvas");
                var context = canvas.getContext("2d");

                if (img.width > img.height) {
                    canvas.width = Math.min(300, img.width);
                    canvas.height = canvas.width * img.height / img.width;
                } else {
                    canvas.height = Math.min(300, img.height);
                    canvas.width = canvas.height * img.width / img.height;
                }

                var virtualCanvas = document.createElement("canvas");
                var virtualContext = virtualCanvas.getContext("2d");
                virtualCanvas.width = 0.5 * img.width;
                virtualCanvas.height = 0.5 * img.height;
                virtualContext.drawImage(img, 0, 0, virtualCanvas.width, virtualCanvas.height);
                virtualContext.drawImage(virtualCanvas, 0, 0, 0.5 * virtualCanvas.width, 0.5 * virtualCanvas.height);

                context.drawImage(virtualCanvas, 0, 0, 0.5 * virtualCanvas.width, 0.5 * virtualCanvas.height, 0, 0, canvas.width, canvas.height);
                var imageDataURL = canvas.toDataURL(imageType);

                updateProfileImagePreview(imageDataURL);
            };
            img.src = dataURL;
        };

        var updateProfileImagePreview = function(imageDataURL) {
            $("#edit-profile-image-initials").addClass("hidden");
            $("#edit-profile-image-preview").css("background-image", "url(" + imageDataURL + ")")
                                            .removeClass("hidden");
            $("#edit-profile-image-preview ~ .button input[name = 'image']").attr("data-content-edited", "")
                                                                            .attr("value", imageDataURL);
        };

        //submit changes
        $("#edit-profile-submission").on("click", function(event) {
            var personId = +route.match(/\/(\d+)/).pop();
            if (personId > 0) {
                //form validation - a person should have a name
                var firstName = $("#edit-profile-form textarea[name = 'first-name']").val().trim();
                if (_.isEmpty(firstName) || $("#edit-profile-form textarea[name = 'first-name']").hasClass("required")) {
                    $("#edit-profile-form textarea[name = 'first-name']").on("keypress paste", function() {
                        $(this).off("keypress paste")
                               .on("paste", editDetectionHandler)
                               .removeClass("required")
                               .next().next(".invalid-message")
                               .addClass("hidden");
                    }).addClass("required")
                      .focus()
                      .next().next(".invalid-message")
                      .removeClass("hidden");
                    return;
                }

                //package the new and edited fields in a consistent packaging scheme here
                var profileAttributes = {
                    edited: {},
                    removed: removedProfileAttributes,
                    added: {}
                };

                //package the edited profile attributes
                $("#edit-profile-form [data-content-edited]").each(function(index, editedProfileAttribute) {
                    var field = $(editedProfileAttribute);
                    var fieldName = field.attr("name");

                    switch (fieldName) {
                        case "first-name":
                        case "last-name":
                        case "title":
                            profileAttributes["edited"][fieldName] = field.val().trim();
                            break;

                        case "image":
                            profileAttributes["edited"][fieldName] = field.attr("value");
                            break;

                        case "biography":
                            var biography = {
                                "id": +field.attr("data-profile-attribute-id"),
                                "value": field.val().trim()
                            };
                            profileAttributes["edited"][fieldName] = biography;
                            break;

                        case "contact":
                            var contactTypeId = field.siblings("form").find("select option:selected").val();
                            var contact = {
                                "id": +field.attr("data-profile-attribute-id"),
                                "value": field.val().trim(),
                                "contactTypeId": contactTypeId
                            };

                            if (!("contacts" in profileAttributes["edited"])) {
                                profileAttributes["edited"]["contacts"] = [contact];
                            } else {
                                profileAttributes["edited"]["contacts"].push(contact);
                            }
                    }
                });

                //package the new profile attributes
                $("#edit-profile .profile-attribute textarea[data-content-added]").each(function(index, addedProfileAttribute) {
                    var field = $(addedProfileAttribute);
                    switch (field.attr("name")) {
                        case "biography":
                            profileAttributes["added"]["biography"] = field.val().trim();
                            break;

                        case "contact":
                            var contactTypeId = field.siblings("form").find("select option:selected").val();
                            var contact = {
                                "contactTypeId": _.isEmpty(contactTypeId) ? defaultContactTypeId : contactTypeId,
                                "value": field.val().trim()
                            };

                            if (!("contacts" in profileAttributes["added"])) {
                                profileAttributes["added"]["contacts"] = [contact];
                            } else {
                                profileAttributes["added"]["contacts"].push(contact);
                            }
                            break;
                    }
                });

                //submit profile changes
                if (_.isEmpty(profileAttributes.edited) && _.isEmpty(profileAttributes.removed) && _.isEmpty(profileAttributes.added)) {
                    window.location.href = route;
                } else {
                    var profile = {
                        "id": personId,
                        "attributes": profileAttributes
                    };

                    $.ajax({
                        type: "POST",
                        url: "/update/profile",
                        data: utilities.formatJSON(profile),
                        success: function(response) {
                            if (response.success) {
                                window.location.href = route;
                            } else {
                                //inform user something went wrong?
                            }
                        },
                        dataType: "json"
                    });
                }
            } else {
                //can't get the person id
            }
        });
    }

    //route: /help
    else if (/^\/help/i.test(route)) {
        var tabHandler = new TabHandler();

        //tabs handler
        $("#help-header a").on("click", function(event) {
            var section = "#" + event.target.getAttribute("data-name");
            tabHandler.goToByScroll(section);

            $("#help-header a.selected").removeClass("selected");
            $(event.target).addClass("selected");
        });

        //back button handler
        $(".back-submit .back-button button").on("click", function() {
            history.go(-1);
        });

        //go to initial section
        var initialSection = "#" + $("#help-header a.selected").attr("data-name");
        tabHandler.goToByScroll(initialSection);
    }

    //navigation bar handling
    $(".navigation-bar #navigation-sidebar-icon").on("click tap touchstart", function(event) {
        event.preventDefault();
        $(event.target).toggleClass("selected");
        $("#navigation-sidebar").toggleClass("hidden");
    });
});