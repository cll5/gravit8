var SortHandler = (function() {
    var sortingMethods = new SortingMethods();

    //merge sort implementation: http://stackoverflow.com/questions/1427608/fast-stable-sorting-algorithm-implementation-in-javascript
    var mergeSort = function(array, comparator) {
        if (!comparator) {
            comparator = function(a, b) {
                if (a < b) {
                    return -1;
                } else if (a > b) {
                    return 1;
                } else {
                    return 0;
                }
            };
        }

        return __mergeSort(array, 0, array.length, comparator);
    };

    var __mergeSort = function(array, startIndex, endIndex, comparator) {
        var arrayLength = endIndex - startIndex;
        if (arrayLength < 2) {
            return;
        }

        var middleIndex = startIndex + Math.floor(arrayLength / 2);
        __mergeSort(array, startIndex, middleIndex, comparator);
        __mergeSort(array, middleIndex, endIndex, comparator);
        __mergeArrays(array, startIndex, middleIndex, endIndex, comparator);

        return array;
    };

    var __mergeArrays = function(array, startIndex, middleIndex, endIndex, comparator) {
        for (; startIndex < middleIndex; ++startIndex) {
            if (comparator(array[startIndex], array[middleIndex]) > 0) {
                var temporary = array[startIndex];
                array[startIndex] = array[middleIndex];
                __insert(array, middleIndex, endIndex, temporary, comparator);
            }
        }
    };

    Array.prototype.swap = function(a, b) {
        var temporary = this[a];
        this[a] = this[b];
        this[b] = temporary;
    };

    var __insert = function(array, startIndex, endIndex, temporary, comparator) {
        while ((startIndex + 1 < endIndex) && (comparator(array[startIndex + 1], temporary) < 0)) {
            array.swap(startIndex, startIndex + 1);
            ++startIndex;
        }
        array[startIndex] = temporary;
    };

    return {
        mergeSort: mergeSort,
        sortCards: function(cards, sortOption) {
            return mergeSort(cards, sortingMethods.sortBy[sortOption]);
        },
        sortLists: function(sortedCards) {
            return !sortedCards ? [] : sortedCards.map(sortingMethods.sortListByCard);
        },
        sortPeople: function(people, sortOption) {
            return mergeSort(people, sortingMethods.sortBy[sortOption]);
        },
        resort: function() {
            $(".sort-by-options select[name = 'sort-by-options']").not(".hidden").trigger("change");
        }
    };
});

var SortingMethods = (function() {
    var __sortByEntityTitleAlphabetical = function(a, b) {
        var titleA = $(a).find(".name").text().toLowerCase();
        var titleB = $(b).find(".name").text().toLowerCase();
        return (titleA < titleB) ? -1 : (titleA > titleB);
    };

    var __sortByEntityCategory = function(a, b) {
        var categoryA = $(a).find("h4:first").text();
        var categoryB = $(b).find("h4:first").text();
        if (categoryA == categoryB) {
            return __sortByEntityTitleAlphabetical(a, b);
        } else if (categoryA == "selected") {
            return -1;
        } else if (categoryB == "selected") {
            return 1;
        } else if ((categoryA == "selected") && (categoryB == "posted")) {
            return -1;
        } else if ((categoryA == "posted") && (categoryB == "selected")) {
            return 1;
        } else if (categoryA == "presented") {
            return 1;
        } else if (categoryB == "presented") {
            return -1;
        } else if (categoryA < categoryB) {
            return -1;
        } else if (categoryA > categoryB) {
            return 1;
        }
    };

    var __sortByMostLikedEntity = function(a, b) {
        var likesA = +$(a).find(".reaction-buttons [data-counter-type = 'like']").text();
        var likesB = +$(b).find(".reaction-buttons [data-counter-type = 'like']").text();
        return (likesB - likesA);
    };

    var __sortByMostVotedEntity = function(a, b) {
        var votesA = +$(a).find(".reaction-buttons [data-counter-type = 'vote']").text();
        var votesB = +$(b).find(".reaction-buttons [data-counter-type = 'vote']").text();
        return (votesB - votesA);
    };

    var __sortByMostComments = function(a, b) {
        var commentsA = +$(a).find(".reaction-buttons [data-counter-type = 'comment']").text();
        var commentsB = +$(b).find(".reaction-buttons [data-counter-type = 'comment']").text();
        return (commentsB - commentsA);
    };

    var __sortByNewestEntity = function(a, b) {
        var idA = +a.pathname.match(/\/(\d+)/).pop();
        var idB = +b.pathname.match(/\/(\d+)/).pop();
        return (idB - idA);
    };

    var __sortByPersonNameAlphabetical = function(a, b) {
        var nameA = $(a).find("h6.person-name").text().toLowerCase();
        var nameB = $(b).find("h6.person-name").text().toLowerCase();
        return (nameA < nameB) ? -1 : (nameA > nameB);
    };

    var __sortByNewestPerson = function(a, b) {
        var idA = +a.pathname.match(/\/(\d+)/).pop();
        var idB = +b.pathname.match(/\/(\d+)/).pop();
        return (idB - idA);
    };

    var __sortListByCard = function(card) {
        var entityId = card.getAttribute("data-entity-id");
        return $("#browse-view .items-container").not(".hidden").find(".list-view[data-entity-id = " + entityId + "]").get(0);
    };

    return {
        sortBy: {
            "entity-title-in-alphabetical": __sortByEntityTitleAlphabetical,
            "entity-title-in-reverse": function(a, b) {
                return __sortByEntityTitleAlphabetical(b, a);
            },
            "entity-category": __sortByEntityCategory,
            "most-liked-entity": __sortByMostLikedEntity,
            "least-liked-entity": function(a, b) {
                return __sortByMostLikedEntity(b, a);
            },
            "most-voted-entity": __sortByMostVotedEntity,
            "least-voted-entity": function(a, b) {
                return __sortByMostVotedEntity(b, a);
            },
            "most-commented-entity": __sortByMostComments,
            "least-commented-entity": function(a, b) {
                return __sortByMostComments(b, a);
            },
            "newest-entity": __sortByNewestEntity,
            "oldest-entity": function(a, b) {
                return __sortByNewestEntity(b, a);
            },
            "person-name-in-alphabetical": __sortByPersonNameAlphabetical,
            "person-name-in-reverse": function(a, b) {
                return __sortByPersonNameAlphabetical(b, a);
            },
            "newest-person": __sortByNewestPerson,
            "oldest-person": function(a, b) {
                return __sortByNewestPerson(b, a);
            }
        },
        sortListByCard: __sortListByCard
    };
});