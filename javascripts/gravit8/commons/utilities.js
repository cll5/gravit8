var utilities = (function() {
    var formatBytes = function(attachmentSize) {
        var units = ["B", "kB", "MB", "GB", "TB"];
        var divider = 1024;
        var bytes = attachmentSize;

        var index = 0;
        for (index = 0; (bytes >= divider) && (index < units.length); index++) {
            bytes /= divider;
        }

        var formattedBytes = bytes.toFixed(2) + " " + units[index];
        return formattedBytes;
    };

    var formatJSON = function(dataModel) {
        return {
            "json": JSON.stringify(dataModel)
        };
    };

    return {
        formatBytes: formatBytes,
        formatJSON: formatJSON
    };
})();