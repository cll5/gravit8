var BrowserHistoryHandler = (function() {
    //NOTE: solution is html5 pushState API - https://developer.mozilla.org/en-US/docs/Web/API/History_API
    var pushState = function(route, state) {
        //defaults
        route = route || "#";
        state = state || {};

        if (window.history) {
            window.history.pushState(state, "", route);
        } else {
            //update the url with the a tag's href, but don't trigger page redirect
            //see: http://stackoverflow.com/questions/846954/change-url-and-redirect-using-jquery
            window.location.replace(route);
        }
    };

    var updateState = function(route, state) {
        //defaults
        route = route || "#";
        state = state || {};

        if (window.history) {
            window.history.replaceState(state, "", route);
        }
    };

    return {
        pushState: pushState,
        updateState: updateState
    };
});

var ExploreRouteHandler = (function() {
    var browserHistoryHandler = new BrowserHistoryHandler();

    function createBrowseTabState(selectedTab) {
        return {
            selectedBrowseTab: selectedTab
        };
    }

    var popStateHandler = function(event) {
        var selectedTab = window.history.state.selectedBrowseTab;

        //show appropriate contents and update browse tabs
        $(".browse-tabs a").removeClass("selected")
                            .filter("[data-container = " + selectedTab + "]")
                            .addClass("selected");

        $(".items-container[data-container]").addClass("hidden")
                                            .filter("[data-container = " + selectedTab + "]")
                                            .removeClass("hidden");
    };

    var pushState = function(route, selectedTab) {
        var state = createBrowseTabState(selectedTab);
        browserHistoryHandler.pushState(route, state);
    };

    var updateState = function(route, selectedTab) {
        var state = createBrowseTabState(selectedTab);
        browserHistoryHandler.updateState(route, state);
    };

    return {
        pushState: pushState,
        updateState: updateState,
        popStateHandler: popStateHandler
    };
});