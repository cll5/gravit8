gravit8.AttachmentHandler = function(attachment, entityId) {
    this.entityId = +entityId;
    this.attachment = attachment;

    //setting up some options
    this.PARTIAL_ATTACHMENT_SIZE = 512 * 1024;
    this.rangeStart = 0;
    this.rangeEnd = this.PARTIAL_ATTACHMENT_SIZE;
    this.totalParts = Math.max(Math.ceil(this.attachment.size / this.PARTIAL_ATTACHMENT_SIZE), 1);
    this.sliceMethod = "slice";
    if ("mozSlice" in this.attachment) {
        this.sliceMethod = "mozSlice";
    } else if ("webkitSlice" in this.attachment) {
        this.sliceMethod = "webkitSlice";
    }

    //resume/pause upload control flag
    this.isPaused = false;

    //for the AJAX request
    this.uploadRequest = new XMLHttpRequest();
    this.uploadRequest.responseType = "json";
    this.uploadRequest.timeout = 5000;
    this.numberOfRetries = 5;   //number of retries if upload fails for whatever reason

    //checksum to detect for data corruption during attachment transfer between client and server
    this.hash = new Rusha();
    this.hash.resetState();
    this.checksum = null;
    this.partialChecksum = null;

    //placeholder identifier for the attachment until a proper attachment identifier is created from the server
    this.attachmentId = "";

    //keep track of the corresponding UI element
    this.uiTemplate = null;
    this.userControl = null;
};

gravit8.AttachmentHandler.prototype = {
    __allUploadsFinished: function() {
        //check if all attachments are done uploading
        return ($("#attachments [data-uploading]").length === 0);
    },
    __updateChecksum: function() {
        //update the checksum with the current chunk of data
        // var self = this;

        // var fileReader =  new FileReader();
        // fileReader.onload = function(event) {
        //     var partialAttachmentAsArrayBuffer = event.target.result;
        //     self.hash.append(partialAttachmentAsArrayBuffer);
        //     self.checksum = self.hash.digestFromArrayBuffer(partialAttachmentAsArrayBuffer);
        //     console.log(self.checksum + "\n");
        //     //should end with 3bfd8311b36ecb329589e4baf9aa9fe7be6f37a6
        // };

        // var partialAttachment = self.attachment[self.sliceMethod](self.rangeStart, self.rangeEnd);
        // fileReader.readAsArrayBuffer(partialAttachment);
    },
    __updateUploadProgress: function() {
        var percentage = (this.rangeEnd / this.attachment.size) * 100;
        if (this.attachment.size === 0) {
            percentage = 100;
        }
        var status = percentage.toFixed(2) + "% (" + utilities.formatBytes(this.rangeEnd) + " / " + utilities.formatBytes(this.attachment.size) + ")";
        this.uiTemplate.find(".status").html(status);
    },
    __getAttachmentExtension: function() {
        var candidateExtension = this.attachment.name.match(/\.(\w+)$/);
        var extension = (candidateExtension && (candidateExtension.length > 1)) ? candidateExtension[1] : "";
        return extension;
    },
    __retryToUpload: function() {
        if (this.numberOfRetries > 0) {
            var self = this;
            setTimeout(function() {
                self.numberOfRetries -= 1;
                self.__upload();
            }, 5000);
        } else {
            //number of retries is exhausted, then what?
            this.pause();
            this.uiTemplate.removeAttr("data-uploading");
        }
    },
    __upload: function() {
        var self = this;
        var chunk;

        //this is what happens when the partial attachment is received by the server
        self.uploadRequest.onload = function(event) {
            if ((event.target.status === 200) && (event.target.response.success)) {
                var response = event.target.response.data;

                //update the attachment id for the UI
                if (!self.attachmentId && response.attachmentId) {
                    self.attachmentId = response.attachmentId;
                    self.uiTemplate.attr("data-attachment-id", response.attachmentId);
                }

                //update UI: update upload progress
                self.__updateUploadProgress();

                if (self.rangeEnd === self.attachment.size) {
                    self.__onUploadComplete();
                    return;
                }

                self.rangeStart = self.rangeEnd;
                self.rangeEnd += self.PARTIAL_ATTACHMENT_SIZE;
                if (!self.isPaused) {
                    self.__upload();
                }
            } else {
                self.__retryToUpload();
            }
        };

        self.uploadRequest.onerror = function(event) {
            self.__retryToUpload();
        };
        self.uploadRequest.ontimeout = function(event) {
            self.__retryToUpload();
        };
        self.uploadRequest.onabort = function(event) {};

        //prepare the next partial attachment to upload
        //prevent index out of bound
        if (self.rangeEnd > self.attachment.size) {
            self.rangeEnd = self.attachment.size;
        }

        self.part = Math.max(Math.ceil(self.rangeEnd / self.PARTIAL_ATTACHMENT_SIZE), 1);
        var partialAttachment = self.attachment[self.sliceMethod](self.rangeStart, self.rangeEnd);

        //TODO: use a web worker to read the file in chunks?
        //update the checksum with the current chunk of data
        var fileReader =  new FileReader();
        fileReader.onload = function(event) {
            //update checksums
            var partialAttachmentAsArrayBuffer = event.target.result;

            var rusha = new Rusha();
            self.partialChecksum = rusha.digestFromArrayBuffer(partialAttachmentAsArrayBuffer);

            //progressively update the checksum
            self.hash.append(partialAttachmentAsArrayBuffer);
            if (self.rangeEnd === self.attachment.size) {
                self.checksum = self.hash.end();
            }

            setTimeout(function() {
                // self.uploadRequest.open("PUT", "/", true);
                self.uploadRequest.open("POST", "/submit/attachment", true);

                //we ought to upload the partial attachment as a binary file
                self.uploadRequest.overrideMimeType("application/octet-stream");

                //include information about this partial attachment to the HTTP header
                self.uploadRequest.setRequestHeader("Content-Attachment-Name", self.attachment.name);
                self.uploadRequest.setRequestHeader("Content-Attachment-Extension", self.__getAttachmentExtension());
                self.uploadRequest.setRequestHeader("Content-Attachment-Type", self.attachment.type);
                self.uploadRequest.setRequestHeader("Content-Attachment-Size", self.attachment.size.toString());
                self.uploadRequest.setRequestHeader("Content-Attachment-Checksum", self.checksum);
                self.uploadRequest.setRequestHeader("Content-Attachment-Id", self.attachmentId);
                self.uploadRequest.setRequestHeader("Content-Attachment-Part", self.part.toString());
                self.uploadRequest.setRequestHeader("Content-Attachment-Partial-Checksum", self.partialChecksum);
                self.uploadRequest.setRequestHeader("Content-Attachment-Total-Parts", self.totalParts.toString());

                if (self.entityId > 0) {
                    self.uploadRequest.setRequestHeader("Content-Entity-Id", self.entityId.toString());
                }

                //send the partial attachment to the server
                self.uploadRequest.send(partialAttachment);
            }, 0);
        };

        if (self.rangeEnd <= self.attachment.size) {
            fileReader.readAsArrayBuffer(partialAttachment);
        }
    },
    __onUploadComplete: function() {
        this.uiTemplate.removeAttr("data-uploading");

        //update UI: hide the pause and resume upload buttons
        this.uiTemplate.find(".pause-button, .resume-button").addClass("hidden");

        //enable the button again
        if (this.__allUploadsFinished()) {
            this.__enableUserControl();
        }
    },
    __startIfAttachmentDoesNotExistsOnServer: function() {
        var self = this;

        //update UI
        var status = "preparing to upload... please wait";
        self.uiTemplate.find(".status").html(status);

        var reader = new FileReader();
        reader.onload = function(event) {
            var rusha = new Rusha();
            self.checksum = rusha.digestFromArrayBuffer(event.target.result);

            var attachment = {
                "checksum": self.checksum
            };

            $.ajax({
                type: "GET",
                url: "/get/attachment",
                data: utilities.formatJSON(attachment),
                success: function(response) {
                    //update UI - enable remove button
                    self.uiTemplate.find(".cancel-remove-button").removeClass("hidden");

                    if (response.data.exists) {
                        var attachment = response.data.attachment;

                        //update attachment id
                        self.attachmentId = attachment.id;
                        self.uiTemplate.attr("data-attachment-id", attachment.id);

                        //TODO: attachment name can be custom to entity and organization
                        //TODO: checksum isn't unique, just fast to hash; maybe use sha256 for unique fingerprint
                        self.attachment.name = attachment.name;
                        self.attachment.size = attachment.size;
                        self.rangeEnd = attachment.size;

                        //enable submit button
                        self.uiTemplate.removeAttr("data-uploading");
                        if (self.__allUploadsFinished()) {
                            self.__enableUserControl();
                        }

                        //update UI
                        self.__updateUploadProgress();
                    } else {
                        self.__upload();
                    }
                },
                dataType: "json"
            });
        };

        reader.readAsArrayBuffer(self.attachment);
    },
    start: function() {
        if (this.__allUploadsFinished()) {
            this.__disableUserControl();
        }
        this.uiTemplate.attr("data-uploading", "");
        this.__startIfAttachmentDoesNotExistsOnServer();
    },
    pause: function() {
        this.isPaused = true;
    },
    resume: function() {
        this.isPaused = false;
        this.__upload();
    },
    cancel: function() {
        this.isPaused = true;
        this.uiTemplate.removeAttr("data-uploading");

        //enable the button again
        if (this.__allUploadsFinished()) {
            this.__enableUserControl();
        }

        if (this.attachmentId > 0) {
            var data = {
                attachmentId: this.attachmentId
            };
            if (this.entityId > 0) {
                data.entityId = this.entityId;
            }

            $.ajax({
                type: "POST",
                url: "/remove/attachment",
                data: utilities.formatJSON(data),
                success: function(response) {
                    if (response.success) {
                        //then what?
                    }
                },
                dataType: "json"
            });
        }
    },
    getAttachmentExtension: function() {
        return this.__getAttachmentExtension();
    },
    setUITemplate: function(attachmentTemplate) {
        this.uiTemplate = attachmentTemplate;
    },
    setUserControl: function(userControl) {
        this.userControl = userControl;
    },
    __disableUserControl: function() {
        //disable the buttons
        this.userControl.find("button").attr("disabled", "");

        //disble the anchor tag for the back button
        //see placeholder hyperlink: http://stackoverflow.com/questions/5292343/is-an-anchor-tag-without-the-href-attribute-safe
        var route = this.userControl.find(".back-button a").attr("href");
        this.userControl.find(".back-button a").attr("data-route", route).removeAttr("href");
    },
    __enableUserControl: function() {
        //enable the buttons
        this.userControl.find("button").removeAttr("disabled");

        //enable the anchor tag for the back button
        var route = this.userControl.find(".back-button a").attr("data-route");
        this.userControl.find(".back-button a").attr("href", route).removeAttr("data-route");
    }
};

//drag and drop photo upload
// $("#edit-profile-form div.section-body[data-dropzone]").on("drag", function(event) {
//     event.preventDefault();
//     console.log(event.type);
//     // $(this).addClass("dropzone");

// }).on("dragstart dragenter", function(event) {
//     console.log(event.type);
//     $(this).addClass("dropzone");

// }).on("dragend", function(event) {
//     console.log(event.type);
//     $(this).removeClass("dropzone");

// }).on("drop", function(event) {
//     event.preventDefault();
//     console.log(event.type);
//     if (event.dataTransfer.files.length > 0) {
//         prepareImageForUpload(event.dataTransfer.files[0]);
//     }
// });