var ThrottleEventDetector = (function() {
    var isScrolling = false;
    var viewportResized = false;
    var searchKeywordChanged = false;

    return {
        isScrolling: isScrolling,
        viewportResized: viewportResized,
        searchKeywordChanged: searchKeywordChanged
    };
});