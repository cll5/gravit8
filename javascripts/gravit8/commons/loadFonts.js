(function() {
    if (!cookie("fontsLoaded")) {
        var montserratFontObserver = new FontFaceObserver("Montserrat");
        var loadFonts = [
            montserratFontObserver.load(),
        ];

        Promise.all(loadFonts).then(function() {
            document.body.classList.add("fonts-loaded");
            // TODO: confirm that google's fonts do expire in 1 day, see last sentence of answer: http://stackoverflow.com/questions/29091014/how-do-i-leverage-browser-caching-for-google-fonts
            cookie("fontsLoaded", true, 1);
            console.log("Montserrat is loaded and cached now");
        }).catch(function(reason) {
            console.error("Montserrat failed to load");
            console.log(reason);
        });
    } else {
        console.log("Fonts were previously cached, getting from cache");
    }
})();