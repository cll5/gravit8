var ScrollbarHandler = (function() {
    var viewportHeight = 0;
    var scrollbarThreshold = 0.80;

    //detecting if scrolled to the bottom, see: https://developer.mozilla.org/en-US/docs/Web/API/Element/scrollHeight
    //determining viewport height: http://stackoverflow.com/questions/1248081/get-the-browser-viewport-dimensions-with-javascript
    var isEndOfPage = function() {
        var contentHeight = document.documentElement.scrollHeight || document.body.scrollHeight || 0;
        contentHeight = Math.floor(scrollbarThreshold * contentHeight);

        var y = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;

        var scrollDelta = (viewportHeight !== 0) ? (contentHeight - y) : 0;
        return (scrollDelta <= viewportHeight);
    };

    var updateMaxScrollTop = function() {
        viewportHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    };

    return {
        isEndOfPage: isEndOfPage,
        updateMaxScrollTop: updateMaxScrollTop
    };
});