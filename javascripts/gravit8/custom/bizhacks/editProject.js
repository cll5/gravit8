$(document).ready(function() {
    var monthOffset = 1;
    var utcOffset = -8;
    var startPitchTime = Date.UTC(2019, 2 - monthOffset, 1, 12 + 4 - utcOffset);
    var endPitchTime = Date.UTC(2019, 2 - monthOffset, 2, 12 + 9 - utcOffset);

    var updatePeriod = 10000;
    function updateEditPermission() {
        var now = Date.now();

        if ((now >= startPitchTime) && (now < endPitchTime)) {
            //user can edit the pitch
            $("#edit-entity-button, #submit-edits").removeClass("hidden");
        } else {
            //user cannot edit the pitch
            $("#edit-entity-button, #submit-edits").addClass("hidden");
        }

        if (now < endPitchTime) {
            setTimeout(updateEditPermission, updatePeriod);
        }
    }

    updateEditPermission();
});