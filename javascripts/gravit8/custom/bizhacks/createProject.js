$(document).ready(function() {
    var monthOffset = 1;
    var utcOffset = -8;
    var startPitchTime = Date.UTC(2019, 2 - monthOffset, 1, 12 + 4 - utcOffset);
    var endPitchTime = Date.UTC(2019, 2 - monthOffset, 2, 12 + 9 - utcOffset);

    var updatePeriod = 10000;
    function updateSubmissionPermission() {
        var now = Date.now();

        if ((now >= startPitchTime) && (now < endPitchTime)) {
            //user can submit the pitch
            $(".back-submit .closed-button.submission").removeClass("hidden");
        } else {
            //user cannot submit the pitch
            $(".back-submit .closed-button.submission").addClass("hidden");
        }

        if (now < endPitchTime) {
            setTimeout(updateSubmissionPermission, updatePeriod);
        }
    }

    updateSubmissionPermission();
});