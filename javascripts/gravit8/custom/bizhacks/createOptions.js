$(document).ready(function() {
    var monthOffset = 1;
    var utcOffset = -8;
    var startPitchTime = Date.UTC(2019, 2 - monthOffset, 1, 12 + 4 - utcOffset);
    var endPitchTime = Date.UTC(2019, 2 - monthOffset, 2, 12 + 9 - utcOffset);

    var updatePeriod = 10000;
    function updateCreateOptions() {
        var now = Date.now();

        if ((now >= startPitchTime) && (now < endPitchTime)) {
            //user can create a pitch
            $("[data-card-option = 'pitch'].create-option-card").not(".disabled-module").removeClass("hidden");
            $("[data-card-option = 'pitch'].create-option-card.disabled-module").addClass("hidden");
        } else {
            //pitching is disabled
            $("[data-card-option = 'pitch'].create-option-card").not(".disabled-module").addClass("hidden");
            $("[data-card-option = 'pitch'].create-option-card.disabled-module").removeClass("hidden");
        }

        if (now < endPitchTime) {
            setTimeout(updateCreateOptions, updatePeriod);
        }
    }

    updateCreateOptions();
});