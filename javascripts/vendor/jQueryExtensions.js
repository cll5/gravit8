$.fn.exists = function () {
    return (this.length !== 0);
};

//see: https://css-tricks.com/snippets/jquery/make-an-jquery-hasattr/
$.fn.hasAttribute = function(attributeName) {
    return $(this)[0].hasAttribute(attributeName);
};

//see: http://stackoverflow.com/questions/1394020/jquery-each-backwards
$.fn.reverse = Array.prototype.reverse;